﻿/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */

var sepStr=ZLR(' ',10), ZI='zzllrrImager', HOME=H+'sites.google.com/site/'+ZI, GTALK='zzllrr-imager', GMAIL='mailto:zzllrr@gmail.com?subject=zzllrr%20Imager%20Geek';

var MIN_WH=100,MAX_WHS=20000,MIN_S=0,TIME_OUT=2000,TIME_INT=3000;
var extArr=imgReg.replace(/\(|\)/g,'').split('|');
var extsArr=extArr.concat('other');
var paraSel=ZLR('sel','Img Css Link Text Favicon Meta Canvas');
var paraMin=ZLR('min','W H S');
var paraMax=ZLR('max','W H S');
var paraAlt=ZLR('alt','Y N R');
var paraTitle=ZLR('title','Y N R');
var paraName=ZLR('name','Y N R');
var paraLink2=ZLR('link2','Y N R');
var paraUrl=ZLR('url','Y N R');

var L=localStorage, _gaq=_gaq||[];//, SS=chrome.storage.sync, SL=chrome.storage.local;
_gaq.push(['_setAccount', L.ZIG||'UA-24024937-'+(LITE?6:5)]);
_gaq.push(['_trackPageview']);

var tArr=ZLR(zlr('getImage',' '+zlr('s',' 1 Tab Window Local Input'))+' getUrl wh sz');
for(var i=tArr.length-1;i>-1;i--){
	L.removeItem(tArr[i]);
}


var f=document.createElement('canvas'), ctx=f.getContext('2d'), bgP=chrome.extension.getBackgroundPage(), cdl=chrome.downloads,
 Colors='000000,F5F5F5,ffffff,FFFAFA;4682B4,F5F5F5,87CEFA,FFF5EE;FA8072,F8F8FF,FFB6C1,FFC0CB;ffffff,DCDCDC,000000,696969;8B0000,B22222,CD5C5C,F08080',
 S1='#codeImgrs ~ span,#codeSaveAs,#ruleAdv > span,.closeUrls,.preview',
 isPop=/pop\.html/.test(location.href), isOut=/out\.html/.test(location.href), isCap=/cap\.html/.test(location.href), isOpt=/opt\.html/.test(location.href);
var Task={
//obj:{url,path,rename}
	download:function(obj,cb){
		//console.log(obj.id);
		if(!obj.id){
			saveAs2(obj.url,obj.path,obj.rename,null,function(did){
				obj.id=did;
				//console.log(obj.id);
				var tr=$('#iH').nextAll('[data-tid="'+obj.tid+'"]');
				tr.attr('data-did',did);
			});
		}

	},
	loop:function(){
		for(var i in bgP.tasks){
			var obj=bgP.tasks[i].ing[0];
			if(obj){
				Task.download(obj)
			}else{
				if(!bgP.tasks[i].pause.length){
					delete bgP.tasks[i]
				}
			}
		}
	},
	addImg:function(img,obj,HVrotate,cb){
		var d=H_d(obj.url),t=bgP.tasks[d];
		if(t){
			t.ing.push(obj);
		}else{
			bgP.tasks[d]={to:[],ing:[obj],pause:[]};
		}
		$('#iH').after(iDom(img,obj,HVrotate));
	},
	pause:function(did,tid,cb){
		if(did){
			cdl.pause(did,function(){
				cdl.search({id:did},function(dl){
					didRefresh(dl[0])
				});
				if(cb){
					cb()
				}
			});
		}
		if(tid){
			var s=bgP.tasks;
			if(s){
				var tr=$('#iH').nextAll('[data-tid='+tid+']');
				if(tr.length){
					var d=H_d(tr.find('a.aurl').attr('href'));
					if(s[d]){
						var s=bgP.tasks[d].ing;
						for(var i=s.length-1;i>-1;i--){
							var t=s[i];
							if(t.tid==tid){
								var p={tid:tid,url:t.url,path:t.path,rename:t.rename};
								if(t.id){
									p.id=t.id
								}
								s.splice(i,1);
								bgP.tasks[d].pause.push(p);
								break;
							}
						}
					}
				}
			}
		}
	},
	resume:function(did,tid,cb){
		if(did){
			cdl.resume(did,function(){
				cdl.search({id:did},function(dl){
					didRefresh(dl[0])
				});
				if(cb){
					cb()
				}
			});
		}
		if(tid){

			var tr=$('#iH').nextAll('[data-tid='+tid+']');
			if(tr.length){
				var d=H_d(tr.find('a.aurl').attr('href'));
				if(bgP.tasks[d]){
					var s=bgP.tasks[d].pause;
					//console.log(s);
					for(var i=s.length-1;i>-1;i--){
						var t=s[i];
						if(t.tid==tid){
							var p={tid:tid,url:t.url,path:t.path,rename:t.rename};
							if(t.id){
								p.id=t.id
							}
							s.splice(i,1);
							bgP.tasks[d].ing.push(p);
							Task.loop();
							break;
						}
					}
				}
			}

		}
	},

	erase:function(did,tid,cb){
		if(did){
			cdl.cancel(did);
			cdl.erase({id:did});
		}else if(tid){
			var s=bgP.tasks;
			if(s){
				var tr=$('#iH').nextAll('[data-tid='+tid+']');
				if(tr.length){
					var d=H_d(tr.find('a.aurl').attr('href'));
					if(s[d]){
						var s=bgP.tasks[d].ing;
						for(var i=s.length-1;i>-1;i--){
							if(s[i].tid==tid){
								s.splice(i,1);
								break;
							}
						}
					}
				}
			}
		}
		if(cb){
			cb();
		}
	},
		
	lastShow:function(){
		cdl.show(+L.lastDid)
	}
};



$(function(){

	if(FREE){
		$('#zimg').attr('src','img/ZI'+(LITE?'L':'G')+'.png');
	}else{
		$('.vipLogo').addClass('isvip');
		if(VER=='__ZIG__vip'){
			$('.vipBtn input').not('#btnVIP').addClass('isvip')
		}else{
			$('.vipBtn input').addClass('isvip')
		}
	}
	if(LITE){$('#appZIL').attr({'id':'appZIG', 'src':'img/ZIG.png'})}

	$('.copyright, #donaTip').on('click',function(){window.open('mailto:zzllrr@gmail.com?subject=zzllrr Imager Geek')});
});

function vipAlert(t){
	alert(gM('vipalert'+(t||'')));
	api('ofhelp&VIP');
}
function getVer(){
	$.getJSON('manifest.json', function(j){
		var ver='V'+j.version;
		$('#ver').html(ver).on('click',function(){window.open(HOM['ZI'+(LITE?'L':'G')])});
		if(L.togTitle=='true' && /(out|cap)\.html/.test(location.href) && L.getSelected){
			document.title=unescape(jSon(L.getSelected).tabThis[0].title);
		}else{
			document.title=gM('zdesc')+'('+ver+')';
		}
	});
}
function getMeta(){
	hopeIt('getMeta');
	getSelected();
}
function getSelected(){
	chrome.tabs.getSelected(null,function(taB){
		var fav='';
		if(taB.favIconUrl){fav=escape(taB.favIconUrl)}
		L.getSelected='{"tabThis":['+JSON.stringify(taB)+']}';
	});
}


function loadwdgts(){
	var t=ZLR('home imgr qrcd func code sech weib memo'), txt=svgf.text('',[20,30,12]);
	for(var i=t.length-1;i>-1;i--){
		var w=t[i];
		$('#nav2').prepend((isPop?'<div>':'')+'<svg id=if' + t[i]+'>'+svgs[w]+txt+'</svg>'+(isPop?dc:''));
	}
	$('#nav2').append((isPop?'<div>':'')+'<svg id=ifdona>'+svgs['dona']+txt+'</svg>'+(isPop?dc:''));
}


var ini={
	para:function(){
		L.minW=L.minW || MIN_WH;
		L.minH=L.minH || MIN_WH;
		L.minS=L.minS || MIN_S;
		L.maxW=L.maxW || MAX_WHS;
		L.maxH=L.maxH || MAX_WHS;
		L.maxS=L.maxS || MAX_WHS;
		L.timeOutImgr=L.timeOutImgr || TIME_OUT;
		L.timeIntImgr=L.timeIntImgr || TIME_INT;
		L.timeOutCode=L.timeOutCode || TIME_OUT;
		L.timeIntCode=L.timeIntCode || TIME_INT;

		if(L.filtDIV===undefined){L.filtDIV='filter,WHS'}

		L.FGC=/#/.test(L.FGC)?L.FGC:'#000000';
		L.BGC1=/#/.test(L.BGC1)?L.BGC1:'#F5F5F5';
		L.BGC=/#/.test(L.BGC)?L.BGC:'#ffffff';
		L.BGC2=/#/.test(L.BGC2)?L.BGC2:'#FFFAFA';

		//L.APAD=L.APAD || 'Good Luck!';
		L.memos=(L.memos||'').trim()||'<tr id=Memo><th>'+strbtn+'+" class=TaskAdd id=TaskAdd /></th><th id=Task>'+gM('Task')+'</th><th id=ReminderTime>'+gM('ReminderTime')+'</th></tr>';
		L.onekeys=!L.onekeys||L.onekeys=='[]'?JSON.stringify([{name:gM('capScr_Q'),v:'#capScr_Qclk_popout'},{name:gM('capScrW_Q'),v:'#capScrW_Qclk_popout'}]):L.onekeys;
		
		var tArr=extsArr.concat(paraSel);
		for(var i in tArr){
			L[tArr[i]]=L[tArr[i]] || (i>10 && i!=15?'false':'true');
		}
		tArr=paraAlt.concat(paraTitle,paraName,paraLink2,paraUrl,'search','codeCode');
		for(var i in tArr){
			L[tArr[i]]=L[tArr[i]] || '';
		}

		L.sechId=L.sechId || 0;
		L.sechNum=L.sechNum || 12;
	//out
		L.playType=(/(Center)|(Tile)|(Stretch)/.test(L.playType)?L.playType:'Center');
		L.playSpeed=L.playSpeed || 1500;
		L.playBgColor=L.playBgColor || 'black';

		var tArr0=ZLR('tile','W bW Shape Rows Cols Cols2 RowsMin ColsMin Cols2Min Sort FontOn FontPos Desc').concat(['fontSize','font']);
		var tArr1=[600,5,0,2,2,2,1,1,1,0,'false',1,'t a renm ',24,'Microsoft Yahei'];
		for(var i in tArr0){
			L[tArr0[i]]=L[tArr0[i]] || tArr1[i];
		}

		L.signPos=L.signPos || 'center center';
		L.urlSign=L.urlSign || '';

		ini.jpeg();

		L.tileKWD=L.tileKWD || '#'+ZI+'# @zzllrr';

	//opt
		L.optTrimErr=L.optTrimErr || 'true';
		L.alarmOn=L.alarmOn || 'true';

		L.geek3=L.geek3 || 'bar no w h ';
		L.nav=L.nav || 0;
		L.geekMini=L.geekMini || 'false';

	},

	renm:function(){
		if( !L.renm || !L.renm.match(INI.renm_R) ){
			L.renm=INI.renm;
		}
		L.renmauto=L.renmauto || 0;
	},
	rule:function(){
		if( !L.rule || !L.rule.match(INI.rule_R) ){
			L.rule=INI.rule;
		}
		L.ruleauto=L.ruleauto || 0;
	},
	weib:function(){
		if( !L.weib || !L.weib.match(INI.weib_R) || rplReg(L.weib,7) ){
			L.weib=INI.weib;
		}
	},

	upld:function(){
		if( !L.upld || !L.upld.match(INI.upld_R) || rplReg(L.upld,7) || /kanbox/.test(L.upld)){
			L.upld=INI.upld;
		}
	},

	sech:function(){
		if( !L.sech || !L.sech.match(INI.sech_R) || rplReg(rplReg(L.sech,6),3) ){
			L.sech=INI.sech;
		}
		var tArr=ZLR('share info BtnEdit upload');
		for(var i=0;i<tArr.length;i++){
			L.sech=L.sech.replace('"'+tArr[i]+'","engine"','"'+gM(tArr[i])+'","engine"');
		}
	},
	func:function(){
		if( !L.func || !L.func.match(INI.func_R) || rplReg(L.func,5) ){
			L.func=INI.func;
		}
	},
	code:function(){
		if( !L.code || !L.code.match(INI.code_R) ){
			L.code=INI.code;
		}
		L.codeauto=L.codeauto || 1;
	},
	imgr:function(){
		if( !L.imgr || !L.imgr.match(INI.imgr_R) ){
			L.imgr=INI.imgr;
		}
	},
	back:function(){
		//contextMenu & Hotkey
		var tArr=ZLR(zlr2(zlr2('get pin show sav bulk','Image')+' '+zlr('cap','Scr ScrR ScrS ScrW MHTML'),'_Q_on'));
		tArr.push('menuOn');
		for(var i in tArr){
			L[tArr[i]]=L[tArr[i]] || 1;
		}
	},
	jpeg:function(){
		var tArr=['','Tile'];
		for(var i in tArr){
			L['capType'+tArr[i]]=L['capType'+tArr[i]]|| 'jpeg';
			L['jpgQ'+tArr[i]]=+L['jpgQ'+tArr[i]] || 100;
		}
	}
}

function merge(hope, str, ord){

	if(!bgP[hope]){bgP[hope]=str;return}

	var json0=jSon(bgP[hope]);
	var picList0=json0.picList;
	var metaList0=json0.metaList;

	var tmpjson=jSon(str);
	var picList1=tmpjson.picList;
	var metaList1=tmpjson.metaList;

	$.merge(picList0,picList1);

	if(ord!='-1'){
		metaList0[0].title += sepStr + metaList1[0].title;
		metaList0[0].url += ' ' + metaList1[0].url;
		metaList0[0].kwd += sepStr + metaList1[0].kwd;
		metaList0[0].desc += sepStr + metaList1[0].desc;

		metaList0[0].pages=parseInt(metaList0[0].pages) + parseInt(metaList1[0].pages);
	}

	metaList0[0].linkCss += ' ' + metaList1[0].linkCss;
	metaList0[0].fonts += ' ' + metaList1[0].fonts;
	bgP[hope]= '{"picList":'+ jSon2str(picList0) + ',"metaList":' + jSon2str(metaList0) + '}';

}



function egOpen(KWD,URI,LNK,FN,SEL,PIC){
	var kwd=KWD.trim();
	var sel=SEL.trim();
	var Url=URI;
	var fn=FN.replace(/fn[0-9a-z]\(\)/g,function(t){return t.replace(')','%n)')});

	var lnk=LNK;
	chrome.tabs.getSelected(null,function(tab){
		var jArr=[tab.url,tab.title];
		if(/fns\(/.test(fn)){
			//fns(webid, url, title, smry, pic)
			Url=fns(lnk, URI||jArr[0], sel||jArr[1], sel||kwd||'', PIC||'');
		}else if(kwd && lnk){
			if(fn){
				var eva=fn.replace(/%n/g,"'" + kwd + "'").replace(/%u/g,"'" + jArr[0] + "'").replace(/%t/g,"'" + jArr[1] + "'");
				chrome.tabs.sendRequest(tab.id,{hope:'eval',eval:eva},function(response){
					Url=lnk.replace(/%s/g,response?response.eval:kwd);
					if(!/^https?:/.test(Url)){Url=H+Url}
					chrome.tabs.create({index: tab.index+1, url: Url, selected: true});
				});
				return;
			}
			Url=lnk.replace(/%s/g,kwd);
		}
		if(!/^https?:/.test(Url)){Url=H+Url}
		chrome.tabs.create({index: tab.index+1, url: Url, selected: true});
	});
}

function surname(r1,n,a,t,r,e,T,U){
	return r1.replace(/<name>/gi,n).replace(/<alt>/gi,a).replace(/<title>/g,t).replace(/<fn>/g,r).replace(/<\[*(jpg|png|gif)\]*>/g,e).replace(/<TITLE>/g,T).replace(/<URL>/g,U)
		.replace(/<date>/g,Time.now('Date')).replace(/<time>/g,Time.now('Time')).replace(/<date_time>/g,Time.now());
}

function save(pic){
	var json0=jSon(pic),id;
	var tmpPicArr=json0.picList;

	var listPic=[];
	var pindex=-1;
	var pext='',psrc='',palt='',ptitle='',pname='',plink2='',pr0='';

	ini.renm();
	var tmpArr=jSon(L.renm);
	var i=+L.renmauto;
	i=(i>0?i:0);
	if(/pop\.html/.test(location.href)){
		var r1=L.r1;
	}else{
		var r1=unescape(tmpArr[i].r1);
	}
	var sNo=r1.match(/<(No\.)?\d*>/),SNO='';
	if(!sNo){
		sNo='';
	}else{
		SNO=sNo[0].replace(/\D*/g,'');
		sNo=SNO?parseInt(SNO,10):1;
		var nums=String(tmpPicArr.length + sNo - 1).length;
	}
	var auto0=/^0\d+/.test(SNO);
	var x=r1.match(/<\[*(jpg|png|gif)\]*>/g);
	if(x){x=x[0].replace(/[<>]/g,'')}
	var d=/\[/.test(x); //is optional

	if(tmpPicArr && tmpPicArr.length) {
		var tl=tmpPicArr.length;
		var w0=parseInt(L.minW);
		var w1=parseInt(L.maxW);
		var h0=parseInt(L.minH);
		var h1=parseInt(L.maxH);
		var PArr=[paraAlt,paraTitle,paraName,paraLink2,paraUrl];
		var tab=jSon(L.getSelected).tabThis[0],TITLE=unescape(tab.title),URL=unescape(tab.url);
		for(var i=0;i<tl;i++){
			var tSrc=tmpPicArr[i].src;
			if(!tSrc){continue}

			if(tSrc.substr(0,11)=='data:image/'){	//data:image
				psrc=tSrc;
				pname=psrc.replace(/[;,].*/,'');
				pext=pname.split('/')[1].toLowerCase().replace('x-icon','ico').replace('+xml','');
			}else{
				psrc=unescape(tSrc);
				pname=decodeURIComponent(psrc.replace(/\?.+$/,'').replace(/.+[/]/,''));
				pext=pname.replace(/.+\./, '').replace(/\&.+$/,'');
			}
			if(extArr.indexOf(pext)<0) {pext='other'}
			palt =unescape(tmpPicArr[i].alt);
			ptitle =unescape(tmpPicArr[i].title);
			plink2 =unescape(tmpPicArr[i].link2);
			pr0 =unescape(tmpPicArr[i].r0);

			if(L[pext]!='true'){continue}
			var b=true;
			var pArr=[palt,ptitle,pname,plink2,psrc];

			for(j=0;j<5;j++){
				var Y=unescape(L[PArr[j][0]]).trim();
				var N=unescape(L[PArr[j][1]]).trim();
				var R=unescape(L[PArr[j][2]]).trim();
				var Rg=new RegExp(R);
				if( (Y && pArr[j].indexOf(Y)<0) ||(N && pArr[j].indexOf(N)>-1)||(R && !Rg.test(pArr[j]) ) ){
					b=false;
					break;
				}
			}
			if(!b){continue}

//sel	Img    Css CssFile CssBG    Link 	Text TextCss    Favicon Meta Canvas
			for(j=0;j<paraSel.length;j++){
				var sel=paraSel[j];
				if(L[sel]!='true'){
					var deep=sel.split('sel')[1];
					if( plink2 == deep ||
					   ( /^Css(File|BG)$/.test(plink2) && deep=='Css') ||
					   (plink2=='TextCss' && deep=='Text') ||
					   (!/^(Css|Link|Text|Favicon|Meta|Canvas)/.test(plink2) && deep=='Img') ){
						b=false;
						break;
					}
				}
			}
			if(!b){continue}


			var w=parseInt(tmpPicArr[i].width);
			var h=parseInt(tmpPicArr[i].height);

			if(w*h>0 && (w>w1 || w<w0 || h>h1 || h<h0)){
				continue;
			}

			if(listPic.indexOf(psrc)>-1){
				continue;
			}

			pindex ++;
			listPic.push(psrc);

			//save
			var a=palt;
			var t=ptitle;
			var n=pname;
			var r=pr0;
			var dot=n.lastIndexOf('.');
			if(dot>0){
				var s0=n.substr(0,dot);
				var s1=n.substr(dot);
			}else{
				var s0=n;
				var s1='';
			}

			var No=sNo==''?'':(auto0?('00000' + (sNo + pindex)).substr(-1*nums):(sNo + pindex));

			if(d && !s1 || !d && x){
				s1='.'+x.replace(/^\[|\]$/g,'');
			}

			var rename=surname(r1,s0,a,t,r,s1,TITLE,URL).replace(/<(No\.)?\d*>/g,No);

			saveAs2(psrc,gM('zdesc')+'/'+gM('savImage_Q'),rename,'',function(id){L.lastDid=id});
		}
	}

	if(pindex>-1){
		noti(null,gM('savImage_Q')+' (Alt + Q)',gM('Downloaded')+' (Ctrl + J):\n'+(pindex+1),null,null,Task.lastShow)
	}
}



function egOpenIt(info,tab){
	var id=info.menuItemId, sel='',kwd='',pic='', fnS=egArr[id][2]=='fns()', seL=info.selectionText, url='';
	if(seL){sel=info.selectionText}
	if(egArr[id][3].indexOf('selection')>-1){
		if(seL){kwd=info.selectionText}
	}else if(egArr[id][3].indexOf('link')>-1){
		kwd=info.linkUrl || info.srcUrl || info.frameUrl || info.pageUrl;
	}else if(egArr[id][3].indexOf('image')>-1 || egArr[id][3].indexOf('video')>-1 || egArr[id][3].indexOf('audio')>-1 ){
		kwd=info.srcUrl || info.linkUrl || info.frameUrl || info.pageUrl;
	}else{
		kwd=info.frameUrl || info.pageUrl;
	}

	if(fnS && info.srcUrl){pic=info.srcUrl}
	if(fnS && info.linkUrl){url=info.linkUrl}

	egOpen(kwd,fnS?url:egArr[id][0],egArr[id][1],egArr[id][2],sel,pic);
}



function addMenu(){
	delMenu();
	if(L.menuOn){

		var cM=chrome.contextMenus, i0=cM.create({title:q_key(gM('zname'),'Z'),contexts:['all']});
		cM.create({title:q_key(gM('getImage_Q')+' (Ctrl+Q)','Q'),parentId:i0,onclick:getImage_Q});
		cM.create({title:q_key(gM('savImage_Q')+' (Alt+Q)','Q'),parentId:i0,onclick:savImage_Q});
		cM.create({title:q_key(gM('pinImage_Q')+' (Ctrl+Alt+Q)','Q'),parentId:i0,onclick:pinImage_Q});
		cM.create({title:q_key(gM('showImage_Q')+' (Ctrl+Alt+Space)','S'),parentId:i0,onclick:showImage_Q});

		cM.create({type:'separator', parentId:i0});
		cM.create({title:q_key(gM('bulk')+' (Shift+J)','J'),parentId:i0,onclick:bulkImage_Q});
		
		cM.create({type:'separator', parentId:i0});
		cM.create({title:q_key(gM('capMHTML_Q')+' (Ctrl+M)','M'),parentId:i0,onclick:capMHTML_Q});
		cM.create({title:q_key(gM('capMHTMLTab_Q')+' (Ctrl+Shift+M)','M'),parentId:i0,onclick:capMHTMLTab_Q});

		cM.create({type:'separator', parentId:i0});

		cM.create({title:q_key(gM('capScrS_Q')+' (Ctrl+Shift+X)','X'),parentId:i0,onclick:capScrS_Q});
		cM.create({title:q_key(gM('capScr_Q')+' (Shift+Q)','Q'),parentId:i0,onclick:capScr_Q});

		cM.create({type:'separator', parentId:i0});

		cM.create({title:q_key(gM('capScrR_Q')+' (Ctrl+Shift+S)','S'),parentId:i0,onclick:capScrR_Q});
		cM.create({title:q_key(gM('capScrW_Q')+' (Ctrl+Shift+U)','U'),parentId:i0,onclick:capScrW_Q});

		cM.create({type:'separator', parentId:i0});

		cM.create({title:q_key(gM('eng')+' (Ctrl+*)','E'),contexts:['all'],parentId:i0,onclick:eng_Q});
		cM.create({title:q_key(gM('quicksav')+' (Ctrl+Shift+*)','E'),contexts:['all'],parentId:i0,onclick:engi_Q});

		cM.create({type:'separator', parentId:i0});
		ini.sech();


		var tmpArr=jSon(L.sech);
		if(L.sech.indexOf('"on":"yes","name"')){

			for(var i=0; i<tmpArr.length;i++){

				if(tmpArr[i].on=='yes'){
					var tn=unescape(tmpArr[i].name);
					if(tn==gM('info')){tn=q_key(tn,'I')}
					if(tn==gM('share')){tn=q_key(tn,'S')}
					if(/translate/i.test(tn)){tn=q_key(tn,'T')}
					if(/similar/i.test(tn)){tn=q_key(tn,'I')}
					if(/Pin/i.test(tn)){tn=q_key(tn,'P')}

					var i1=cM.create({title:tn, contexts:tmpArr[i].menu, parentId:i0});

					var eg=tmpArr[i].engine;
					for(var j=0; j<eg.length;j++){
						if(eg[j].on=='yes'){
							var dataArr= new Array();
							dataArr[0]=unescape(eg[j].href);
							dataArr[1]=unescape(eg[j].lnk);
							dataArr[2]=unescape(eg[j].fn);
							dataArr[3]=tmpArr[i].menu;

							tn=unescape(eg[j].title);
							if(/google/i.test(tn)){tn=q_key(tn,'G')}
							if(/百度/i.test(tn)){tn=q_key(tn,'B')}
							if(/Pin/i.test(tn)){tn=q_key(tn,'P')}
							if(/Tineye/i.test(tn)){tn=q_key(tn,'T')}
							if(/搜狗/i.test(tn)){tn=q_key(tn,'S')}
							var i2=cM.create({title:tn, contexts:tmpArr[i].menu, parentId:i1, onclick:egOpenIt});
							egArr[i2]=dataArr;
						}
					}
				}
			}
		}

		cM.create({type:'separator', parentId:i0});

		var i1=cM.create({title:q_key(gM('help'),'H'), parentId:i0});
		cM.create({title:q_key(gM('Abt'),'A'),parentId:i1,onclick:function(){api('ofinfo')}});
		cM.create({title:q_key(gM('dona'),'D'),parentId:i1,onclick:function(){api('ofdona')}});
		cM.create({title:q_key(gM('Imgr'),'G'),parentId:i1,onclick:function(){api('ofhelp&Imgr')}});
		cM.create({title:q_key(gM('btnsave'),'S'),parentId:i1,onclick:function(){api('ofhelp&BtnSave')}});
		cM.create({title:q_key(gM('VIP'),'V'),parentId:i1,onclick:function(){api('ofhelp&VIP')}});
	}
}
function delMenu(){chrome.contextMenus.removeAll()}

function eng_Q(){getMeta(); setTimeout(function(){api('','eng')}, 100)}
function engi_Q(){hopeIt('engi')}
function getImage_Q(){Image_Q('get')}
function pinImage_Q(){Image_Q('pin')}
function showImage_Q(){Image_Q('show')}
function bulkImage_Q(){Image_Q('bulk')}
function savImage_Q(){Image_Q('sav')}


function Image_Q(t){
	getMeta();
	L.getType='getImage';
	L.getTypeImgr='getImage';
	bgP.getImage='';
	L.r1='';

	chrome.tabs.getSelected(null,function(taB){
		var fav='';
		if(taB.favIconUrl){fav=escape(taB.favIconUrl)}

		ini.rule();
		ini.renm();
		var tmpArr=jSon(L.rule),tmpArr2=jSon(L.renm);
		var i=+L.ruleauto,j=+L.renmauto;

		i=(i>0 && i<tmpArr.length?i:0);
		j=(j>0 && j<tmpArr2.length?j:0);
		L.ruleauto=i;
		L.renmauto=j;

		chrome.tabs.sendRequest(taB.id,
			{hope:t.replace(/pin|show/,'get')+'Image', img0rule:tmpArr[i].img0, img1rule:tmpArr[i].img1,img2rule:tmpArr[i].img2, url2rule:tmpArr[i].url2, r0:tmpArr2[j].r0, fav:fav},
			function(response){if(/get|pin|show|bulk/.test(t)){window.open(chrome.extension.getURL('out.html?'+t))}
		});
	});
}




function capScrW_Q(){hopeIt('capScrW_Q')}
function capScrR_Q(){hopeIt('capScrR_Q')}


function capScr_Q(){
	chrome.tabs.captureVisibleTab(null,{format:L.capType,quality:+L.jpgQ},function(dataUrl){
		var d=new Date(), d1=gM('zdesc')+'_'+gM('capScrS_Q')+Time.now()+'.'+L.capType;
		saveAs2(dataUrl,gM('zdesc')+'/'+gM('CapScr'),d1,'',function(id){L.lastDid=id});
		noti(null,gM('capScr_Q')+' (Shift + Q)',gM('Downloaded')+' (Ctrl + J):\n'+d1,null,null,Task.lastShow);
	});
}
function capScrTab_Q(){
	chrome.tabs.query({'active':true},function(t){
		var tid=t.id;
    	chrome.tabs.getAllInWindow(null, function(tabArr){
			var ids=[];
	        for(var i=0, len=tabArr.length; i<len; i++){
	            var tab=tabArr[i],ti=tab.id;
	            if(ti!=tid){
	            	ids.push(ti)
				}
			 }
			var f=function(){
				if(ids.length){
					chrome.tabs.update(ids[0],{'active':true},function(t){
console.log(ids[0],tid,ids);

							chrome.tabs.captureVisibleTab(null,{format:L.capType,quality:+L.jpgQ},function(dataUrl){
								var d=new Date(), d1=gM('zdesc')+'_'+gM('capScrS_Q')+Time.now()+'.'+L.capType;
	console.log(ids.length);
								if(dataUrl){
									saveAs2(dataUrl,gM('zdesc')+'/'+gM('CapScr'),d1,null,function(){
										ids.shift();
										f();
									});
								}else{
									console.log('no dataurl');
									ids.shift();
									f();
								}
							});

					});
				}else{
					console.log('ok');
				}
			};
			f();
		 });
	});
}
function capScrS_Q(){
	L.capScrY=0;
	getMeta();
	chrome.tabs.captureVisibleTab(null,{format:'png'},function(dataUrl){
		bgP.capScrRsrc=dataUrl;
		window.open(chrome.extension.getURL('cap.html'));
	});
}

function capScr(t){
	chrome.tabs.captureVisibleTab(null,{format:'jpeg',quality:100},function(dataUrl){
		var img=new Image();
		img.onload=function(){
			hopeIt('scrollDown');

			var image=this;

			var w=+L.capScrw,h=+L.capScrh,H=+L.capScrH;
			var i=+L.capScr,n=+L.capScrN;
			if(i==0){
				ctx.drawImage(image, 0,0, w,h, 0,0, w,h);
				if(n==2){
					hopeIt('hideTop');
					//hopeIt('hideFixed');
				}else if(n>2){
					hopeIt('hideFixed');
				}
			}else if(i<n-1){
				hopeIt('hideFixed');
				ctx.drawImage(image, 0,0, w,h, 0,h*i, w,h);
				if(i==n-2){hopeIt('showBottom')}
			}else{
				ctx.drawImage(image, 0,h*n-H, w,H-h*i, 0,h*i, w,H-h*i);
				hopeIt('scrollY');
				hopeIt('showFixed');
			}
			ctx.save();

			L.capScr=i+1;
			if(i+1<n){
				setTimeout(function(){capScr(t)},300);
			}else{
				capScrThen(t);
			}
		}
		img.src=dataUrl;
	});
}
function capScrThen(t){
	if(L.capType=='png'){
		var imgUrl=f.toDataURL('image/png');
	}else{
		var q=+L.jpgQ/100;
		if(!(q>0 && q<=1)){q=1.0}
		var imgUrl=f.toDataURL('image/jpeg',q);
	}

	if(t=='R'){
		bgP.capScrRsrc=imgUrl;
		window.open(chrome.extension.getURL('cap.html'));
	}else{
		var d=new Date(), d1=gM('zdesc')+'_'+gM('capScrR_Q')+Time.now()+'.'+L.capType;
		saveAs2(imgUrl,gM('zdesc')+'/'+gM('CapScr'),d1,'',function(id){L.lastDid=id});
		noti(null,gM('capScrW_Q')+' (Ctrl + Shift + U)',gM('Downloaded')+' (Ctrl + J):\n'+d1,null,null,Task.lastShow);
	}
}

function capMHTML_Q(){
	chrome.tabs.getSelected(null,function(tab){
		chrome.pageCapture.saveAsMHTML({tabId: tab.id},function(mhtml){
			var url=window.webkitURL.createObjectURL(mhtml), p=(tab.title || tab.url)+'_'+Time.now()+'.mhtml';
			saveAs2(url,gM('zdesc')+'/MHTML',p,'',function(id){L.lastDid=id});
			noti(null,gM('capMHTML_Q')+' (Ctrl + M)',gM('Downloaded')+' (Ctrl + J):\n'+p,null,null,Task.lastShow);
		});
	});
}

function capMHTMLTab_Q(){
    chrome.tabs.getAllInWindow(null, function(tabArr){
        for(var i=0, len=tabArr.length; i<len; i++){
            var tab=tabArr[i];
			chrome.pageCapture.saveAsMHTML({tabId: tab.id},function(mhtml){
				var url=window.webkitURL.createObjectURL(mhtml), p=(tab.title || tab.url)+'_'+Time.now()+'.mhtml';
				saveAs2(url,M('zdesc')+'/MHTML',p,'',function(id){L.lastDid=id});

			});
        }
		if(len){noti(null,gM('capMHTMLTab_Q')+' (Ctrl + Shift + M)',gM('Downloaded')+' (Ctrl + J):\n'+len,null,null,Task.lastShow)}
    });
}

function api(t, page){
	chrome.tabs.getSelected(null,function(tab){
		chrome.tabs.create({index: tab.index+1, url:(page||'opt')+'.html?' + t, selected: true});
	});
}

function tabNew(obj){
	var me=$(obj);
	var KWD = $('#search').val().trim();
	var URI = me.attr('data-href');
	var FN = me.attr('data-fn');
	var LNK = me.attr('data-lnk');
	var SEL = KWD;
	if(FN=='fns()' && !KWD.match(/^https*:/)){KWD=''}
	egOpen(KWD,URI,LNK,FN,SEL);
}

function hopeIt(t){chrome.tabs.getSelected(null,function(tab){chrome.tabs.sendRequest(tab.id,{hope:t})	}) }

function rplReg(t,n){
	var reg=new RegExp('{(:,){'+n+'}:}', 'g');
	return t.replace(/"[^"]*"/g,'').replace(reg,'').replace(/\[,*\]/g,'')
}

function wrap(c,n,t){
	var ttl=(t?' title="'+t+'"':''),tph=(t?' placeholder="'+t+'"':'');
	if(n==0){return unescape(c)}
	if(n==1){return (t?strchkbx0.replace('checkbox','radio" name="'+t):strchkbx0)+ (/^yes|true$/.test(c)?chked:'') + '/>'}
	if(n==2){return '<input type="text" value="' + unescape(c) + '"' + ttl + '/>'}
	if(n==3){return '<textarea' + ttl + '>' + unescape(c) + '</textarea>'}
	if(n==4){return '<textarea' + tph + '>' + unescape(c) + '</textarea>'}
if(n==5){return '<img width=16 height=16 class=ico src="' + (c?unescape(c):(t?imgFav(t):'img/16.gif')) + '" title="' + gM('href') + '" />'}
	if(n==6){return ['<select><option value=js >JS','jsFile >JSFile','css >CSS','cssFile >CSSFile</option></select>'].join(strop).replace('value='+c+
					' ','value='+c+seled)}
	if(n==7){return '<textarea readonly>' + unescape(c) + '</textarea>'}

	if(n==8){
		var tArr=[];
		$.each(Auth, function(j, n){
		    tArr.push(j + ' >'+(/Pg/.test(j)?gM(j.replace('Pg',''))+'-'+gM('Pg'):gM(j)));
		});
		tArr[tArr.length-1] += '</option></select>';
		tArr[0]='<img class=weib src="img/'+Auth[c||'tsina'].ico+'" /><select class=site><option value='+tArr[0];
		return tArr.join(strop).replace('value='+c+' ','value='+c+seled);
	}
	if(n==9){
		var tArr=[];
		$.each(Auth2, function(j, n){
		    tArr.push(j + ' >'+gM(j));
		});
		tArr[tArr.length-1] += '</option></select>';
		tArr[0]='<img class=upld src="img/'+Auth2[c||'vdisk'].ico+'" /><select class=site><option value='+tArr[0];
		return tArr.join(strop).replace('value='+c+' ','value='+c+seled);
	}

}

function toggleColor(){
	if(isPop){
		var arr=ZLR('FGC BGC BGC1 BGC2');
		for(var i=arr.length-1;i>-1;i--){
			L[arr[i]]=$('#'+arr[i]).val() || L[arr[i]];
		}
	}
}

function toggleColorTile(){
	var arr='FTC,BTC,BTC1,BTC2'.split(',');
	for(var i=arr.length-1;i>-1;i--){
		L[arr[i]]=$('#'+arr[i].replace('T','G')).val() || L[arr[i]];
	}


	$('#Tiles').css('color',L.FTC);

	if(isCap){
		if(L.drawShape=='allEraser'){scrn('eraser',true);return}
		var shpN=L.drawShapeNow, isTxt=/Text/.test(shpN);
		if(isTxt){
			$('#'+shpN).css({color:L.FTC, 'border-color':L.BTC});
		}else{
			$('#'+shpN).find('[stroke!=none]').attr('stroke',L.FTC).end().children('[fill]')
				.attr('fill',['none',L.BTC,L.FTC][+$('#strkFill').val()]);
			$('#'+shpN).find('marker path[fill!=none]').attr('fill',L.FTC);
		}
	}
}

function hoverIt(t){
	$(t).css('background-image',bgfrom +L.BGC1+ '),color-stop(.5,'+L.BGC+'), to(' +L.BGC2+ '))');
}
function hoverItOff(t,from,to){
	$(t).css('background-image',bgfrom +L['BGC'+from]+ '), to(' +L['BGC'+to]+ '))');
}
