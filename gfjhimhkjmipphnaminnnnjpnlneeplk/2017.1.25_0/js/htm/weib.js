﻿/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */

$(function(){
	if(FREE){$('#vip').css('-webkit-filter','grayscale(1)')}
	var v=L.tileKWD ||'';
	$('#tileKWD').val(v).on('change keyup',function(){
		L[this.id]=this.value;
		$('#TileKWD').attr('title',this.value);
	});
	$('#TileKWD').attr('title',v);

	$('#wei span, #upl span').attr('title',function(){return gM(this.id.substr(4))});

	$('#tileWeib').val(gM('Weib'));
	$('#tileImg option').html(function(){return gM(this.value)});

});
$(document).on('click','#tileKwd span',function(){
	var v='#'+$(this).text().replace(/\s+./g,function(i){return i.substr(-1).toUpperCase()})+'#';
	$('#tileText').val(function(){return (this.value+' ').replace(/^ /,'')+v});
	TileKWD();

}).on('click','#tileWeibResult h2 > img',function(){$(this).parent().empty();

}).on('click','#tileWeibs img',function(){
	$(this).toggleClass('toggle');
	var tWs=jSon(L.weib);
	$('#tileWeibs img').each(function(i){
		tWs[i].on=$(this).is('.toggle')?'yes':'';
	});
	L.weib=jSon2str(tWs);

}).on('change keyup mouseup', '#tileText', function(){TileKWD()});

function loadkwd(){
	var sep = new RegExp(sepStr,'g');
	var pg = jSon(L.meta);
	var tab = jSon(L.getSelected).tabThis[0];
	var Url = unescape(tab.url);
	var T = unescape(tab.title);
	var favUrl=tab.favIconUrl?unescape(tab.favIconUrl):imgFav(Url);
	var Kwd = unescape(pg.kwd).replace(sep,'\n').replace(/^\s*|\s*$/g,'');
	var Desc = unescape(pg.desc).replace(sep,'\n').replace(/^\s*|\s*$/g,'').replace(/\s+/g,' ');

	$('#tileURL').attr('title',Url);
	$('#tileTitle').text(T || Desc).attr('title',Desc || T);
	$('#tileRenm').val((T || Desc) +'_'+ gM('zdesc')+'_'+gM('capscr')).attr('title', gM('renm'));
	$('#tileURL').attr('src',favUrl);
	if(Kwd){
		var t=Kwd.replace(/[,，]+/g,',').replace(/^,*|,*$/g,''), tArr=[];
		t = t.replace(/([^,]*)/g, function($0,$1,i){
			if(t.indexOf($1) == i){tArr[tArr.length]=$1}
		});
		$('#tileKwd').prepend(t?('<span>'+tArr.join('</span><span>')+'</span>'):'');
	}
	$('#tileText').val(T || Desc);
	TileKWD();
	loadweib();
	var isOut=/out\.html/.test(location.href);
	if(isOut){loadupld()}
}




function tileWeibResult(i,htm,type){
	var tWs=jSon(L.weib),w=tWs[i],t=w.site;
	var img='<img src="img/'+Auth[t].ico+'" />';
	if(type==0){
		$('#tileWeibResult'+i).html('<a>'+img+'</a>'+prog);
	}else{
		$('#tileWeibResult'+i).html('<a '+(type==1?'target=_blank href':'title')+'="'+
			htm+'">'+img+'</a><img src="img/'+(type==1?'':'un')+'check.png" />');
	}
}
function TileKWD(){
	var v=$('#tileText').val(), hz='';
	var n0=v.length, n1=n0;
	if(v.match(hanziRe)){
		n1 +=v.match(hanziRe).length;
		hz='['+(n1>140?scRed(n1):n1)+']';
	}
	$('#tileTextCnt').html((n0>140?scRed(n0):n0) + hz);
}



function loadweib(){
	ini.weib();
	var Weib=jSon(L.weib);
	$('#tileWeibs,#tileWeibResult').empty();
	for(var i=0;i<Weib.length;i++){
		var W=Weib[i],t=W.site;

		$('#tileWeibs').append('<img title="'+(unescape(W.name) || gM(t) || (gM(t.replace(/Pg$/,''))+'-'+gM('Pg')))+'" id=tileWeib'+i+
			' data-name="'+unescape(W.name)+
			'" data-id="'+W.id+
			'" data-blog="'+t+
			'" data-token="'+W.token+
			'" data-end="'+W.end+
			'" data-info="'+W.info+
			'"'+(W.on=='yes'?' class=toggle':'')+
			' src="img/'+Auth[t].ico+'" />');
		$('#tileWeibResult').append('<h2 id=tileWeibResult'+i+'></h2>');
	}
}
function loadupld(){
	ini.upld();
	var Upld=jSon(L.upld);
	$('#uplds').empty();
	for(var i=0;i<Upld.length;i++){
		var W=Upld[i],t=W.site;

		$('#uplds').append('<option id=upld'+i+' value='+i+
			' data-name="'+unescape(W.name)+
			'" data-id="'+W.id+
			'" data-blog="'+t+
			'" data-token="'+W.token+
			'" data-end="'+W.end+
			'" data-info="'+W.info+
			'" data-ico="'+Auth2[t].ico+'"'+
			(W.on=='yes'?seled:'')+
			'>' + gM(t) + (W.id?'(' +unescape(W.name)+ ')':'') + '</option>');

	}
	cng_weib($('#uplds')[0]);
}

function clk_weib(obj){
	var me=$(obj), id=obj.id||'', pa=me.parent(), pid=pa.attr('id'), onekey=$('#Addonekey').prop('checked'), c=me.attr('class')||'';
	if(onekey){
		var k=[],sel,val='';//值clk_popout选择器;值cng_popout选择器;;
		
		if(L.onekey){
			k.push(L.onekey)
		}
		if(id){
			sel='#'+id
		}else{
			sel=(pid?'#'+pid+' ':'')+(c?zlr('.',c,''):me[0].nodeName);
			if(me.is('span')){
				sel+=':contains('+me.text()+')'
			}
			if(me.is(':button')){
				sel+=':button[value='+me.val()+']'
			}
		}
		
		if(me.is('input:checkbox')){
			val=me.prop('checked');
		}

		k.push(sel+'clk_weib'+val);
		L.onekey=k.join(';');
	}

	if(/(weib|upld)Opt/.test(id)){api('ofimgr&'+id.substr(0,1).toUpperCase()+id.substr(1,3))}
	if(/(weib|upld)Re(load|voke)/.test(id)){
		var tp=id.substr(0,4), tWs=jSon(L[tp]);

		if(/Revoke/.test(id)){
			for(var i=0;i<tWs.length;i++){
				var w=tWs[i];
				if(w.on!='yes'){continue}
				w.token='';
				w.end='';
				w.code='';
			}
			L[tp]=jSon2str(tWs);
		}

		if(tp=='weib'){
			$('#tileWeibs,#tileWeibResult').slideUp('slow',function(){loadweib()}).slideDown();
		}else{
			$('#uplds').after('<img src="img/loading.gif" width=16 />');
			$('#upldPath, #upldCount').empty();
			setTimeout(function(){$('#uplds').next('img').remove(); loadupld()},150);
		}
	}
	if(/(weib|upld)(Url|Logout)/.test(id) ){
		var tp=id.substr(0,4), tWs=jSon(L[tp]), Ath=tp=='weib'?Auth:Auth2;
		for(var i=0;i<tWs.length;i++){
			var w=tWs[i],Id=w.id;
			if(w.on!='yes'){continue}
			if(/Url/.test(id)){
				var url=Ath[w.site].userUrl;
				if(Id){url += Id}else{url=url.split('/')[0]}
			}else{
				var url=Ath[w.site].logoutUrl;
			}
			url = H+url;
			window.open(url);
		}
	}


	if(id=='TileKWD' || /tile(URL|Title)$/.test(id)){$('#tileText').val(($('#tileText').val() + ' ').replace(/^ /,'') + me.attr('title'));TileKWD()}

	if(/(tile|shot)(Save|Weib)$/.test(id)){
		L.tileTool=id;
		L.noCapTile=false;

		var isCap=/cap\.html/.test(location.href), isPop=/pop\.html/.test(location.href);

		if(isCap){
			var t=$('#outFile').val(), imgUrl, notes=$('#Caps > div~*').length, q=Number(L.jpgQ)/100, A='', B=[], cvs=$('#caps')[0],d=new Date();
			var d1=$('#tileRenm').val() || gM('zdesc')+'_'+$('#outFile :selected').html().replace(/\(.+\)/,'')+Time.now();
			if(q==0 || q>1){q=1.0}
			imgUrl=cvs.toDataURL('image/'+L.capType,q);

			if(id=='shotSave'){
				if(t=='capscr' || (t=='capscrnote' && !notes)){
					$('#code').val(imgUrl).after('<div><a href="'+imgUrl+'" target=_blank><img src="'+imgUrl+'" height=100 /></a></div>').next().nextAll().remove();
					saveAs2(imgUrl,'ZIG/'+gM('CapScr'),d1+'.'+L.capType.replace('e',''));
					return;
				}

				if(t!='capscrnote'){
					$('#Caps textarea').each(function(){A+=this.id;B.push(escape(this.value))});
					$('#Caps svg').attr('xmlns',xmlns);
					var note=notes?'<foreignObject x="0" y="0" width="'+cvs.width+'" height="'+cvs.height+
						'"><body xmlns="'+xmlns.replace('2000/svg','1999/xhtml')+'" onload="zig()">'+
						$('#Caps').html().replace(/<canvas [\s\S]*<[/]div>/,'')+'</body></foreignObject>':'';

					var svg='<svg xmlns="'+xmlns+'" xmlns:xlink="'+xmlns.replace('2000/svg','1999/xlink')+
						'"><desc name="zzllrr Imager Geek" url="'+HOM.ZIG+'">'+'ZIG'+'</desc>'+
						(notes?'<style type="text/css">\n<![CDATA[textarea:hover{resize:auto}textarea{resize:none;overflow:hidden}.bold{font-weight:bold}.italic{font-style:italic}'+
						['.underline','underline}.overline','overline}.though','line-through}'].join('{text-decoration:')+
						']]></style>\n<script type="text/ecmascript">\n<![CDATA[function zig(){var A="'+A.substring(4)+'".split("Text"),B="'+B.join(',')+
						'";for(var i=0;i<A.length;i++){var d=document.getElementById("Text"+A[i]); if(d){d.value=unescape(B.split(",")[i])}}}]]></script>\n':'')+
						(/scr/.test(t)?'<image x="0" y="0" width="'+cvs.width+'" height="'+cvs.height+'" xlink:href="'+imgUrl+'"></image>':'')+
						note+'</svg>';

					$('#code').val(svg);
					saveText(svg,d1+'.svg');
					return;
				}
			}
		}else{
			$('#capTypeTile,#jpgQTile,#tileW,#tilebW,#urlSign,#signPos').each(function(){L[this.id]=$(this).val()});
			if(FREE && id=='tileSave'){
				$('#Tiles').siblings().twinkle();
				vipAlert(2); return;
			}
		}


		if(id=='tileWeib'){
			if(!me.is(':visible')){return}
			var tileImg=$('#tileImg').val(), Url=$('#urlImg').val(), bg=$('#urlImg').attr('data-bg');
			var tWs=jSon(L.weib);
			if(!tWs){return}

			if(FREE && tileImg!='none'){vipAlert()}

			for(var i=0;i<tWs.length;i++){
				var w=tWs[i],t=w.site;
				if(w.on!='yes'){continue}
				if(t!='immio'){
					if(!w.token || (new Date()).getTime() > Number(w.end)){
						if(isPop){
							alert((!w.token?'尚未绑定':'授权已过期')+'！\n请在拼图页或截图页，\n点击“分享左图”，并成功发布一条微博后\n再使用！');
							return
						}
						auth.grant(i);return
					}
					if(t!='tsina' && t!='t163' && !w.id){auth.token(i);return}
				}

				if(FREE){sendTxt(i); continue}
				imgPreRe.lastIndex=0;
				if(tileImg=='none' || tileImg=='other' && !imgPreRe.test(Url) && bg===undefined){
					sendTxt(i);
					continue;
				}
				if(tileImg=='other'){

					if(/^http/.test(Url)){
						sendPic(i,Url);
					}else if(bg){
						if($('#upImg').val()){
							sendPic(i,null,$('#upImg')[0].files[0]);
						}else{
							sendPic(i,bg);
						}
					}
				}
			}

			if(FREE || isPop || L.noCapTile=='true'){return}
		}

		if(isCap){
			L.tileW=cvs.width;
			L.tileH=cvs.height;
			$('#Caps').attr('title','');
			$('#tileTool').hide();
			
		}else{
			$('#Tiles').siblings().hide();
		}

		L.tileX=$('#tiles,#caps').position().left;
		L.tileY=$('#tiles,#caps').position().top;

		L.tileScrH=$(window).height();
		L.tileScrN=Math.ceil((Number(L.tileY)+Number(L.tileH))/Number(L.tileScrH));
		L.tileScr=0;

		$('body').scrollTop(0);
		f.width=Number(L.tileW);
		f.height=Number(L.tileH);
		ctx.clearRect(0,0,f.width,f.height);
		setTimeout(capTile,100);

	}

	if(id=='btnUpload'){
		var upl=$('#upl:visible').length>0;
		if(!upl){loadupld(); $('#upl').show();return}
		if(FREE){vipAlert(2); return}
		codeIt();
		$('[data-upld]').removeAttr('data-upld');
		if($('#upldCount + progress').length || $('#list :checked, #upldNew :file[value!=""]').length<1){
			upldResult(3);
			return;
		}
		upldResult(0);

		var upl=$('#uplds option:selected'), i=Number(upl.val());

		if(!upl.attr('data-token') || (new Date()).getTime() > Number(upl.attr('data-end'))){auth.grant(i,2);upldResult(3);return}
		//if(t!='tsina' && t!='t163' && !w.id){auth.token(i,2);return}

		create_folder(i);
	}
}



function cng_weib(obj){
	var me=$(obj), id=obj.id||'', v=me.val(), pa=me.parent(), pid=pa.attr('id'), onekey=$('#Addonekey').prop('checked'), c=me.attr('class')||'';
	if(onekey){
		var k=[],sel,val='';//值clk_popout选择器;值cng_popout选择器;;
		
		if(L.onekey){
			k.push(L.onekey)
		}
		if(id){
			sel='#'+id
		}else{
			sel=(pid?'#'+pid+' ':'')+(c?zlr('.',c,''):me[0].nodeName);

		}
		k.push(sel+'cng_weib'+v);
		L.onekey=k.join(';');
	}


	if(id=='tileImg'){$('#otherImg').toggle(v=='other'); $('#tileWeibs img[data-blog=immio]').toggle(v!='none')}

	if(/^up(Img|Sign)$/.test(id)){
		var he=$('#'+id.replace('p','rl'));
		he.val(' ');
		if(v){
			var src=webkitURL.createObjectURL(obj.files[0]);
			he.css('background','url('+src+') bottom right/contain no-repeat').attr('data-bg',src);
			if(id=='upSign'){loadTiles()}
		}else{
			he.css('background','none').removeAttr('data-bg');
		}
	}

	if(id=='uplds'){
		var upl=$('#uplds option:selected'), i=upl.index()+1, str='"on":"', uplA=L.upld.replace(/"on":"yes/g,str).split(str);
		$('#upldimg').attr('src','img/'+upl.attr('data-ico'));
		uplA[i] = 'yes'+uplA[i];
		L.upld=uplA.join(str);
	}
	$('body')[0].tabIndex=0;
	$('body').focus();
}

function capTile(){
	chrome.tabs.captureVisibleTab(null,{format:'jpeg',quality:100},function(dataUrl){

		var img=new Image();
		img.onload=function(){
			//(image, x, y)
			//(image, x, y, width, height)
			//(image, sourceX,sourceY, sourceWidth,sourceHeight, destX,destY, destWidth,destHeight)

			var tX=Number(L.tileX),tY=Number(L.tileY),tSH=Number(L.tileScrH),tW=Number(L.tileW),tH=Number(L.tileH);
			var ti=Number(L.tileScr),tn=Number(L.tileScrN);
			if(ti==0){
				ctx.drawImage(this, tX,tY, tW,tSH-tY, 0,0, tW,tSH-tY);
			}else if(ti < tn-1){
				ctx.drawImage(this, tX,0, tW,tSH, 0,tSH*ti-tY, tW,tSH);
			}else{
				ctx.drawImage(this, tX,tSH*tn-tH-tY, tW,tH-tSH*ti+tY, 0,tSH*ti-tY, tW,tH-tSH*ti+tY);
			}
			ctx.save();

			L.tileScr=ti+1;
			$('body').scrollTop(tSH*(ti+1));

			if(ti+1<tn){
				setTimeout(capTile,100);
			}else{
				capTileThen();
			}

		}
		img.src=dataUrl;

	});
}
function capTileThen(){

	var isCap=/cap\.html/.test(location.href), Tile=(isCap?'':'Tile'), q=Number(L['jpgQ'+Tile])/100;
	if(q==0 || q>1){q=1.0}
	var imgUrl=f.toDataURL('image/'+L['capType'+Tile],q);
	if(isCap){
		$('#tileTool').fadeIn();
		$('#Caps').attr('title',$('#caps').width()+'x'+$('#caps').height());
	}else{
		$('#Tiles').siblings().fadeIn();

	}
	$('#'+(isCap?'code':'tileTitle')).after('<div><a href="'+imgUrl+'" target=_blank><img src="'+imgUrl+'" height=100 /></a></div>').next().nextAll().remove();

	if(/Save/.test(L.tileTool)){
		if(FREE && !isCap){vipAlert(2); return}
		var d=new Date(), d1=($('#tileRenm').val() || gM('zdesc')+'_'+(isCap?$('#outFile :selected').html().replace(/\(.+\)/,''):gM('btnTile'))+Time.now())+'.'+L['capType'+Tile].replace('e','');
		saveAs2(imgUrl,'ZIG/'+gM(isCap?'CapScr':'BtnTile'),d1);

	}else if(L.tileTool=='tileWeib'){
		if(FREE){vipAlert(); return}
		var tWs=jSon(L.weib);
		for(var i=0;i<tWs.length;i++){
			if(tWs[i].on!='yes'){continue}
			sendPic(i,imgUrl);
		}
	}
}



function sendTxt(i){

	var tWs=jSon(L.weib), w=tWs[i],t=w.site,token=w.token,info=jSon(unescape(w.info)),Id=w.id,Name=unescape(w.name);
	L.noCapTile=true;
	var txt=$('#tileText').val(),txtE=encodeURIComponent(txt);
	if(!txt){$('#tileText').focus();return}

	tileWeibResult(i,null,0);
	var a=Auth[t],tdata, Url=a.updateUrl||a.apiUrl;

	if(t=='douban'){tdata={text:txt,access_token:token,source:a.key}}
	if(t=='tsina' || t=='t163'){tdata={status:txt,access_token:token}}
	if(t=='tqq'){tdata={content:txt,access_token:token,
		oauth_version:'2.a',scope:'all',oauth_consumer_key:a.key,
		openid:info.openid}
	}

	if(/renren/.test(t)){tdata={status:UTF8.encode(txt),access_token:token,
		v:'1.0',method:'status.set',format:'JSON'};
		if(/Pg/.test(t)){
			tdata.page_id=Id;
			tdata.method='pages.setStatus';
		}

		var kv=[];
		for(var k in tdata){kv.push([k+'='+tdata[k]])}
		kv.sort();
		tdata.sig=hex_md5(kv.join('')+a.secr);
	}

	if(/fb/.test(t)){tdata={message:txt,access_token:token};
		Url = Url.replace(/me/,Id);
		//console.log(Url);
		//if(/Pg/.test(t)){bugs: https://developers.facebook.com/search?selection=%28%23210%29+Subject+must+be+a+page}
	}

	//console.log('update begin');
	//console.log(tdata);
	var ajax={
		type:(t=='douban'?'GET':'POST'),
		url:Url,
		data:tdata,
		error:function (xhr, status, error){
			console.log(xhr);
			console.log(status);
			console.log(error);
			tileWeibResult(i,status||error,2);
		},
		success:function (data, status){
			//console.log(data);
			var tWs=jSon(L.weib), w=tWs[i],t=w.site,Id=w.id,Name=unescape(w.name);

			if(/tsina|t163|douban/.test(t)){
				var u=data.user,Id=u.id,Name=u.screen_name;
				if(t=='t163'){Id=u.screen_name;Name=u.name}
				w.id=Id;
				w.name= escape(Name);
				L.weib=jSon2str(tWs);
				$('#tileWeib'+i).attr({'data-id':Id,'data-name':Name});
			}

			var htm=H+a.userUrl+Id+'" title="'+Name;
			tileWeibResult(i,htm,1);
		}
	};
	if(/fb/.test(t)){ajax.dataType='json'}
	$.ajax(ajax);
}