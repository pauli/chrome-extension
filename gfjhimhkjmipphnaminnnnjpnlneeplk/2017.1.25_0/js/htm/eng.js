﻿/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */

$('title').text(gM('eng') + ' - zzllrr Imager Geek(ZIG)');
L.search=decodeURIComponent(location.search.substr(1));
if(L.getSelected){
	var tab = jSon(L.getSelected).tabThis[0];
	TITLE=unescape(tab.title), URL=unescape(tab.url);
}
if(!L.search && L.meta){
	L.search=JSON.parse(L.meta).sech;
}
$(function(){

	$('#sech').html(svgs['sech']+svgf.text(gM('eng'),[22,30,20])).attr('title', gM('opt'));
	$('#sech').css('width',$('#sech').children().last().width()+35);


	$('select, :file, :color').on('change',function(){cng_eng(this)});


	$(document).on('click','button,:button, #SECH > span, #sech, .engine, #engine img, .result > .resultHead, .resultHead > .cX',function(e){
		if($(this).is('img,a,.cX')){e.stopPropagation()}
		clk_eng(this)
	});
	$('#searchIt option').html(function(){return gM(this.value)});
	$('span[id]:empty').html(function(){return gM(this.id)});
	$('#search').val(L.search).on('dblclick', function(){
		// keyup mouseup
		if($(this).val().trim()){
			Engin.update();
		}
	}).on('keydown',function(e){
		var k=e.keyCode, shft=e.shiftKey, ctrl=e.ctrlKey, alt=e.altKey;

		if(!(shft || ctrl)){
			if(k==13){
				Engin.update();
			}
		}
	});

	$('body').on('keydown',function(e){
		var k=e.keyCode, shft=e.shiftKey, ctrl=e.ctrlKey, alt=e.altKey, b=document.activeElement.tagName;
		if(b){b=b.toLowerCase()=='body'}
		if(b){
			if(!(shft || ctrl)){
				if(k==72){
					api('ofsech')
				}
			}
		}
	});
	loadsech(L.sechId);


});


function loadsech(x){
	var Re=$('#Eng + #Results');
	ini.sech();
	L.sechId=x;
	var sN = parseInt(L.sechNum);
	var tmpArr = jSon(L.sech);
	var tmp=[], tmp1=[];
	for(var i=0; i<tmpArr.length;i++){
		tmp.push('<option value=' + i + (unescape(tmpArr[i].on)=='yes'?' >':' disabled hidden >') + unescape(tmpArr[i].name) + '</option>');
		if(!Re.children('#R'+i).length){
			Re.append('<div id=R'+i+'>'+dc);
		}
	}
	$('#searchIt').html(tmp.join(''));
	var sop=$('#searchIt option');
	if(sop){
		var y=sop.eq(0).attr('value');
		if($('#searchIt option[value=' + x + ']').length){y=x}
		$('#searchIt').val(y);
		var eg=tmpArr[y].engine;
		tmp=[];
		tmp1=[];
		var j=-1, Rx=Re.children('#R'+x);
		for(var i=0; i<eg.length;i++){
			if(eg[i].on=='yes'){
				j += 1;
				var thref=unescape(eg[i].href), tlnk=unescape(eg[i].lnk), tsrc=unescape(eg[i].ico), tfn=unescape(eg[i].fn), ttl=unescape(eg[i].title), tsz=unescape(eg[i].sz)||'';
				if(tlnk && tfn!='fns()'){tlnk=H+H_w(tlnk)}
				if(thref==''){thref=H+H_W(tlnk)}
				if(tsrc==''){tsrc=imgFav(thref)}
				tmp1.push('<div id=Re'+x+'_'+i+'>'+dc);
				tmp.push(SC+'"engine'+(i==0?' toggle':'')+'" id=e'+x+'_'+i+'><img width=16 height=16 src="' +tsrc +
					'" title="' + ttl+
					'" data-href="' + thref+
					'" data-lnk="' + tlnk+
					'" data-fn="' + tfn+
					'" data-sz="' + tsz+
					'" />'+ttl+sc);
			}
		}
		$('#engine').html(tmp.join(''));
		if(!Rx.children().length){
			Rx.append(tmp1.join(''));
		}
	}
}


var Engin={
	update:function(){
		var engs=$('#engine > .toggle.engine'), k=$('#SECH > #search').val().replace(/['"]/g,'').trim();
		if(k){
			for(var i=0;i<engs.length;i++){
				Engin.updateEng(k, engs.eq(i).attr('id'));
			}
		}
	},

	updateEng:function(k, sechID){
//console.log(sechID);
		var kwd=k.trim(), idA=sechID.substr(1).split('_');
		var me=$('#'+sechID).children('img'), Re=$('#Eng + #Results').children('#R'+idA[0]).children('#R'+sechID), re=Re.children().filter('[data-kwd="'+kwd+'"]');
		if(re.length){re.fadeIn(); return}
		Re.append(DC+'result data-kwd="'+kwd+'">'+DC+'resultHead>'+
			me.parent().html()+': <a target=_blank>'+XML.encode(kwd)+'</a>'+SC+'cX>╳'+sc+dC+'resultBody>'+prog+dc+dc);
		re=Re.children().last().children('.resultBody');

		var Url=me.attr('data-href');
		var fn=me.attr('data-fn'), Fns=fn=='fns()';
		var lnk=me.attr('data-lnk');
		var sz=me.attr('data-sz');

		if(Fns && !kwd.match(/^https*:/)){kwd=''}

		fn=fn.replace(/fn[0-9a-z]\(\)/g,function(t){return t.replace(')','%n)')});


		var jArr=[tab.url,tab.title], eva='';
		if(Fns){
			//fns(webid, url, title, smry, pic)
			Url=fns(lnk, Url||jArr[0], kwd||jArr[1], sel||kwd||'', '');
		}else if(lnk){
			if(fn){
				eva=fn.replace(/%n/g,"'" + kwd + "'").replace(/%u/g,"'" + jArr[0] + "'").replace(/%t/g,"'" + jArr[1] + "'");
			}
		}
		chrome.tabs.sendRequest(tab.id,{hope:'eval',eval:eva},function(response){
			if(!Fns){Url=lnk.replace(/%s/g,response?response.eval:kwd)}
			if(!/^https?:/.test(Url)){Url=H+Url}
			re.prev().find('a').attr({'href':Url, 'title':Url});
			$.ajax({type:'get', url:Url, error:function(xhr, status, error){
						Engin.bdLoadHtml(error, re)}, success:function(d){
				var sep='<body', tArr=d.split(sep), html, node=sz || 'body,BODY';
				if(tArr.length<2){
					sep=sep.toUpperCase(), tArr=d.split(sep);
				}
				if(tArr.length>1){
					html=sep+tArr[1].substr(0, tArr[1].lastIndexOf(sep.replace('<','</')+'>')+7);
					html=Engin.bd(blking(html), Url);
					t=html.find(node).eq(0);
					if(t.length<1){t=html}
					t.find('footer').nextAll().remove();
					Engin.bdLoadHtml(t.html(), re);
				}
			}});
		});
	},


	bd:function(html, u){
		var bd=$(XML.wrapE('div', (html||'')));

		bd.find(zlr2('img :image','[src]',',')).each(function(){
			var me=$(this), s0=me.attr('file')||me.attr('data-original')||me.attr('src'), s=H_a(s0, u);
			if(s!=s0){me.attr('src', s)}
		});

		bd.find('form').attr('method','');
		bd.find(':button, :submit').attr('disabled','disabled');
		var ZBD='#ZBD'+Time.now5()+' ';
		bd.attr('id', 'ZBD').find('style').html(function(i, v){
			return ZBD+v.trim().replace(/[\},(\*\/)][\n\s]+/g, '$&'+ZBD).trim();
		});

		bd.find(zlr3('[on','click load',']',',')).removeAttr(zlr('on','click load'));
		bd.find(zlr3('a[href','^="javascript:" =#',']',',')).removeAttr('href');
		bd.find('a[href]').not('[href^=#]').attr('target','_blank').not(zlr3('[href^=','http "mailto:"',']',',')).attr('href', function(i,v){return H_a(v,u)});

		bd.find('object').has('embed[src]').each(function(){
			var em=$(this).find('embed[src]'), src=em.attr('src'), wd=em.attr('width'), ht=em.attr('height');
			$(this).replaceWith('<iframe src="'+src+'" style="width:'+(wd||500)+'px;height:'+(ht||400)+'px" >');
		});

		return bd;
	},

	bdLoadHtml:function(html, aB){
		aB.html(html||'');
		return Engin
	}
};

function blking(t, Neg){
	var s=t, arr;
	//s=s.replace(/<script[^>]*>[\D\d]*<[/]script>/gi,'');
	arr=s.split(/<\/script>/gi);
	for(var i in arr){
		arr[i]=arr[i].split(/<script/gi)[0];
	}
	s=arr.join('');

	arr=s.split(/<\/style>/gi);
	for(var i in arr){
		arr[i]=arr[i].split(/<style/gi)[0];
	}
	s=arr.join('');
	return s;
}

function clk_eng(obj){
	var me=$(obj), id=me.attr('id'), engs=$('#engine > span'), engT=engs.filter('.toggle'), engN=engs.not('.toggle');
	if(id=='sech' || id=='eng'){api('ofsech')}


	if(me.is('.engine > img')){
		tabNew(obj);
	}
	if(me.is('.engine')){
		me.toggleClass('toggle');
		var tog=me.is('.toggle');
		$('#R'+id).children('.result').toggle(tog);
	}
	if(me.is('.cX')){
		me.parents('.result').hide();
	}
	if(me.is('.resultHead')){
		me.next('.resultBody').toggle()
	}
}

function cng_eng(obj){
	var me=$(obj), id=me.attr('id'), v=me.val();
	if(id=='searchIt'){loadsech(obj.value)}
}