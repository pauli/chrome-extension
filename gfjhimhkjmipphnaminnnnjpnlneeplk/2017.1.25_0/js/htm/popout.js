﻿/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */



function loadpara(){
	ini.para();
	var tArr=extsArr.concat(paraSel);
	for(var i in tArr){
		$('#' +tArr[i]).prop('checked',L[tArr[i]]=='true');
	}
	tArr=paraMin.concat(paraMax,'timeOutImgr','timeIntImgr','timeOutCode','timeIntCode');
	for(var i in tArr){
		$('#' +tArr[i]).val(unescape(L[tArr[i]]));
	}
}

function settime(t){
	var tArr=['timeOut'+t,'timeInt'+t];
	for(var i=0;i<tArr.length;i++){
		var id=tArr[i];
		if(!/^\d+$/.test($('#' +id).val())){$('#' +id).val((/Out/.test(id)?2000:3000))}
		L[id] = $('#' +id).val();
	}
}

function set(){
	var tArr=extsArr.concat(paraSel);
	for(var i=0;i<tArr.length;i++){
		var id=tArr[i];
		L[id] = $('#'+id).prop('checked');
	}

	tArr=paraMin.concat(paraMax);
	for(var i=0;i<tArr.length;i++){
		id=tArr[i];
		L[id] = $('#'+id).val();
	}

	tArr=paraAlt.concat(paraTitle,paraName,paraLink2,paraUrl);
	for(var i=0;i<tArr.length;i++){
		id=tArr[i];
		L[id] = escape($('#'+id).val());
	}

	if(isOut){
		var gType=L.getTypeImgr;
		$('#disp,#upImgsImgr').hide();
		loadpic(gType,0);
		setTimeout(function(){filtThen(true)},500);
	}
}

function para(){
	$('#filter').children('.red').remove();
	$('#FiltMore tr').each(function(){
		var tY=$(this).find(':text[id$=Y]').val();
		var tN=$(this).find(':text[id$=N]').val();
		var tR=$(this).find(':text[id$=R]').val();
		var t=(tY?'['+tY+']':'')+(tN?'-['+tN+']':'')+(tR?'/'+tR+'/':'');
		if(t){$('#filter').append(scRed(t))}
	});
}

function fixed4(d){
	return d.toFixed(4).replace(/(\.[^0]*)0+/,'$1').replace(/\.$/,'')
}
function fetchSend(tabid,t){
	if(/Code/.test(t)){
		var codeCode = $('#codeCode').val().trim(), reg=escape($('#codeReg').val());
		if(!codeCode){codeCode="return 'Good Luck!'"}
		chrome.tabs.get(tabid,function(tab){
			chrome.tabs.sendRequest(tabid, {hope:t, codeCode:escape(codeCode), reg:reg}, function(response) {});
		});

	}else{
		if( $('#ruleOnSrc').prop('checked') ){$('#url2rule').val('')}
		L.r1=$('#r1').val();
		chrome.tabs.get(tabid,function(taB){
			var fav = '';
			if(taB.favIconUrl){fav = escape(taB.favIconUrl)}
			chrome.tabs.sendRequest(tabid, {hope:t, img0rule:escape($('#img0rule').val()), img1rule:escape($('#img1rule').val()),
				img2rule:escape($('#img2rule').val()), url2rule:escape($('#url2rule').val()), r0:escape($('#r0').val()), fav:fav}, function(response) {});
		});
	}
}

function imgrMode(){
	var t=$('#getType').val();
	$('#timeImgr').toggle(t=='getImages1');
	switch(t){
		case 'getImagesInput':
		case 'getImages':
		case 'getImages1':
			$('#inputUrlsImgr').html(function(i, htm){return t=='getImagesInput'?htm.replace(/\.html/g,'.jpg'):htm.replace(/\.jpg/g,'.html')});
			$('#urlToolImgr').toggle(t=='getImages');
			$('#urlsImgr, #advInputImgr, #advImgr').fadeIn();
			$('#getUrls').show();
			break;

		default:
			$('#urlsImgr, #advInputImgr,#getUrls').hide();
	}
}

function loadcode(t,j){
	ini[t]();
	var tArr = jSon(L[t]);
	var tmp=[];
	for(var i=0;i<tArr.length;i++){
		var iN=unescape(tArr[i].name);
		tmp.push('<option value=' + i + '>' + iN + '</option>');

	}
	var i=(j>0 && j<tArr.length?j:0);
	$('#'+t+'s').html(tmp.join('')).val(i).next().next('.Autorun').toggleClass('auto',i==Number(L.codeauto) && i>0);
	$('#'+t+'Code').val(unescape(tArr[i].code));
}

function loadrule(j){
	ini.rule();
	var tA=jSon(L.rule),tmp=[];
	for(var i=0; i<tA.length;i++){
		tmp.push('<option value=' + i + '>' + (i<1?'--'+gM('rules')+'--':unescape(tA[i].name)) + '</option>');
	}
	var i=(j>0 && j<tA.length?j:0);
	$('#rules').html(tmp.join('')).val(i);

	$('#RULE ~ .Autorun').toggleClass('auto',i==Number(L.ruleauto) && i>0);

	loadruleVal(tA[i]);
	ruleOnSrc();
}
function ruleOnSrc(){
	var rs=$('#rules option:selected');
	$('#RULE').children('.red').remove();
	if(rs.val()>0){$('#RULE').append(scRed(rs.text()))}
}

function fetchSend2(){
	if(parseInt(L.order)<parseInt(L.get1s)-1){
		L.order=parseInt(L.order)+1;
		var c=/Code/.test(L.getType);
		var i=parseInt(L['timeInt'+(c?'Code':'Imgr')]);
		var o=parseInt(L['timeOut'+(c?'Code':'Imgr')]);
		if(c){advClick('fetchCodes')}
		setTimeout(fetchSend2,i);
		var Url=L.get1.split('\n')[parseInt(L.order)];
		chrome.tabs.update(+L.tabId,{url: Url}, function(taB){
			var delay=(taB.status=='loading'?o:100);
			setTimeout(function(){fetchSend(+L.tabId,L.getType)},delay);
		});
	}else if(L.getType=='getImages1'){
		window.open('out.html');
	}else{
		advClick('fetchCodes');
	}
}

function fetch(t){
    if(/get(Imag|Cod)e$/.test(t)){
		if(L.getSelected){
			var tab = jSon(L.getSelected).tabThis[0];
			var tabId = parseInt(tab.id);
			if(tabId>0){fetchSend(tabId,t)}
		}
	}

	if(/get(Imag|Cod)es1$/.test(t)){
 		var tmpAry = urlTran($("#urls" + (/Code/.test(t)?'Code':'Imgr')).val()).match(imgPre);
        if(tmpAry){
			L.order=-1;
			L.get1=tmpAry.join('\n');
			L.get1s=tmpAry.length;
			chrome.tabs.create({url: "chrome://newtab/", selected:false}, function(taB){
				L.tabId = taB.id;
				fetchSend2();
			});
        }
	}

	if(/get(Imag|Cod)es$/.test(t)){
        var Tabs=L.tabId.trim();
        if(Tabs){
            var tabsArr= Tabs.split(' ');
            for (var i=0; i<tabsArr.length; i++){
                fetchSend(parseInt(tabsArr[i]),t);
            }
        }
	}

    if(/get(Imag|Cod)esTab$/.test(t)){
        chrome.tabs.getAllInWindow(null, function(tabArr){
            for(var i=0; i<tabArr.length; i++){
                fetchSend(tabArr[i].id,t);
            }
        });
	}

	if(/get(Imag|Cod)esWindow$/.test(t)){
        chrome.windows.getAll({populate:true}, function (winArr){
            for(var i=0; i<winArr.length; i++){
                var tabArr = winArr[i].tabs;
                for(var j=0; j<tabArr.length; j++){
                    fetchSend(tabArr[j].id,t);
                }
            }
        });
	}

	if(t=='getImagesInput'){
        var imgRule0 = new RegExp($('#img0rule').val());
        var imgRule1 = $('#img1rule').val().trim();
        var imgRule2 = $('#img2rule').val().trim();
        var tmpAry = urlTran($('#urlsImgr').val()).match(imgPre);
        if(tmpAry){
            var pic = '{"picList":[';
            var linkCss=' ';
            var fonts=' ';
            for(var i=0 ; i < tmpAry.length; i++){
                var tmpPic = tmpAry[i];

                if(imgRule0){
                    if(!imgRule0.test(tmpPic)){continue}
                }

				if(imgRule1){
					var tmpArr=imgRule1.split(',');
					if(/^[/]/.test(tmpArr[0])){
						var s=new RegExp(tmpArr[0]);
					}else{
						var s=tmpArr[0].replace(/^['"]|['"]$/g,'');
					}
					tmpPic= tmpPic.replace(s,tmpArr[1]);
				}
                if(imgRule2){tmpPic= eval(imgRule2.replace(/@/g,tmpPic))}

                tmpPic=escape(tmpPic);
                if(pic.indexOf('{"src":"'+ tmpPic + '","alt":"')<0){
                    if(tmpPic.match(cssLinkRe)){
                        if(linkCss.indexOf(tmpPic + ' ')<0 ){linkCss += tmpPic + ' '}
                        continue;
                    }else if(tmpPic.match(fontRe)){
                        if(fonts.indexOf(tmpPic + ' ')<0 ){fonts += tmpPic + ' '}
                        continue;
                    }
                    tmpPic = '{"src":"'+ tmpPic + '","width":"0","height":"0","alt":"","title":"","link2":"Input","r0":""},';
                    pic += tmpPic;
                }
            }

            bgP.getImagesInput = pic.replace(/,$/, '') +
                '],"metaList":[{"title":"","url":"","kwd":"","desc":"","linkCss":"' + linkCss +
                           '","fonts":"' + fonts + '","pages":"1"}]}';
        }
    }
}


function advClick(t){
	var wait=(L.tabId||'').trim().split(/\s+/).length * 400;
	if(/fetchImageQ?$/.test(t)){
		var p=$('#getType').val();
		L.getType=p;
		L.getTypeImgr=p;
		bgP[p]='';
		if(/getImages(1|Input)?$/.test(p) ){
			if(!urlTran($('#urlsImgr').val()).match(imgPre)){$('#urlsImgr').twinkle();return}
		}
		fetch(p);
		if(t=='fetchImage'){
			if(L.getType!='getImages1'){
				var rfs=$('#count');
				if(rfs.length){
					rfs.click()
				}else{
					chrome.tabs.getSelected(null,function(taB){
						chrome.tabs.create({index: taB.index+1, url: 'out.html', selected: true});
					});
				}
			}
		}else{
			setTimeout(function(){save(bgP[L.getTypeImgr])},wait+100);
		}
	}

	if(t=='fetchCode'){
		var p=$('#codeType').val();
		L.getType=p;
		bgP.getCODE[p]='';
		if(/getCodes1?$/.test(p) ){
			if(!urlTran($('#urlsCode').val()).match(imgPre)){$('#urlsCode').twinkle();return}
		}
		fetch(p);
		advClick(t+'s');
	}

	if(t=='fetchCodes'){setTimeout(function(){
		$('#codeResult').val(unescape(bgP.getCODE[$('#codeType').val()]).substr(1))

		},wait)
	}
	if(t=='fetchUrls'){setTimeout(function(){
			$('#urlsImgr').val(filterURL('imgr'));
		},wait)
	}
}


function filterURL(t){
	if(bgP.getUrl){
		var Arr = unescape(bgP.getUrl).replace(/z@z@l@l@r@r/g,'%').split('\n'),arr=[];
		var Y=$('#'+t+'Y').val().trim().toLowerCase();
		var N=$('#'+t+'N').val().trim().toLowerCase();
		var R=$('#'+t+'R').val().trim();
		var Rg=new RegExp(R);

		for(var i=0;i<Arr.length;i++){
			var a=Arr[i];
			if( !a || arr.indexOf(a)>-1 || (Y && a.toLowerCase().indexOf(Y)<0) ||(N && a.toLowerCase().indexOf(N)>-1)||(R && !Rg.test(a) ) ){
				
			}else{
				arr.push(a);
			}
		}
		return arr.join('\n').replace(/[\n]{1,}/g,'\n').replace(/^[\n]|[\n]$/g,'');
	}
	return ''
}


function loadrenm(j){
	ini.renm();
	var tA = jSon(L.renm), tmp=[];
	for(var i=0; i<tA.length;i++){
		tmp.push('<option value=' + i + '>' + (i<1?'--'+gM('renms')+'--':unescape(tA[i].name)) + '</option>');
	}
	var i=(j>0 && j<tA.length?j:0);
	$('#renms').html(tmp.join('')).val(i);
	$('#renms ~ .Autorun').toggleClass('auto',i==Number(L.renmauto) && i>0);

	$('#r0').val(unescape(tA[i].r0));
	$('#r1').val(unescape(tA[i].r1));

	$('#RENM').children('.red').remove();
	var rs=$('#renms option:selected');
	if(rs.val()>0){$('#RENM').append(scRed(rs.text()))}
	if($('#renmPreview').length){renameCode(true)}
}

function auto(obj){
	var o=$(obj), a=o.hasClass('auto');
	o.toggleClass('auto',!a);
	if(o.prev().attr('id')=='codeSaveAs'){
		L.codeauto=a?0:$('#codes').val();
	}else{
		if(isPop && $('#rules').length){
			var t=o.prevAll('summary').attr('id').toLowerCase();
			L[t+'auto']=a?0:$('#'+t+'s').val();
		}else{
			L.renmauto=a?0:$('#renms').val();
		}
	}
}

function closeUrls(){
	var Tabs = L.tabId.trim();
	if(Tabs){
		var tabsArr= Tabs.split(' ');
		for (var i=0; i<tabsArr.length; i++){
			chrome.tabs.remove(parseInt(tabsArr[i]));
		}
	}
	L.tabId='';
}

function resultIt(t){
	if(t=='resultVIP'){api('ofhelp&VIP');return}
	var isOut=$('#resultCopy').length<1;
	var me=$('#code'+(isOut?'':'Result')), v=me.val(), bg=me.attr('data-bg');

	L.resultCopy=L.resultCopy || '';
	if(/Out/.test(t)){me.val(L.resultCopy)}

	if(v){
		if(v==' ' && !bg){return}
		var txtExt='.txt';
		var d1=Time.now();
		if(isOut){
			d1=$('#codeIt label.hd2').text() + ' '+d1;
			if(/XML/.test(d1)){
				txtExt='.xml';
				v='<?xml version="1.0" encoding="UTF-8"?>\r\n<desc name="zzllrr Imager Geek" url="'+HOM.ZIG+'">'+v+'\r\n</desc>';
			}
			if(/CSV/.test(d1)){
				txtExt='.csv';
				//v=v.replace(/,/g,'.').replace(/\t/g,',');
			}
		}
		v=v.replace(/\n/g,'\r\n').replace(/\r+\n/g,'\r\n');
		if(/Copy/.test(t)){
			if(/2/.test(t)){
				L.resultCopy += '\n'+v;
			}else{
				L.resultCopy=v;
			}
			$('#'+t).after(prog);
			setTimeout(function(){$('#'+t).next('img').remove()},100);

		}else if(/Save/.test(t)){
			var p=$('#resultName').val() || d1;
			if(v==' '){saveAs2(bg,'ZIG/'+gM('GetImagesInput'),p+'.jpg')}else{saveText2(v,'ZIG/TEXT',p+txtExt)}
			if(!isOut){noti(null,p+(v==' '?'.jpg':txtExt))}
		}
	}
}


function previewWH(){
	var Cl=$('#cssClip').prop('checked'), WH1=$('#WH'+(Cl?'1':'')).text().split('x'), WH2=$('#WH2').attr('data-scale').split(','), r13=/[13]/.test($('#WH3').attr('data-rotate')), WH4=$('#WH4').attr('data-shadow').split(','), dsX, dsY, dsB;
	WH1[0]=Number(WH1[0]); WH1[1]=Number(WH1[1]);
	WH2=[Math.ceil(WH1[0]*Number(WH2[0])), Math.ceil(WH1[1]*Number(WH2[1]))];
	$('#WH2').text(WH2.join('x'));
	if(r13){WH2.reverse()};
	$('#WH3').text(WH2.join('x'));

	dsX=Number(WH4[0]);
	dsY=Number(WH4[1]);
	dsB=Number(WH4[2]);

	WH2[0]+=Math.max(Math.abs(r13?dsY:dsX), dsB)+dsB;
	WH2[1]+=Math.max(Math.abs(r13?dsX:dsY), dsB)+dsB;

	$('#WH4').text(WH2.join('x'));

	$('#WH1').toggle(Cl);
	$('#WH2').toggle($('#WH2').attr('data-scale')!='1,1');
	$('#WH3').toggle(/[13]/.test($('#WH3').attr('data-rotate')));
	$('#WH4').toggle($('#cssDropShadow').prop('checked') && $('#WH4').attr('data-shadow')!='0,0,0');
	$('#WH~span').attr('data-on',function(){return $(this).is(':visible')?'yes':''});
}

$(function(){
	loadpara();
	if(!isCap){
		var tmpArr = jSon(INI.rule0);
		var t='';
		for(var i=-1; i<tmpArr.length;i++){
			t+='<option value=' + i + ' data-rule="'+ (i==-1?'">' +gM('RULE0'):escape(JSON.stringify(tmpArr[i]))+'">'+unescape(tmpArr[i].name)) +'</option>';
		}
		$('#RULE0S').html(t);

		tmpArr = jSon(INI.renm0);
		t='';
		for(var i=0; i<tmpArr.length;i++){
			t+=SC+'RENM0 data-rule="'+escape(JSON.stringify(tmpArr[i]))+'">'+unescape(tmpArr[i].name)+sc;
		}
		$('#RENM0').html(t);

		tmpArr = jSon(INI.code0);
		t='';
		var t2='';
		for(var i=0; i<tmpArr.length;i++){
			var j=tmpArr[i],n=unescape(j.name),u=/url|link|src/i.test(n) && !/-urls/i.test(n),v=escape(JSON.stringify(j));
			n=n.replace(' urls','');
			t+=SC+'CODE0 data-code="'+v+'">'+n+sc;
			if(u){
				j.code=escape(unescape(j.code).replace(/.join\('\\n'\)\s*$/,''));
				v=escape(JSON.stringify(j));
				t2+=SC+'CODE0 data-code="'+v+'">'+n+sc;
			}
		}
		$('#CODE0').html(t);
		$('#IMGR0,#DLD0').html(t2);
	}

	$('#imgr :checkbox, #other').not('#imgrLocal').after(function(){return gM(this.id).replace(/^sel/,'')});

	$('#timeImgr input').before(function(){return gM(this.id.slice(0,7))});

	$('.Y').text(gM('include'));
	$('.N').text(gM('exclude'));
	$('.R, #CodeReg').text(gM('regExp'));
	$('#R0').text(gM('ImgRule2'));
	$('.urlRule1,#UrlRule1').text(gM('FetchUrls'));

	$('.inputUrl').html('← '+gM('inputUrl')).append('<mark>[*xxx]</mark>');
	$('#inputUrlsDld .inputUrl').append(gM('renm')+' →');
	$('#renmDld').attr('placeholder',gM('renm'));

	$(zlr('.multi','a b c',',')).html(function(){return $(this).attr('class').substr(-1)+') '+gM($(this).attr('class'))});


	$(zlr3('span:empty[id^=','rule renm code imgr',']',',')).html(function(){return gM(this.id.substr(4))});
	$('#selErr').html(gM('error'));
	$('#selDel').html(gM('Del'));
	$('#iH th:empty').html(function(){return gM(this.id.substr(1))});
	$(zlr2('span p summary th',':empty[id]',',')).html(function(){return gM(this.id)});
	$('#getType').children().html(function(){return gM(this.value)});
	$('span:empty[class]').html(function(){return gM($(this).attr('class'))});
	$('#codeType option').html(function(){return gM(this.value.replace('Code','Image'))});
	$('#funcType option').html(function(){return gM(this.value.replace('Func','Image'))});
//hide
	$('#tab > *,#color ~ *,.urlRule1 ~ *,legend ~ *').addClass('hidden');
//attr
	$('mark').attr('title','API');
	$('#W,#H,#S,#E,#deep,#FiltMore span,span[id^=imgRule]').attr('title',gM('reset'));
	$('#Color :color,#capType,#jpgQ').attr('title',function(){return gM(this.id)});

	$('#COLOR :text').attr('readonly','');

	$('#filtDIV input[id^=m],#timeImgr input').attr({step:50, min:0});
	$('#timeCode input').attr({step:50}).before(function(){return gM(this.id.slice(0,7))});

	$('#FiltMore :text').on('change keyup mouseup',function(){para()});

//val
	$('#Color :color,#capType,#jpgQ').val(function(){return L[this.id]});
	$(':button').filter(function(){return !this.value}).val(function(){return gM($(this).attr('class') || this.id)});



//change
	$('select,:file,:color,:range').on('change',function(){cng_popout(this)});
	$('#editOpt :number').on('change keyup mouseup',function(){cng_popout(this)});

	$('#filtDIV input').on('change', function(){para();set()});
	$('#timeImgr input,#timeCode input').on('change keyup mouseup', function(){settime($(this).parent().attr('id').substr(4))});

	if(!isCap){
		$(window).unload(function(){
			set();
			settime('Code');
			settime('Imgr');
			if(isPop){
				//L.APAD=$('#APAD').text();
				savememo();
				if($('#Addonekey').prop('checked')){
					$('#Addonekey').click();
				}
			}
		});

		para();

		ini.code();

		loadrule(Number(L.ruleauto)||0);
		loadrenm(Number(L.renmauto)||0);
		loadcode('imgr',0);
		
		if(isPop){
			loadonekeys();
			loadcode('code',Number(L.codeauto)||0);
			loadsech(L.sechId);
			loadfunc();
			loadmemo();
		}

		setTimeout(function(){
			if(L.getSelected){
				var j=jSon(L.getSelected).tabThis[0], u=unescape(j.url), t=unescape(j.title), qu=fnq(u,0,parseInt(Math.random()*4)), d=H_d(u), r0=jSon(L.rule).slice(1).concat(jSon(INI.rule0)), dA=[],
				 t0=t.toLowerCase(), tA=d.split('.'), jrA=[];
				for(var i=0;i<r0.length;i++){
					var ri=r0[i], rn=unescape(ri.name), rA=rn.toLowerCase().match(/[A-z\d-]+/g), f=false, n=0;
					for(var j=0;j<rA.length;j++){
						var r=rA[j];
						if(!(j==rA.length-1 && /^(com|cn|net|gov|me|cc)$/.test(r))){
							if(d.indexOf(r)>-1){
								if(f){
									n+=r.length*2
								}else{
									var jri=JSON.stringify(ri);
									if(jrA.indexOf(jri)<0){
										jrA.push(jri);
										dA.push(SC+'RULE0 data-rule="'+escape(jri)+'" data-n=>'+rn+sc);
										f=true
									}
								}
							}
						}
					}

					rA=rn.replace(/[\x00-\xff]/g,'');
					for(var j=0;j<rA.length;j++){
						var r=rA[j].split('');
						for(var k=0;k<r.length;k++){
							if(t0.indexOf(r[k])>-1){
								if(f){
									n++;
								}else{
									var jri=JSON.stringify(ri);
									if(jrA.indexOf(jri)<0){
										jrA.push(jri);
										dA.push(SC+'RULE0 data-rule="'+escape(jri)+'" data-n=>'+rn+sc);
										f=true
									}
								}
							}
						}
					}

					if(f && dA.length){
						dA[dA.length-1]=dA[dA.length-1].replace('data-n=', 'data-n='+n);
					}
				}
				if(dA.length){
					var fn=function(a){
						return Number(a.split('data-n=')[1].split('>')[0])
					};
					dA.sort(function(a,b){
						return fn(a)-fn(b)
					});
					dA = dA.slice(-10);
					dA.reverse();
					$('#RULE0').append(dA.join(''));
					$('#RULE').append(SC+'italic>('+dA.length + ' '+gM('optional')+')'+sc);
				}
			}
		},300);

		var shares='';
		for(var k in FNS){
			if(k!='translate'){
				shares += imgSRC+FNS[k].split(' ')[1]+'" title="'+(gM(k)||k)+'" data-share='+k+' />';
			}
		}
		$('#shares').html(shares);
	}
	var t=zlr('#url','Sign Img Cap',',')+',#Caps textarea, #ren+#code,#codeResult', C='#Css', c='#css';
	$(t).attr('placeholder',gM('urlImg'));
	$('#preview ~ input').attr('title', function(){return gM(this.id.substr(3)).substr(0, isCN?1:100)});


	$('#editOpt img, #tileToolCap img, #barRight img').attr('src','img/16.gif');
	$(':number[title=deg]').attr('max', 360);
	$('#WH').attr('title', gM('w')+' x '+gM('h'));
	$('#WH1').attr('title', gM('clip')+'➜ ');
	$('#WH2').attr('title', gM('scale')+'➜ ');
	$('#WH3').attr('title', gM('rotate')+'➜ ');
	$('#WH4').attr('title', gM('dropShadow')+'➜ ');

	$('#editOpt img,'+C+'Filter img, .scale, .skew, .clip').attr('title', gM('reset')).on('click',function(){
		var me=$(this);
		cng_popout(me.parent().find('input').val(function(){return $(this).attr('value')}).last()[0]);
		me.parent().find('.rngv').text(function(){return $(this).prev().attr('value')});
	});


	$('#CssRotate6 img, #barRight img').on('click',function(){
		$('#rotate ~ input').val(0);
		var id=this.id, cR='#cssRotate';
		if(/flipx|y/.test(id)){$(cR+this.id.substr(-1).toUpperCase()).val(180)}
		if(/rotate/.test(id)){var i=Number(id.substr(-1)); $(cR+'Z').val(i<3?i*90:-90)}
		cng_popout($(cR+'Z')[0]);

	});

	$('body').on('paste',t,function(e){
		//console.log(e);
		var ts = e.originalEvent.clipboardData.items, me=$(this), id=this.id, sgn=id=='urlSign', cap=id=='urlCap';
		
		if(ts.length){
			for (var i=0;i<ts.length;i++){
				if(ts[i].kind == 'file'){
					if(/url/.test(id)){$('#'+id.replace('rl','p')).val('')}
					blob = ts[i].getAsFile(),reader=new FileReader();
					//console.log(ts[i]);  DataTransferItem {type: "image/png", kind: "file"}
					reader.onload = function(event){
						//console.log(event);
						var src = event.target.result; //webkitURL.createObjectURL(blob);
						me.val(' ').css('background','url('+src+') bottom right/contain no-repeat').attr('data-bg',src);
						if(sgn){loadTiles()}
						if(cap){loadCaps()}
					};
					reader.readAsDataURL(blob);
					break;
				}
			}
		}
	}).on('change keyup mouseup',t,function(e){
		var me=$(this), id=this.id, sgn=id=='urlSign', cap=id=='urlCap', src=me.val();
		imgPreRe.lastIndex=0;
		if(imgPreRe.test(src) || src==''){
			if(/url/.test(id)){$('#'+id.replace('rl','p')).val('')}
			if(src==''){me.css('background','none').removeAttr('data-bg')}else{me.css('background','url('+src+') bottom right/contain no-repeat').attr('data-bg',src)}
			if(sgn){loadTiles()}
			if(cap){loadCaps()}
		}
		e.stopPropagation();
	}).on('change keyup mouseup','#memos :date,#memos :time,.task,.Status',function(e){
		var me=$(this);
		if(me.is('input')){
			var v=me.val();
			if(v){
				me.attr('value',v)
			}
		}
		savememo();
		e.stopPropagation();
	}).on('dragover',t,function(e){e.stopPropagation();e.preventDefault();
	}).on('dragenter',t,function(e){e.stopPropagation();e.preventDefault();$(this).addClass('drop');
	}).on('dragleave',t,function(e){e.stopPropagation();e.preventDefault();$(this).removeClass('drop');
	}).on('drop',t,function(e){e.stopPropagation();e.preventDefault();$(this).removeClass('drop');
		var me=$(this), id=this.id, sgn=id=='urlSign', cap=id=='urlCap', src=me.val();;
		if(/url/.test(id)){$('#'+id.replace('rl','p')).val('')}
		var f=e.originalEvent.dataTransfer.files[0];
		if(!f || f.type.indexOf('image')<0){return}
		//console.log(f); File {webkitRelativePath: "", lastModifiedDate: xxx, name: "VIP.png", type: "image/png", size: 10628}

		var reader=new FileReader();
		reader.onload = function(event){
			var src = this.result;
			me.val(' ').css('background','url('+src+') bottom right/contain no-repeat').attr('data-bg',src);
			if(sgn){loadTiles()}
			if(cap){loadCaps()}
		};
		reader.readAsDataURL(f);
	});


//click
	$(document).on('click','legend,button,:button,:checkbox'+
		zlr(',.','openUrls closeUrls Autorun preview Y N R tileFontPos urlRule1 urlClickOff RULE0 RENM0 CODE0')+
		zlr(',#','local fetchUrls ImgRules ruleOnSrc W H S E deep imgrSaveAs codeSaveAs CodeReg TileKWD lite upldimg vip vipLogo codeClickOff cssClip cssCut HELP count Tiles btnReget Disp')+
		zlr(',#tile','Title Local URL _0 _1 _2 FontOn')+','+
		zlr3('#','FiltMore wei upl fontCSS tileFontCSS tileSort~','span').replace(/span/g,' $&,')+
		'#shape img,#COLOR input:text,#tileTool :radio,span[id^=page]',function(){clk_popout(this)

	});



	$('#CssTransform span, #CssFilter span').filter(function(){
		return $(this).prevAll('img').length
	}).attr('title',gM('reset')).on('click', function(){
		$(this).prevAll('img').click();
	});
	$('#CssFixed span').on('click', function(){
		var me=$(this),nx=me.next();
		me.toggleClass('bold');
		if($('#CssFixed .bold').length>2){
			me.removeClass('bold')
		}
		var mb=me.is('.bold');
		if(mb){
			nx.removeAttr('disabled')
		}else{
			nx.attr('disabled',true);
		}

	});

	$('#GeekNav2').on('click',function(){var me=$(this);c=me.prop('checked');L.nav=c?$('#tab > div:visible').index():0;if(c){me.parent().hide()} });
	
	$('#ruleAdv,#renmAdv').each(function(){
		var me=$(this),t=this.id.substr(0,4),s='',a=ZLR('SaveAs In Out Opt VIP');
		for(i=0;i<5;i++){
			s+=strbtn+gM(a[i])+'" id='+t+a[i]+' />'
		}
		me.append(s);
		
	});
	$('#ruleAdv,#renmAdv').find(':button').on('click',function(){popoutIt(this.id)});
	$('#advRule > span,#advRenm > span').on('click', function(){
		$(this).nextAll('textarea:visible').eq(0).val('');
	});
	$('#resultSav *:not(#codeClickOff)').on('click',function(){resultIt(this.id)}).html(function(){return gM(this.id.replace('result',''))});

	$('#barLeft img').on('click',function(){
		clk_popout(this);
	});
});


function toggleColors(isPop){
	if(isPop){
		toggleColor();
	}else{
		toggleColorTile();
		if(isOut){
			tileFontOnThen();
		}
	}
}


function popoutIt(id){
	var t=id.slice(0,4), u=id.slice(4), r=t=='rule', n=t=='renm', c=t=='code', m=t=='imgr', dft='--' + (n?'rename':(m?'code':t)+'s') + '--';

	if(u=='Reset' && confirm(gM('resetAlert'))){
		L.renm=INI.renm;
		loadrenm(0);
		$('#code').val('');
		alert(gM('resetDone'));
	}

	if(u=='SaveAs'){
		var strR=
			escape($('#img0rule').val()) + '","img1":"' +
			escape($('#img1rule').val()) + '","img2":"' +
			escape($('#img2rule').val()) + '","url2":"' +
			escape($('#url2rule').val()),
			strN=escape($('#r0').val()) + '","r1":"' + escape($('#r1').val());

		if(r || m || c){
			var s='#'+t+'s',gS=L.getSelected||'',sv=+$(s).val();
			if(gS && sv==0){
				gS=H_d(jSon(gS).tabThis[0].url).replace(/\.com(\..+|$)/,'').replace(/^[^\.]+\./,'');
				var gSn=$(s+' option:contains('+gS+')').length;
				if(gSn){gS+=' '+(gSn+1)}
			}else{
				gS=$(s+' option').eq(sv).text()
			}
			var ok=prompt(gM('SaveName'),gS);
			if(ok==dft && !n ){
				//alert(ok + ' ' + gM('SaveAlert'));
			}else if(ok!=null){
				if(c||m){var ecode=escape($('#'+t+'Code').val())}
				var tmp=L[t];

				var tmpStr='{"name":"' + escape(ok) + '","' + (r?'img0':(n?'r0':'code'))+ '":"';

				if(tmp.indexOf(tmpStr)>0){
					var tmpArr = tmp.split(tmpStr);
					var tmpStr2 =tmpArr[1];
					tmpStr2=(r?strR:(n?strN:ecode)) + tmpStr2.substr(tmpStr2.indexOf('"}'));
					L[t] =tmpArr[0] + tmpStr + tmpStr2;
					$(s).val(tmpArr[0].split('{"name":').length-1);
				}else{
					var len = $(s+' option').length;
					L[t]=tmp.substr(0,tmp.length-1) + ',{"name":"'+
						escape(ok) + '","' + (r?'img0":"' + strR:(n?'r0":"' + strN:'code":"' + ecode)) + '"}]';
					$(s).append('<option value=' + len + '>' + ok + '</option>').val(len);

					if(r){$('#rules + .Autorun').toggleClass('auto',false);
						$('#RULE .red').text(ok);
					}
					if(c){$(s).next().next().toggleClass('auto',false)}
				}
			}
		}else{
			var rv=+$('#renms').val();
			var ok=prompt(gM('SaveName'),rv?$('#renms option').eq(rv).text():dft);
			if(ok==dft){
				alert(ok + ' ' + gM('SaveAlert'));
				return;
			}
			if(ok!=null){
				var tmp=L.renm;
				var tmpStr='{"name":"' + escape(ok) + '","r0":"';
				if(tmp.indexOf(tmpStr)>0){
					var tmpArr = tmp.split(tmpStr);
					var tmpStr2 =tmpArr[1];
					tmpStr2=strN + tmpStr2.substr(tmpStr2.indexOf('"}'));
					L.renm =tmpArr[0] + tmpStr + tmpStr2;
					$('#renms').val(tmpArr[0].split('{"name":').length-1);
				}else{
					var len = $('#renms option').length;
					L.renm=tmp.substr(0,tmp.length-1) + ',' +tmpStr+ strN + '"}]';
					$('#renms').append('<option value=' + len + '>' + ok + '</option>').val(len).next().toggleClass('auto',false);
					$('#RENM .red').text(ok);
				}
			}
		}
	}


	if(u=='Preview'){renameCode(true)}

	if(u=='OK'){
		if($('#code').attr('data-renb')){
			$('#code').val(unescape($('#code').attr('data-renb')));
			$('#renTip').html(gM('renTip' + L.os));
		}else{
			$('#Path').focus();
		}
		$('#renmOK').hide();
	}

	if(u=='In'){
		var v=(prompt(gM('In'))||'').trim();
		if(v){
			if( r && INI.rule_r.test(v) ||
				n && INI.renm_r.test(v) ){
				var j=jSon(v),s='#'+t+'s';
				var vn=unescape(j.name),i=-1;
				$(s+' option').each(function(index){
					if($(this).text()==vn){
						i=index;
						return false;
					}
				});
				if(i>-1){$(s).val(i)}
				if(r){
					loadruleVal(j);
					ruleOnSrc();
				}else{
					$('#r0').val(unescape(j.r0));
					$('#r1').val(unescape(j.r1));
				}
			}else{
				alert(gM('inAlert'));
			}
		}


	}


	if(u=='Out'){
		var s='#'+t+'s',sv=+$(s).val(),v='{"name":"' +
			escape(sv?$(s+' option').eq(sv).text():dft) +'","'+ (r?'img0":"' +
			escape($('#img0rule').val()) + '","img1":"' +
			escape($('#img1rule').val()) + '","img2":"' +
			escape($('#img2rule').val()) + '","url2":"' +
			escape($('#url2rule').val()):'r0":"' +
			escape($('#r0').val()) + '","r1":"' +
			escape($('#r1').val())) + '"}';
		prompt(gM('Out'),v);
	}

	if(u=='Opt'){api('ofimgr'+(n?'&Renm':''))}
	if(u=='API'){api('ofapi')}
	if(u=='VIP'){api('ofhelp&VIP')}
}


function clk_popout(obj){
	var me=$(obj), id=me.attr('id')||'', pa=me.parent(), pid=pa.attr('id'), onekey=$('#Addonekey').prop('checked'), c=me.attr('class')||'';
	if(/^(TileKWD|btnUpload)$/.test(id) || /(weib|upld)(Opt|Reload|Revoke|Url|Logout)/.test(id) || /shotS(ave|har)/.test(id) || /tile(URL|Title|Weib|Save$)/.test(id)){clk_weib(obj);return}
	var shpN=L.drawShapeNow, isTxt=/Text/.test(shpN);
	if(onekey && id!='Addonekey'){
		var k=[],sel,val='';//值clk_popout选择器;值cng_popout选择器;;
		if(L.onekey){
			k.push(L.onekey)
		}
		if(id){
			sel='#'+id
		}else{
			sel=(pid?'#'+pid+' ':'')+(c?zlr('.',c,''):(me[0].nodeName).toLowerCase());
			if(me.is('span')){
				sel+=':contains('+me.text()+')'
			}
			if(me.is(':button')){
				sel+=":button[value='"+me.val()+"']"
			}
		}
		if(me.is('input:checkbox')){
			val=me.prop('checked');
		}

		k.push(sel+'clk_popout'+val);
		L.onekey=k.join(';');
	}
	if(id=='Addonekey'){
		if(onekey){
			L.onekey='';
			
		}else{
			var keys=jSon(L.onekeys||'[]');
			keys.push({name:gM('hotkey')+(keys.length+1),v:L.onekey});
			L.onekeys=JSON.stringify(keys);
			loadonekeys();
		}
	}
	
	
	
	
//non id
	if(pid=='fontCSS'){
		var i=me.index(), txta=$('textarea#'+shpN);

		txta.toggleClass(ZLR('bold italic underline overline through')[i]);

		me.toggleClass('toggle');
		return;
	}

	if(pid=='tileFontCSS'){
		var i=me.index();
		$('#tilesTb td').toggleClass(ZLR('bold italic underline overline through')[i]);
		me.toggleClass('toggle');
		tileFontOnThen();
		return;
	}
	if(pid=='Share'){
		window.open(fns('more', HOM.ZIG, gM('zname'), gM('zdesc2'), HOM.ZIGPic));
		return;
	}
	if(pid=='shares'){
		var psrc=$('#list :checked ~ div img').filter('[data-src^="http://"]').not('[lowsrc]').attr('src');
		window.open(fns(me.attr('data-share'),TURL,TITLE,'',psrc));
		return;
	}


	if(me.attr('name')=='capIO'){
		var In=me.val()=='in', jp=/scr(|note)$/.test($('#outFile').val());
		$('#outFile,#capOut').toggle(!In);
		$('#capType').toggle(!In && jp);
		$('#jpgQ').toggle(!In && jp && $('#capType').val()!='png');
		$('#inFile,#capIn').toggle(In);

		if(!In && 0){
			
			var imgUrl, q=Number(L.jpgQ)/100, cvs=$('#caps')[0];
			if(q==0 || q>1){q=1.0}
			imgUrl=cvs.toDataURL('image/'+L.capType,q);

			$('#code').val(imgUrl).after('<div><a href="'+imgUrl+'" target=_blank><img src="'+imgUrl+'" height=100 /></a>'+dc).next().nextAll().remove();
		}
		return;
	}


	if(me.is('legend')){
		/*
		pa.toggleClass('show').siblings('fieldset').removeClass('show').children('div,table').hide();
		pa.siblings().toggle();
		*/
		
		me.siblings().toggle();
		
	}

	//class
//console.log(me.attr('class'),me[0].outerHTML);
	if(me.is('.RULE0')){
		var r=jSon(unescape(me.attr('data-rule'))), rn=me.text(), ro=$('#rules option').filter(function(){return $(this).text()==rn});
		if(ro.length){$('#rules').val(ro.index())}
		loadruleVal(r);
	}else if(me.is('.RENM0')){
		var r=jSon(unescape(me.attr('data-rule')));
		$('#r0').val(unescape(r.r0));
		$('#r1').val(unescape(r.r1));
		$('#renmPreview').click();
	}else if(me.is('.CODE0')){
		var c=jSon(unescape(me.attr('data-code')));
		if(pa.is('#DLD0')){
			$('#urlDld').val('...');
			//$('#fetchUrls').click();

			bgP.getUrl='';
			var c=c.code;

			var Tabs=(L.tabId||'').trim();
			if(Tabs){
				var tabsArr= Tabs.split(" ");
				for(var i=0; i<tabsArr.length; i++){
					chrome.tabs.sendRequest(parseInt(tabsArr[i]), {hope:'getUrl', imgrCode:c}, function(response){});
				}
			}else{
				if(L.getSelected){
					var tab = jSon(L.getSelected).tabThis[0];
					var tabId = parseInt(tab.id);
					chrome.tabs.sendRequest(tabId, {hope:'getUrl', imgrCode:c}, function(response) {});
				}
			}

			setTimeout(function(){
				$('#urlDld').val(filterURL('dld')).focus();
			},800);
		}else{
			$('#'+(pa.is('#CODE0')?'codeCode':'imgrCode')).val(unescape(c.code));
			$('#fetch'+(pa.is('#CODE0')?'Code':'Urls')).click();
		}

	}else if(me.is('.tileFontPos')){
		$('.tileFontPos').removeClass('toggle');
		me.addClass('toggle');
		L.tileFontPos=me.index('.tileFontPos');
		loadTiles();
	}else if(me.is('.urlRule1')){
		me.siblings().andSelf().toggle().parent().siblings().toggle()
	}else if(me.is('.urlClickOff')){
		me.prevAll('.urlRule1').click()
	}else if(me.is('.openUrls')){
		var t=me.parent().attr('id').substr(7);
		var urls=$('#urls' + t).val();
		var tabsArr= urlTran(urls).split('\n');
		if(!tabsArr[0]){$('#urls' + t).twinkle();return}
		L.tabId='';
		L.url='';
		for(var i=0; i<tabsArr.length; i++){
			if(tabsArr[i]){
				L.url += tabsArr[i] + '\n';
				setTimeout(function(){
					L.url0=L.url.split('\n')[0];
					L.url=L.url.split('\n').slice(1).join('\n');
					chrome.tabs.create({url: L.url0, selected: false}, function(taB){L.tabId += taB.id +" "})
				},1000);
			}
		}
	}else if(me.is('.closeUrls')){
		closeUrls();
	}else if(me.is('.Autorun')){
		auto(obj);
	}else if(me.is('.Y,.N,.R')){
		me.next('input').val('');
		para();
	}else if(me.is('.preview')){
		var t=me.parent();
		var t2=t.parents('fieldset').attr('id').replace('inputUrls','urls').replace('urlsDld','urlDld');
		t2=$('#'+t2).val();
		t.siblings().toggle();
		t.next().val(urlTran(t2));
	}else if(me.is('.TaskAdd')){
		var tr=pa.parent();
		tr.after('<tr class=memo><td class=Status>'+imgSRC+'check.png" class=status /></td><td contenteditable=true class=task>'+gM('Task')+'#'+(tr.siblings().length+1)+'</td><td><input type=date /><input type=time /></td><td class=btn2>'+
			strbtn+'-" class=TaskDel />'+strbtn+'+" class=TaskAdd />'+'</tr>');
		$('#TaskAdd').hide();
		savememo();
	}else if(me.is('.TaskDel')){

		pa.parent().remove();
		var M=$('#Memo');
		if(M.next().length<1){
			$('#TaskAdd').show();
		}
		savememo();
	}

	//id



	if(id=='Edit'){
		if(me.prop('checked')){
			$('#home input').show();
			$('.onekey span').hide();
			$('#home').sortable({disabled: false});
		}else{
			$('.onekey span').each(function(){
				var me=$(this);
				me.text(me.next().val()).show().nextAll().hide()
			});
			saveonekeys();
			$('#home').sortable({disabled: true})
		}
		
	}

	if(/page[lr]/.test(id)){
		var sN = parseInt(L.sechNum);
		var v=$('#engine img:visible');
		var t=(id=='pagel' ? v.first().prevAll() : v.last().nextAll());
		if(t.length){
			$('#pagel, #pager').addClass('btn2');

			for (var i=0; i<sN; i++){
				if(id=='pagel'){
					v.slice(sN-i-1,sN-i).hide();
					t.slice(sN-i-1,sN-i).fadeIn();
				}else{
					v.slice(i,i+1).hide();
					t.slice(i,i+1).fadeIn();
				}
			}

			$('#pagel').toggleClass('btn2', $('#engine img:visible').first().prevAll().length==0);
			$('#pager').toggleClass('btn2', $('#engine img:visible').last().nextAll().length==0);
		}
	}

	if(id=='color'){me.siblings().toggle()}


	if(id=='local'){$('#imgrCode').attr('placeholder',(obj.checked?'@:url':'return url array'))}
	if(id=='fetchUrls'){
		bgP.getUrl='';
		var imgrCode = $('#imgrCode').val().trim();
		if($('#local').prop('checked')){
			var tmpArr = urlTran($('#urlsImgr').val()).split('\n');
			if(tmpArr){
				for(var i=0; i<tmpArr.length; i++){
					if(tmpArr[i]){
						if(imgrCode.indexOf('@')>0){
							var Str=eval(imgrCode.replace(/@/g,tmpArr[i]));
							tmpArr[i]=Str;
						}
					}
				}
				$('#urlsImgr').val( tmpArr.join('\n') );
			}
		}else{
			if(!imgrCode){imgrCode="return urlArr('a[href]:has(img)')"}
			var Tabs = L.tabId.trim();
			if(Tabs){
				var tabsArr= Tabs.split(" ");
				for (var i=0; i<tabsArr.length; i++){
					chrome.tabs.sendRequest(parseInt(tabsArr[i]), {hope:'getUrl', imgrCode:escape(imgrCode)}, function(response){});
				}
			}else{
				if(L.getSelected){
					var tab = jSon(L.getSelected).tabThis[0];
					var tabId = parseInt(tab.id);
					chrome.tabs.sendRequest(tabId, {hope:'getUrl', imgrCode:escape(imgrCode)}, function(response) {});
				}
			}
			advClick(id);
		}
		$('#urlsImgr').focus();
	}

	if(id=='quicksav'){
		if(onekey){
			
		}
		advClick('fetchImageQ')
	}
	
	//if(id=='bulk'){bulkImage_Q()}
	if(id=='add'){
		var dA=$('#dldAdd');
		if(dA.is(':visible')){
			var v=urlTran($("#urlDld").val()).match(imgPre),v1=urlTran($("#renmDld").val()).match(imgPre), dlf=pathTxt($('#downloadPath').val());
			if(v){
				for(var i=v.length-1;i>-1;i--){
					var src0=v[i], ren=v1 && v1[i]?v1[i].replace(/^http:../,''):'',tid=(new Date()).getTime()+(Math.random()+'').substr(2);
					if(!ren){
						var dot=src0.lastIndexOf('.');
						if(dot>0){
							var s0=src0.substr(0,dot);
							var s1=src0.substr(dot);
						}else{
							var s0=src0;
							var s1='zig';
						}
						s0=s0.replace(/.+\//,'');
						if(/^data:/.test(src0)){
							s0='unnamed.';
							s1=src0.split('/')[0].split(':')[1];
							if(s1=='text'){
								s1='txt';

							}else{
								s1=src0.split('/')[1].split(';')[0].replace(/\+.+/,'');
							}
						}
						ren=s0+s1;
					}
					var obj={url:src0,path:dlf,rename:ren,tid:tid};
					Task.addImg(null,obj);
				}
				
				Task.loop();
				$('#dldAddClickOff').nextAll().add('#items').fadeIn();
			}
			$('#dldClickOff').click();
		}
	}
	if(id=='start'){
		$('#iH').nextAll('.seled').has('.paused').each(function(){
			var me=$(this),did=+me.attr('data-did'),tid=me.attr('data-tid');

			Task.resume(did,tid);
			me.find('span').eq(0).attr({'class':'in_progress','title':gM('in_progress')}).find('img').attr('src','img/imgdl.png');

		});
	}
	if(id=='pause'){
		$('#iH').nextAll('.seled').has('.in_progress,.pending').each(function(){
			var me=$(this),did=+me.attr('data-did'),tid=me.attr('data-tid');

			Task.pause(did,tid);
			me.find('span').eq(0).attr({'class':'paused','title':gM('paused')}).find('img').attr('src','img/pause.png');

		});

	}
	if(id=='del'){
		$('#iH').nextAll('.seled').each(function(){
			var me=$(this),did=+me.attr('data-did');
			Task.erase(did);
			me.remove();
		});
	}
	if(id=='ruleOnSrc'){$('#img2rule,#url2rule').toggle()}
	if(id=='capScr_Q'){capScr_Q()}
	if(id=='capScrS_Q'){capScrS_Q()}
	if(id=='capScrW_Q'){capScrW_Q()}
	if(id=='capScrR_Q'){capScrR_Q()}
	if(id=='capMHTML_Q'){capMHTML_Q()}
	if(id=='capMHTMLTab_Q'){capMHTMLTab_Q()}
	if(id=='fetchCode'){advClick(id)}

	if(id=='CodeReg'){me.toggleClass('toggle').next().toggle()}

	if(id=='lite'){$('#imgr').toggle()}

	if(id=='fetch'){
		if(isPop && onekey){
			
		}
		if(isOut){$('#imgr').hide()}
		set();
		advClick('fetchImage')
	}


	if(/^[WHS]$/.test(id)){$('#min'+id).val(id=='S'?MIN_S:MIN_WH); $('#max'+id).val(MAX_WHS)}
	if(id=='E'){$('#e').prev().andSelf().find('input').prop('checked',true)}
	if(id=='deep'){$('#selImg').prop('checked',true); $('#e ~ td').find('input').not('#selImg').prop('checked',false)}


	if(/^[ATNL]\d?$/.test(id)){me.parent().next().find(':text').val(''); para()}

	if(/^(imgr|code)SaveAs/.test(id)){popoutIt(id)}

	if(id=='zR'){window.open(HOM.ZRL)}
	if(id=='zzllrr'){window.open(HOM.Z)}

	if(id=='upldClickOff'){$('#upl').hide()}
	if(id=='codeClickOff'){$('#codeClick').hide()}


	if(id=='renmBeta'){me.parent().siblings().toggle(); $('#renmOK').hide()}

	if(me.is('.vipLogo')){api('ofhelp&VIP')}
	if(id=='Help'){api('ofhelp&BtnSave')}
	if(id=='Play0'){Play()}
	if(id=='playq'){play(-1)}
	if(id=='play_1'){play(1)}
	if(/play[01]/.test(id)){$('#playo').prevAll().hide(); $('#play3').click()}
	if(id=='playo'){playo()}
	if(id=='playp'){play(-2)}
	if(id=='play3'){if($('#play3').val()=='►'){Play()}else{play(3)}}
	if(id=='play_2'){play(2)}
	if(id=='play_3'){playOff()}
	if(/^Top[01]/.test(id)){Scroll('scroll'+(id=='Top1'?'B':'T'))}

	if(id=='Hotkey'){api('ofhotkey')}

	if(id=='Disp'){$('#disp').fadeToggle()}

	if(/color\d/.test(id)){
		if(id=='color5'){
			$('#Color input[type=color]').each(function(){$(this).val('#'+('00000'+Math.floor(Math.random()*Math.pow(16,6)).toString(16)).substr(-6))});
		}else{
			$('#Color input[type=color]').each(function(i){$(this).val('#'+Colors.split(';')[Number(id.replace('color',''))].split(',')[i])});
		}
		toggleColors(isPop);

	}

	if(/tile\d/.test(id)){
		me.addClass('toggle').siblings().removeClass('toggle');

		var shp=Number(id.substr(-1));
		$('#tile_0').attr('src','img/tile_'+shp+'.gif');
		if(shp>3){$('#tile_1').attr('src','img/tile_'+shp+'_1.gif')}
		$('#col2').toggle(shp>4);
		$('#row').toggle(shp>3);
		$('#desc').toggle(shp==3);
		$('#Sign').toggle(!(L.tileFontOn=='true' && shp==3));

		L.tileShape=shp;
		loadTiles();
	}
	if(/tile_\d/.test(id)){
		me.next().val(1).hide().next().val(0).next().val(2);
	}
	if(/tileSort\d/.test(id)){
		me.addClass('toggle').siblings('span').removeClass('toggle');
		L.tileSort=id.substr(-1);
		loadTiles();
	}
	if(id=='tileFontOn'){
		var chk=me.prop('checked');
		L.tileFontOn=chk;
		$('#Sign').toggle(!chk);
		me.nextAll().toggle(chk);
		loadTiles();
	}

	if(/^(btnReget|count)$/.test(id)){
		var gType=L.getTypeImgr, bR=id=='btnReget';
		$('#disp').hide();

		L.btnReget=bR?1:0;
		loadfore();
		if(bR){loadpic(gType)}else{output(gType,0)}
	}

	if(id=='btnVIP'){$('#ZIGVIP').toggle()}


	if(/btnSave/.test(id)){
		var bsq=/Q/.test(id);
		filtSel('selTrim');
		filtSel('selTrimErr');
		var dlf=pathTxt($('#downloadPath').val());
		L.downloadPath=$('#downloadPathList option[value="'+dlf+'"]').index();
		saveGA();
		var t=$('#barRight img.toggle').attr('id')||'flip0';
		var savType=$('#savType').val(), jpgQ=Number($('#savjpgQ').val())/100, extOpt=/<\[.+\]>/.test($('#r1').val());

		renameCode(false);
		var Imgs=$('#list :checked ~ div img').not('[lowsrc]'),n=Imgs.length;

		if(n){
			$('#dldProg').attr({'max':n,'low':n,'optimum':n}).val(0);

		}else{
			
		}
		if(t=='flip0' && (jpgQ==0 || jpgQ==1)){
			Imgs.each(function(){
				var me=$(this),src0=me.attr('data-src'), ren=me.attr('data-rename1')||me.attr('data-rename'),tid=(new Date()).getTime()+(Math.random()+'').substr(2);
				var obj={url:src0,path:dlf,rename:ren,tid:tid};
				me.attr('data-tid',tid);
				if(bsq){
					saveAs2(src0,dlf,ren);
				}else{
					Task.addImg(this,obj);
				}
			});
		}else{
			if(jpgQ==0){jpgQ=1.0}
			Imgs.each(function(){
				var me=$(this),src0=me.attr('data-src'),tid=(new Date()).getTime()+(Math.random()+'').substr(2);
				var f=document.createElement('canvas');
				var ctx = f.getContext('2d');
				var wh=me.attr('data-wxh').split('×');

				ctx.globalAlpha=0;

				if(/flip/.test(t)){
					f.width=wh[0];
					f.height=wh[1];
					if(t=='flip0'){
						ctx.drawImage(this, 0, 0);
					}else if(t=='flipy'){
						ctx.translate(wh[0], 0);
						ctx.scale(-1, 1);
						ctx.drawImage(this, 0, 0);
						ctx.translate(wh[0], 0);
						ctx.scale(-1, 1);
					}else if(t=='flipx'){
						ctx.translate(0, wh[1]);
						ctx.scale(1, -1);
						ctx.drawImage(this, 0, 0);
						ctx.translate(0, wh[1]);
						ctx.scale(1, -1);
					}
				}else{
					var deg=Number(t.substr(-1));
					f.width=wh[deg%2];
					f.height=wh[1-deg%2];
					ctx.rotate(deg*Math.PI/2);
					ctx.drawImage(this, (deg==1?0:-wh[0]), (deg<3?-wh[1]:0));
				}

				var ren=me.attr('data-rename1')||me.attr('data-rename');
				if(extOpt){ren=ren.replace(/\.[^\.]+$/,'')}
				ren+='.'+savType.replace('e','');
				var obj={url:f.toDataURL('image/'+savType,jpgQ),path:dlf,rename:ren,tid:tid};
				me.attr('data-tid',tid);
				if(bsq){
					saveAs2(src0,dlf,ren);
				}else{
					Task.addImg(this,obj,/rotate[13]/.test(t));
				}
			});
		}
		if(!bsq){
			Task.loop();
		}
	}

	if(/upldF(ile|older)/.test(id)){
		$('#upldNew').append('<div><input type=file id=upld' + Time.now5() + (/Folder/.test(id)?' webkitdirectory':'') +
			' /><input type=button value=- title="'+gM('del')+'" /></div>');
		toggleColor();
	}
	if(id=='upldimg'){$('#upldUrl').click()}

	if(id=='Tile0'){Tile()}
	if(id=='Tiles'){
		if(FREE){$('#tileSave').click(); return}
		$('#Tiles').siblings().fadeTo('slow', (Number($('#Tiles').siblings().css('opacity'))<0.5?1:0))
	}
	if(id=='tileLocal'){$('#upImgsTile').toggle();loadTiles()}
	if(id=='btnRenm'){$('#codeIt :radio[value=renm]').click();codeIt(true);$('#codeClick').fadeIn()}
	if(id=='tileBack'){
		if(isCap){
			drawClr();
			$('#Caps').attr('title','');
			$('#tileTool,#caps').hide();
			$('#capsimg').nextAll().show('fast',function(){$('body').scrollTop(Number(L.capScrY))});
		}else{
			$('#Tile').fadeOut('fast',function(){$('#ZIG').show()})
		}
	}

	if(id=='editReset'){
		var sp=$('#'+shpN), C='#Css', c='#css', ckb='DropShadow Clip Cut';
		$(C+'Filter img, #flip0, .clip, .scale, .skew').click();
		if(isOut){sp=$('#editImg')}
		$(zlr(c,ckb,',')).prop('checked',false);
		$(zlr(C,ckb,',')+', #WH ~ span').hide();
		$('#editImg').removeAttr('style');
		$(C+'Fixed .bold').removeClass('bold');

		$('#WH2').attr('data-scale','1,1');
		$('#WH3').attr('data-rotate',0);
		$('#WH4').attr('data-shadow','0,0,0');
		previewWH();
	}
	if(id=='editSave'){
		if(FREE){vipAlert(2); return}
		editSave();
	}

	if(/btn(Clip|Scale|Cut|Beautify)/.test(id)){
		var isV=$('#edit').is(':visible'),d=$('#B'+id.substr(1)).parent(),isO=d.is('[open]'), chkbox=d.find('summary input'),haschkbox=chkbox.length>0;
		if(isV && isO){
			$('#edit').hide();
		}else{
			d.attr('open','open');
			if(haschkbox && !chkbox.prop('checked')){
				chkbox.click();
			}

			if(!isV){
				$('#editImg').attr('src', function(){return $(this).attr('data-src')});
				$('#editOpt details').not(d).removeAttr('open');
				$('#edit').show();
			}
			d.attr('open','open');

		}
	}

	if(/^css/.test(id)){
		id=id.substr(3);
		var chk=me.prop('checked'), sp=$('#'+shpN), C='#Css', c='#css';
		if(isOut){sp=$('#editImg')}


		if(id=='Matrix'){
			$(C+id+'3d').nextAll().toggle(!chk);
			$(c+id+'3D').toggle(chk)
		}

		if(id=='Clip'){
			$(C+id+',#WH1').toggle(chk);
			if(chk){
				cng_popout($(c+id+'T')[0]);
			}else{
				sp.attr('style', function(i,v){return v.replace(/clip:[^;]+/,'')});
				if(isOut){previewWH()}
			}
		}

		if(id=='Cut'){
			$(C+id).toggle(chk);
		}

		if(id=='TranStyle'){sp.css('-webkit-transform-style',chk?'preserve-3d':'flat')}
		if(id=='BackVisi'){sp.css('-webkit-backface-visibility',chk?'visible':'hidden')}
		if(id=='TransOpt'){$(C+id).toggle(chk)}

		if(id=='DropShadow'){
			$(C+'Dropshadow, #WH4').toggle(chk);
			if(chk){
				cng_popout($(c+id+'X')[0]);
			}else{
				sp.css('-webkit-filter', function(i,v){
					return v.replace(/rgba?\([^\)]+\)/i,'').replace(/drop-shadow\([^\)]+\)/,'');
				}).filter('textarea').css('box-shadow','none');
				if(isOut){previewWH()}
			}
		}
	}
}

function loadruleVal(r){
	$('#img0rule').val(r?unescape(r.img0):'');
	$('#img1rule').val(r?unescape(r.img1):'');
	$('#img2rule').val(r?unescape(r.img2):'');
	$('#url2rule').val(r?unescape(r.url2):'');
	var t=!$('#url2rule').val();
	$('#img2rule').toggle(t);
	$('#url2rule').toggle(!t);
	$('#ruleOnSrc').prop('checked',t);
}

function cng_popout(obj){
	var me=$(obj), id=me.attr('id')||'', v=me.val(), pa=me.parent(), pid=pa.attr('id'), onekey=$('#Addonekey').prop('checked'), c=me.attr('class')||'';
	if(id=='tileImg' || /^up(Img|Sign)$/.test(id) || id=='uplds'){cng_weib(obj); return}
	if(onekey){
		var k=[],sel,val='';//值clk_popout选择器;值cng_popout选择器;;
		
		if(L.onekey){
			k.push(L.onekey)
		}
		if(id){
			sel='#'+id
		}else{
			sel=(pid?'#'+pid+' ':'')+(c?zlr('.',c,''):me[0].nodeName);

		}
		k.push(sel+'cng_popout'+v);
		L.onekey=k.join(';');
	}


	if(id=='searchIt'){loadsech(obj.value)}
	if(/^capType(Tile)?$/.test(id)){me.next().toggle(v=='jpeg'); L[id]=v}
	if(id=='savType'){me.next().toggle(v=='jpeg')}
	if(id=='codeType'){codeMode()}


	if(id=='tileFontCenter'){if(isCap){changeTextCSS()}else{changeTilesCSS()}}

	if(id=='resize'){changeView(v)}
	if(id=='disp'){changeDisp()}

	if(/^sort(Type)*$/.test(id)){
		sortIt($('#sort').val().replace('renm','r'));
	}
	if(id=='tileSort'){
		if(v!='sort'){
			var i=$('#tileSort ~ .toggle').index('#tileSort ~ span');
			if(i<2){sortIt(v.replace('renm','r'))}
			loadTiles();
		}
	}
	if(id=='info'){$('#codeIt :radio[value='+v+']').click();codeIt(true);$('#codeClick').fadeIn();me.val(id)}
	
	if(id=='pin2'){pinIt(v)}
	if(id=='pgInfs'){pgInfs(Number(v))}

	if(id=='playBgColor'){L.playBgColor=v; slideshow(v,0,Number(L.origin))}
	if(id=='playType'){L.playType=v; slideshow(v,0,Number(L.origin))}
	if(id=='playSpeed'){L.playSpeed=v}



	if(id=='signPos'){loadTiles()}
	if(id=='tileDesc'){
		var t=$('#tileDesc').val();
		if(t){L.tileDesc=t.join(' ')+ ' '}else{L.tileDesc=''}
		if($('#tileFontOn').prop('checked')){loadTiles()}
	}
	if(/tileCol/.test(id) || id=='tileRow'){me.prev().toggle(v=='2'); loadTiles()}

	if(/upImgs/.test(id)){
		var isTile=/Tile/.test(id), isImgr=/Imgr/.test(id), isShow=/Show/.test(id),  loc='#'+id.replace('upImgs','Local');
		$(loc).empty();
		if(v){
			if(isImgr){L.getTypeImgr='getImagesLocal'}
			var files=obj.files;
			for(var i=0;i<files.length;i++){
				var f=files[i];
				//console.log(f.type);
				if(/^image[/]/.test(f.type)){
					var src=(URL||webkitURL).createObjectURL(f);

					var img=new Image(), s=f.size, ext=f.type.replace(/image[/]/,'').replace('x-icon','ico').replace('+xml','');
					if(!s){s='?KB'}else{
						s=s/1024;
						if(s>=1024){s=(s/1024).toFixed(1)+'MB'}else{s=s.toFixed(1)+'KB'}
					}
					if(isTile || isImgr){
						$(img).attr({'data-name':f.name,'data-no':i+1,'data-size':f.size,'data-sz':s,'data-ext':ext});
						img.onload=function(){
							var w=this.width;
							var h=this.height;
							var s=w+'×'+h;
							$(this).attr({'data-wxh':s, 'data-src':this.src});
							$(this).removeAttr('src');
							if($(loc+' img:not([data-wxh])').length==0){
								if(isTile){loadTiles()}
								if(isImgr){
									var pic = '{"picList":[', localImgs=$('#LocalImgr img');
									for(var i=0; i<localImgs.length; i++){
										var image=localImgs.eq(i), tmpAry=image.attr('data-wxh').split('×'), src=escape(image.attr('data-src'));
										if(pic.indexOf('{"src":"'+ src + '","alt":"')<0){
											pic += '{"src":"'+ src + '","width":"'+tmpAry[0]+'","height":"'+tmpAry[1]+
												'","alt":"' +escape(image.attr('data-name'))+
												'","title":"' +escape(image.attr('data-ext'))+ '","link2":"Input","r0":""},';
										}
									}

									bgP.getImagesLocal = pic.replace(/,$/, '') + metaL;
									loadpic('getImagesLocal');
								}
							}
						};
					}
					if(isShow){
						$(img).attr('data-src',src)
					}else{
						img.src=src;
					}
					$(loc).append(img);
				}
			}

			if(isShow){$('#play0').val($(loc+' img').length);play(0)}
		}else{
			if(isTile){
				loadTiles()
			}else if(isImgr){
				L.getTypeImgr='getImage';	//L.getType
				loadpic(L.getTypeImgr)
			}else{
				$('#play0').val(0)
			}
		}
	}

	if(/^[FB]GC[12]?$/.test(id)){toggleColors(isPop)}




	if(/^(imgr|code)s$/.test(id)){loadcode(id.substr(0,4), Number(v))}
	if(id=='getType'){imgrMode()}
	if(id=='rules'){loadrule(Number(v))}
	if(id=='renms'){loadrenm(Number(v))}
	if(id=='RULE0S'){
		var t=jSon(unescape(me.children().filter('[value='+v+']').attr('data-rule')));
		loadruleVal(t);
		me.val(-1);
	}



	if(/^css/.test(id)){
		id=id.substr(3);
		var C='#Css', c='#css';
		var shpN=L.drawShapeNow, isTxt=/Text/.test(shpN), sp=$('#'+shpN);
		if(isOut){sp=$('#editImg')}


		var pid=me.prevAll('span[id]').attr('id');
		if(/DropShadow/.test(id)){pid='dropShadow'}



		if(me.parents(C+'Filter').length){
			var v0, dsv;
			sp.css('-webkit-filter', function(i,s){
				var v1='';
				pid=pid.replace('bright','brightness').replace('opa','opacity').replace('R','-r').replace('S','-s');
				v0=s.replace('none','');
				if(/-s/.test(pid)){
					var ds=c+'DropShadow';
					$('#WH4').attr('data-shadow', '0,0,0');
					if($(ds).prop('checked') && $(C+'Dropshadow > input').filter(function(){return $(this).val()!='0'}).length){
						var XYBC=[$(ds+'X').val(), $(ds+'Y').val(), $(ds+'Blur').val(), hex2rgba($(ds+'Color').val(), $(ds+'Opac').val())];
						v1=XYBC.join('px ');
						dsv=v1;
						v0=v0.replace(/rgba?\([^\)]+\)/i,'');
						$('#WH4').attr('data-shadow', XYBC.slice(0,3).join(','));
					}
					$(ds+'Opac').next().text($(ds+'Opac').val())
				}else{
					if(me.attr('value')!=v){
						v1=v+(me.attr('title')||'');
					}
					me.next().text(v);
				}
				var reg=new RegExp(pid+'\\([^\\)]+\\)','gi');
				if(v1){
					v1=pid+'('+v1+')';
					if(v0.indexOf(pid)<0){
						return (v0+' '+v1).trim();
					}else{
						return v0.replace(reg, v1);
					}
				}else{
					return v0.replace(reg, '')||'none'
				}
			}).filter('textarea').css('box-shadow',dsv);

		}else{
			var tf='-webkit-transform', pp='-webkit-perspective';

			if(/^[XY]$/.test(id)){sp.css(id=='X'?'left':'top', v+'px')}
			if(id=='Z'){sp.css('z-index',v)}

			if(id=='Perspec'){sp.css(pp,v)}
			if(/Orig/.test(id)){sp.css((/P/.test(id)?pp:tf)+'-origin',v)}


			if(me.parent(C+'Clip').length){
				var cC=c+'Clip', cT=Number($(cC+'T').val()), cR=Number($(cC+'R').val()), cB=Number($(cC+'B').val()), cL=Number($(cC+'L').val());
				if(isOut){
					var W2=cR-cL, H2=cB-cT;

					var img=sp[0], W=img.naturalWidth, H=img.naturalHeight, w=img.width, h=img.height;
					if(w<W){
						cT=Math.ceil(cT*w/W);cR=Math.ceil(cR*w/W);cB=Math.ceil(cB*w/W);cL=Math.ceil(cL*w/W);
					}
					$('#WH1').html((W2>W?scRed(W2):W2)+'x'+(H2>H?scRed(H2):H2));
				}
				sp.css('clip', 'rect('+[cT,cR,cB,cL].join('px,')+'px)');

			}else if(me.parents(C+'Transform').length){
				var v1='';
				if(isOut){
					$('#WH2').attr('data-scale','1,1');
					$('#WH3').attr('data-rotate','0');
				}
				if(id=='Matrix3d'){
					var mal=v.split(',').length;
					if(mal>5){v1='matrix'+(mal<16?'':'3d')+'('+v+')'}
				}else{
					var cR=c+'Rotate', r='#rotate', f='#flip', ro=$('input[name=rotate]:checked'), rov=(ro.val()||'').substr(9), imgs=$('#list img, #origin img');
					$(C+'Rotate6 img, #barRight img').removeClass('toggle');
					imgs.removeClass('flipx flipy rotate1 rotate2 rotate3');

					if(rov=='3d'){
						v1=$(cR+rov).val();
						if(v1){v1='rotate3d('+v1+')'}
					}else{

						var vArr=[], n0=0;
						$(r+' ~ input').each(function(){
							var v=$(this).val();
							if(v!='0'){vArr.push('rotate'+this.id.substr(-1)+'('+v+'deg)')}else{n0+=1}
						});

						v1=vArr.join(' ');

						if(n0>2){
							$(f+'0').addClass('toggle');

						}else if(n0>1){
							var vX=$(cR+'X').val(), vY=$(cR+'Y').val(), vZ=$(cR+'Z').val(), vv;
							if(vX=='180'||vY=='180'){
								vv=vX=='180'?'x':'y';
								$(f+vv).addClass('toggle');
								imgs.addClass((f+vv).substr(1));
							}
							vZ=Number(vZ);
							if(vZ%90==0 && vZ!=0){
								vZ=vZ/90;
								if(vZ==-1){vZ=3}
								$('#WH3').attr('data-rotate',vZ);
								$(r+vZ).addClass('toggle');
								imgs.addClass((r+vZ).substr(1));
								$('#WH3').attr('title', gM('rotate'+vZ)+'➜ ');
							}
						}
					}


					if($('#skew ~ input').filter(function(){return $(this).val()!='0'}).length){
						var cS=c+'Skew';
						v1+=' skew('+$(cS+'X').val()+'deg,'+$(cS+'Y').val()+'deg)'
					}

					if(/Fixed|Scale/.test(id)){
						id=id.substr(5);
						var pr=me.prev();
						var fw=$('#fW').is('.bold'),fh=$('#fH').is('.bold'),fwh=$('#fWH').is('.bold'),cF=c+'Fixed',cS=c+'Scale',cW=$(cF+'W'),cH=$(cF+'H'),cWH=$(cF+'WH'),cX=$(cS+'X'),cY=$(cS+'Y'),
							wh=($('#editImg').attr('data-wxh')||'x').split('x'),w=+wh[0],h=+wh[1],v=+me.val();
						if(pr.is('.bold') || /[XY]/.test(id)){
							if(id=='W'){
								if(fh){
									cWH.val(fixed4((+cW.val())/(+cH.val())));
								}
								if(fwh){
									cH.val(fixed4(v/(+cWH.val())));
									cY.val(fixed4(v/(+cWH.val())/h));
								}
								cX.val(fixed4(v/w));
							}
							if(id=='H'){
								if(fw){
									cWH.val(fixed4((+cW.val())/(+cH.val())));
								}
								if(fwh){
									cW.val(fixed4(v*(+cWH.val())));
									cX.val(fixed4(v*(+cWH.val())/w));
								}
								cY.val(fixed4(v/h));
							}
							if(id=='WH'){
								if(fw){
									cH.val(fixed4((+cW.val())/v));
									cY.val(fixed4((+cW.val())/v/h));
								}else if(fh){
									cW.val(fixed4(v*(+cH.val())));
									cX.val(fixed4(v*(+cH.val())/w));
								}else{
									cY.val(fixed4((+cW.val())/v/h));
								}
							}
							if(id=='X'){
								cW.val(fixed4(v*w)).attr('disabled',true).prev().removeClass('bold');
								if(fwh){
									cH.val(fixed4(v*w/(+cWH.val())));
									cY.val(fixed4(v*w/(+cWH.val())/h));
								}else{
									cWH.val(fixed4(v*w/(+cH.val())));
								}
							}
							if(id=='Y'){
								cH.val(fixed4(v*h)).attr('disabled',true).prev().removeClass('bold');
								if(fwh){
									cW.val(fixed4(v*h*(+cWH.val())));
									cX.val(fixed4(v*h*(+cWH.val())/w));
								}else{
									cWH.val(fixed4((+cW.val())/v/h));
								}
								
							}
							if(/[XY]/.test(id) && +cX.val()==1 && +cY.val()==1){
								cW.val(w);
								cH.val(h);
								cWH.val(fixed4(w/h));
								$(cF+' .bold').removeClass('bold');
								$(cF+' input').attr('disabled',true)
								
							}
							
						}
						
					}
					if($('#scale ~ input').filter(function(){return $(this).val()!='1'}).length){
						var cS=c+'Scale', SX=Number($(cS+'X').val()), SY=Number($(cS+'Y').val()), SXY=[SX,SY].join(',');
						v1+=' scale'+(isOut?'':'3d')+'('+SXY+(isOut?'':','+$(cS+'Z').val())+')';
						$('#WH2').attr('data-scale',SXY);
					}

				}
				sp.css(tf,v1.trim()||'none');
			}

		}
		if(isOut){previewWH()}

	}

	if(me.is('select, :range, :checkbox')){
		bodyFocus()
	}
}