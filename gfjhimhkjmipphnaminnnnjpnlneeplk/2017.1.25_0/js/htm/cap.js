﻿/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */
var svgid='#svgShape svg[id';

$(function(){
	setTimeout(loadkwd,300);
	getVer();
	L.drawEnd='';
	L.drawMove='';
	L.drawShape='Pointer';
	L.drawShapeNow='';
	L.drawCopy='';
	L.svgTexts=L.svgTexts||["✆✈✉✍✎囍㊣卍卐♀♂↖↗↙↘←→§‰¥￥£￡＄€฿®©¢￠℡™№☁☃☂✆✉☾☽☻☺☎☏"].join('\n');
	var cvs=$('#caps')[0], capctx = cvs.getContext('2d');
	var img=$('#capsimg')[0];
	img.onload=function(){
		var Y=+L.capScrY,W=$(window).width(),H=$(window).height(),w=400,h=parseInt(w*H/W);
	   	$('body').scrollTop(Y);
		$('#capsimg').Jcrop({
			minSize:[50,50],
			maxSize:[0,0],
			setSelect: [parseInt(W/2-w),parseInt(Y+H/2-h),parseInt(W/2+w),parseInt(Y+H/2+h)],
			borderOpacity:0.3,
			boundary:0,
			onDblClick:function(obj){
				cvs.width=obj.w;
				cvs.height=obj.h;

				capctx.drawImage(img,obj.x,obj.y,obj.w,obj.h,0,0,obj.w,obj.h);


				$('#caps').show().nextAll().hide();
				$('#tileTool').show('fast',function(){
					if(obj.w+$('#tileTool').width()>$(window).width()){
						CapsTip();
						if(L.drawShape=='Crop'){$('#tileTool').fadeOut()}
					}

				});

			},
			onChange:function(obj){
				$('.jcrop-vline + .jcrop-tracker').attr('title',[obj.w+' x '+obj.h,gM('zname'),gM('capscr'),gM('shotTip')].join(' - '));
			}
		});
	}

	img.src=chrome.extension.getBackgroundPage().capScrRsrc;


	//attr
	$('#svgSel > *,#SvgOpt,#COLOR, #svgLines div:gt(1), #svgLines ~ div').hide();

	$('#Tile :button').attr('title',function(i,v){return gM('hotkey')+': '+v});
	$('#font').attr('placeholder',gM('font'));
	$('#jpgQ,#capType,#HELP,#BGC,#CssRotate6 img').attr('title',function(){return gM(this.id)});
	$('#cssClip').attr('title',gM('clip'));
	$('#svgArwE,#svgArwS').attr('title',gM('arrow'));
	$('#tileFontCenter').attr('title',gM('center')).before(gM('center'));
	$('#svgShape svg, #svgTog svg').attr('stroke-width',2);


	$('#capio :radio').attr('name','capIO').after(function(){return gM(this.value)});
	$('#CssRotate :radio').attr('name','rotate');

	//val
	$('#tileRenm').on('mouseover',function(){
		this.select();
	});
	$('#jpgQ,#font,#fontSize').on('change keyup mouseup',function(){
		L[this.id]=this.value;
		if(/font/.test(this.id)){changeTextCSS()}
	}).add('#capType').val(function(){return L[this.id]});

	$('#shotSave').val(gM('save')).on('mouseover',function(){
		var t=$('[name=capIO]:checked').val();
//if(t=='in'){$('[name=capIO][value=out]').click()}
	});
	$('#shotShar').val(gM('share'));

	$(':button[value=""], :button:not([value])').val(function(){return gM(this.id)});

	//color
	$('#COLOR summary input:text').attr('readonly','');
	$('#Color :color').val(function() {
		return L[this.id.replace('G','T')] || L[this.id];
	}).next('input').add('#cssDropShadowOpac').attr('title',gM('opa'));


	//html
	$('#fontCSS span').html('A');
	$('span:empty[id]').html(function(){return gM(this.id)});

	var tArr='←→↑↓↖↗↙↘',str='';
	for(var i=0;i<8;i++){
		str+='<option value='+i+'>'+'←→↑↓↖↗↙↘'[i]+'</option>';
	}
	$('#copyDir').append(str);
	$('#copyDir').val(['1']);

	$('#inFile option,#outFile option').html(function(){var v=this.value, out=$(this).parent().attr('id')=='outFile';
		if(v=='capscr'){return gM(v)}
		if(v=='capnote'){return gM(v)+(out?' (*.svg)':'')}
		if(v=='capscrnote'){return gM('capscr')+(out?' (':' + ')+gM('capnote')+(out?')':'')}
		if(v=='capscrnoteSvg'){return gM('capscr')+' (*.svg)'}
	});

	tArr=' ∧△◇□○▲◆■●';str='';
	for(var i in tArr){
		str+='<option value='+i+'>'+tArr[i]+'</option>';
	}
	$('#svgArwE,#svgArwS').append(str);

	tArr='￣﹉﹊';str=''; //﹋﹌
	for(var i in tArr){
		str+='<option value='+i+'>'+tArr[i]+'</option>';
	}
	$('#svgLine').append(str);

	tArr=ZLR('Flipy Flipx Rotate1 Rotate2 Rotate3');str='';
	for(var i in tArr){
		str+='<svg id=scr'+tArr[i]+' class='+tArr[i].toLowerCase()+'><image xlink:href="img/ZIL.png" x="5" y="5" height="20px" width="20px"/></svg>'
	}
	$('#scrTool').append(str);

	$('#tileToolCap svg').not('#Pointer,#allEraser,#Text,[id^=svgPg]').children().not('defs').attr({'stroke':'white','fill':'none'});

	$('#svgArwE').val(6);
	$('#svgTexts').val(L.svgTexts);

	$('filter').attr({x:0,y:0,width:'100%',height:'100%'});

	$('#Line marker').attr({orient:'auto'}).children().attr({stroke:'white',fill:'none'});

	for(var i=2;i<6;i++){
		$('#ArwE'+i+'Line').clone().appendTo('#Line defs').attr('id','ArwE'+(i+4)+'Line').children().attr('fill','white');
		$('#ArwS'+i+'Line').clone().appendTo('#Line defs').attr('id','ArwS'+(i+4)+'Line').children().attr('fill','white');
	}

	$('#Line defs').clone().prependTo('#svgLines svg:not(#Line)').children().attr('id',function(i,v){return v.replace('Line','')+$(this).parent().parent().attr('id')});



	$('#CssFilter :range').add('#OpaFGC, #OpaBGC').each(function(){
		var me=$(this);
		me.after(SC+'rngv title="'+(me.attr('title')||me.prev().text())+'">'+me.attr('value')+sc);
	});

	$('select, :range, :file, :color,#svgTexts').on('change',function(){cng_cap(this)});
	$(':number, #strkDash, #CssTransform :text').not('#jpgQ, #fontSize').on('change keyup mouseup',function(){cng_cap(this)});


	$('#svgShape svg').on('click',function(){
		var id=this.id;
		L.drawShape=id;
		L.drawShapeNow='';
		toggleSvg();

		drawEnd();
		L.drawEnd2='';

		tileToolCap(id);
		$('#caps').css('cursor','default');
		$('#scrTool').toggle($(this).parent().is('#svgTool,#scrTool') && !/Eraser|Copy/.test(id));
		$('#svgCssTransform').toggle(!/Grid|Copy/.test(id));
		$('#copyOpt').toggle(/Copy/.test(id));
		if(/Pointer|Eraser|scr|Copy/.test(id)){
			var scr=/scr/.test(id), note=/note/.test(id),all=/all/.test(id);
			$('#svgSel > *,#SvgOpt').hide();
			$('#COLOR').toggle(scr||all);
			$('#capsimg ~ *').css('cursor',id=='Pointer'?'move':(id=='Copy'?'copy':'no-drop'));
			if(scr || all){
				var cvs=$('#caps')[0],w=cvs.width,h=cvs.height,hv=/[13]/.test(id);
				$('#scrW').val(hv?h:w);
				$('#scrH').val(hv?w:h);
				scrn(id.substr(3).toLowerCase());
				if(all){$('#BGC').show().siblings().hide()}
				if(scr){$('#Pointer').click()}
			}
			if(note){drawClr()}
		}else{
			$('#capsimg ~ *').css('cursor','auto');

			if(/Crop/.test(id)){
				$('#svgSel > *,#SvgOpt,#COLOR').hide();
				$('#strkFill').val(0);
				if($('#caps').width()+$('#tileTool').width()>$(window).width()){
					$('#tileTool').fadeOut();
					CapsTip();
				}
				$('#caps').css('cursor','crosshair');
			}
		}

		var c='#css', C='#Css', isTxt=/Text/.test(id);

		$(C+'Filter img, #flip0, .clip, .scale, .skew').click();

		$(zlr(c,'Perspec Rotate3d',',')).val(0);
		$(zlr(c,'Rotate3d Matrix3d',',')).val('');
		$(c+'PerspecOrig').val('50% 50%');
		$(c+'TransOrig').val('50% 50% 0');
		$(zlr(c,'TranStyle TransOpt Matrix DropShadow Clip',',')).prop('checked',false);
		$(c+'BackVisi').prop('checked',true);

		$(zlr(C,'TransOpt Dropshadow Clip',',')).hide();
		$(C+'Matrix3d').nextAll().show();

		$('#strkW').val(isTxt?0:4);
		$('#strkR').val(isTxt?0:20);
	});

	$('#svgTog svg').on('click',function(){
		var me=$(this), id=me.attr('id');
		me.find('text').attr('fill','yellow').text('○');
		me.siblings().find('text').attr('fill','white').text('-');
		var n=+id.replace('svgPg',''), divSib=$('#svgSel').nextAll(), divs=divSib.not(':has(div)').add(divSib.find('div')), divi=divs.eq(n-1);
		divi=divs.slice(5*(n-1),5*n);

		divs.not(divi).hide();
		divi.show();
	});

	toggleSvg();
	$('#svgShape svg').hover(function(){
		var id=this.id, t,h=gM('hotkey')+' ';
		if(/Pointer/.test(id)){t=h+'P'}
		if(/^Eraser/.test(id)){t=gM('Del')+' '+h+'Delete(D)'}
		if(/noteEraser/.test(id)){t=gM('Del')+' '+h+'Shift+Delete(D)'}
		if(id=='Crop'){t=h+'C'}
		if(id=='Text'){t=h+'T'}
		if(id=='Line'){t=h+'L'}
		if(id=='ArectNote'){t=h+'N'}
		if(id=='ARect'){t=h+'R'}
		if(id=='Copy'){t=gM(id)+' '+h+'Ctrl+V'}
		if(/^scr/.test(id)){t=gM(id.substr(3))}
		if(t){
			$('#Caps').attr('title',t)
		}
		},function(){
			var c=$('#caps'),w=c.width(),h=c.height(),wh=w+'x'+h;
			$('#Caps').attr('title',wh);
			$('#scrW').val(w);
			$('#scrH').val(h);
	});
	$('#Caps').on('mousedown', 'svg, textarea',function(e){mDn(e)}).on('mousemove', 'svg, textarea', function(e){mMv(e)}).on('mouseup', 'svg, textarea', function(e){mUp(e)});
	$('#caps').on('mousedown',function(e){mDn(e)}).on('mousemove',function(e){mMv(e)}).on('mouseup',function(e){mUp(e)});

	$('body').on('keydown',function(e){
		var k=e.keyCode, act=document.activeElement, acti=act.tagName.toLowerCase(),isTxt=acti=='textarea';

		if(isTxt && e.ctrlKey){
			if(k==83){$(act).nextAll(':has([id$=SaveAs])').children('[id$=SaveAs]').click();return false}
			if(k==69){act.value='';return false}
			if(k==66){$('#fontCSS .bold').click()}
			if(k==73){$('#fontCSS .italic').click()}

		}
		if(acti=='input'){
			if(k==13){
				if($(act).is('#tileRenm')){
					$('#shotSave').click()
				}
			}
		}
		if(acti=='body'){
			if(e.ctrlKey){
				if(k==67){L.drawCopy=L.drawShapeNow}
				if(k==86){drawCopy()}
			}else{

				if(k==72){api('ofhotkey')}
				if(k==13){drawEnd()}
				if(k==27){$('#tileTool').fadeToggle()}
				if(k==87){$('#tileWeib').click()}
				if(k==83){$('#shotSave').click()}
				if(k==8){$('#tileBack').click()}
				if(k==80){$('#Pointer').click()}
				if(k==46 || k==68){
					$('#'+L.drawShapeNow).remove();
					if(e.shiftKey){drawClr()}
				}
				if(k==37 || k==39){
					$('#cssX').val(Math.max(+$('#cssX').val()-38+k,0)).change();
				}
				if(k==38 || k==40){
					$('#cssY').val(Math.max(+$('#cssY').val()-39+k,0)).change();
				}
				if(k==65||k==76||k==78||k==82){
					if(k==65){$('#Text').click()}
					if(k==76){$('#Line').click()}
					if(k==78){$('#ArectNote').click()}
					if(k==82){$('#ARect').click()}
					$('#svgPg1').mouseover();
				}
				if(k==67){
					$('#Crop').click();
					if($('#caps').width()+$('#tileTool').width()>$(window).width()){
						$('#tileTool').fadeOut();
						CapsTip();
					}
				}
			}
		}
	}).on('dblclick',drawEnd);

	//toggleColor();
	toggleColorTile();
});

function drawCopy(){
	var c=$('#'+L.drawCopy);

	if(c.length){
		var C=$('#Caps'),a=ZLR('left top width height'),B=[],zi=[],m=+$('#copyMargin').val(),n=+$('#copyn').val(),D=$('#copyDir').val(),s='',M=c[0].outerHTML.split(/\d+.+height: \d+px/);
		for(var i=0;i<4;i++){
			B.push(+c.css(a[i]).replace('px',''))
		}
		//C.children('svg,textarea').each(function(){zi.push(+$(this).css('z-index')||2000)});
		//zi=Math.max.apply(null,zi)+1;
		//←→↑↓↖↗↙↘
		for(var j=0;j<D.length;j++){
			var d=D[j],A=[].concat(B);
			for(var i=1;i<=n;i++){
				if(/[046]/.test(d)){
					A[0]-=A[2]+m
				}
				if(/[157]/.test(d)){
					A[0]+=A[2]+m
				}
				if(/[245]/.test(d)){
					A[1]-=A[3]+m
				}
				if(/[367]/.test(d)){
					A[1]+=A[3]+m
				}
				s+=M.join(idStyle('',A).substr(9));
			}
		}
		C.append(s);
		if(c.is('textarea')){
			C.children().slice(-n*D.length).val(c.val())
		}
	}
}
function drawClr(){$('#Caps > div').nextAll().remove()}
function drawOn(){$('#Caps').attr('title',gM('shotTip'))}
function drawEnd(){
	L.drawEnd='';
	$('#svgShape svg').eq(0).mouseout()
}
function tileToolCap(t, val){

	var id=t.replace(/\d/g,''), txt=id=='Text', ln=+$('#svgLine').val(), isntP=L.drawShape!='Pointer', tme=$('#'+t);
	var C='#Css', c='#css', s='#strk', v=':visible', s_='stroke-';
	var polyln=/Polylin/.test(id), isLn=/Line/.test(id), isln=/line/i.test(id), fil=+$('#strkFill').val();
	$('#bWR,'+s+'R').toggle(/A|Rnd/.test(id) || txt);

	$('#FontSize,#fontSize,#Font').toggle(txt);//#svgTexts,
	$('#svgTEXT').toggle(txt && isntP);
	$('#svgTextN,#svgTextDetails').toggle(txt && isntP && $('#svgText').val()!='0');

	$('#svgLine').toggle(!txt);
	$('#Dash').toggle(ln>0);

	$('#svgArwE,#svgArwS').toggle(isln); // ||path,polygon

	$(s+'LnJoin').toggle($('#'+id).children('path,polygon,rect').not('[rx]').length>0);
	$(s+'LnCap').toggle($('#'+id).children('line,path').length>0);

	$(s+'Fill').toggle(!isLn  && !txt && !/LinWav[HXTV]/.test(id));


	$('#OpaBGC').next().andSelf().toggle(txt || !isLn && fil>0);

	$('#BGC').toggle(txt || !isLn && fil>0);

	$('#copyN,#copyNum').toggle(/Dbl|Wav|Copy/.test(id));
	$('#gridR,#gridC,#gridN').toggle(/Grid/.test(id));
	$('#SvgOpt,#COLOR,#FGC,#OpaFGC, #OpaFGC + span').show();
	$('#copyOpt').hide();
	$('#scrWH').toggle(/scr|all|Crop/.test(id));
	//val

	if(val){
		if(txt){
			$('#fontCSS span').each(function(){
				var me=$(this);
				me.toggleClass('toggle',tme.is('.'+me.attr('class').split(' ')[0]))
			})
			$('#tileFontCenter').val(ZLR('left center right').indexOf(tme.css('text-align')));
			$('#font').val(tme.css('font-family'));
		}
		$(c+'X').val(tme.css('left').replace(/\D/g,''));
		$(c+'Y').val(tme.css('top').replace(/\D/g,''));
		$(c+'Z').val(tme.css('z-index').replace(/\D/g,'')||2000);

		$(s+'W').val((txt?tme.css('border-width').replace(/\D/g,''):tme.find('['+s_+'width]').attr(s_+'width'))||4);

		if(/A|Rnd/.test(id)||txt){
			$(s+'R').val(tme.find('[rx]').attr('rx')||(tme.css('border-radius')||'').replace(/\D/g,'')||0);
		}
		$(s+'Dash').val(tme.find('['+s_+'dasharray]').attr(s_+'dasharray')||'');
		$(s+'DashOffset').val(tme.find('['+s_+'dashoffset]').attr(s_+'dashoffset')||0);
		$(s+'LnJoin').val(tme.find('['+s_+'linejoin]').attr(s_+'linejoin')||'miter');
		$(s+'LnCap').val(tme.find('['+s_+'linecap]').attr(s_+'linecap')||'butt');
		
		var fgA=(txt?tme.css('color').replace(/.+\(|\).*$/g,'').trim().split(','):tme.find('[stroke]').attr('stroke'))||'#000';
		var bgA=(txt?tme.css('border-color').replace(/.+\(|\).*$/g,'').trim().split(','):tme.find('[fill]').attr('fill'))||'#fff';
		var fgOpa=(txt?(fgA[3]||'').trim().substr(0,3):tme.find('[stroke-opacity]').attr('stroke-opacity'))||1;
		var bgOpa=(txt?(bgA[3]||'').trim().substr(0,3):tme.find('[fill-opacity]').attr('fill-opacity'))||1,opa;
		$('#FGC').val(txt?rgb2hex(fgA[0],fgA[1],fgA[2]):fgA);
		$('#BGC').val(txt?rgb2hex(bgA[0],bgA[1],bgA[2]):bgA);
		
		$('#OpaFGC').val(fgOpa).next().text(fgOpa);
		$('#OpaBGC'+v).val(bgOpa).next().text(bgOpa);

		var wf='-webkit-filter', tf='-webkit-transform', pp='-webkit-perspective', Wf=tme.css(wf).replace(/^none/,''), Tf=tme.css(tf).replace(/^none/,''), v0;

		var reg0=/.+\(|\)$/g, Ds=C+'DropShadow', ds=c+'DropShadow';

		if(/drop/.test(Wf)){
			var co=Wf.match(/rgba?\([^\)]+\)/i);
			v0=Wf.replace(/ ?rgba?\([^\)]+\) ?/i,'').match(/shadow\([^\)]+\)/)[0], tA=v0.replace(/.+\(|\)$|px/g,'').split(' ');
			if(co){
				co=co[0].replace(/.+\(|\)$|\s/g,'').split(',');
				$(ds+'Color').val(rgb2hex(co[0],co[1],co[2]));
				opa=co[3]||.5;

			}else{
				$(ds+'Color').val(tA[0]);
				opa=1;
			}


			$(ds+'X').val(tA[0]);
			$(ds+'Y').val(tA[1]);
			$(ds+'Blur').val(tA[2]);
			$(C+'Dropshadow').show();
			$(ds).prop('checked', true);
		}else{
			$(C+'Dropshadow').hide();
			$(ds).prop('checked', false);
			$(Ds+' :number').val(0);
			$(ds+'Color').val('#000000');
			opa=.5;
		}
		$(ds+'Opac').val(opa).next().text(opa);

		$(C+'Filter input').not(Ds+' input').each(function(){
			var me=$(this), id=this.id.substr(3), pid=me.prevAll('span[id]').attr('id');
			pid=pid.replace('bright','brightness').replace('opa','opacity').replace('R','-r');

			var reg=new RegExp(pid+'\\([^\\)]+\\)','gi'), rngv;
			if(reg.test(Wf)){
				v0=Wf.match(reg)[0];
				rngv=v0.replace(/[^\d\.]/g,'');
			}else{
				rngv=+(me.parent().index()<4);
			}
			me.val(rngv).next().text(rngv);
		});

		$(c+'TransOrig').val(tme.css(tf+'-origin')||'50% 50% 0');
		$(c+'TranStyle').prop('checked',tme.css(tf+'-style')!='flat');
		$(c+'Perspec').val(tme.css(pp).replace(/^none/,'')||0);
		$(c+'PerspecOrig').val(tme.css(pp+'-origin')||'50% 50%');
		$(c+'BackVisi').prop('checked',tme.css('-webkkit-backface-visibility')!='hidden');
		$(c+'TransOpt').prop('checked',false);
		$(C+'TransOpt').hide();

		var ma=/matrix/.test(Tf), ro=/rotate/.test(Tf), sl=/scale/.test(Tf), sk=/skew/.test(Tf), r3=C+'Rotate3 :number';
		$(c+'Matrix').prop('checked', ma);
		$(c+'Matrix3D').toggle(ma);
		$(C+'Matrix3d').nextAll().toggle(!ma);
		$(c+'Matrix3d').val((Tf.match(/matrix.?.?\([^\)]+\)/)||[''])[0].replace(/.+\(|\)$|\.0{2,}\d+/g,''));
		$(C+'Rotate6 img').removeClass('toggle');
		$('input[name=rotate][value$=XYZ').prop('checked',true);
		$(c+'Rotate3d').val('');
		$(r3).val(0);
		$('#flip0').addClass('toggle');

		if(ro){
			if(/rotate3d/.test(Tf)){
				$('input[name=rotate][value$=3d').prop('checked',true);
				$(c+'Rotate3d').val(Tf.match(/rotate3d\([^\)]+\)/)[0].replace(/.+\(|\)$/g,''));

			}else{
				$(r3).val(function(){
					var me=$(this), id='r'+this.id.substr(4);
					if(/Z/.test(id)){id+='?'}
					var reg=new RegExp(id+'\\([^\\)]+\\)','gi');
					return (Tf.match(reg)||[0])[0].replace(/[^\d-]/g,'')
				});

				var r30=$(r3).filter(function(){return $(this).val()=='0'}), r30n=r30.length, r3z=$(c+'RotateZ').val();

				if(r30n>2){$('#flip0').addClass('toggle')}else if(r30n>1){
					$('#flipx,#flipy').filter(function(){return $(c+'Rotate'+this.id.substr(-1).toUpperCase()).val()=='180'}).addClass('toggle');
					if(r3z=='180')$('#rotate2').addClass('toggle');
					if(r3z=='90')$('#rotate1').addClass('toggle');
					if(r3z=='-90')$('#rotate3').addClass('toggle');
				}

			}
		}

		if(sl){
			v0=Tf.match(/scale3d\([^\)]+\)/)[0].replace(/.+\(|\)$|\s/ig,'');
			var tA=v0.split(','), cS=c+'Scale';
			$(cS+'X').val(tA[0]);
			$(cS+'Y').val(tA[1]);
			$(cS+'Z').val(tA[2]);
		}else{
			$('#scale ~ input').val(1);
		}

		if(sk){
			v0=Tf.match(/skew\([^\)]+\)/)[0].replace(/.+\(|\)$|\s|deg/ig,'');
			var tA=v0.split(','), cS=c+'Skew';
			$(cS+'X').val(tA[0]);
			$(cS+'Y').val(tA[1]);
		}else{
			$('#skew ~ input').val(0);
		}
	}
}
function toggleSvg(){
	$('#svgShape svg *[stroke=yellow]').attr('stroke','white');
	$('#svgShape svg *[fill=yellow]').attr('fill','white');

	$('#svgTool ~ div svg').css('background-image','-webkit-linear-gradient(white, #ac0 20%, #ac0 80%, white)');
	$('#'+L.drawShape).css('background','none').find('[stroke=white]').attr('stroke','yellow').end().find('[fill=white]').attr('fill','yellow');
}

function ltwh(t){return {left:t[0],top:t[1],width:t[2],height:t[3]}}

function mMv(e){

	var eos=e.originalEvent.srcElement, act=eos.tagName;
	if(act!='CANVAS'){e.stopPropagation()}

	var X=$('body').scrollLeft()+e.clientX, Y=$('body').scrollTop()+e.clientY, WD, HT;
	L.X=X;
	L.Y=Y;

	var shp=L.drawShape, x0=+L.X0, y0=+L.Y0, offX=X-x0, offY=Y-y0, rnd=/A|Rnd/.test(shp), shpN=$('#'+L.drawShapeNow), lt=offX<0?X:x0, tp=offY<0?Y:y0;

	if(L.drawEnd=='no'){

		var sw=+$('#strkW').val(), d=Math.ceil(sw/2), w=Math.abs(offX), h=Math.abs(offY), w2=Math.ceil(w/2), h2=Math.ceil(h/2), wh=w>h;
		if(/Sq|Penta|Heart|Equi/.test(shp)){
			if(/Sq|Penta|Heart|Right[tx]Equi/.test(shp) && shp!='LineSq' && shp!='PolylinSq'){w=Math.max(w,h);h=w}

			if(/Equi/.test(shp)){
				if(/Moon/.test(shp)){
					if(wh){
						w=Math.max(w,h*2);h=Math.ceil(w/2);
					}else{
						h=Math.max(h,w*2);w=Math.ceil(h/2);
					}

				}
				if(/Iso/.test(shp)){
					var t=Math.sqrt(3);
					if(wh){
						w=Math.max(w,parseInt(h*2/t));h=Math.ceil(w*t/2);
					}else{
						h=Math.max(h,parseInt(w*2/t));w=Math.ceil(h*t/2);
					}
				}
			}
			X=x0-w;
			Y=y0-h;
		}

		var w2=Math.ceil(w/2), h2=Math.ceil(h/2), px=(offX<0?w:0), py=(offY<0?h:0), r=+$('#strkR').val(), tArr=[], pnt, c=+(offX*offY>0);
		lt=offX<0?X:x0;
		tp=offY<0?Y:y0;

		if(/Rect|Crop|Grid/.test(shp)){
			var isCrop=/Crop/.test(shp);

			WD=w+(isCrop?2:sw);
			HT=h+(isCrop?2:sw);
			$('#Caps svg:last').children('rect').attr({width:w,height:h}).parent()
			.css(ltwh([lt+(isCrop?-2:0),tp+(isCrop?-2:0),WD,HT]));
			if(/Grid/.test(shp)){
				$('#Caps svg:last').attr('data-txta',[lt,tp,WD,HT].join(','));
			}
			if(isCrop){
				$('#scrW').val(w);
				$('#scrH').val(h)
			}
			var c=$('#caps'),cwh=w+'x'+h;
			$('#Caps').attr('title',cwh);
		}

		if(/Ellipse/.test(shp)){
			WD=w+d*3;
			HT=h+d*3;
			$('#Caps svg:last').children('ellipse').attr({cx:w2+d,cy:h2+d,rx:w2,ry:h2}).parent()
			.css(ltwh([lt,tp,WD,HT]));
		}

		if(/Line/.test(shp)){
			var Shp=shp;
			if(shp=='LineSq'){
				var t=Math.atan2(h,w)/Math.PI;
				if(t>=0 && t<1/8 ){Shp='LineH'}
				if(t>=1/8 && t<3/8 ){Shp='LineD'}
				if(t>=3/8 && t<1/2 ){Shp='LineV'}
			}
			if(Shp=='LineH'){h=0;Y=y0}
			if(Shp=='LineV'){w=0;X=x0}
			if(Shp=='LineD'){if(w>h){h=w;Y=y0-h}else{w=h;X=x0-w}}

			var x1=(offX<0?w:0), y1=(offY<0?h:0), x2=(offX<0?0:w), y2=(offY<0?0:h);

			var x3=x1, x4=x2, y3=y1, y4=y2;

			if(Shp=='LineH'){y1=0;y2=y1}
			if(Shp=='LineV'){x1=0;x2=x1}

			if(/X/.test(Shp)){if(w>h){y3=y2;y4=y1}else{x3=x2;x4=x1}}
			if(/T/.test(Shp)){y1=h2;y2=y1;x3=w2;x4=x3}


			lt=offX<0?X:x0;
			tp=offY<0?Y:y0;

			WD=w+sw*4;
			HT=h+sw*4;

			
			$('#Caps svg:last').children('line').attr({x1:x1,y1:y1,x2:x2,y2:y2,transform:'translate('+sw*(w?1:2)+','+sw*(h?1:2)+')'}).parent()
			.css(ltwh([lt,tp,WD,HT]));

			if(/X|T/.test(Shp)){$('#Caps svg:last').children('line:eq(1)').attr({x1:x3,y1:y3,x2:x4,y2:y4})}

		}

		if(/line/.test(shp)){
			px=(offX<0?w-sw:sw*3); py=(offY<0?h-sw:sw*3);
			//px=(offX<0?w:0); py=(offY<0?h:0);
			var rx=r*(offX<0?-1:1), ry=r*(offY<0?-1:1), hv=/H/.test(shp);
			pnt=['M',px,py];

			if(/[LA]/.test(shp)){
				if(hv){
					tArr=[px,w2+sw,'V',h+sw*2-py];
					tArr2=['H',w+sw*2-px];
					tArr3=['L',px,h+sw*2-py];
				}else{
					tArr=[py,h2+sw,'H',w+sw*2-px];
					tArr2=['V',h+sw*2-py];
					tArr3=['L',w+sw*2-px,py];
				}

				if(/D/.test(shp)){
					if(hv){
						pnt[1]=w+sw*2-px;
						pnt.push('H',px+rx,'A',r,r,0,0,1-c,px,py+ry,'V',h+sw*2-py-ry,'A',r,r,0,0,1-c,px+rx,h+sw*2-py);
					}else{
						pnt[2]=h+sw*2-py;
						pnt.push('V',py+ry,'A',r,r,0,0,c,px+rx,py,'H',w+sw*2-px-rx,'A',r,r,0,0,c,w+sw*2-px,py+ry);
					}
					tArr.splice(1,1);

				}
				if(/Z/.test(shp)){
					if(hv){
						pnt.push('H',w2+sw-rx,'A',r,r,0,0,c,w2+sw,py+ry,'V',h+sw*2-py-ry,'A',r,r,0,0,1-c,w2+sw+rx,h+sw*2-py);
					}else{
						pnt.push('V',h2+sw-ry,'A',r,r,0,0,1-c,px+rx,h2+sw,'H',w+sw*2-px-rx,'A',r,r,0,0,c,w+sw*2-px,h2+sw+ry);
					}
					tArr.shift();

				}
				if(/S/.test(shp)){
					if(hv){
						pnt.push('V',h+sw*2-py-ry,'A',r,r,0,0,1-c,px+rx,h+sw*2-py);
					}else{
						pnt.push('H',w+sw*2-px-rx,'A',r,r,0,0,c,w+sw*2-px,py+ry);
					}
					tArr=tArr.slice(-1);
				}

				if(/L/.test(shp)){
					pnt=pnt.slice(0,4).concat(tArr);
				}
				pnt=pnt.concat(tArr2);
				lt=(offX<0?X-sw:x0); tp=(offY<0?Y-sw:y0);
			}


			if(/curve/.test(shp)){
				var px2=(offX<0?-w:w*2), py2=(offY<0?-h:h*2);
				pnt.push('Q');

				if(hv){
					pnt.push(sw*2+w2,(offY<0?sw*2-h:h*2-sw*2),w+sw*2-px,py);
				}else{
					pnt.push((offX<0?sw*2-w:w*2-sw*2),sw*2+h2,px,h+sw*2-py);
				}

				w += sw*2; h += sw*2;
			}



			if(/Iso/.test(shp)){
				if(hv){
					pnt=['M',px+sw*2,sw*3,'L',w+sw*2-px,h2+sw*2,px+sw*2,h+sw];
				}else{
					pnt=['M',sw*3,py+sw*2,'L',w2+sw*2,h+sw*2-py,w+sw,py+sw*2];
				}

				w += sw*2; h += sw*2;
			}

			WD=w+sw*2;
			HT=h+sw*2;
			$('#Caps svg:last').children('path').attr({d:pnt.join(' ')}).parent()
			.css(ltwh([lt,tp,WD,HT]));
		}


		if(/gon/.test(shp)){

			if(/TriangonRight/.test(shp)){
				if(/tx/.test(shp)){
					var a=Math.atan2(h,w), s=Math.sin(a), c=Math.cos(a);

					if(/H/.test(shp) || /Equi/.test(shp) && wh){
						h=parseInt(h*c*c);
						py=(offY<0?h:0);
						tp=y0+(offY<0?-parseInt(w*c*s):0);
						pnt=[d,d+py,d+w,d+py,d+parseInt(w*(offX<0?s*s:c*c)),d+h-py];
					}else{
						w=parseInt(w*s*s);
						px=(offX<0?w:0);
						lt=x0+(offX<0?-parseInt(h*c*s):0);
						pnt=[d+px,d,d+px,d+h,d+w-px,d+parseInt(h*(offY<0?c*c:s*s))];
					}

				}else{
					if(/V/.test(shp) || /Equi/.test(shp) && wh){
						pnt=[d,d+py,d+w,d+py];
					}else{
						pnt=[d+px,d,d+px,d+h];
					}
					pnt.push(d+w-px,d+h-py);
				}

			}

			if(/TriangonIso/.test(shp)){
				if(/V/.test(shp) || /Equi/.test(shp) && wh){
					pnt=[d,d+py,d+w2,d+h-py,d+w,d+py];
				}else{
					pnt=[d+px,d,d+w-px,d+h2,px+d,d+h];
				}
			}

			if(/dia/.test(shp)){
				pnt=[d,d+h2,d+w2,d+h,d+w,d+h2,d+w2,d];
			}

			if(/Penta/.test(shp)){
				var s=Math.sin(Math.PI*2/5), c=Math.cos(Math.PI*2/5), s2=Math.sin(Math.PI/5), c2=Math.cos(Math.PI/5);

				pnt=[
					d+parseInt(w2*(1-s)),d+parseInt(w2*(1-c)), d+parseInt(w2*(1-c*s/c2)),d+parseInt(w2*(1+c*c/c2)),
					d+parseInt(w2*(1-s2)),d+parseInt(w2*(1+c2)), d+w2,d+parseInt(w2*(1+c/c2)),
					d+parseInt(w2*(1+s2)),d+parseInt(w2*(1+c2)), d+parseInt(w2*(1+c*s/c2)),d+parseInt(w2*(1+c*c/c2)),
					d+parseInt(w2*(1+s)),d+parseInt(w2*(1-c)), d+parseInt(w2*(1+c*s2/c2)),d+parseInt(w2*(1-c)),
					d+w2,sw, d+parseInt(w2*(1-c*s2/c2)),d+parseInt(w2*(1-c))];

				if(/T/.test(shp)){for(var i=0;i<5;i++){pnt[i*4+2]=''; pnt[i*4+3]=''}}
			}


			if(/Paral|Trape/.test(shp)){
				if(L.drawEnd2==''){
					pnt=[d,d,d,d+h,d+w,d+h,d+w,d];

				}else{
					var x1=+L.X1, offx=x1-x0, offX1=X-x1, w0=Math.abs(offx);
					var y1=+L.Y1, offy=y1-y0, offY1=Y-y1, h0=Math.abs(offy);

					if(w0>h0){
						lt=(offx<0 && offX1>0?x1:(offx<0 && offX1<0?X:x0));
					}else{
						tp=(offy<0 && offY1>0?y1:(offy<0 && offY1<0?Y:y0));
					}

					if(shp=='Paralgon'){
						if(w0>h0){
							if(offx>0 && offX1<0){lt=X-offx}
							py=(offY*offX1<0?h:0);
							px=(offx*offX1<0?w0+Math.abs(offX1):w);

							pnt=[d,d+py,d+w0,d+py,d+px,d+h-py,d+px-w0,d+h-py];
						}else{
							if(offy>0 && offY1<0){tp=Y-offy}
							px=(offX*offY1<0?w:0);
							py=(offy*offY1<0?h0+Math.abs(offY1):h);

							pnt=[d+px,d,d+px,d+h0,d+w-px,d+py,d+w-px,d+py-h0];
						}
					}

					if(/Iso|Right/.test(shp)){
						if(w0>h0){
							py=(offY*offX1*offx<0?0:h);
						}else{
							px=(offX*offY1*offy<0?0:w);
						}
					}


					if(/Iso/.test(shp)){
						if(w0>h0){
							if(offx>0 && offX1>0){lt=x0-offX1}
							px=(offx*offX1<0?w0:w+Math.abs(offX1));

							pnt=[d,d+py,d+px,d+py,d+w,d+h-py,d+px-w,d+h-py];
						}else{
							if(offy>0 && offY1>0){tp=y0-offY1}
							py=(offy*offY1<0?h0:h+Math.abs(offY1));

							pnt=[d+px,d,d+px,d+py,d+w-px,d+h,d+w-px,d+py-h];
						}
					}

					if(/Right/.test(shp)){
						if(w0>h0){
							px=w0+(offx*offX1<0?0:Math.abs(offX1));

							pnt=[d,d+py,d+px,d+py,d+px+(offx<0?0:-Math.abs(offX1)),d+h-py,d+(offx<0?Math.abs(offX1):0),d+h-py];
						}else{
							py=h0+(offy*offY1<0?0:Math.abs(offY1));

							pnt=[d+px,d,d+px,d+py,d+w-px,d+py+(offy<0?0:-Math.abs(offY1)),d+w-px,d+(offy<0?Math.abs(offY1):0)];
						}
					}

					if(w0>h0){w=px}else{h=py}
				}

			}

			WD=w+sw;
			HT=h+sw;

			$('#Caps svg:last').children('[points]').attr({points:pnt.join(' ')}).parent()
			.css(ltwh([lt,tp,WD,HT]));
		}


		if(/Curve|Gon|LinWav|Note|arrow|Poly|Free/.test(shp)){

			if(/Curve/.test(shp)){
				var px2=(offX<0?-w:w*2), py2=(offY<0?-h:h*2), Wav=/Wav/.test(shp), tArr2;

				if(Wav){
					px=w2;
					py=h2;
					px2=(offX<0?-w2:w+w2);
					py2=(offY<0?-h2:h+h2);
				}

				if(wh){
					tArr=['M',d,d+py,'Q'];
				}else{
					tArr=['M',d+px,d,'Q'];
				}
				tArr2=[].concat(tArr);

				if(/Dbl|Wav/.test(shp)){
					var n=+$('#copyNum').val(), wn=parseInt(w/n), hn=parseInt(h/n), w2=parseInt(w/2/n), h2=parseInt(h/2/n), w4=parseInt(w/4/n), h4=parseInt(h/4/n);

					if(Wav){
						if(wh){
							tArr.push(d+w4,d+py2,d+w2,d+py,'T',d+wn,d+py);
							tArr2.push(d+w4,d+h-py2,d+w2,d+py,'T',d+wn,d+py);
						}else{
							tArr.push(d+px2,d+h4,d+px,d+h2,'T',d+px,d+hn);
							tArr2.push(d+w-px2,d+h4,d+px,d+h2,'T',d+px,d+hn);
						}
					}

					for(var i=+(Wav);i<n;i++){
						if(wh){
							tArr.push(d+w2*(2*i+1),d+(Wav?py:py2),d+w2*2*(i+1),d+py);
							tArr2.push(d+w2*(2*i+1),d+py,d+w2*2*(i+1),d+py);
						}else{
							tArr.push(d+(Wav?px:px2),d+h2*(2*i+1),d+px,d+h2*2*(i+1));
							tArr2.push(d+px,d+h2*(2*i+1),d+px,d+h2*2*(i+1));
						}
					}

					if(/^D/.test(shp)){tArr=tArr.concat(tArr2)}
				}

				if(/Quo/.test(shp)){
					var px2=+(offX>=0), py2=+(offY<0);
					if(wh){
						tArr.push(h2,h2,0,0,py2,d+h2,d+h2,'H',d+w2-h2,'A',h2,h2,0,0,1-py2,d+w2,h-py,h2,h2,0,0,1-py2,d+w2+h2,d+h2,'H',d+w-h2,'A',h2,h2,0,0,py2,d+w,d+py);
					}else{
						tArr.push(w2,w2,0,0,px2,d+w2,d+w2,'V',d+h2-w2,'A',w2,w2,0,0,1-px2,w-px,d+h2,w2,w2,0,0,1-px2,d+w2,d+h2+w2,'V',d+h-w2,'A',w2,w2,0,0,px2,d+px,d+h);
					}
					tArr[3]='A';
				}

			}

			if(/LinWav/.test(shp)){

				if(/T/.test(shp)){
					if(wh){
						tArr=['M',d,d+h2,'H',d+w];
					}else{
						tArr=['M',d+w2,d,'V',d+h];
					}
				}else if(/[XHV]/.test(shp)){
					tArr=['M',d,d+h*(1-c),'L',d+w,d+h*c];


				}else{
					if(w>h){
						tArr=['M',d,d+py];
					}else{
						tArr=['M',d+px,d];
					}
				}

				var n=+$('#copyNum').val(), wn=parseInt(w/n), hn=parseInt(h/n), w2=parseInt(w/2/n), h2=parseInt(h/2/n);

				if(/tri/.test(shp)){
					tArr.push('L');
					if(/S/.test(shp)){
						if(wh){
							for(var i=0;i<n;i++){
								tArr.push(d+w2*(i*2+1),d+(h-py),d+w2*2*(i+1),d+py);
							}
						}else{
							for(var i=0;i<n;i++){
								tArr.push(d+(w-px),d+h2*(i*2+1),d+px,d+h2*2*(i+1));
							}
						}
					}else{
						if(wh){
							var tArr2=['M',d+wn*n,d+(n%2==1?py:h-py)];
							for(var i=0;i<n;i++){
								tArr.push(d+wn*(i+1),d+(i%2==0?h-py:py));
								tArr2.push(d+wn*(n-i-1),d+((n-i)%2==0?py:h-py));
							}
						}else{
							var tArr2=['M',d+(n%2==1?px:w-px),d+hn*n];
							for(var i=0;i<n;i++){
								tArr.push(d+(i%2==0?w-px:px),d+hn*(i+1));
								tArr2.push(d+((n-i)%2==0?px:w-px),d+hn*(n-i-1));
							}
						}
						tArr=tArr.concat(tArr2);
					}
				}

				if(/rect/.test(shp)){
					if(wh){
						for(var i=0;i<n;i++){
							tArr.push('V',d+h-py,'H',d+w2*(i*2+1),'V',d+py,'H',d+w2*2*(i+1));
						}
					}else{
						for(var i=0;i<n;i++){
							tArr.push('H',d+w-px,'V',d+h2*(i*2+1),'H',d+px,'V',d+h2*2*(i+1));
						}
					}
				}


				if(/T/.test(shp)){
					if(wh){
						for(var i=0;i<n;i++){
							tArr.push('M',d+parseInt(w*(i*2+1)/n/2),d,'V',d+h);
						}
					}else{
						for(var i=0;i<n;i++){
							tArr.push('M',d,d+parseInt(h*(i*2+1)/n/2),'H',d+w);
						}
					}
				}



				if(/SLinWav/.test(shp)){
					var sx=+(offX<0), sy=+(offY<0);
					if(/H/.test(shp)){
						for(var i=sx;i<n+sx;i++){
							tArr.push('M',d+parseInt(w*i/n),d+parseInt(h*(c==1?i:n-i)/n),'H',d+parseInt(w*(i+1-sx*2)/n));
						}
					}else{
						for(var i=sy;i<n+sy;i++){
							tArr.push('M',d+parseInt(w*(c==1?i:n-i)/n),d+parseInt(h*i/n),'V',d+parseInt(h*(i+1-sy*2)/n));
						}
					}
				}

				if(/DLinWav/.test(shp)){
					if(/H/.test(shp)){
						for(var i=1;i<n;i++){
							tArr.push('M',d+parseInt(w*(i-1/2)/n),d+parseInt(h*(c==1?i:n-i)/n),'H',d+parseInt(w*(i+1/2)/n));
						}
					}else{
						for(var i=1;i<n;i++){
							tArr.push('M',d+parseInt(w*(c==1?i:n-i)/n),d+parseInt(h*(i-1/2)/n),'V',d+parseInt(h*(i+1/2)/n));
						}
					}

				}

				if(/X/.test(shp)){
					for(var i=0;i<n;i++){
						tArr.push('M',d+parseInt(w*i/n),d+parseInt(h*(c==1?i+1:n-i-1)/n),'L',d+parseInt(w*(i+1)/n),d+parseInt(h*(c==1?i:n-i)/n));
					}
				}

			}


			if(/Note/.test(shp)){
				var w12=Math.ceil(w/12),h12=Math.ceil(h/12),txta=[];
				var sx=+(offX<0), px0=px,px=(offX<0?w12*2:w12*10), px1=(offX<0?w12:w12*11), px2=(offX<0?w12*5:w12*7), px3=(offX<0?h12*2:w-h12*2);
				var sy=+(offY<0), py0=py,py=(offY<0?h12*2:h12*10), py1=(offY<0?h12:h12*11), py2=(offY<0?h12*5:h12*7), py3=(offY<0?w12*2:h-w12*2);

				if(/rect/.test(shp)){
					var Px0=px0+r*(1-sx*2), Py0=py0+r*(1-sy*2),Px3=px3+r*(sx*2-1),Py3=py3+r*(sy*2-1);

					if(wh){
						tArr=['M',d+px,d+py3,'L',d+px1,d+h-py0,d+px2,d+py3,
							'H',d+Px0,'A',r,r,0,0,c,d+px0,d+Py3,
							'V',d+Py0,'A',r,r,0,0,c,d+Px0,d+py0,
							'H',d+w-Px0,'A',r,r,0,0,c,d+w-px0,d+Py0,
							'V',d+Py3,'A',r,r,0,0,c,d+w-Px0,d+py3];
						if(!rnd){tArr=tArr.slice(0,8).concat(['H',d+px0,'V',d+py0,'H',d+w-px0,'V',d+py3])}
						txta.push(lt+r,tp+r+sy*(d+py3),w-2*r-sw,Math.abs(py0-py3)-2*r);
					}else{
						tArr=['M',d+px3,d+py,'L',d+w-px0,d+py1,d+px3,d+py2,
							'V',d+Py0,'A',r,r,0,0,1-c,d+Px3,d+py0,
							'H',d+Px0,'A',r,r,0,0,1-c,d+px0,d+Py0,
							'V',d+h-Py0,'A',r,r,0,0,1-c,d+Px0,d+h-py0,
							'H',d+Px3,'A',r,r,0,0,1-c,d+px3,d+h-Py0];
						if(!rnd){tArr=tArr.slice(0,8).concat(['V',d+py0,'H',d+px0,'V',d+h-py0,'H',d+px3])}
						txta.push(lt+r+sx*(d+px3),tp+r,Math.abs(px0-px3)-2*r,h-2*r-sw);
					}
				}

				if(/ellipse/.test(shp)){
					var h0=h2-w12-d, w0=w2-h12-d;
					px2=(offX<0?w12*4:w12*8);
					py2=(offY<0?h12*4:h12*8);
					px3=Math.ceil(offX<0?w-w0*(1+Math.sqrt(5)/3):w0*(1+Math.sqrt(5)/3));
					py3=Math.ceil(offY<0?h-h0*(1+Math.sqrt(5)/3):h0*(1+Math.sqrt(5)/3));
					px4=Math.ceil(offX<0?w-w0*(1+Math.SQRT2*2/3):w0*(1+Math.SQRT2*2/3));
					py4=Math.ceil(offY<0?h-h0*(1+Math.SQRT2*2/3):h0*(1+Math.SQRT2*2/3));
					if(w>h){
						tArr=['M',d+px,d+py3,'L',d+px1,d+h-py0,d+px2,d+py4,'A',w2,h0,0,1,c,d+px,d+py3];
					}else{
						tArr=['M',d+px3,d+py,'L',d+w-px0,d+py1,d+px4,d+py2,'A',w0,h2,0,1,1-c,d+px3,d+py];
					}
					w+=sw*3;
					h+=sw*3;
				}

				if(/fan/.test(shp)){
					if(/A/.test(shp)){
						var r1=Math.ceil(w-Math.SQRT2*h), r2=Math.ceil(w/Math.SQRT2), r3=Math.ceil(h-Math.SQRT2*w), r4=Math.ceil(h/Math.SQRT2);
						px=w2+h-r2;
						py=h2+w-r4;
						var px1=(offY<0?px:h-px), py1=(offX<0?py:w-py);
						if(/H/.test(shp)){
							tArr=['M',d+px,d+h-py0,'A',r1,r1,0,0,1-sy,d+w-px,d+h-py0,'L',d+w,d+px1,'A',r2,r2,0,0,sy,d,d+px1];
						}else{
							tArr=['M',d+w-px0,d+py,'A',r3,r3,0,0,sx,d+w-px0,d+h-py,'L',d+py1,d+h,'A',r4,r4,0,0,1-sx,d+py1,d];
						}
					}else{
						if(/H/.test(shp) && w>h*2 || /V/.test(shp) && h>w*2){return}
						var ax=parseInt(Math.sqrt(h*h-w*w/4)), ay=parseInt(Math.sqrt(w*w-h*h/4));
						var px1=(offY<0?ax:h-ax), py1=(offX<0?ay:w-ay);
						if(/H/.test(shp)){
							tArr=['M',d+w2,d+py,'L',d+w,d+px1,'A',h,h,0,0,sy,d,d+px1];
						}else{
							tArr=['M',d+px,d+h2,'L',d+py1,d+h,'A',w,w,0,0,1-sx,d+py1,d];
						}
					}
				}

				px=(offX<0?w:0); py=(offY<0?h:0);

				if(/Moon/.test(shp)){
					w=w||1;
					h=h||1;
					var rx=Math.ceil(w2+h*h/8/w), ry=Math.ceil(h2+w*w/8/h);
					if(w<h){
						tArr=['M',d+px,d,'A',rx,rx,0,0,1-sx,d+px,d+h];
					}else{
						tArr=['M',d,d+py,'A',ry,ry,0,0,sy,d+w,d+py];
					}
				}

				if(/Star/.test(shp)){
					if(/L/.test(shp)){
						tArr=['M',d,d+h2,'A',w2,h2,0,0,1,d+w2,d+h,w2,h2,0,0,1,d+w,d+h2,w2,h2,0,0,1,d+w2,d,w2,h2,0,0,1,d,d+h2];
					}else{
						tArr=['M',d,d+h2,'L',d+w2-r,d+h2+r,d+w2,d+h,d+w2+r,d+h2+r,d+w,d+h2,d+w2+r,d+h2-r,d+w2,d,d+w2-r,d+h2-r];
					}
				}

				if(/Heart/.test(shp)){
					tArr='15 8 15 7 14 5 10 5 4 5 4 10 4 10 4 16 8 20 15 24 22 20 26 16 26 12 26 12 26 5 20 5 17 5 15 7 15 8'.split(' ');
					for(var i=0;i<tArr.length;i++){
						tArr[i]=Math.ceil(+tArr[i]*w/30);
						if(i%6==2){tArr[i]='C'+tArr[i]}
					}
					tArr.unshift('M');
				}

				if(/Rnd$/.test(shp)){
					var px1=(offX<0?r:w-r), py1=(offY<0?r:h-r);
					tArr=['M',d+px,d+py,'H',d+w-px,'V',d+py1,'A',r,r,0,0,c,d+px1,d+h-py,'H',d+w-px1,'A',r,r,0,0,c,d+px,d+py1];

					if(/L/.test(shp)){tArr=tArr.slice(0,7).concat('L',tArr.slice(13,17))}
					if(/SA/.test(shp)){tArr=tArr.slice(0,16).concat(d+px)}
					if(/DL/.test(shp)){tArr.push('L',d+px,d+py1)}
					if(/SL/.test(shp)){tArr.push(d+px)}

				}

				if(/dart/.test(shp)){
					var px1=(offX<0?h2:w-h2), py1=(offY<0?w2:h-w2);
					if(wh){
						tArr=['M',d+px,d+h,'H',d+px1,'L',d+w-px,d+h2,d+px1,d,'H',d+px,'L',d+w-px1,d+h2];
						tArr2=[d+w-px,'V'];
						tArr3=[d+w-px,'L',d+px1,d+h2,d+w-px,d];
						tArr4=['M',d+w-px1,d,'L',d+px,d+h2,d+w-px1,d+h];
					}else{
						tArr=['M',d+w,d+py,'V',d+py1,'L',d+w2,d+h-py,d,d+py1,'V',d+py,'L',d+w2,d+h-py1];
						tArr2=[d+h-py,'H'];
						tArr3=[d+h-py,'L',d+w2,d+py1,d,d+h-py];
						tArr4=['M',d,d+h-py1,'L',d+w2,d+py,d+w,d+h-py1];
					}
					if(/M/.test(shp)){
						if(/D/.test(shp)){
							tArr=tArr4.concat(tArr.slice(3,10));
						}else{
							tArr=tArr.slice(0,12);
						}
					}
					if(/F/.test(shp)){
						if(/D/.test(shp)){
							tArr=tArr.slice(0,4).concat(tArr3,tArr.slice(-5));
						}else{
							tArr=tArr.slice(0,4).concat(tArr2,d,tArr.slice(-5));
						}
					}
				}

			}



			if(/arrow/.test(shp)){
				var w4=Math.ceil(w/4), h4=Math.ceil(h/4), px1=(offX<0?h2:w-h2), py1=(offY<0?w2:h-w2);
				if(wh){
					tArr=['M',d+w-px,d+h2,'L',d+px1,d,'V',d+h4,'H',d+w-px1,'V',d,'L',d+px,d+h2,d+w-px1,d+h,'V',d+h-h4,'H',d+px1,'V',d+h];
					tArr2=[d+px,'L',d+w-px1,d+h2,d+px,d+h-h4];
				}else{
					tArr=['M',d+w2,d+h-py,'L',d,d+py1,'H',d+w4,'V',d+h-py1,'H',d,'L',d+w2,d+py,d+w,d+h-py1,'H',d+w-w4,'V',d+py1,'H',d+w];
					tArr2=[d+py,'L',d+w2,d+h-py1,d+w-w4,d+py];
				}
				if(/S/.test(shp)){tArr=tArr.slice(0,9).concat(tArr2.slice(0,1),tArr.slice(-6))}
				if(/X/.test(shp)){tArr=tArr.slice(0,9).concat(tArr2.slice(0,1),tArr2.slice(-5),tArr.slice(-4))}
			}


			if(/GonRnd/.test(shp)){

				if(/LO/.test(shp)){
					tArr=['M',d,d+r,'V',d+h-r,'H',d+r,'V',d+h,'H',d+w-r,'V',d+h-r,'H',d+w,'V',d+r,'H',d+w-r,'V',d,'H',d+r,'V',d+r];
				}
				if(/AO/.test(shp)){
					tArr=['M',d,d+r,'Q',d+r,d+h2,d,d+h-r,'H',d+r,'V',d+h,'Q',d+w2,d+h-r,d+w-r,d+h,'V',d+h-r,'H',d+w,
									'Q',d+w-r,d+h2,d+w,d+r,'H',d+w-r,'V',d,'Q',d+w2,d+r,d+r,d,'V',d+r];
				}

				var r2=Math.ceil(r/2);
				if(/LI/.test(shp)){
					if(/T/.test(shp)){
						tArr=['M',d,d+h2-r2,'V',d+h2+r2,'H',d+w2-r2,'V',d+h,'H',d+w2+r2,'V',d+h2+r2,'H',d+w,'V',d+h2-r2,'H',d+w2+r2,'V',d,'H',d+w2-r2,'V',d+h2-r2];
					}else{
						var a=Math.atan2(h,w), s=Math.sin(a), c=Math.cos(a), wi=Math.ceil(r/2/(s||.001)), hi=Math.ceil(r/2/(c||.001));
						tArr=['M',d,d+c*r,'L',d+w2-wi,d+h2,d,d+h-c*r,d+s*r,d+h,d+w2,d+h2+hi,d+w-s*r,d+h,d+w,d+h-c*r,d+w2+wi,d+h2,d+w,d+c*r,d+w-s*r,d,d+w2,d+h2-hi,d+s*r,d];
					}


				}
				if(/AI/.test(shp)){
					if(/T/.test(shp)){
						tArr=['M',d+r2,d+h2-r2,'A',r2,r2,0,0,0,d+r2,d+h2+r2,'H',d+w2-r2,'V',d+h-r2,'A',r2,r2,0,0,0,d+w2+r2,d+h-r2,'V',d+h2+r2,'H',d+w-r2,'A',r2,r2,0,0,0,d+w-r2,d+h2-r2,'H',d+w2+r2,'V',d+r2,'A',r2,r2,0,0,0,d+w2-r2,d+r2,'V',d+h2-r2];
					}else{
						tArr=['M',d,d+r,'Q',d+r,d+h2,d,d+h-r,'L',d+r,d+h,'Q',d+w2,d+h-r,d+w-r,d+h,'L',d+w,d+h-r,
										'Q',d+w-r,d+h2,d+w,d+r,'L',d+w-r,d,'Q',d+w2,d+r,d+r,d];
					}
				}

				if(/LG/.test(shp)){
					if(/Sq/.test(shp)){
						if(wh){
							var ha=parseInt(w*Math.sqrt(3)/4), wa=parseInt(w/4);
							tArr=['M',d,d+w2,'L',d+wa,d+w2+ha,'H',d+wa*3,'L',d+w,d+w2,d+wa*3,d+w2-ha,'H',d+wa];
						}else{
							var wa=parseInt(h*Math.sqrt(3)/4), ha=parseInt(h/4), ht=parseInt(3*h/4);
							tArr=['M',d+h2-wa,d+ht,'L',d+h2,d+h2*2,d+h2+wa,d+ht,'V',d+ha,'L',d+h2,d,d+h2-wa,ha];
						}
					}else{
						tArr=['M',d,d+r,'V',d+h-r,'L',d+r,d+h,'H',d+w-r,'L',d+w,d+h-r,'V',d+r,'L',d+w-r,d,'H',d+r,'L',d,d+r];
					}
				}
				if(/AG/.test(shp)){
					tArr=['M',d,d+r,'V',d+h-r,'A',r,r,0,0,1,d+r,d+h,'H',d+w-r,'A',r,r,0,0,1,d+w,d+h-r,
									'V',d+r,'A',r,r,0,0,1,d+w-r,d,'H',d+r,'A',r,r,0,0,1,d,d+r];
				}
			}

			if(/Poly|Free/.test(shp)){
				var p=$('#Caps svg:last').children('path'), dd=p.attr('d');

				if(!dd){$(svgid+'='+L.drawShape+']').click()}

				var tArr=dd.replace(/[MLz]/g,'').split(' '),tArr0=[],tArr1=[], Shp;

				if(/Free/.test(shp)){
					tArr.push(+tArr.slice(-2,-1)+offX,+tArr.slice(-1)+offY);
					L.X0=X; L.Y0=Y;
				}


				if(shp=='PolylinSq'){

					var t=Math.atan2(h,w)/Math.PI;

					if(t>=0 && t<1/8 ){Shp='LineH'}
					if(t>=1/8 && t<3/8 ){Shp='LineD'}
					if(t>=3/8 && t<1/2 ){Shp='LineV'}

					if(Shp=='LineH'){h=0;Y=y0;offY=0}
					if(Shp=='LineV'){w=0;X=x0;offX=0}
					if(Shp=='LineD'){if(w>h){h=w;Y=y0-h*(offY<0?1:-1);offY=Y-y0}else{w=h;X=x0-w*(offX<0?1:-1);offX=X-x0}}

					var offSgn={"x":offX>0, "y":offY>0};

				}


				for(var i=0;i<tArr.length;i++){
					if(i%2==0){
						tArr0.push(+tArr[i]);
					}else{
						tArr1.push(+tArr[i]);
					}
				}
				var n=tArr0.length;
				if(/Poly/.test(shp)){
					tArr0[n-1] =tArr0[n-2]+offX;
					tArr1[n-1] =tArr1[n-2]+offY;

				}




				var minW=Math.min.apply(null,tArr0), minH=Math.min.apply(null,tArr1);
				w=Math.max.apply(null,tArr0)-minW;
				h=Math.max.apply(null,tArr1)-minH;

				for(var i=0;i<tArr0.length;i++){
					tArr0[i] -= minW-d;
					tArr1[i] -= minH-d;
					tArr[i*2]=tArr0[i];
					tArr[i*2+1]=tArr1[i];
				}
				tArr[0]='M'+tArr[0];
				tArr[2]='L'+tArr[2];
				if(/Gon/.test(shp)){tArr[2*n-1] += 'z'}



				lt=X-tArr0[n-1]; tp=Y-tArr1[n-1];
				if(shp=='PolylinSq'){
					if(offSgn.x ){lt += offX*2}
					if(offSgn.y ){tp += offY*2}
				}


				if(/Poly/.test(shp)){drawOn()}

			}

			if(/GonRnd|Note|arrow/.test(shp)){tArr.push('z')}

			WD=w+sw;
			HT=h+sw;

			$('#Caps svg:last').children('path').attr('d',tArr.join(' ')).parent().css(ltwh([lt,tp,WD,HT]));
			if(/rectNote/.test(shp)){
				$('#Caps svg:last').attr('data-txta',txta.join(','))
			}
		}

	}else{
		if(shp=='Pointer' && L.drawMove=='yes'){
			var lt,tp;
			shpN.css({left:function(i,v){lt=+v.replace('px','')+offX; return lt+'px'},top:function(i,v){tp=+v.replace('px','')+offY; return tp+'px'}});
			L.X0=X;
			L.Y0=Y;
			WD=shpN.css('width').replace('px','');
			HT=shpN.css('height').replace('px','');
		}

	}


	if(shpN.length && (L.drawEnd || L.drawMove || act=='TEXTAREA')){

		var c='#css', Z=shpN.css('z-index').replace(/\D/g,'')||2000;

		if(!(act=='TEXTAREA' && !L.drawMove)){
			$(c+'X').val(lt);
			$(c+'Y').val(tp);
			$(c+'Z').val(Z);
		}
		WD=shpN.css('width').replace('px','');
		HT=shpN.css('height').replace('px','');
		$('#WH').text(WD+'x'+HT);
	}

}

function mDn(e){

	var eos=e.originalEvent.srcElement, act=eos.tagName;

	if(act!='CANVAS'){e.stopPropagation()}
	var X=$('body').scrollLeft()+e.clientX, Y=$('body').scrollTop()+e.clientY;
	L.X=X;
	L.Y=Y;


	var shp=L.drawShape;
	if(L.drawEnd=='no'){
		if(/Poly/.test(shp)){
			$('#Caps svg:last').children('path').attr('d',function(i,v){
				return /Gon/.test(shp)?v.replace(/\d+ \d+z/,'$& $&').replace('z ',' '):v.replace(/\d+ \d+$/,'$& $&');
			});
			L.X0=X;
			L.Y0=Y;
		}else if(/Paral|Trape/.test(shp)){

		}else{
			L.drawEnd='';
		}

	}else{
		var isTxt=/Text/.test(shp);
		//$('#svgTexts').toggle(isTxt);
		if(/Pointer/.test(shp)){
			var id=$(eos).closest('svg,textarea').attr('id');
			if(!id){return}

			L.drawShapeNow=id;
			tileToolCap(id, true);
			L.drawMove='yes';
			L.X0=X;
			L.Y0=Y;
			return;
		}
		if(/Eraser|Copy/.test(shp) || /textarea/.test(act.toLowerCase())){return}

		var tim=Time.now5(), shpN=shp+tim, s_='stroke-', s='#strk', sw=+$(s+'W').val(), d=Math.ceil(sw/2), r=+$(s+'R').val(), WD=1+sw, HT=1+sw;

		if(isTxt){WD=25;HT=25}
		var sty=idStyle(shpN,[X,Y,WD,HT]);

		L.X0=X;
		L.Y0=Y;


		var lnJoin=$(s+'LnJoin').val(), fg=$('#FGC').val(), bg=$('#BGC').val(), fgOpa=$('#OpaFGC').val(), bgOpa=$('#OpaBGC').val(), nv=$('#svgTextN').val();

		var strk=strks(/Crop/.test(shp)?shp:'');

		if(isTxt){
			$('#Caps').append(textareas('',sty));
			if(nv){$('#svgTextN').val($('#svgTextN option:selected').next().val())}
		}


		if(/Rect|Crop|Grid/.test(shp)){
			var rnd=/A/.test(shp);
			$('#Caps').append('<svg'+sty+'"><rect x='+d+' y='+d+ (rnd?' rx='+r+' ry='+r:'') +' width=1 height=1'+strk+' /></svg>');
		}

		if(/Ellipse/.test(shp)){
			$('#Caps').append('<svg'+sty+'"><ellipse rx=1 ry=1 cx='+(d+1)+' cy='+(d+1)+strk+' /></svg>');
		}

		if(/gon/.test(shp)){
			$('#Caps').append('<svg'+sty+'"><polygon points="0,0 1,1 1,0" stroke-linejoin='+lnJoin+strk+' /></svg>');
		}

		if(/line/i.test(shp)){
			var tag='path d="M0 0 L1 1z"';
			if(/Line/.test(shp)){tag='line x1='+d+' y1='+d+' x2='+(d+1)+' y2='+(d+1)}


			$('#Caps').append('<svg'+sty+'"><'+tag+strk+(/X|T/.test(shp)?' /><'+tag+strk:'')+' /></svg>');

			$('#'+shp+' defs').clone().prependTo('#'+shpN).children()
				.attr('id',function(i,v){return v+tim})
				.find('[stroke=yellow]').attr('stroke',fg).end().find('[fill=yellow]').attr('fill',fg);

			$('#'+shpN).children('line,path').attr({'marker-end':'url(#ArwE'+
				$('#svgArwE').val()+shpN+')','marker-start':'url(#ArwS'+$('#svgArwS').val()+shpN+')'});
		}

		if(/Note|Curve|arrow|Wav|Gon|Poly|Free/.test(shp)){
			$('#Caps').append('<svg'+sty+'"><path d="M0 0 L1 1" '+s_+'linejoin='+lnJoin+strk+' /></svg>');
		}

		if(!isTxt){L.drawEnd='no'}
		if(/Poly/.test(shp)){drawOn()}


		L.drawShapeNow=shpN;
		var ShpN=$('#'+shpN), zi=[2000];

		$('#Caps').children('svg,textarea').each(function(){zi.push(+$(this).css('z-index')||2000)});

		var C='#Css', c='#css', ds=c+'DropShadow', fs=$(C+'Filter').children(), vf='', vt='', Z=Math.max.apply(null,zi)+1;

		if($(ds).prop('checked') && $(C+'Dropshadow > input').filter(function(){return $(this).val()!='0'}).length){
			vf='drop-shadow('+[$(ds+'X').val(), $(ds+'Y').val(), $(ds+'Blur').val(), hex2rgba($(ds+'Color').val(), $(ds+'Opac').val())].join('px ')+')';
		}
		fs.slice(0,4).filter(function(){return $(this).find('input').val()!='1'}).each(function(){
			vf+=' '+$(this).find('span').attr('id').replace('bright','brightness').replace('opa','opacity')+'('+$(this).find('input').val()+')';
		});
		fs.slice(4,9).filter(function(){return $(this).find('input').val()!='0'}).each(function(){
			vf+=' '+$(this).find('span').attr('id').replace('R','-r')+'('+($(this).find('input').val()+$(this).find('input').attr('title')||'')+')';
		});

		var ma=$(c+'Matrix3d').val();
		if($(c+'Matrix').prop('checked') && ma){
			vt='matrix'+(ma.split(',').length<16?'':'3d')+'('+ma+')';
		}else{
			if($('#skew ~ input').filter(function(){return $(this).val()!='0'}).length){
				var cS='#cssSkew';
				vt+=' skew('+$(cS+'X').val()+'deg,'+$(cS+'Y').val()+'deg)'
			}
			if($('#scale ~ input').filter(function(){return $(this).val()!='1'}).length){
				var cS='#cssScale';
				vt+=' scale3d('+[$(cS+'X').val(),$(cS+'Y').val(),$(cS+'Z').val()].join(',')+')';
			}
			var rt=$('input[name=rotate]:checked').val().substr(9);
			if(rt=='3d'){
				var r3d=$(c+'Rotate3d').val();
				if(r3d){vt+=' rotate3d('+r3d+')'}
			}else{
				$('#rotate ~ input').each(function(){
					var v=$(this).val();
					if(v!='0'){vt+=' rotate'+this.id.substr(-1)+'('+v+'deg)'}
				});
			}
		}

		vf=vf.trim()||'none';

		if(vt){
			if(!$(c+'BackVisi').prop('checked')){ShpN.css('-webkit-backface-visibility','hidden')}
			if($(c+'TranStyle').prop('checked')){ShpN.css('-webkit-transform-style','preserve-3d')}
		}
		vt=vt.trim()||'none';

		ShpN.css({'z-index':Z,'-webkit-filter':vf,'-webkit-transform':vt});

		$(c+'X').val(X);
		$(c+'Y').val(Y);
		$(c+'Z').val(Z);
		$('#WH').text(WD+'x'+HT);

	}


}
function mUp(e){

	var eos=e.originalEvent.srcElement, act=eos.tagName;
	if(act!='CANVAS'){e.stopPropagation()}

	var shp=L.drawShape, X=$('body').scrollLeft()+e.clientX, Y=$('body').scrollTop()+e.clientY, shpN=$('#'+L.drawShapeNow);

	if(L.drawEnd=='no'){
		if(/Poly/.test(shp)){
			L.X0=X;
			L.Y0=Y;
			mDn(e);

		}else if(/Paral|Trape/.test(shp)){
			if(L.drawEnd2==''){
				L.drawEnd2='no';
				L.X1=X;
				L.Y1=Y;
			}else{
				L.drawEnd2='';
				L.drawEnd='';
			}
		}else{
			L.drawEnd='';

			if(/Crop/.test(shp)){
				var cvs=$('#caps')[0], pos=shpN.position();
				scrn('Crop',null,ltwh([pos.left+1,pos.top+1,shpN.width()-2,shpN.height()-2]));

				shpN.add('#caps ~ svg[id^=Crop]').remove();

				if(cvs.width+$('#tileTool').width()>$(window).width()){
					$('#tileTool').fadeOut();
					CapsTip();
				}else{
					$('#tileTool').fadeIn();
				}
			}else if(/Grid|rectNote/.test(shp)){

				var tA=shpN.attr('data-txta').split(','), r=+$('#gridR').val(),c=+$('#gridC').val(),sw=+$('#strkW').val();


				if(/Grid/.test(shp)){
					var w=parseInt((+tA[2]-(c+1)*sw)/c),h=parseInt((+tA[3]-(r+1)*sw)/r),m='',str='';
					
					for(var i=0;i<r;i++){
						for(var j=0;j<c;j++){
							var mx=(sw+w)*j,my=(sw+h)*i,ta=[+tA[0]+sw+mx,+tA[1]+sw+my,w-sw/4,h-sw/4];
							if(i==0 && j>0){
								m+='M'+parseInt(mx+sw/2)+' 0 V'+tA[3]+' ';
							}
							if(j==0 && i>0){
								m+='M0 '+parseInt(my+sw/2)+' H'+tA[2]+' ';
							}
							str+=textareas(ta);
						}
					}
					
					str+='<svg'+idStyle('',tA,'svg')+'"><path d="'+m+'"'+strks()+'></path></svg>';
					shpN.after(str);

				}else if(/rectNote/.test(shp)){
					shpN.after(textareas(tA));
				}
				if($('#svgTextN').val()){$('#svgTextN').val($('#svgTextN option:selected').next().val())}
				$('#Pointer').click();
			}
			if(/Crop|Rect|Grid/.test(shp)){
				$('#svgShape svg').eq(0).mouseout()
			}
		}


	}else{
		if(shp=='Pointer'){
			L.drawMove='';
		}
		if(shp=='Eraser'){
			$(eos).closest('svg,textarea').remove();
		}
		if(shp=='Copy'){
			L.drawCopy=$(eos).closest('svg,textarea').attr('id');
			drawCopy();
			$('#Copy').click()
		}

	}


}

function idStyle(id,A,shp){
	return ' id="'+(id?id:(shp?shp:'Text')+Time.now5()+(Math.random()+'').substr(2))+'" style="position:absolute;left:'+A[0]+'px;top:'+A[1]+'px;width:'+A[2]+'px;height:'+A[3]+'px'
}
function textareas(A,sty){
	var sz=+$('#fontSize').val(), tArr=ZLR('bold italic underline overline through');
	var pos=ZLR('left center right')[$('#tileFontCenter').val()], cls='', nv=$('#svgTextN').val(), mathFont=+$('#svgText').val()>3;
	var fg=$('#FGC').val(), bg=$('#BGC').val(), fgOpa=$('#OpaFGC').val(), bgOpa=$('#OpaBGC').val(),sw=+$('#strkW').val();
	for(var i=0;i<tArr.length;i++){
		if($('#fontCSS .toggle.'+tArr[i]).length){cls+=tArr[i]+' '}
	}

	return '<textarea'+(sty?sty:idStyle('',A))+';font-size:'+sz+'px;'+
		(mathFont?'':'font-family:\''+$('#font').val()+'\';')+'z-index:'+($('#Caps').children('svg, textarea').length+2001)+';'+
		'background:transparent;color:'+hex2rgba(fg,fgOpa)+';border-color:'+hex2rgba(bg,bgOpa)+';border-width:'+
		(sty?sw:0)+'px;opacity:'+(sty?fgOpa:1)+';text-align:'+(sty?pos:'center')+
		'" class="'+cls+(mathFont?' mathFont':'')+'" placeholder="'+gM('urlImg')+'">'+(nv||'')+'</textarea>'
}
function strks(t){
	var s_='stroke-', s='#strk', sw=+$(s+'W').val();
	var lnJoin=$(s+'LnJoin').val(), lnCap=$(s+'LnCap').val(), lnDash=$(s+'Dash').val(), lnDashOffset=$(s+'DashOffset').val();
	var fg=$('#FGC').val(), bg=$('#BGC').val(), fgOpa=$('#OpaFGC').val(), bgOpa=$('#OpaBGC').val();
	var fil=' fill='+['none',bg,fg][+$(s+'Fill').val()]+' fill-opacity='+bgOpa;
	if(t=='Crop'){
		return ' '+s_+'width=1 stroke=red '+s_+'opacity=1 fill=none stroke-dasharray="15,5,4,5"'
	}
	return ' '+s_+'width='+sw+' stroke='+fg+' '+s_+'opacity='+fgOpa+fil+
		' '+s_+'dasharray="'+lnDash+
		'" '+s_+'dashoffset="'+lnDashOffset+
		'" '+s_+'linecap="'+lnCap+
		'" '+s_+'linejoin="'+lnJoin+'"';

}


function scrn(t,bgc,o){
	var cvs=$('#caps')[0], ctx=cvs.getContext('2d'),w=cvs.width,h=cvs.height,img=new Image();
	img.src=cvs.toDataURL('image/png');
	if(t=='eraser'){
		ctx.fillStyle=bgc?$('#BGC').val():'#fff';
		ctx.fillRect(0,0,w,h);
	}else if(/Crop/.test(t)){
		var imgData=ctx.getImageData(o.left,o.top,w,h);
		cvs.width=o.width;
		cvs.height=o.height;
		ctx.putImageData(imgData,0,0);
	}else if(/flip/.test(t)){
		if(t=='flipy'){
			ctx.translate(w, 0);
			ctx.scale(-1, 1);
			ctx.drawImage(img, 0, 0);
			ctx.translate(w, 0);
			ctx.scale(-1, 1);
		}else if(t=='flipx'){
			ctx.translate(0, h);
			ctx.scale(1, -1);
			ctx.drawImage(img, 0, 0);
			ctx.translate(0, h);
			ctx.scale(1, -1);
		}
	}else{
		if(/[13]/.test(t)){
			cvs.width=h;
			cvs.height=w;
		}
		var deg=+t.substr(-1);
		ctx.rotate(deg*Math.PI/2);
		ctx.drawImage(img, (deg==1?0:-w), (deg<3?-h:0));
	}
	ctx.save();
	$('#svgShape svg').eq(0).mouseout();
}

function CapsTip(){$('#Caps').attr('title','← '+gM('hotkey')+' Esc →')}

function loadCaps(){
	var bg=$('#urlCap').attr('data-bg'), infile=$('#inFile').val(), ext, cvs=$('#caps')[0], capctx=cvs.getContext('2d'), img, w, h;
	if(!bg){
		if(infile=='capscr'){scrn('eraser')}
		return;
	}

	if(bg.substr(0,11)=='data:image/'){
		ext=bg.replace(/[;,].*/,'').split('/')[1].toLowerCase().replace('x-icon','ico').replace('+xml','');
		if(ext=='svg'){
			bg=Base64.decode(bg.replace("data:image/svg+xml;base64,",''));
			if(bg.indexOf('desc name="zzllrr Imager Geek"')>0){
				var tArr=bg.split('<foreignObject x='), Scr=/<image x=/.test(tArr[0]), Note=tArr.length==2;
				if(!Note && !Scr){return}
				if(Scr && infile!='capnote'){
					var imgs=tArr[0].split('" xlink:href="'), wh=imgs[0].split('" height="');
					h=+wh[1];
					w=+wh[0].split(' width="')[1];
					img=imgs[1].split('"></image>')[0];
				}
				if(Note && infile!='capscr'){
					tArr[1]=tArr[1].split('</body')[0].split('zig()">')[1];
					var Txt=/<textarea id=/.test(tArr[1]);
					if(Txt){
						var txts=tArr[0].split('".split("Text"),B="');
						var A=txts[0].split('var A="')[1].split('Text'), B=txts[1].split('"')[0].split(',');
					}
					var last=$('#Caps').children().last(), tim=Time.now5();
					last.after(tArr[1]);
					if(Txt){
						for(var i=0;i<A.length;i++){
							last.nextAll('#Text'+A[i]).val(unescape(B[i]));
						}
					}
					last.nextAll().css({'z-index':function(i,v){return +v+$(this).index()*100}}).find('[id]').andSelf().attr('id',function(){return this.id+tim});
				}

			}else{
				if(infile=='capscr'){
					img=$('#urlCap').attr('data-bg');
				}else{
					bg=bg.substr(bg.indexOf('<svg')-4);
					bg=bg.substr(0,bg.lastIndexOf('</svg')+6);
					bg=bg.replace(/<title>.*<[/]title>/g,'').replace(/<desc>.*<[/]desc>/g,'');
					var last=$('#Caps').children().last(), tim=Time.now5();
					last.after('<svg id=SVG'+tim+'>'+bg+'</svg>');
					last.next().css({'z-index':$('#Caps').children('svg, textarea').length+2001,position:'absolute',left:0,top:0})
				}
			}
		}else{
			img=bg;
		}
	}else{
		img=bg;
	}

	if(img){
	var Img=new Image();
		Img.onload=function(){
			if(infile){
				cvs.width=this.width;
				cvs.height=this.height;

				capctx.drawImage(this,0,0);
				$('#svgShape svg').eq(0).mouseout();
				$('#tileTool').show('fast',function(){
					if(this.width+$('#tileTool').width()>$(window).width()){CapsTip()}
				});
			}
		}
		Img.src=img;
	}

}
function changeTextCSS(){
	var shpN=L.drawShapeNow;
	if(/Text/.test(shpN)){
		$('#'+shpN).css({'font-family':$('#font').val(),'font-size':$('#fontSize').val()+
			'px','text-align':ZLR('left center right')[$('#tileFontCenter').val()]});
	}
}

function cng_cap(obj){
	var me=$(obj), id=me.attr('id'), v=me.val();
	if(id=='tileImg' || /^up(Img$|Sign$)/.test(id)){cng_weib(obj); return}
	if(/^css/.test(id)||id=='capType'){cng_popout(obj); return}
	if(/scr[WH]$/.test(id)){
		var cvs=$('#caps')[0],w=+$('#scrW').val(),h=+$('#scrH').val();

		
		if(/allEraser/.test(L.drawShape)){
			cvs.width=w;
			cvs.height=h;
			scrn('eraser');
		}
		if(/Crop/.test(L.drawShape)){
			scrn('Crop',null,ltwh([0,0,w,h]))
		}
	}
	if(id=='outFile'){
		var jp=/scr(|note)$/.test(v);
		$('#capType').toggle(jp);
		$('#jpgQ').toggle(jp && $('#capType').val()!='png');
	}


	if(id=='upCap'){
		var loc='#'+id.replace('up','Local');
		$(loc).empty();
		if(v){
			var files=obj.files;
			for(var i=0;i<files.length;i++){
				var file=files[i];

				if(/^image[/]/.test(file.type)){
					if(/svg/.test(file.type)){
						var reader=new FileReader();
						reader.onload = function(event){
							var src = this.result;
							$('#urlCap').val(' ').css('background','url('+src+') bottom right/contain no-repeat').attr('data-bg',src);
							loadCaps();
						};
						reader.readAsDataURL(file);
					}else{
						$('#urlCap').attr('data-bg',webkitURL.createObjectURL(file));
						loadCaps();
					}
				}
			}

		}else{
			$('#urlCap').attr('data-bg','');
			loadCaps();
		}
	}

	if(/^[FB]GC[12]?$/.test(id)){toggleColorTile()}
	/*

http://www.unicode.org/charts/
parseInt(num,16)
'✈'.charCodeAt(0)
String.fromCharCode(parseInt('0080',16))

parseInt('25FF',16)-parseInt('25A0',16)

for(var i=parseInt('2000',16);i<parseInt('2060',16);i++){console.log(String.fromCharCode(i))}
*/

	if(id=='svgTexts'){L[id]=v}
	if(id=='svgText'){
		var sn=$('#'+id+'N'), sd=$('#'+id+'Details');
		if(v=='0'){sn.add(sd).hide()}else{

			sn.html('');

			v=+v;
			var sArr=['0,0','10102,30;9451,20;12881,15;12977,15;37,1;8240,2;9450,1;48,10;65296,10;9312,60;8304,1;185,1;178,2;8308,6;8320,10;188,3;8531,13',
				'12832,10;12928,10;12992,12;13280,31;13144,25;12690,14;12842,26;12938,39',
				'8544,16;8560,16;913,25;945,25',
				'9372,78;65,26;97,26;65313,26;65345,26;170,1;8319,1;8336,5',
				'161,30;8448,80;8352,26;9216,37;12880,1;12927,1;13004,4;13169,111;13311,1;65504,15',
				'33,15;58,7;91,6;123,4;10629,2;12289,32;12317,4;65040,10;65072,1;65104,8;65119,2;65128,4;65281,15;65306,7;65339,6;65371,11',
				'8592,112;9166,1;9735,2;10132,43;10226,14;10496,124;11012,10;65513,4',
				'161,30;8531,13;8208,24;8240,26;8273,1;8286,1;8314,6;8330,11;8704,242;8960,123;9150,17;9178,2;10620,388;11381,2',
				'8413,4;9472,150;9632,10;9642,8;9650,10;9660,10;9670,10;9680,10;9690,8;9698,10;9708,4;12272,12',
				'8576,5;9280,11;9728,112;9985,103;21325,1;21328,1;22221,1'
				];
			var s=[], s2=[], tArr=sArr[v].split(';');
			for(var i=0;i<tArr.length;i++){

				var arr=tArr[i].split(','), arr0=+arr[0], arrN=+arr[1];

				for(k=0;k<arrN;k++){
					var t=arr0+k;
					s.push(t);
					if(v==1 && (arrN>=10 && arrN%10==0 && (k+1)%10==0 || arr0==12881 && k==9 ||arr0==12977 && (k==4 || k==14)) ||
						v>1 && k==arrN-1 ||
						v==3 && (k+1)%10==0 ||
						v==4 && (k+1)%26==0
						){
						s.push(13);
					}
					s2.push(Options(String.fromCharCode(t)));
				}

			}

			s=fCC(s);
			if(v==4){
				var f2=function(t){
					var s1='',s2='';
					for(var j=0;j<2;j++){
						var Ltn=Latin(t,!j).join('');
						s1+=Ltn+'\n';
						s2+=Options(Ltn.split('')).join('');
					}
					return [s1,s2]
				}
				var Ltn=Arrf(f2,entity);
				s+=Arri(Ltn,0).join('');
				s2=s2.concat(Arri(Ltn,1));
			}

			sd.val(s);
			sn.html(s2.join(''));
			sn.add(sd).show();
		}
	}



	var shpN=L.drawShapeNow, isTxt=/Text/.test(shpN), sp=$('#'+shpN);

	if(id=='svgLine'){
		v=+v;
		$('#strkDash').val(['','10,5','15,5,4,5'][v]);
		$('#Dash').toggle(v>0);

		sp.find('[stroke-dasharray]').attr('stroke-dasharray',$('#strkDash').val());
		$('#svgShape svg').not('#svgTool svg').children().not('defs').attr('stroke-dasharray',['','5,1','3,1,1,1'][v]);
	}
	if(/svgArw/.test(id)){
		toggleSvg();
		var mk=/E/.test(id)?'end':'start';
		$('#svgLines svg'+(shpN?', #'+shpN+'[id*=ine]':'')).children('line, path').attr('marker-'+mk,function(){return 'url(#'+
			$(this).prevAll('defs').children('[id^=Arw'+id.substr(-1)+']').eq(+v).attr('id')+')'});
	}

	if(/^strk/.test(id)){
		id=id.substr(4);
		if(id=='Fill'){
			$('#BGC').toggle(v!='0');
			$('#OpaBGC').next().andSelf().toggle(v!='0');
			toggleColorTile();
		}

		if(id=='Dash'){sp.find('[stroke-dasharray]').attr('stroke-dasharray',v)}
		if(id=='DashOffset'){sp.find('[stroke-dashoffset]').attr('stroke-dashoffset',v)}
		if(id=='W'){sp.find('[stroke-width]').attr('stroke-width',v); if(isTxt){sp.css('border-width', v+'px')}}
		if(id=='R'){sp.find('[rx]').attr({rx:v,ry:v}); if(isTxt){sp.css('border-radius', v+'px')}}

		if(id=='LnJoin'){sp.find('[stroke-linejoin]').attr('stroke-linejoin',v)}
		if(id=='LnCap'){sp.find('[stroke-linecap]').attr('stroke-linecap',v)}
	}


	if(id=='OpaFGC'){
		sp.find('[stroke-opacity]').attr('stroke-opacity',v);
		if(isTxt){sp.css('color',hex2rgba($('#FGC').val(), $('#OpaFGC').val()))}
		me.next().text(v)
	}
	if(id=='OpaBGC'){
		sp.find('[fill-opacity]').attr('fill-opacity',v);
		if(isTxt){sp.css('border-color',hex2rgba($('#BGC').val(), $('#OpaBGC').val()))}
		me.next().text(v)
	}

	if(id=='tileFontCenter'){changeTextCSS()}

	if(id=='copyNum'){

	}


	if(me.is('select')){
		$('body')[0].tabIndex=0;
		$('body').focus();
	}
}
