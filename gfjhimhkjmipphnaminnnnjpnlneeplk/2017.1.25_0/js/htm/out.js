﻿/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */
var metaL='],"metaList":[{'+zlr('"','title url kwd desc linkCss fonts pages','":"",')+'":"1"}]}';

var Dona=DONOR[Math.floor(Math.random()*DONOR.length)], TITLE='ZIG', TURL=HOM.ZIG, bwh=bgP.wh, bsz=bgP.sz, bext=bgP.ext;

getVer();

if(L.getSelected){
	var tab = jSon(L.getSelected).tabThis[0];
	TITLE=unescape(tab.title), TURL=unescape(tab.url);
}else{
	window.open('opt.html?ofhelp');
}


$(function(){
	setTimeout(loadkwd,300);
	//setTimeout(function(){$('#SelSearch').keyup()},1000);
	bgP._gaq.push(['_trackEvent', VER, TURL, TITLE, 1]);

	L.btnReget=0;
	L.btnTile=0;
	//cdl.setShelfEnabled(false);

	//attr
	$(zlr('#tile_','0 1 2',',')).attr('title',gM('reset'));
	$('upl > input').attr('title',gM('new'));

	$('#close2').attr({title:gM('CloseUrls')+'\n'+gM('hotkey')+'B',src:'img/Close.gif'})
		.toggle(!!L.tabId && /^getImages1*$/.test(L.getTypeImgr))
		.on('click',function(){closeUrls();$('#close2').hide()});

	$('#barRight img,#Color select,#play select,#playSpeed,#share').attr('title',function(){return gM(this.id)});

	$('#resize').attr('title',gM('hotkey')+': [+]'+gM('zoomin')+' [-]'+gM('zoomout')+' [0]'+gM('zoom0'));
	$('#playo').attr('title',gM('opt')+'\n'+gM('hotkey')+'O');
	$('#bar').attr('title',gM('viewOri') + '\n'+gM('hotkey')+' V');
	$('#VIPOpt').attr('title',gM('Opt'));
	$('#selectors [title]:not(:range)').filter(function(){return /\+|^.$/.test(this.title)}).add('#btnLeft input, #Top0, #Top1, #Tile :button').add($('#playo').nextAll()).attr('title',function(i,v){return gM('hotkey')+': '+v});
	$('#savjpgQ').attr('title', gM('jpgQ')).val(L.jpgQ);
	$('#dldProg').attr('title',gM('bulk')).on('click',function(){
		$('#dldManager').toggle();
		$('#selector').toggleClass('btn1')
	});
	$('#codeIt :radio').attr('name','code').on('click',function(){codeIt(true)}).parent().hide();

	$('#os').children().slice(1).addClass('hidden');


	//val
	$('#tileSave,#btnSaveQ').val(gM('btnSave'));
	$('#btnSave').val(gM('Task'));

	$(zlr('#btn','Beautify Upload Renm Cut Clip Scale',',')).val(function(){return gM(this.id.substr(3))});
	$('#editBtn input,#renmPreview').val(function(){return gM(this.id.substr(4))});



	var zd=gM('Zdesc'),zn=gM('Zname'),tnd=Time.now('Date'),www=L.getSelected?H_d(TURL):'zzllrr',ov={'www':www,'Date':tnd,'zdesc':zd,'Title':TITLE};

	$('datalist option').each(function(){
		var me=$(this),up=/upload/.test(me.parent().attr('id'));
		var v=(me.val()||me.attr('label')).replace(/(www)|(zdesc)|(Date)|(Title)/g,function(t){return ov[t]});
		me.val(up?pathTxt(v):v);
	});
	var dp=L.downloadPath?+L.downloadPath:-1;
	$('#downloadPath').attr('placeholder', gM('zoom0').replace(/ .+/g,'')).val(dp<0?'':$('datalist option').eq(dp).val());
	$('#uploadPath').val(/zzllrr/.test(VER)?'PIC-SEX':'ZIG');

	$('#dldManager del').text(function(){
		return gM(this.id.substr(6))
	}).on('click',function(){
		var t=this.id.substr(6);
		if(/complete|error|in_progress|pending|paused/.test(t)){
			$('#iH').nextAll(':has(.'+t+')').each(function(){
				var me=$(this),did=+me.attr('data-did');
				Task.erase(did);//me.remove();
			}).remove();
		}
		if(/interrupted|deleted/.test(t)){
			$('#iH').nextAll(':has(.error)').filter(function(){return $(this).children().eq(2).text()==gM(t)}).each(function(){
				var me=$(this),did=+me.attr('data-did');
				Task.erase(did);
			}).remove();
		}

		if(t=='duplicated'){
			var uA=[];
			$('#iH').nextAll().each(function(){
				var me=$(this),u=me.find('a').eq(0).attr('href');
				if(uA.indexOf(u)>-1){
					me.addClass('duplicated');
				}else{
					uA.push(u)
				}
			});
			$('#iH').nextAll('.duplicated').each(function(){
				var me=$(this),did=+me.attr('data-did');
				Task.erase(did);
			}).remove();
		}

	});


	$('#Tile0').val(gM('btntile'));
	$('#Play0').val(gM('Show'));

	$(':button').not('[value]').val(function(){return gM(this.id)});


	$('#playType,#playBgColor').val(function(){return L[this.id]});
	$('#playSpeed').val(+L.playSpeed);

	$('#VIPURLs').val(L.resultCopy || '');



	//html
	$('#playType option').html(function(){var v=this.value;return gM(v.replace(/[XY]$/,''))+(/[XY]$/.test(v)?v.substr(4):'')});

	$('#iinfo').html(gM('danger')+'/'+gM('error'));
	$('#iStartEnd').html(gM('start')+'/'+gM('endTime'));

	$('#fW').html(gM('w'));
	$('#fH').html(gM('h'));
	$('#fWH').html(gM('w')+' ÷ '+gM('h'));
	$('#sort,#tileSort,#info').children(':empty').html(function(){
		var t=this.value;
		if(/sort|info/.test(t)){return '-- '+gM(t)+' --'}
		if(t=='wxh'){return gM('w')+' × '+gM('h')}
		if(t=='w/h'){return gM('w')+' ÷ '+gM('h')}
		return gM(t);
	});
	$('#disp,#tileDesc,#getType').find('option').html(function(){return $(this).text() || gM(this.value)});
	$('#downloaded').attr('title',gM('path')+'/').on('click',function(){
		cdl.showDefaultFolder()
	});

	$('#imgrLocal').after(gM('local')).on('click',function(){
		var b=$(this).prop('checked'),uii=$('#upImgsImgr');
		uii.toggle(b);
		if(b){
			$('#imgrPreview').prop('checked',!b)
		}else{
			uii.val('').change();
		}
	});
	$('#imgrPreview').after(gM('Preview')).on('click',function(){
		$('#list img').toggleClass('btn2',!$(this).prop('checked'))
	});

	
	$('#disp').val(ZLR(L.geek3));
	$('#tileDesc').val(ZLR(L.tileDesc));

	$('#tileFontCSS span').html('A');

	$('#editOpt summary').not(':has(input)').html(function(){return gM(this.id.substr(3))});
	$(zlr2('span h2 del summary',':empty[id]',',')).html(function(){return gM(this.id)});
	if(!isCN){$('#W,#H').html(function(){return this.id})}
	$('#renmBeta').before(gM('renmBeta')+' ');
	$('#uploadPath').before(gM('path')+'/');
	$('#editOpt summary input').before(function(){return gM(this.id.substr(3))});
	
	$('#codeIt :radio').after(function(){return $(this).parent().text()?'':gM(this.value)});


	$('#CssFilter :range').each(function(){
		var me=$(this);
		me.after(SC+'rngv title="'+(me.attr('title')||me.prev().text())+'">'+me.attr('value')+sc);
	});
	//hover
	$(zlr('#btn','Reget css VIP',',')+',#count').hover(function(){
		var t='', id=this.id;
		if(/VIP/.test(id)){
			t=gM('VIP')+' '+scRed('new!');
		}else if(/count/.test(id)){
			t=gM('refreshTip');
		}else{
			t=gM(this.id.substr(3) + 'Tip');
		}
		$('#btnTip').html(t);
	},function(){
		btnTipClear();
	});

	//click
	$('#bar *').on('click', function(e){
		e.stopPropagation();
	});
	$('#btnTip').on('click','.sponsor', function(){
		window.open($(this).attr('href'));
		bgP._gaq.push(['_trackEvent', 'Sponsor', TURL, TITLE, 1]);
	}).on('click','.donor', function(){
		api('ofdona');
		bgP._gaq.push(['_trackEvent', 'Donor', TURL, TITLE, 1]);
	});

	$('#dldadd').attr('title',gM('add')).on('click',function(){$('#dldAdd').fadeToggle();$('#add,#dldAddClickOff,#dldURLfilter').toggle()});
	$('#dldAddClickOff').on('click',function(){$('#dldAdd,#add,#dldAddClickOff,#dldURLfilter').hide()});
	$('#bulk').on('click',function(){$('#dldProg').click()});
	$('#SelAll').on('click',function(){
		$('#iH').nextAll().addClass('seled');
	});
	
	
	$('#SelToggle').on('click',function(){
		var sel=$('#iH').nextAll().filter('.seled'),sel0=$('#iH').nextAll().not('.seled');
		sel.removeClass('seled');
		sel0.addClass('seled');
	});
	$('#SelSearch').attr('title',gM('search') + ' | '+ gM('reload')).on('change',function(){	//'change keyup mouseup'
		var v=this.value.trim(), qry={limit:0};
		if(v){
			qry.query=v.split(' ');
			$('#iH').nextAll().removeClass('seled');
		}
		cdl.search(qry,function(dl){
			var dids=[],trs=[];
			for(var i=dl.length-1;i>-1;i--){
				var dlf=function(d0,v){
					var id=d0.id, tr=$('#iH').nextAll('[data-did='+id+']'), d=H_d(d0.url),t=bgP.tasks[d];
					if(tr.length){
						if(v){
							tr.addClass('seled')
						}
					}else{
						tr=didRefresh(d0);
						if(v){tr.addClass('seled')}
					}
				};
				dlf(dl[i],v);
			}
		});
	});

	$('#share').hover(function(){$(this).next().fadeIn()}, function(){});
	$('#Share').hover(function(){}, function(){$(this).children('span').hide()});

	$('span[id^=sel], #selector del').on('click', function(){filtSel(this.id)});

	$('#bar').on('click', function(){
		var ov=$('#origin').is(':visible');

		if(!ov){
			var Imgs=$('#list :checked ~ div img').not('[lowsrc]');
			if(Imgs.length){
				$('#codeClickOff, #upldClickOff').click();
				$('#selector, #ZIGVIP, #edit,#imgr,#dldManager').slideUp('fast',function(){
					$('#list').hide();
					$('#origin').show();
				});

				$('#origin').html('');

				renameCode(false);

				Imgs.clone().prependTo('#origin');
				$('#origin img').removeAttr('id style hidden').each(function(){
					$(this).wrap('<a href="' + $(this).attr('data-src') + '" target="_blank"></a>');
				});
			}
		}

		if(ov && $('#list input:checked').length){
			$('#origin').hide();
			$('#selector,#list').fadeIn();
		}

	});

	$('#editImg').on('load', function(){
		var W=this.naturalWidth, H=this.naturalHeight, w=this.width;
		$(this).attr({'data-wxh':W+'x'+H});
		$('#WH').text(W+'x'+H);
		$('#cssFixedW').val(W).prev().removeClass('bold');
		$('#cssFixedH').val(H).prev().removeClass('bold');
		$('#cssFixedWH').val(fixed4(W/H)).prev().removeClass('bold');
		$('#preview').text(w<W?'('+gM('preview')+': '+(w/W*100).toFixed(0)+'%)':'');
	}).on('mousedown', function(e){
		var X=$('body').scrollLeft()+e.clientX, Y=$('body').scrollTop()+e.clientY;
		$(this).css({'left':X-$(this).width()/2,'top':Y-$(this).height()/2});
	});

	$('#ren *').on('change',function(){$('#renmOK').hide();$('#renTip').html('');$('#code').val('')});

	$('#code').on('change keyup mouseup',function(e){
		if($('[name=code]').filter(':checked').val()=='renm'){
			var t=$('#code').val().trim();
			if(t && !/^@/.test(t)){
				t=t.split('\n');
				$('#list :checked ~ div img').each(function(i){
					try{
						var t0=$(this).attr('data-rename'), t1=t[i].split('➜')[1].trim();
						$(this).attr('data-rename1',t1||t0)

					} catch(e) {
						 console.log(e);
					}
				});
			}
		}
		e.stopPropagation();
	});

	$('#dldClickOff').on('mouseover',function(){
		$('#dldProg').click()
	});
	$('#upldClickOff,#codeClickOff').on('mouseover',function(){
		$(this).click()
	});

	$('#list').on('click','input',function(){filtSel(this.id)});

	$('#upl').on('click','#upldNew :button', function(){$(this).parent().remove()});

	$('body').on('keydown',function(e){
		var k=e.keyCode, act=document.activeElement, ctrl=e.ctrlKey, alt=e.altKey, shft=e.shiftKey;
		if(act.tagName.toLowerCase()=='textarea' && ctrl){
			if(k==83){$(act).nextAll(':has([id$=SaveAs])').children('[id$=SaveAs]').click();return false}
			if(k==69){act.value='';return false}
		}
		if(act.tagName.toLowerCase()=='body'){
			if(k==72){api('ofhotkey')}
			var bar=$('#bar:visible').length;
			var hasImg=$('#list:visible').length;
			var PLAY=$('#PLAY:visible').length;
			var TILE=$('#Tile:visible').length;
			if(bar){
				if(k==90){$('#lite').click()}
				if(k==32){Play()}
				if(k==84){Tile()}
				if(k==86){$('#bar').click()}
				if(hasImg){
					if(ctrl){
						if(k==83){saveGA();filtSel('selTrimErr')}
						if(k==192){$('#list').parent().toggleClass('btn2')}
					}else{
						if(/^1[08][79]$/.test(k)){$('#resize').val(+$('#resize').val()+(k%10==9?-1:1)*50);changeView($('#resize').val())}
						if(k==48||k==96){$('#resize').val(0);changeView(0)}
						if(k==67){$('#btnRenm').click()}
						if(k==78){renameCode(true)}
						if(k==82){$('#count').click()}
						if(k==76){$('#btnReget').click()}
						if(k==83){$('#btnSave').click()}
						if((k==68 || k==46) && !shft){$('#selDel').click()}
						if((k==68 || k==46) && shft || k==85 && !shft){filtSel('selTrim')}
						if(k==85 && shft){$('#btnUpload').click()}
						if(k==69){$('#selErr').click()}
						if(k==65){$('#selAll').click()}
						if(k==66){$('#btnBeautify').click()}
						if(k==73){$('#selToggle').click()}

						if(k==80){$('#btnVIP').click()}
						if(k==66){$('#close2').click()}
					}
				}
			}else if(PLAY){
				if(/^3[79]$/.test(k)){var i=k-38;play((ctrl?i*2:i))}
				if(k==27){playOff()}
				if(k==32){play((L.originTime?3:0))}
				if(k==38 || k==40){$('#play').fadeToggle()}
				if(k==79){$('#playo').click()}
			}else if(TILE){
				if(k==27){$('#tiles').click()}
				if(k==87){$('#tileWeib').click()}
				if(k==83){$('#tileSave').click()}
				if(k==84||k==8){$('#tileBack').click()}
			}
		}
	});

	$(document).on('click','#items tr',function(){
		$(this).toggleClass('seled');



	}).on('click','.filename',function(){
		var did=+$(this).parents('tr').attr('data-did');
		if(did){
			cdl.open(did)
		}
	}).on('click','.folder',function(){
		var did=+$(this).parents('tr').attr('data-did');
		if(did){
			cdl.show(did)
		}
	}).on('mouseover','#items tr',function(){
		var me=$(this);
		me.find('.folder').removeClass('btn2');
	}).on('mouseout','#items tr',function(){
		var me=$(this);
		me.find('.folder').addClass('btn2');
	});

	if(L.r1){$('#r1').val(L.r1)};

	loadfore();
	//toggleColor();

	btnTipClear();


	output(L.getTypeImgr,0,location.search);
	$('#selectors').fadeIn();
	$('#VIPTable').on('click',':button', function(){clk_VIP(this)})
		.on('change','select', function(){cng_VIP(this)});
//VIP
	if(!FREE){
		var vip=L.VIPWeb || '';
		$('#VIPWeb').val(vip);
		$('#Sina_weibo').toggle(/sina_weibo/.test(vip));
		cng_VIP($('#VIPWeb')[0]);
	}else{
		$('#VIPpages').hide();
	}
});

function output(gettype,delay,type){
	if(type=='?bulk'){
		$('#count').text('0/0');
		$('#list').empty();
		$('#dldManager').fadeIn();
		$('#selector').addClass('btn1');
		
		if(L.getSelected){
			var tab = jSon(L.getSelected).tabThis[0];
			var tabId = parseInt(tab.id);
			if(tabId>0){fetchSend(tabId,L.getTypeImgr)}
		}
		return
	}
	setTimeout(function(){loadpic(gettype);},delay+800);
	setTimeout(function(){filtSel('selTrim')},delay+1500);
	if(type){
		if(type=='?pin'){
			$('#tiles').height(500);
			L.btnTile=2;
			//console.log(type);
			Tile();
			setTimeout(loadTiles,delay+1600);
		}
		if(type=='?show'){
			$('body').css({'background-color':L.playBgColor,'background-image':'none'});
			$('#ZIG').fadeOut('fast',function(){$('#PLAY').show()});
			Max();
			setTimeout(function(){play(0)},delay+1600);
		}
	}
}

function btnTipClear(){
	var c=Dona[2]||'China', donor=SC+'donor title="'+gM('dona')+'">'+
		gM('Donator')+': '+SC+'"country '+ c +'" title='+c+'>'+Dona[1]+sC+'donaInf>'+
		' - '+(/\$/.test(Dona[3])?'':'￥')+Dona[3]+
		' - '+(Dona[4]||(isCN?'通过二维码捐赠':'Donate zzllrr Imager'))+
		' - '+Time.str2date(Dona[0].replace(/\d$/,'201$&'), 'local', 'Date')+
		sc+sc+SC+'"sponsors hidden">'+gM('Sponsor')+': '+SC+'sponsor title="'+gM('Sponsor')+'" href="'+SPONSOR[0][0]+'">'+SPONSOR[0][1]+sc+sc;
	
	$('#btnTip').html(donor);
}

function loadfore(){
	L.origin='';
	$('#barRight .toggle').removeClass('toggle');
	$('#flip0').addClass('toggle');
	$('#list').empty();
	$('#btnReget').text('?');
	$('#count').html(prog)
}

function loadpic(getType){
	try {
		L[getType + 'cnt']='0';
		$('#list').empty();
		var bR=L.btnReget=='1', loc=getType=='getImagesLocal', str='', listImgs=[];
		
		if(!bgP[getType]){filtThen();return}

		var tmpJson = jSon(bgP[getType]);
		var tmpPicArr = tmpJson.picList;

		var listPic='',pext='',psrc='',palt='',ptitle='',pname='',plink2='',pr0='';

		if(tmpPicArr && tmpPicArr.length) {
			var tl=tmpPicArr.length;
			L[getType + 'cnt']=tl;

			if(!bR){
				var w0=+L.minW;
				var w1=+L.maxW;
				var h0=+L.minH;
				var h1=+L.maxH;
				var s0=+L.minS*1024;
				var s1=+L.maxS*1024;
				var PArr=[paraAlt,paraTitle,paraName,paraLink2,paraUrl];
			}

			for(var i=0;i<tl;i++){
				var pi=tmpPicArr[i], tSrc=pi.src;
				if(!tSrc){continue}
				
				imgPreReData.lastIndex=0;
				if(imgPreReData.test(tSrc)){
					psrc = unescape(tSrc);
					pname = psrc.replace(/[;,].*/,'');
					pext = pname.split('/')[1].toLowerCase().replace('x-icon','ico').replace('+xml','');
				}else{
					psrc = unescape(tSrc).replace(/z@z@l@l@r@r/g,'%');
					pname = psrc.replace(/\?.+$/g,'').replace(/.+[/]/,'');
					try{
						pname=decodeURIComponent(pname);
					} catch(e) {
						console.log(pname);
						 console.log(e);
					}
					pext = pname.replace(/.+\./, '').replace(/\&.+$/,'');
				}

				if(listImgs.indexOf(psrc)>-1){
					continue;
				}

				palt =unescape(pi.alt).replace(/"/g,"'");
				ptitle =unescape(pi.title).replace(/"/g,"'");
				plink2 =unescape(pi.link2).replace(/z@z@l@l@r@r/g,'%').replace(/"/g,"'");
				pr0 =unescape(pi.r0).replace(/"/g,"'");

				if(loc){
					pext = ptitle;
					pname = palt
				}
				if(extArr.indexOf(pext)<0){pext=bext[psrc]||'other'}
/*
if(/advpic/.test(psrc)){
	console.log(psrc,pname,pext);
}
*/
				if(!bR){
					if(L[pext]!='true'){continue}
					var b=true;
					var pArr=[palt,ptitle,pname,plink2,psrc];

					for(j=0;j<5;j++){
						var Y=unescape(L[PArr[j][0]]).trim().toLowerCase();
						var N=unescape(L[PArr[j][1]]).trim().toLowerCase();
						var R=unescape(L[PArr[j][2]]).trim();
						var Rg=new RegExp(R);
						if( (Y && pArr[j].indexOf(Y)<0) ||(N && pArr[j].indexOf(N)>-1)||(R && !Rg.test(pArr[j]) ) ){
							b=false;
							break;
						}
					}
					if(!b){continue}

//sel	Img    Css CssFile CssBG    Link 	Text TextCss    Favicon Meta Canvas
					for(j=0;j<paraSel.length;j++){
						var sel=paraSel[j];
						if(L[sel]!='true'){
							var deep = sel.split('sel')[1];
							if( plink2 == deep ||
							   ( /^Css(File|BG)$/.test(plink2) && deep=='Css') ||
							   (plink2=='TextCss' && deep=='Text') ||
							   (!/^(Css|Link|Text|Favicon|Meta|Canvas)/.test(plink2) && deep=='Img') ){
								b=false;
								break;
							}
						}
					}
					if(!b){continue}
				}

				var w=+pi.width;
				var h=+pi.height;
				var sz=-1;
/*
if(/advpic/.test(psrc)){
	console.log(psrc,w,h,bwh[psrc]);
}
*/
				if(bwh[psrc]){
					var wh=bwh[psrc].split('×');
//console.log(wh);
					if(wh){
						w=+wh[0];
						h=+wh[1];
					}

				}
				if(!bR && (w*h>0) && (w>w1 || w<w0 || h>h1 || h<h0)){
					continue;
				}
/*
if(/advpic/.test(psrc)){
	console.log(psrc,w,h);
}
*/
				var s='';
				if(bsz[psrc]){
					sz=+bsz[psrc];
				}else if(loc){
					var loc0=$('#LocalImgr').find('img[data-src="'+psrc+'"]');
					if(loc0.length){
						sz=+loc0.attr('data-size');
						s=' - '+loc0.attr('data-sz');
					}
				}




				if(!bR && sz>-1 && (sz>s1 || sz<s0)){
					continue;
				}


				if(sz>=0 && !loc){
					s=sz/1024;
					if(s<1){
						s=sz+'B'
					}else{
						if(s>=1024){s=(s/1024).toFixed(1)+'MB'}else{s=s.toFixed(1)+'KB'}
					}
					s=' - '+s;
				}

				str+='<label>'+strchkbx0+'checked="checked" id="picChk' +i+
					'" />'+SC+'inf2>'+'...'+sc+'<div><img hidden data-no="'+ (i+1) +
					'" data-wxh="'+ w+'×'+h +
					'" data-sz="'+ s.replace(' - ','') +
					'" data-size="'+ sz +
					'" data-src="'+ psrc +
					'" data-alt="'+ palt +
					'" data-title="'+ ptitle +
					'" data-name="'+ pname +
					'" data-ext="'+ pext +
					'" data-link2="'+ plink2 +
					'" data-r0="'+ pr0 +
					'" title="'+ (pr0||palt||ptitle||plink2) + '" id="pic'+i+
					'" />'+SC+'inf0>'+(pr0||palt||ptitle||pname)+sC+
					'inf1>'+SC+'wxh>'+w+'×'+h+sC+
					'size>'+s+sC+
					'ext> - '+pext+sc+sc+'</div></label>';
/*
if(/advpic/.test(psrc)){
	console.log(psrc,i);
}
*/
				listImgs.push(psrc);
			}
			$('#list').append(str);

			$('#list img').error(function(){
				this.lowsrc=0;
				if(L.optTrimErr=='true' && L.btnReget=='0'){
					$(this).parent().parent().remove();
					filtThen(true);
				}else{
					$(this).show().parent().parent().find('.inf2').html('?');
				}


			}).on('load', function(){
				var w=this.naturalWidth,w0=+L.minW,w1=+L.maxW;
				var h=this.naturalHeight,h0=+L.minH,h1=+L.maxH;
				var s=w+'×'+h,me=$(this),mes=me.attr('data-src');
				imgPreReData.lastIndex=0;
				if(!imgPreReData.test(this.src)){
					bwh[mes]=s;
				}
/*
if(/advpic/.test(mes)){
	console.log(mes,w,h);
}
*/
				if(L.btnReget=='0' && (w>w1 || w<w0 || h>h1 || h<h0)){
/*
if(/advpic/.test(mes)){
	console.log(w0,w1,h0,h1);
	console.log(w,h);
}
*/
					me.parent().parent().remove();
					filtThen(true);
				}else{
				//	console.log(me.attr('data-ext')+' 格式');
					if(!loc && me.attr('data-ext')=='other'){
						refreshSize(me,true);
					}
					me.css('width','100%').attr('data-wxh',s).toggleClass('btn2',!$('#imgrPreview').prop('checked')).show().nextAll('.inf1').children('.wxh').html(s);
					disp(this);
					var inf2=me.parent().prevAll('.inf2');
					inf2.children('.w').html(w);
					inf2.children('.h').html(h);
					me.parent().parent().show();
				}
				/*
				if(/\.png$/i.test(this.src)){
					APNG.animateImage(this);
				}
				*/

			}).attr('src', function(){
				return $(this).attr('data-src');
			});
			/*
			if(!loc){
				$('#list img:not([lowsrc])').each(function(){
						refreshSize($(this));
				});
			}
			*/
		}
		if(bR){filtSel('selAll')}


		changeView($('#resize').val());


		$('#btnReget').text( L[getType + 'cnt'] + '+' );
		setTimeout(function(){filtThen(true);},1000);	//picNul
	} catch(e) {
		 console.log(e);
	}
}


function trimErr(t){
	t.lowsrc=0;
	if(L.optTrimErr=='true' && L.btnReget=='0'){
		$(t).parent().parent().remove();
		filtThen(true);
	}
}

function refreshSize(t,extonly){
	if(t){
		var src=t.attr('data-src'),sz=t.attr('data-size');
		imgPreReData.lastIndex=0;
		if(imgPreReData.test(src) || sz!='-1'){
			return
		}
		if(L.minS=='0' && L.maxS=='20000' && !extonly){
			return 
		}
		var xhr=new XMLHttpRequest();
		xhr.open('HEAD', src, true);
		xhr.onerror = function(){
			xhr.abort();
		};
		xhr.onreadystatechange = function(){
		  if(xhr.readyState==4){
			if(xhr.status==200 || xhr.status==206){
				var s=xhr.getResponseHeader('Content-Length'),sb=s,ext=t.attr('data-ext');
				bsz[src]=s||'';
				
			//	console.log(ext);
			if(ext=='other' && xhr.getResponseHeader('Content-Type')){
				//Content-Type: [type]/[subtype]; parameter
				ext=xhr.getResponseHeader('Content-Type').replace(/image[/]/,'').replace(/application[/]/,'').replace(/;.*/,'');
				
				if(/text/.test(ext)){//强制转成png
					ext='png';
				}
				ext=ext.replace(/.*[-_\+\.]/,'');
				bext[src]=ext;
			}
/*
if(/advpic/.test(src)){
	console.log(src,xhr.getResponseHeader('Content-Type'),ext);
}
*/
			//	console.log(ext);
				var s0=+L.minS*1024;
				var s1=+L.maxS*1024;
				if(L.btnReget=='0' && (s<s0 || s>s1 || extsArr.indexOf(ext)>-1 && L[ext]!='true' || extsArr.indexOf(ext)<0 && L.other!='true')){
/*
if(/advpic/.test(src)){
	console.log(src,ext);
}
*/
					t.parent().parent().remove();
					filtThen(true);
				}else{
/*
if(/advpic/.test(src)){
	console.log(src,xhr.getAllResponseHeaders(),s);
}
*/
					if(!s){s='?KB'}else{
						if(s<1024){
							s+='B'
						}else{
							s=s/1024;
							if(s>=1024){s=(s/1024).toFixed(1)+'MB'}else{s=s.toFixed(1)+'KB'}
						}
					}

					t.attr({'data-size':(sb?sb:''),'data-sz':s,'data-ext':ext})
						.nextAll('.inf1').children('.size').text(' - '+s)
						.next().text(' - '+ext);
					var inf2=t.parent().prevAll('.inf2');
					inf2.children('.s').html(s);
					inf2.children('.e').html(ext);
					//console.log(xhr.getAllResponseHeaders());
				}
			}
		  }
		};
		xhr.send();
	}
}

function filtThen(fast){
	var chk=$('#list input:checked');
	var unchk=$('#list input:not(:checked)').removeAttr('checked');
	var chkimg=chk.parent().find('img');
	var unchkimg=unchk.parent().find('img');
	codeIt();
	$('#count').html(chk.length + '/' + (chk.length + unchk.length));

	$('#list input').each(function(i){
		 $(this).next().children('.no').html(i+1);
		 $(this).parent().find('img').attr('data-no',i+1);
	});
	picNul();
	$('#disp').hide();

	var img=chkimg.eq(0),eI=$('#editImg'),s0=eI.attr('data-src'),s1=img.attr('data-src');
	if(s0!=s1){
		eI.attr('data-src', s1);
		if(eI.attr('src')){
			eI.attr('src',s1)
		}
	}
	bodyFocus();
}

function filtSel(id){
	if(id=='selAll'){$('#list input').prop('checked',true)}
	if(id=='selToggle'){
		var chk=$('#list input:checked');
		var unchk=$('#list input:not(:checked)');
		chk.prop('checked',false);
		unchk.prop('checked',true);
	}
	if(id=='selErr'){$('#list input').prop('checked',false);$('#list img[lowsrc$=0]').parent().siblings('input').prop('checked',true)}
	if(id=='selDel'){$('#list :checked').parentsUntil('#list').remove()}
	if(id=='selTrim'){$('#list input:not(:checked)').parentsUntil('#list').remove()}
	if(id=='selTrimErr'){$('#list img[lowsrc$=0]').parentsUntil('#list').remove()}
	filtThen();
}

function sortIt(type){
	if(type=='sort'){$('#sortType').hide(); return}

	var t1 = type;
	var t2 = $('#sortType').show().val();
	var jq = $('#list label');
	if(/[wh]/.test(t1)){
		var jq2 = jq.sort(function(a,b){
			var wha = $(a).find('img').attr('data-wxh').split('×');
			var whb = $(b).find('img').attr('data-wxh').split('×');
			var wa = +wha[0];
			var ha = +wha[1];
			var wb = +whb[0];
			var hb = +whb[1];
			if(t1=='w' && wa>wb || t1=='h' && ha>hb || t1=='wxh' && wa*ha > wb*hb || t1=='w/h' && wa/ha > wb/hb) {return 1}
			if(t1=='w' && wa<wb || t1=='h' && ha<hb || t1=='wxh' && wa*ha < wb*hb || t1=='w/h' && wa/ha < wb/hb) {return -1}
			return 0;
		})
	}
	if(/^[entarl2]$/.test(t1)){
		var jq2 = jq.sort(function(a,b){
			var data = 'data-' + ['ext','name','title','alt','r0','src','link2']['entarll2'.indexOf(t1)];
			var wha = $(a).find('img').attr(data);
			var whb = $(a).find('img').attr(data);
			if(wha>whb) {return 1}
			if(wha<whb) {return -1}
			return 0;
		})
	}

	if(t1=='s'){
		var jq2 = jq.sort(function(a,b){
			var wha = parseInt($(a).find('img').attr('data-size'));
			var whb = parseInt($(b).find('img').attr('data-size'));
			if(wha>whb) {return 1}
			if(wha<whb) {return -1}
			return 0;
		})
	}

	var listPic=[];
	if(t2=='up'){
		jq2.each(function(){
			listPic.push('<label class=btn0>' + $(this).html() + '</label>');
		});

	}else{
		jq2.each(function(){
			listPic.unshift('<label class=btn0>' + $(this).html() + '</label>');
		});
	}

	$('#list').html(listPic.join(''));
//check them
	jq.each(function(){
		jq2 = $(this).find('input');
		$('#' + jq2.attr('id')).prop('checked',jq2.prop('checked'));
	});
	filtThen();
}



function changeView(val){
	var viewOut =+val + 180;
	$('#list div').width(viewOut);//.width(viewOut);
}

function codeIt(resultName){
	var codeArr=[], tArr=ZLR('','src rename wxh sz alt title ext r0 link2');

	$('#code').val('');
	$('#renTip').html('');
	$('#codeIt > *:not(label,br), #ren').hide();
	$('[name=code]').parent().toggleClass('hd2',false);
	var t=$('[name=code]:checked');
	t.parent().toggleClass('hd2',true);
	var Imgs=$('#list :checked ~ div img');

	t.parent().nextUntil('label,br').show();
	if(resultName){$('#resultName').val(t.parent().text()+' '+Time.now())}

	switch(t.val()){
		case 'url':
			Imgs.each(function(){
				codeArr.push($(this).attr('data-src'));
			});
			break;

		case 'html':
			Imgs.each(function(){
				codeArr.push('<img src="' + $(this).attr('data-src') + '"><br />');
			});
			break;

		case 'ubb':
			Imgs.each(function(){
				codeArr.push('[img]' + $(this).attr('data-src') + '[/img]');
			});
			break;

		case 'xml':
		case 'zml':
			Imgs.each(function(i){
				var str='<img id="' +i+'">\n';
				for(var j=0;j<tArr.length;j++){
					var attr=tArr[j].replace('sz','size'), t=$(this).attr('data-'+tArr[j]) || '';
					str +='\t'+XML.wrapE(attr, XML.encode(t)!=t?XML.wrap(t):t)+'\n';
				}
				codeArr.push(str+'</img>\n');
			});
			break;

		case 'exif':
			codeArr.push('Coming soon...');
			break;

		case 'picInf':
			codeArr.push('No.\t'+tArr.join('\t').replace('sz','size'));
			Imgs.each(function(i){
				var str=i;
				for(var j=0;j<tArr.length;j++){
					str +='\t'+($(this).attr('data-'+tArr[j])||'');
				}
				codeArr.push(str);
			});
			break;

		case 'tabInf':
			codeArr.push(JSON.stringify(tab).replace(/"?,"/g,'\n\n').replace(/":"?/g,': ').replace(/^\{"|"?\}$/g,''));
			break;

		case 'pgInf':
			$('#pgInfs').val(0).show();
			var sep = new RegExp(sepStr,'g');
			var getType= L.getTypeImgr;

			if(bgP[getType]){
				var pg = jSon(bgP[getType]).metaList[0];

				var Url = pg.url.replace(/\s+/g,'\n');
				var T = unescape(pg.title).replace(sep,'\n');
				var Kwd = unescape(pg.kwd).replace(sep,'\n');
				var Desc = unescape(pg.desc).replace(sep,'\n');
				codeArr.push('URL: ' + Url +
					'\n\nTitle: ' + T +
					'\n\nKeywords: ' + Kwd +
					'\n\nDescription: ' + Desc +
					'\n\nPages: ' + (+pg.pages) +
					'\n\nCSS link:\n' + uniq(pg.linkCss) +
					'\n\nFonts:\n' + uniq(pg.fonts));
			}
			break;

		case 'renm':
			if(L.os===undefined){
				L.os=(navigator.platform.substr(0,3)=='Win'?'Windows':'Other');
			}
			$('#Os').val(L.os);
			$('#ren').show();
			renameCode(true);
			return;

		default:
			return;
	}

	$('#code').val(codeArr.join('\n'));
	if(t.val()=='zml'){$('#code').val(ZML.encode($('#code').val()))}
}
function pgInfs(t){
	if(t==0){codeIt();return}
	var sep = new RegExp(sepStr,'g'),getType=L.getTypeImgr;

	if(bgP[getType]){
		var pg = jSon(bgP[getType]).metaList[0];
		var Url = pg.url.replace(/\s+/g,'\n');
		var T = unescape(pg.title).replace(sep,'\n');
		var Kwd = unescape(pg.kwd).replace(sep,'\n');
		var Desc = unescape(pg.desc).replace(sep,'\n');
		var tmpArr=[Url,T,Kwd,Desc,(+pg.pages),uniq(pg.linkCss),uniq(pg.fonts)];
		$('#code').val(tmpArr[t-1]);
	}
}

function uniq(t){
	var Arr = unescape(t).split(/\s+/);
	//unique
	for(var i=0;i<Arr.length;i++){
		if(Arr[i]){
			for(var j=i+1;j<Arr.length;j++){
				if(Arr[j]==Arr[i]){
					Arr[j]='';
				}
			}
		}
	}
	return Arr.join(' ').replace(/\s+/g,' ').trim().replace(/\s+/g,'\n');
}


function renameCode(codeIt){
	if(codeIt){
		var codes='';
		$('#code').val('').attr('data-renb','');
		$('#renmOK').hide();
		$('#renTip').html('');
		var renmB=$('#renmBeta').prop('checked');
		if(renmB){
			L.os=$('#Os').val();
			var path=$('#Path').val();
			if(L.os=='Windows'){
				var reg=/.+:\\.*/;
				var renStr='ren';
				var drive=path.split(':')[0] + ':';
				var codeB='@echo off\ncd "' + path + '"\n' + drive + '\n';
			}else{
				var reg=/^[/].*/;
				var renStr='mv';
				var drive='';
				var codeB='cd "' + path + '"\n' + drive + '\n';
			}
			if(!reg.test(path)){
				$('#Path').focus();
				return;
			}
		}
	}
	var Img=$('#list :checked ~ div img'), r1=$('#r1').val();

	var sNo=r1.match(/<(No\.)?\d*>/),SNO='';
	if(!sNo){
		sNo='';
	}else{
		SNO=sNo[0].replace(/[^\d]*/g,'');
		sNo=SNO?parseInt(SNO,10):1;
		var nums=String(Img.length + sNo - 1).length;
	}
	var auto0=/^0\d+/.test(SNO);
	var x=r1.match(/<\[*(jpg|png|gif)\]*>/g);
	if(x){x=x[0].replace(/[<>]/g,'')}
	var d=/\[/.test(x); //is optional

	Img.each(function(i){
		var a=$(this).attr('data-alt');
		var t=$(this).attr('data-title');
		var n=$(this).attr('data-name');
		var r=$(this).attr('data-r0');
		var dot=n.lastIndexOf('.');
		if(dot>0){
			var s0=n.substr(0,dot);
			var s1=n.substr(dot);
		}else{
			var s0=n;
			var s1=$(this).attr('data-ext').replace('other','').replace(/.+[/].+/,'');
			if(s1){s1='.'+s1}
		}

		var No=sNo==''?'':(auto0?('00000' + (sNo + i)).substr(-1*nums):(sNo + i));

		if(d && !s1 || !d && x){
			s1='.'+x.replace(/^\[|\]$/g,'');
		}

		var rename=surname(r1,s0,a,t,r,s1,TITLE,TURL).replace(/<(No\.)?\d*>/g,No);
		$(this).attr({'data-rename':rename}); //,'title':rename
		if(codeIt){
			codes += n + '    ➜    '+ rename + '\n';
			if(renmB){
				codeB += renStr + ' "' + n + '" "'+ rename + '"\n';
			}
		}
	});
	if(codeIt){
		$('#code').val(codes).focus();
		if(renmB && codes){
			$('#code').attr('data-renb',escape(codeB + 'exit\n'));
			$('#renmOK').show();
		}
	}
}




function saveGA(){if(bgP._gaq){bgP._gaq.push(['_trackEvent', H_W(TURL), TURL, TITLE, $('#list img').length])}}

function picNul(){
	var t=$(zlr('#btnSave',' Q',',')+',#sort,#sortType');
	if($('#list :checked').length){
		t.removeAttr('disabled');
	}else{
		t.attr('disabled','');
		$('#edit').hide();
	}
}

function playOff(){
	Min();
	play(3);
	picNul();

	$('body').css({'background':L.BGC,'background-image':'none'});
	//toggleColor();
	if($('#origin').length){
		$('#PLAY').hide('fast',function(){$('#ZIG').fadeIn()});
	}
}

function play(j){
	L.origin=L.origin||'0';

	$('#playo').prevAll().hide();
	$('#playSpeed').nextAll().fadeIn();

	if(j!=3){$('body').css({'background-color':L.playBgColor,'background-image':'none'})}
	$('#ZIG').hide();
	$('#PLAY').fadeIn();

	if($('#LocalShow img').length){
		var d=$('#LocalShow img');
	}else if($('#list :checked').length){
		var d=$('#list :checked ~ div img').not('[lowsrc]');
	}else{
		L.origin=0;
		if(j!=3){$('#upImgsShow').click()}
		return;
	}
	var n=d.length;
	var o=+L.origin;
	var i=o+j+n;
	var s=+L.playSpeed;
	if(j==0){
		i++;
		L.origin=i%n;
		if(L.originTime){
			clearTimeout(+L.originTime);
		}
		L.originTime=setTimeout(function(){play(0)},s*2);

		if($('#play3').val()=='►'){$('#play3').val('▐ ▌')}

		slideshow(L.playType,s,i%n);
	}else{
		if(L.originTime){
			clearTimeout(+L.originTime);
			L.originTime='';
		}
		if(j!=3){

			if(j==2){
				i=n-1;
			}else if(j==-2){
				i=0;
			}
			slideshow(L.playType,s,i%n);
			L.origin=i%n;
		}else{
			i=o+n;
		}

		if($('#play3').val()=='▐ ▌'){$('#play3').val('►')}

	}
	$('#play1').val(n-i%n-1);
	$('#play0').val(i%n+1);
}
function Play(){
	Max();
	play(0);
}
function playo(){
	if($('#playBgColor:visible').length){
		$('#playo').prevAll().hide();
		Play();
	}else{
		play(3);
		$('#playo').prevAll().fadeIn();
	}
}

function slideshow(type,s,i){
	var b=document.body, bg_size='auto', bg_rept=(/X$/.test(type)?'':'no-') +'repeat '+ (/Y$/.test(type)?'':'no-') + 'repeat';
	if(type=='Tile'){bg_rept='repeat'}
	if(type=='Stretch'){
		var W=b.scrollWidth, H=b.scrollHeight;
		bg_size=W+'px '+H+'px';
	}
	if($('#LocalShow img').length){
		var src=$('#LocalShow img').eq(i).attr('data-src');
	}else{
		var src=$('#list :checked ~ div img').not('[lowsrc]').eq(i).attr('src');
	}
	var bg_url='url('+src+')';
	var bg_color=L.playBgColor;
	$('body').delay(s).removeAttr('style').css({'background-color':bg_color,'background-image':bg_url,'background-size':bg_size,'background-repeat':bg_rept});
}



function changeDisp(){
	$('#list .inf2').empty();
	var t=$('#disp').val();
	if(t){L.geek3=t.join(' ')+' '}else{L.geek3=''}
	$('#list img').each(function(){
		 disp(this);
	});
}
function disp(img){
	var t='',arr1=ZLR('no wh s e n t a l l2 renm'),arr2=ZLR('no wxh sz ext name title alt src link2 r0');
	for(i in arr1){
		t += geek3(arr1[i],$(img).attr(i==7?'src':'data-'+arr2[i]));
	}
	var inf2=$(img).parent().prevAll('.inf2');
	inf2.html(t).children().first().filter('.bar').remove();
	imgPreReData.lastIndex=0;
	if(imgPreReData.test($(img).attr('data-src'))){inf2.children('.s').prev('.bar').andSelf().remove()}
}
function geek3(d,t){
	var bar=SC+'bar> '+(L.geek3.indexOf('bar')>-1?'- ':'')+sC;
	if(d=='wh'){
		if(L.geek3.indexOf('h')<0){
			if(L.geek3.indexOf('w')<0){
				return '';
			}else{
				return bar+'w>'+t.split('×')[0]+sc;
			}
		}else{
			if(L.geek3.indexOf('w')<0){
				return bar+'h>'+t.split('×')[1]+sc;
			}else{
				return bar+'w>'+t.replace('×',sc+'×'+SC+'h>')+sc;
			}
		}
	}
	if(L.geek3.indexOf(d+' ')>-1 && d=='s'){return bar+'s>'+t+sc}
	if(L.geek3.indexOf(d+' ')>-1 && t){return bar+d+'>'+t+sc}
	return '';
}



function tileDesc(img){
	var bar=(L.tileDesc.indexOf('bar')>-1?' - ':' '),t='',d=L.tileDesc.replace('w h ','wh '),arr1=ZLR('no wh s e n t a l l2 renm'),arr2=ZLR('no wxh sz ext name title alt src link2 r0');
	for(i in arr1){
		var v='';
		if(d.indexOf(arr1[i]+' ')>-1){v=(i==7 && /^data/.test($(img).attr('data-src')))?'':$(img).attr('data-'+arr2[i])}
		if(i==1 && /[wh] /.test(L.tileDesc) && d.indexOf('wh ')<0){v=$(img).attr('data-wxh').split('×')[/w /.test(L.tileDesc)?0:1]}
		if(v){t += (t==''?'':bar) + v}
	}
	return t;
}

function Min(){
	try{
		if(L.winState=='fullscreen'){L.winState='normal'}
		chrome.windows.update(chrome.windows.WINDOW_ID_CURRENT,{state:L.winState});
	} catch(e) {
		 console.log(e);
	}
}
function Max(){
	try{
		chrome.windows.getCurrent(function(win){L.winState=win.state});
		chrome.windows.update(chrome.windows.WINDOW_ID_CURRENT,{state:'fullscreen'});
	} catch(e) {
		 console.log(e);
	}
}

function Tile(){
	if(L.btnTile=='0'){
		L.btnTile=1;
		loadTile(true);
	}else if(L.btnTile=='1'){
		loadTiles();
	}
	if(L.btnTile=='2'){
		L.btnTile=1;
		loadTile();
	}
	$('#ZIG').fadeOut('fast',function(){$('#Tile').show()});
}

function loadTile(bool){
	$('#tileFontCenter').attr('title',gM('center'));

	$('#font').attr({placeholder:gM('font'),title:gM('font')});
	$('#tileFontOn').prop('checked',L.tileFontOn=='true');
	$('.tileFontPos').html(gM('tileFontOn')).eq(+L.tileFontPos).addClass('toggle');
	$('#Sign').toggle(!(L.tileFontOn=='true' && L.tileShape=='3'));

	$(zlr3('#','TileWH row col col2','>:number',',')).add('#fontSize,#font')
	.on('change keyup mouseup',function(e){
		L[this.id]=this.value;
		if(/font/.test(this.id)){changeTilesCSS()}else{
			loadTiles()
		}
		e.stopPropagation();
	}).add('#capTypeTile,#jpgQTile')
	.val(function(){return L[this.id]});

	$('#urlSign').val(function(){return L[this.id]});

//color
	$('#Color input').val(function() {
		return L[this.id.replace('G','T')] || L[this.id];
	});


	$('#tileLocal').after(gM('tileLocal'));

	var shp=+L.tileShape;
	$('#tile'+shp).addClass('toggle');
	$('#tile_0').attr('src','img/tile_'+shp+'.gif');
	if(shp>3){
		$('#row').show();
		$('#tile_1').attr('src','img/tile_'+shp+'_1.gif');
		if(shp>4){
			$('#col2').show();
		}
	}
	if(shp==3){$('#desc').show()}


	$('#tileSort'+L.tileSort).addClass('toggle');

	$('#capTypeTile,#jpgQTile').attr('title',function(){return gM(this.id.replace('Tile',''))});
	$('#jpgQTile').toggle(L.capTypeTile=='jpeg');

	//tiles
	$('#tilesTb').on('keyup mouseup',function(){tileFontOnThen()});

	toggleColorTile();
	if(FREE){$('#tiles').addClass('abt')}
	if(bool){
		loadTiles();
		
	}
}



function sortNum(a,b){return a - b}
function rand(n,k){
	var tArr=new Array(n), rArr=[];
	for(var i=0;i<n;i++){tArr[i]=i}
	if(k>n/2){
		for(var i=0;i<n-k;i++){
			tArr.splice(parseInt(Math.random()*tArr.length),1);
		}
		return tArr.sort(sortNum);
	}else{
		for(var i=0;i<k;i++){
			var j=parseInt(Math.random()*tArr.length);
			rArr.push(tArr[j]);
			tArr.splice(j,1);
		}
		return rArr.sort(sortNum);
	}
}
function arrMaxTrim(arr){
	var m=Math.max.apply(null,arr), mArr=[];
	for(var i=0;i<arr.length;i++){if(arr[i]==m){mArr.push(i)}}
	arr[mArr[rand(mArr.length,1)[0]]] -= 1;
}

function tileH(ht){
	L.tileH=ht;
	$('#tileH').text(ht);
	$('#tiles').height(ht);
}

function changeTilesCSS(){
	var pos=['left','center','right'][$('#tileFontCenter').val()];
	$('.tileFontPos').parent().css('text-align',pos);
	$('#tilesTb').css({'font-family':$('#font').val(),'font-size':+$('#fontSize').val(),'text-align':pos});
	$('#tiles table').show();
	tileH($('#tilesTb').height());
	var tilesBg0=bgfrom +L.BTC1+ '),color-stop(.5,'+L.BTC+'), to(' +L.BTC2+ '))',v0=tilesBg0 + '0 0/cover no-repeat';
	$('#tiles').css('background',v0);
}
function loadTiles(){
	var W=+L.tileW, bW=+L.tilebW, shp=+L.tileShape, Sort=+L.tileSort;
	$('#Tiles,#tiles').width(W);
	$('#tiles table').hide();

	var tilesBg0=bgfrom +L.BTC1+ '),color-stop(.5,'+L.BTC+'), to(' +L.BTC2+ '))';
	if($('#tileLocal').prop('checked')){var imgS=$('#LocalTile img[data-wxh]')}else{var imgS=$('#list :checked ~ div img').not('[lowsrc]')}

	if(Sort==1){
		imgS=imgS.sort(function(a,b){
			var na =+$(a).attr('data-no');
			var nb =+$(b).attr('data-no');
			if(na<nb) {return 1}
			if(na>nb) {return -1}
			return 0;
		});
	}
	if(Sort==2){imgS=imgS.sort(function(a,b){return Math.random()>.5 ? -1:1})}
	var v='', v0=tilesBg0 + '0 0/cover no-repeat', n=imgS.length;
// <bg-image> || <position> [ / <bg-size> ]? || <repeat-style> || <attachment> || <box>{1,2}

	var col=+$('#tileCol').val(), row=+$('#tileRow').val(), col2=+$('#tileCol2').val();
	var l1=+L.tileCols2Min, l0=+L.tileCols2;
	var m1=+L.tileRowsMin, m0=+L.tileRows;
	var n1=+L.tileColsMin, n0=+L.tileCols;

	if(shp==3){
		if(tileFontOn()){
			var W_=W-bW*(n0+1), w=parseInt(W_/n0);
			$('#tilesTb').empty();

			for(var i=0;i<n0;i++){$('#tilesTb').append('<td width='+(i==n0-1?W_-w*(n0-1):w)+'></td>')}

			$('#tilesTb td').css({'padding-top':bW,'padding-right':bW});
			$('#tilesTb td:eq(0)').css('padding-left',bW);

			imgS.each(function(i){
				var div0='<div><img src="'+$(this).attr('data-src')+'" /></div>', div1='<div>'+tileDesc(this)+'</div>', div=div0+div1;
				if(L.tileFontPos=='0'){div=div1+div0}
				$('#tilesTb td:eq('+(i%n0)+')').append(div);
			});

			//$('#tilesTb div:not(:empty)').css('padding-bottom',bW);

			changeTilesCSS();
		}else{
			var W_=W-bW*(n0+1),yArr=new Array(n0);
			for(var i=0;i<n0;i++){yArr[i]=bW}
			var w=parseInt(W_/n0),h;

			imgS.each(function(i){
				var wxh=$(this).attr('data-wxh');
				var wh=wxh.split('×');
				if(/^0|×0$/.test(wxh)){wh=[1,1]}
				h=parseInt(w*(+wh[1])/+wh[0]);

				var x=i%n0,y=Math.floor(i/n0);
				v +=',url('+$(this).attr('data-src')+') '+ (bW+(w+bW)*x)+'px '+ yArr[x] +'px/'+ (x==n0-1?W_-w*x:w) +'px '+ h +'px no-repeat';
				yArr[x] += h+bW;
			});

			tileH(Math.max.apply('',yArr));
		}

	}else if(shp==4){

		var xi=bW, yi=bW, R,R0, W_, h;

		var xyArr=[],j,k;
		for(var i=0;i<n+1;i++){

			if(col==0){k=n0}
			if(col==1){k=(k==n0?Math.max(n0-1,1):n0)}
			if(col==2){k=parseInt(n1+Math.random()*(parseInt((n-i)/m1)-n1+1))}
			k=Math.min(k,n-i);
			if(k<1){break}
			var tArr=new Array(k), ii=0;

			for(var m=0;m<k;m++){
				if(row==0){j=m0}
				if(row==1){j=(j==m0?Math.max(m0-1,1):m0)}
				if(row==2){j=parseInt(m1+Math.random()*(m0-m1+1))}

				tArr[m]=j;
				ii += j;
			}
			if(i+ii>n){
				for(var m=0;m<i+ii-n;m++){arrMaxTrim(tArr)}

			}
			i += ii-1;
			xyArr.push(tArr);
		}

		var i=0, i0=0;
		for(var m=0;m<xyArr.length;m++){

			var y0=yi, k=xyArr[m].length, RArr=new Array(k), wArr=new Array(k);
			W_=W-bW*(k+1);
			R=0;
			R0=0;
			for(var j=0;j<k;j++){
				RArr[j]=0;
				for(var x=0;x<xyArr[m][j];x++){

					var wh=imgS.eq(i).attr('data-wxh').split('×');
					if(wh[0]=='0' || wh[1]=='0'){wh=[1,1]}
					RArr[j] +=+wh[1]/+wh[0];
					i++;
				}
				R += 1/RArr[j];
				R0 += (xyArr[m][0]-xyArr[m][j])/RArr[j];
			}

			h=parseInt((W_-bW*R0)/R+(xyArr[m][0]-1)*bW);

			for(var j=0;j<k;j++){

				wArr[j]=parseInt(((W_-bW*R0)/R+(xyArr[m][0]-xyArr[m][j])*bW)/RArr[j]);

				for(var x=0;x<xyArr[m][j];x++){

					var img=imgS.eq(i0+x), wh=img.attr('data-wxh').split('×');
					if(wh[0]=='0' || wh[1]=='0'){wh=[1,1]}
					var hi=parseInt(+wh[1]*((W_-bW*R0)/R+(xyArr[m][0]-xyArr[m][j])*bW)/RArr[j]/+wh[0]);
					v +=',url('+img.attr('data-src')+') '+ xi +'px '+ yi + 'px/'+
						(j==k-1?W-xi-bW:wArr[j]) +'px '+ (x==xyArr[m][j]-1?y0+h-yi:hi) +'px no-repeat';

					yi += hi+bW;
					if(x==xyArr[m][j]-1){
						xi += wArr[j]+bW;
						yi = y0;
						i0 += x+1;
					}
				}
			}

			xi = bW;
			yi += h+bW;
		}

		tileH(yi);

	}else if(shp==5){

		var xi=bW, yi=bW, R,R0, W_, h;

		var xyArr=[],j,k,kk,i=0;
		for(var ii=0;ii<n;ii++){

			if(col==0){k=n0}
			if(col==1){k=(k==n0?Math.max(n0-1,1):n0)}
			if(col==2){k=parseInt(n1+Math.random()*(parseInt((n-i)/m1)-n1+1))}
			k=Math.min(k,n-i);
			if(k<1){break}
			var tArr=new Array(k);

			for(var m=0;m<k;m++){
				if(row==0){j=m0}
				if(row==1){j=(j==m0?Math.max(m0-1,1):m0)}
				if(row==2){j=parseInt(m1+Math.random()*(m0-m1+1))}
				j=Math.min(j,n-i);
				tArr[m]=[];

				for(var l=0;l<j;l++){
					if(col2==0){kk=l0}
					if(col2==1){kk=(kk==l0?Math.max(l0-1,1):l0)}
					if(col2==2){kk=parseInt(l1+Math.random()*(l0-l1+1))}
					tArr[m].push(kk);
					i += kk;
					if(i>=n){
						for(var jj=0;jj<i-n;jj++){arrMaxTrim(tArr[m])}
						tArr.splice(m+1);
						ii=n+1;
						break;
					}
				}
				if(ii>n){break}
			}

			xyArr.push(tArr);
		}

		var i=0, i0=0;
		for(var m=0;m<xyArr.length;m++){

			var y0=yi, k=xyArr[m].length, RArr=new Array(k), wArr=new Array(k), R2Arr=new Array(k);
			W_=W-bW;
			R=0;
			R0=0;
			for(var j=0;j<k;j++){
				RArr[j]=0;
				R2Arr[j]=[];
				var rArr=new Array(xyArr[m][j].length);
				for(var x=0;x<rArr.length;x++){	//每列x行
					rArr[x]=0;
					for(var y=0;y<xyArr[m][j][x];y++){	//每行y列

						var wh=imgS.eq(i).attr('data-wxh').split('×');
						if(wh[0]=='0' || wh[1]=='0'){wh=[1,1]}
						rArr[x] +=+wh[0]/+wh[1];
						i++;
					}
					R2Arr[j].push(rArr[x]);
					RArr[j] += 1/rArr[x];
				}
				R += 1/RArr[j];
			}

			h=parseInt(W_/R);

			for(var j=0;j<k;j++){	//每列

				wArr[j]=parseInt(W_/R/RArr[j]);

				for(var x=0;x<xyArr[m][j].length;x++){	//每列的行
					var x0=xi;

					for(var y=0;y<xyArr[m][j][x];y++){	//每行的每列

						var img=imgS.eq(i0+y), wh=img.attr('data-wxh').split('×');
						if(wh[0]=='0' || wh[1]=='0'){wh=[1,1]}
						var hi=parseInt(W_/R/RArr[j]/R2Arr[j][x])-bW;
						var wi=parseInt(W_/R/RArr[j]*+wh[0]/+wh[1]/R2Arr[j][x])-bW;
						if(y==xyArr[m][j][x]-1){ //每小行最后一列
							wi=x0+wArr[j]-xi-bW;
							if(j==k-1){wi=W-xi-bW}
						}
						if(x==xyArr[m][j].length-1){ //每列最后一行
							hi=y0+h-yi-bW;
						}

						v +=',url('+img.attr('data-src')+') '+ xi +'px '+ yi + 'px/'+ wi +'px '+ hi +'px no-repeat';

						if(y==xyArr[m][j][x]-1){		//每小行最后一列
							xi = x0;
							yi += hi+bW;
							i0 += y+1;

							if(x==xyArr[m][j].length-1){ //每小行最后一列，且每列最后一行
								xi = x0 + wArr[j];
								yi = y0;
							}
						}else{
							xi += wi+bW;
						}
					}
				}
			}
			xi = bW;
			yi += h;
		}
		tileH(yi);

	}else{

		var xi=bW, yi=bW, R, W_, w, h, k0=0, k1=(col==2?parseInt(n1+Math.random()*(n0-n1+1)):n0);
		k1=Math.min(k1,n);

		if(shp==2){
			var WT=0,HT=0;
			imgS.each(function(i){
				var wh=$(this).attr('data-wxh').split('×');
				WT += parseInt(wh[0]);
				HT += parseInt(wh[1]);
			});
			var r=WT/HT;
		}
		imgS.each(function(i){
			var x=i-k0;

			if(x==0){
				W_=W-bW*(k1+1);
				if(shp==0){
					R=0;
					for(var j=0;j<k1;j++){
						var wh=imgS.eq(k0+j).attr('data-wxh').split('×');
						if(wh[0]=='0' || wh[1]=='0'){wh=[1,1]}
						R +=+wh[0]/+wh[1];
					}
					h=parseInt(W_/R);
				}else{
					w=parseInt(W_/k1);
				}
				if(shp==1){h=w}
				if(shp==2){h=parseInt(W_/k1/r)}
			}

			if(shp==0){
				var wh=$(this).attr('data-wxh').split('×');
				if(wh[0]=='0' || wh[1]=='0'){wh=[1,1]}
				w=parseInt(+wh[0]*W_/(R*+wh[1]));
			}

			v +=',url('+$(this).attr('data-src')+') '+ xi +'px '+ yi + 'px/'+
				(x==k1-1 && (shp==0 || shp>0)?W-xi-bW:w) +'px '+ h +'px no-repeat';

			xi += w+bW;
			if(x==k1-1){
				xi = bW;
				yi += h+bW;
				k0 += k1;

				if(col==0){k1=n0}
				if(col==1){k1=(k1==n0?Math.max(n0-1,1):n0)}
				if(col==2){k1=parseInt(n1+Math.random()*(n0-n1+1))}
				k1=Math.min(k1,n-k0);
			}
		});

		tileH(yi);
	}


	var src='',bg=$('#urlSign').attr('data-bg');
	imgPreRe.lastIndex=0;
	if(imgPreRe.test($('#urlSign').val())){
		src=$('#urlSign').val();
	}else if(bg){
		src=bg;
	}
	if(src){v ='url('+src+') '+$('#signPos').val()+' no-repeat'+v}

	v=v.replace(/^,/,'');

	$('#tiles').css('background',v+','+v0);
}
function tileFontOn(){return $('#tileFontOn').prop('checked') && $('#tile3').is('.toggle')}
function tileFontOnThen(){if(tileFontOn()){changeTilesCSS()}else{

	loadTiles()
	}
}
cdl.onChanged.addListener(function(d){
	var tr=$('#iH').nextAll('[data-did="'+d.id+'"]'),t,goon=false,T;
	if(tr.length){
		if(d.state){
			t=d.state.current;
			if(t=='in_progress'){
				
			}else{
				goon=true;
			}
			if(t=='complete'){
				$('#dldProg').val(function(i,v){return +v+1});
			}
		}
		if(d.error){
			goon=true;
		}


		if(goon){
			cdl.search({id:d.id},function(dl){
				var d0=dl[0];
				if(d0){
					var U=d0.url,u=H_d(U),btu=bgP.tasks[u];
					if(btu){
						var ting=btu.ing;
						if(ting[0].url==U){
							bgP.tasks[u].ing.shift();
							if(ting.length){
								Task.download(bgP.tasks[u].ing[0]);
							}else{
								if(!bgP.tasks[u].pause.length){
									delete bgP.tasks[u]
								}
							}
						}
					}
				}else{
					for(var j in bgP.tasks){
						var bt=bgP.tasks[j];
						if(bt.ing[0] && bt.ing[0].id==d.id){
							bt.ing.shift();
							if(bt.ing[0]){
								Task.download(bt.ing[0]);
							}
							break;
						}
					}
				}
			});
		}

		cdl.search({id:d.id},function(dl){
			if(dl[0]){
				didRefresh(dl[0])
			}
		});
	}
});
cdl.onErased.addListener(function(did){
	var tr=$('#iH').nextAll('[data-did="'+did+'"]'),t;
	if(tr.length){
		tr.remove();
	}
});
cdl.onCreated.addListener(function(d){
	
});
function iDom(img,obj,HVrotate){
	var i=img?$(img):null,sz=i?+i.attr('data-size'):0, noteA=[(i?i.attr('title'):'')||''],n0=pathTxt((obj.path||'')+'/'+(obj.rename||'')),n1=obj.filename,
		domA=[SC+'pending title="'+gM('pending')+'">'+imgSRC+'loading.gif" />'+sc,
		'<img title="'+(i?i.attr('data-ext')+'('+
		(HVrotate?i.attr('data-wxh').split('×').reverse().join('×'):i.attr('data-wxh'))+')':'')+'"/>'+
		SC+'"filename underline" title="'+gM('open')+' '+(n1?n1:n0)+'">'+(n1?n1:(n0?n0:obj.url)).replace(/.+[\/\\]/,'')+
		sC+'"folder btn2">'+imgSRC+'folder.png" title="'+gM('path')+'" />',
		'<meter max='+sz+' low='+sz+' optimum='+sz+' value=0></meter>',
		(i?i.attr('data-sz'):''),
	//	'<input type=text value="'+noteA.join(' ').trim()+'" title="'+noteA.join('\n').replace(/\n+/g,'\n').trim()+'" />',
		SC+'time title="'+(new Date())+'">'+Time.lite(new Date())+sC+'time>'+sc,'',
		'<a target="_blank" class=aurl href="'+obj.url+'" title="'+obj.url+'">URL</a><a target="_blank" class=areferrer /></a>',
		''],
	domS='<tr'+(obj.did?' data-did='+obj.did:'')+(obj.tid?' data-tid='+obj.tid:'')+'>'+
		Arrf(function(t){return '<td>'+t+'</td>'},domA,'str')+'</tr>';
	
	return domS
}
function didRefresh(d,cb){
	var tr=$('#iH').nextAll('[data-did='+d.id+']'),
		aUrl=d.filename||d.url,dUrl=(d.filename?d.filename:d.url).replace(/.+[\/\\]/,'');
	if(!tr.length){
		$('#iH').after(iDom(null,{url:d.url,filename:d.filename,did:d.id}));
		tr=$('#iH').next()
	}
	var m=tr.find('meter'),tds=tr.find('td'),T0,T1,t0,t1,lt0,lt1,t,ds=d.state,icon=tr.find('td').eq(1).find('img').eq(0);

	if(ds=='complete'){
		m.val(m.attr('max'));
		m.filter('[max=0]').attr({max:1,value:1});

	}else{
		t=d.totalBytes;
		if(t>0){
			m.attr({max:t,low:t,optimum:t})
		}
		m.val(d.bytesReceived);
	}

//console.log(d);
	if(d.filename){
		cdl.getFileIcon(d.id, {size:16}, function(u){
			icon.attr('src',u);
		});
	}

	icon.attr('title',d.mime);
	if(!icon.attr('src') && d.mime){
		var s=$('#items').find('img[title="'+d.mime+'"][src]').eq(0);
		if(s.length){
			icon.attr('src',s.attr('src'))
		}else{
			icon.attr('src','img/16.gif')
		}
	}
	
	t=d.totalBytes;
	if(t<1){t=d.fileSize}
	if(t<1){t=d.bytesReceived}
	if(t<1024){
		t+='B'
	}else{
		t=+(t/1024).toFixed(0);
		if(t>=1024){t=(t/1024).toFixed(1)+'MB'}else{t=(t).toFixed(1)+'KB'}
		t=t.replace('.0','');
	}
	tds.eq(3).text(t);

	if(ds=='complete'){
		var img=$('#list img[data-src="'+d.url+'"]'),lb=img.parents('label');
		img.attr('data-sz',t);
		lb.find('.s').text(t);
		lb.find('.size').text(' - '+t);
	}

	var s=tr.find('td').eq(0).find('span'),src=['check','uncheck','imgdl'];

	if(ds=='complete'){
		s.attr({'class':'complete','title':gM('complete')});
		s.find('img').attr('src','img/check.png')
	}
	if(d.paused && d.canResume){
		s.attr({'class':'paused','title':gM('paused')});
		s.find('img').attr('src','img/pause.png')
	}
	if(ds=='interrupted' && !d.canResume || !d.exists){
		s.attr({'class':'error','title':gM('error')});
		s.find('img').attr('src','img/uncheck.png');

		if(!d.exists){
			m.replaceWith(SC+'red>'+gM('deleted')+sc);
			tr.find('.folder').hide();
			tr.find('.filename').removeClass('underline');
		}else{
			m.replaceWith(SC+'red>'+gM('interrupted')+sc);
		}
	}

	if(ds=='in_progress'){
		s.attr({'class':'in_progress','title':gM('in_progress')});
		s.find('img').attr('src','img/imgdl.png')
	}

	if(d.error){
		tds.last().html(SC+'red>'+gM(d.error)+sc);
	}



	T0=d.startTime;
	t0=new Date(T0);
	lt0=Time.lite(t0);
	tr.find('.time').eq(0).attr('title',T0).text(lt0);

	T1=d.endTime || d.estimatedEndTime;
	if(!T1){
		T1='?';
		lt1='?';
	}else{
		t1=new Date(T1);
		lt1=Time.lite(t1);
	}
	tr.find('.time').eq(1).attr('title',T1).text((lt1==lt0||lt1=='?')?'':'/'+lt1+(d.endTime?'':'('+gM('estimated')+')'));
	t=(T1=='?'?(new Date()):(new Date(T1)))-(new Date(T0));
	t=t/1000;
	if(t>=60){
		t=Math.round(t/60)+'min'
	}else{
		t+='s'
	}
	tds.eq(5).html(t);

	t=d.referrer;
	if(t && t!=d.url){
		tr.find('a.areferrer').attr({'href':t,'title':t}).text('referrer');
	}

	tr.find('.filename').attr('title', (d.exists?gM('open')+' ':'')+aUrl).html(dUrl);



	//td=tds.last();
	//td.html(d.byExtensionName).attr('title',d.byExtensionId);
	//td=td.prev();
	td=tds.last();
	if(/safe|accepted/.test(d.DangerType)){
		td.append('OK');
	}else if(/url|content|host/.test(d.DangerType)){
		td.append(SC+'red>'+d.DangerType+sc)
	}else{
		//td.append('?');
	}

	//tr.find('a.aurl').attr({href:d.url,title:d.url});
	if(cb){cb()}


	if(!$('#iH').nextAll().has('.pending,.paused,.in_progress').length){
		$('#dldProg').val($('#dldProg').attr('max'));
	}
	return tr
};