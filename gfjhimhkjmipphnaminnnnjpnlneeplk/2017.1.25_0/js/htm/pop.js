﻿/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */
var t3=L.bing||'', t0='img/ZIG.png';
getMeta();
L.tabId='';

$(function(){
	loadwdgts();

//html
	$('#resultCopy2').html(gM('copy')+'+');

	$('#geekMini').text(L.geekMini=='false'?'▲':'▼').on('click',function(){
		var me=$(this);
		me.html(function(i,v){return v=='▼'?'▲':'▼'});
		L.geekMini=me.text()=='▼';
		$('#nav2').toggle(L.geekMini=='false');
		setTimeout(function(){
			var s=$('#nav2 svg:eq(' + L.nav + ')');
			s.css('width',s.children().last().width()+35);
		},0);
	});

//hide
	$('#jpgQ').toggle(L.capType=='jpeg').on('change keyup mouseup',function(){L.jpgQ=this.value});
	$('#nav2').toggle(L.geekMini=='false');

//attr

	$('#nav2 svg').attr('title',function(){return gM(this.id.substr(2,4))})
		.on('mouseover',function(){nav2img(this)}).on('click', function(){
			api('of'+this.id.substr(2,4));
	}).width(30);
	$('#geekMini, button').attr('title',function(i,v){return gM('hotkey')+': '+v});
	$('#Addonekey').attr('title',function(i,v){return gM(this.id)});

//val
	$('#resultName').val(Time.now());
	$('#quicksav').val(function(){return gM(this.id)});

	$('#qrRun').val(gM('qrcd')).on('click',function(){
		var u=$('#qrURL').val();
		if(u){
			var f=function(){
				$('#qrImg').show();
				$('#qrImgUrl > img').remove();
				var qu=fnq(u,0,parseInt(Math.random()*4+1));
				$('#qrImgUrl').attr('href',qu).append('<img hidden />');
				$('#qrImgUrl > img').on('error',function(){	
					f();
				}).on('load',function(){$(this).show();$('#qrImg').hide()}).attr('src',qu);
			};
			f();
		}
	});


//click
	$('#GeekNav2').on('click',function(){L.nav=$(this).prop('checked')?$('#tab > div:visible').index():0});
	$('#shares img').on('click',function(){
		window.open(fns($(this).attr('data-share'),$('#qrURL').val(),$('#qrTtl').val(),''));
	})

//change

	$('body').on('keydown',function(e){
		var k=e.keyCode,act=document.activeElement,n=$('#tab > div:visible').index();
		if(k==13){
			if(act.id=='search'){
				api($('#search').val().trim(),'eng')
			}
			if(act.id=='qrURL'){
				$('#qrRun').click()
			}
		}
		if(e.ctrlKey){
			var t=$('#nav2 svg:eq(' + n + ')').attr('id').substr(2);
			if(k==13){$('#' + t).find(':button:visible').eq(0).trigger('click')}
			if(k==83){$(act).parent().find('[id$=SaveAs]:visible').eq(0).trigger('click')}
			if(k==69){act.value=''; return false}

		}else if(!/(textarea)|(input)|(h1)/i.test(act.tagName)){
			if(/^3[79]$/.test(k)){
				nav2img($('#nav2 svg:eq(' + (n+(k==37?-1:1)) % $('#nav2 svg').length + ')')[0])
			}
			if(k==38 || k==40){
				$('#geekMini').click();
			}

			if($('#iffunc').length && k>=112 && k<=135){
				$('#iffunc').mouseover();
				$('#funcs input').eq(k-112).trigger('click');
			}
		}
	}).on('mouseover','#engine img',function(){
		$('#engine .hoverTip').remove();
		var me=$(this);
		me.after(SC+'hoverTip>'+me.attr('title')+sc)
	}).on('mouseover','.memo',function(){
		$(this).children().last().removeClass('btn2')
	}).on('mouseout','.memo',function(){
		$(this).children().last().addClass('btn2')
	}).on('click','.hoverTip',function(){
		var me=$(this);
		me.prev().click();
	}).on('click','.Status',function(){
		var me=$(this),m=me.next(),dt=m.next().find('input');
		me.toggleClass('done');
		if(me.is('.done')){
			m.removeAttr('contenteditable');
			dt.attr('disabled','disabled')
		}else{
			m.attr('contenteditable',true);
			dt.removeAttr('disabled')
		}
	}).on('click','.onekey',function(){
		var me=$(this),dv=me.attr('data-v'),vs=dv.split(';'),reg=/clk_popout|cng_popout|clk_weib|cng_weib/,clk=/clk_popout|clk_weib/,cng=/cng_popout|cng_weib/, edt=$('#Edit').prop('checked');
		if(edt){
			return;
		}
		var f=function(){
			var v=vs[0];
			if(!v && vs.length<2){clearInterval(sid); return}
			vs.shift();
			
			var vi=v.split(reg),v0=vi[0],v1=vi[1];
			//console.log(v0);
			var d=$(v0.replace(/"/g,"'"));
//console.log(dv,v,vi);
			if(/:contains/.test(v0)){
				d=d.filter(function(){return $(this).text()==v0.split(':contains(')[1].split(')')[0]}).eq(0);
			}
			if(clk.test(v)){
				if(v1){
					d.prop('checked',v1=='false');
				}
				d.click();
			}
			if(cng.test(v)){
				d.val(v1);
				d.change();
			}
		};
		var sid=setInterval(f,300);
	}).on('change','.onekey span',saveonekeys)
	.on('sortupdate','.onekey',function(){
		saveonekeys();
		//console.log('排序 后：'+L.onekeys);

	}).on('click','#home .del',function(){
		$(this).parent().remove();
		saveonekeys();
		//console.log('del 后：'+L.onekeys);
	});

	$('#alipay .email').on('mouseover',function(){
		this.select()
	});



	setTimeout(function(){
		advClick('fetchCode');
		loadkwd();

		if(L.getSelected){
			var j=jSon(L.getSelected).tabThis[0], u=j.url, t=unescape(j.title), qu=fnq(u,0,parseInt(Math.random()*4)), d=H_d(u), r0=jSon(L.rule).slice(1).concat(jSon(INI.rule0)), dA=[],
			 t0=t.toLowerCase(), tA=d.split('.'), jrA=[];

			$('#qrIco').attr('src',j.favIconUrl || imgFav(u)).on('click',function(){
				$('#qrURL').val(u);
				$('#qrTtl').val(t);
			}).click();

			if(L.nav=='1'){$('#qrRun').click()}
			$('#funcs input[data-auto="yes"]').each(function(){
				var re=new RegExp(unescape($(this).attr('data-regexp')));
				if(re.test(u)){$(this).trigger('click')}
			});
		}
	},300)


	nav2img('#nav2 svg:eq('+L.nav+')');

	//toggleColor();
	$('#zbody').show();
});



function nav2img(obj){
	var me=$(obj),t=me.attr('id').substr(2),i1=me.parent().index(),i0=+L.nav,tt=me.attr('title');
	if($('#' + t+':visible').length){
		
		return
	}
	
	if(i1==2){$('#qrRun').click()}
	$('#AddOnekey').toggle(/[134]/.test(i1));
	$('#EditOnekey').toggle(i1==0);
	setTimeout(function(){
		me.attr('class','on').children().last().text(tt);
		me.css('width',me.children().last().width()+35);
		me.parent().siblings().children().removeAttr('class').width(30).children().last().text('');
	},0);

	$('#tab > div').not('#' + t).hide();
	$('#' + t).show();


	$('#GeekNav2').prop('checked',i0==i1).parent().toggle(i0!=i1);
	var desc=gM('Zdesc')+'\nZIG - '+gM(t.substr(0,4));
	if(t=='imgr'){desc +=['\n'+gM('getImage_Q')+
		' Ctrl+Q',gM('savImage_Q')+
		' Alt+Q',gM('capScr_Q')+
		' Shift+Q',gM('capScrW_Q')+
		' Ctrl+Shift+U',gM('pinImage_Q')+
		' Ctrl+Alt+Q'].join('\n') }
	chrome.browserAction.setTitle({title:desc});
}

function codeMode(){
	var p=$('#codeType').val();
	$('#advCode').toggle(/^getCodes1*$/.test(p));
	$('#urlToolCode').toggle(p=='getCodes')
	$('#timeCode').toggle(p=='getCodes1');
}

function loadsech(x){
	ini.sech();
	L.sechId=x;
	var sN = parseInt(L.sechNum);
	var tmpArr = jSon(L.sech);
	var tmp=[];
	for(var i=0; i<tmpArr.length;i++){
		tmp.push('<option value=' + i + (unescape(tmpArr[i].on)=='yes'?' >':' disabled hidden >') + unescape(tmpArr[i].name) + '</option>');
	}
	$('#searchIt').html(tmp.join(''));
	var sop=$('#searchIt option');
	if(sop){
		var y=sop.eq(0).attr('value');
		if($('#searchIt option[value=' + x + ']').length){y=x}
		$('#searchIt').val(y);
		var eg=tmpArr[y].engine;
		var tmp=[];
		var j=-1;
		for(var i=0; i<eg.length;i++){
			if(eg[i].on=='yes'){
				j += 1;
				var thref=unescape(eg[i].href), tlnk=unescape(eg[i].lnk), tsrc=unescape(eg[i].ico), tfn=unescape(eg[i].fn);
				if(tlnk && tfn!='fns()'){tlnk=H+H_w(tlnk)}
				if(thref==''){thref=H+H_W(tlnk)}
				if(tsrc==''){tsrc=imgFav(thref)}

				tmp.push('<img width=16 height=16 src="' +tsrc +
					'" title="' + unescape(eg[i].title) +
					'" data-href="' + thref +
					'" data-lnk="' + tlnk +
					'" data-fn="' + tfn +
					'" '+(j>sN-1?'hidden ':'')+'/>');
			}
		}
		$('#engine').html(tmp.join(''));
		$('#engine img').on('click',function(){tabNew(this)});
		$('#pagel').addClass('btn2');
		$('#pager').toggleClass('btn2',$('#engine img').length <= sN);
	}
}


function loadfunc(){
	ini.func();
	var func2html=function(o){
		return 	'<input type="button" value="' + unescape(o.name) + '" data-auto="' + (o.auto||'') + '" data-regexp="' + (o.regexp||'') +
				'" data-code="' + o.code + '" data-type="' + o.type + '" />'
	};
	var fsA = jSon(INI.func0);
	for(var j=0; j<fsA.length;j++){
		var fj=fsA[j], str='<div>';
		for(var i=0; i<fj.length;i++){
			str+=func2html(fj[i]);
		}
		$('#funcs').append(str+dc);
	}
	var f=jSon(L.func),fl=f.length;
	for(var j=0; j<Math.ceil(fl/6);j++){
		var str='<div>';
		for(var i=0;i<6;i++){
			if(j*6+i>=fl){break}
			str+=func2html(f[j*6+i]);
		}
		$('#funcs').append(str+dc);
	}
	$('#funcs').find('input').on('click',function(){
	var c=unescape($(this).attr('data-code')), typ=$(this).attr('data-type'), o={"allFrames":true};
		if(/File/.test(typ)){
			o.file=c;
		}else{
			o.code=c;
		}
		var tp=$('#funcType').val(),funcTab=function(taB,o){
			if(typ.slice(0,2)=='js'){
				chrome.tabs.executeScript(taB.id,o, function(response){});
			}else{
				chrome.tabs.insertCSS(taB.id,o, function(response){});
			}
		};
		if(tp=='getFunc'){
			chrome.tabs.getSelected(null,function(taB){
				funcTab(taB,o)
			});
		}
	    if(/getFuncsTab$/.test(tp)){
	        chrome.tabs.getAllInWindow(null, function(tabArr){
	            for(var i=0; i<tabArr.length; i++){
	                funcTab(tabArr[i],o);
	            }
	        });
		}

		if(/getFuncsWindow$/.test(tp)){
	        chrome.windows.getAll({populate:true}, function (winArr){
	            for(var i=0; i<winArr.length; i++){
	                var tabArr = winArr[i].tabs;
	                for(var j=0; j<tabArr.length; j++){
	                    funcTab(tabArr[j],o);
	                }
	            }
	        });
		}
		
	});
}


function loadapad(){
	var t2=gM('apad');
	$('#apad').html('<h1 contenteditable=true id=APAD>'+L.APAD+'</h1><a href="' + t3 +
					'" id=Apad target="_blank">'+prog+'<img id=apadImg hidden alt="' + t2 + '" title="' + t2 + '"></img></a>');
	var img=$('#apadImg')[0];
	img.onload=function(){$(this).prev().remove(); $(this).fadeIn() }
	img.onerror=function(){
		if(this.src==t0){
			$('#Apad').remove();
		}else{
			this.src=t0;
			$('#Apad').attr('href',t0);
		}
	}
	img.src=t3;
	
	if(L.today!=Time.now('Date')){
		$.get(H+'www.bing.com', function(d){
			var u=d.match(/g_img=\{url:'[^']+/)||'';
			if(u){u=u[0].replace(/.+url:['"]?/gi,'')}
			if(/^[/]/.test(u)){u=H_a(u,H+'s.cn.bing.net')}
			L.bing=u;
		});
		L.today=Time.now('Date');
	}
}

function loadmemo(){
	$('#memos tbody').html(L.memos);
}

function savememo(){
	L.memos=$('#memos>tbody').html();
}
function loadonekeys(){
	//console.log('load: '+L.onekeys);
	var k=jSon(L.onekeys),s='';
	for(var i=0;i<k.length;i++){
		var ki=k[i];
		s+=SCtv('onekey" data-v="'+ki.v.replace(/"/g,"'"),'<span>'+ki.name+sc+'<input type=text value="'+ki.name+'" class=onekeyname hidden />'+strbtn+'-" class=del title="'+gM('del')+'" hidden />')
	}
	$('#home').html(s)
}
function saveonekeys(){
	//console.log('save: '+L.onekeys);
	var keys=[];
	$('#home > span').each(function(){
		var me=$(this);
		keys.push({name:me.find('.onekeyname').val(),v:me.attr('data-v')});
	});
	L.onekeys=JSON.stringify(keys);
	//console.log(L.onekeys);
}