/*
 * ZIG - zzllrr Imager Geek <http://goo.gl/n8SMk>
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */
var imgRule0,imgRule1,imgRule2,renm0,Y;




var baseUrl = document.baseURI.replace(/"/g,'%22');
var attrStr='","width":"0","height":"0","alt":"';
var attrStr2= attrStr+'","title":"","link2":"';


if(baseUrl.indexOf('/',8)>0){
	var baseUrl0 = baseUrl.substr(0,baseUrl.indexOf('/',8))+'/';
	var baseUrl1 = baseUrl.substr(0,baseUrl.lastIndexOf('/'))+'/';
}else{
	var baseUrl0 = baseUrl+'/';
	var baseUrl1 = baseUrl+'/';
}



function absUrl(u,norule){
	if(/^#|javascript:/i.test(u)){return ''}
	var src=H_a(u, baseUrl);

	/*
	if(imgRule1){src= eval('(function (){return src.replace('+imgRule1+')})()');
	if(imgRule2){src= eval('(function (){return '+imgRule2.replace(/@/g,src)+')})()');
	*/
	imgPreReData.lastIndex=0;
	if(!norule && !imgPreReData.test(u)){
		if(imgRule0 && !imgRule0.test(src)){return ''}
		if(imgRule1){src= eval('src.replace('+imgRule1+')')}
		if(imgRule2){src= eval(imgRule2.replace(/@/g,src))}
	}

	if(!/^data/.test(u)){
		try{
			var t=decodeURI(src);
		}catch(e){
			src=src.replace(/%/g,'z@z@l@l@r@r');
		}
	}
	return escape(src);
}

function R0(ZIG_r0){
	var r='';
	if(renm0){r= eval(renm0.replace(/@/g,'ZIG_r0'))}
	return escape(r);
}

function cssUrl(tmpPic,u){
	if( tmpPic.match(cssImgRe)){
		var t=tmpPic.match(cssImgRe)[0].replace(/([\s:,]?url\(['"]?)|(['"]?\))/gi,'');
		return fontReData.test(t)?t:absUrl(u?H_a(t,u):t,true);
	}else{
		return "";
	}
}

function linkUrl(tmpPic){
	var tmpLink2 = "";
	if ( $(tmpPic).parents("a").length ) {tmpLink2 = absUrl($(tmpPic).parents('a')[0].href,true)}
	if ( $(tmpPic).attr('usemap')) {
		tmpLink2 = $(tmpPic).attr('usemap').substr(1);
		var u=[];
		$('map[name="'+tmpLink2+'"] area').each(function(){
			var h=absUrl($(this).attr('href'),true);
			if(h && u.indexOf(h)<0){u.push(h)}
		});
		tmpLink2 = u.join(' ');
	}
	return tmpLink2;
}

function getUrl(imgrCode){
	var imgrCode = eval('(function (){'+unescape(imgrCode)+'})()'), u=[];
	for(var i=0;i<imgrCode.length;i++){
		var h=absUrl(imgrCode[i],true);
		if(h && u.indexOf(h)<0){u.push(h)}
	}
	return u.join('\n');
}

function getCode(codeCode){return escape(eval('(function (){'+unescape(codeCode)+'})()'))}

function getImage(img0rule,img1rule,img2rule,url2rule,r0,fav){
	//Scroll('scrollB');
	//window.stop();
	var pic=['{"picList":['],linkCss=[],fonts=[];
	var tmpPic, tmpPic2, SRC='{"src":"';
	if(fav){pic.push(SRC+absUrl(unescape(fav),true)+attrStr2+'Favicon","r0":"'+R0(this)+'"},')}

	if(	url2rule){

		var Url2rule = eval('(function (){'+unescape(url2rule)+'})()');
		for(var i=0;i<Url2rule.length;i++){
			var h=SRC+absUrl(Url2rule[i],true)+attrStr2+'Img","r0":"'+R0(this)+'"},';
			if(pic.indexOf(h)<0){pic.push(h)}
		}

	}else{


		imgRule0 = new RegExp(unescape(img0rule));
		imgRule1 = unescape(img1rule).trim();
		imgRule2 = unescape(img2rule).trim();
		renm0 = unescape(r0).trim();
		if(imgRule2.indexOf('@')<0){imgRule2 = ''}
		if(renm0.indexOf('@')<0){renm0 = ''}

		$('link[rel=stylesheet][href]').each(function(){
			var h=absUrl($(this).attr('href'),true);
			if(linkCss.indexOf(h)<0){linkCss.push(h)}
		});

		var n,attrArr=['src','mce_src','image-src','lazy-src','data-src','data-url','data-image','data-img','data-original','data-lazyload','srcset'],wcanvas=[];


		n='img,source';
		Node(n).each(function(){
			for(var i=0; i<attrArr.length;i++){
				var att=attrArr[i];
				if($(this).attr(att)){
					var sA=$(this).attr(att).split(',');
					for(var j=0;j<sA.length;j++){
						tmpPic = absUrl(sA[j].split(' ')[0]);
						if(tmpPic){
							tmpPic = SRC+tmpPic+((imgRule1||imgRule2||att=='srcset')? '","width":"0","height":"0' :
									'","width":"'+(this.naturalWidth||0)+
									'","height":"'+(this.naturalHeight||0))+
									'","alt":"'+escape(this.alt||'')+
									'","title":"'+escape(this.title||'')+
									'","link2":"'+(linkUrl(this) || 'Img')+
									'","r0":"'+R0(this)+
									'"},';
							//HTML5 naturalWidth !
							if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
						}
					}
				}
			}
		});

		n=':image[src]';// source[src]
		Node(n).each(function(){
			tmpPic = absUrl($(this).attr('src'));
			if(tmpPic){
				tmpPic = SRC+tmpPic+attrStr+'","title":"'+escape(this.title||'')+'","link2":"Img","r0":"'+R0(this)+'"},';
				if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
			}
		});



		n='[background], image';
		Node(n).each(function(){
			var image=$(this).attr('xlink:href')||'', l2=image?'Img':'CssBG';
			tmpPic = absUrl($(this).attr('background') || $(this).attr('xlink:href') || '');
			if(image && image!=tmpPic){
				this.setAttribute('xlink:href', unescape(tmpPic));
			}
			if(tmpPic){
				tmpPic = SRC+tmpPic+attrStr2+l2+'","r0":"'+R0(this)+'"},';
				if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
			}
		});

		n='svg';
		Node(n).each(function(){
			tmpPic = svgAs(this,true);
			tmpPic = SRC+tmpPic+attrStr2+'Img","r0":"'+R0(this)+'"},';
			if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
		});



		var META=attrStr2+'Meta","r0":"'+R0(this)+'"},';
		n="meta[property='og:image'], meta[property='og:photo'], meta[itemprop='image'], meta[name*='image'], meta[name*='IMAGE'], meta[name*='Image'], meta[name^='thumbnail']";
		Node(n).each(function(){
			tmpPic = absUrl($(this).attr('content'));
			if(tmpPic){
				tmpPic = SRC+tmpPic+META;
				if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
			}
		});
		n="meta[name='msapplication-task'][content*='icon-uri=']";
		Node(n).each(function(){
			tmpPic = absUrl($(this).attr('content').split('icon-uri=')[1].split(';')[0]);
			if(tmpPic){
				tmpPic = SRC+tmpPic+META;
				if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
			}
		});

		n="link[rel*='icon']";
		Node(n).each(function(){
			tmpPic = absUrl($(this).attr('href'));
			if(tmpPic){
				tmpPic = SRC+tmpPic+META;
				if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
			}
		});


		n="[style*='url(']";
		Node(n).each(function(){
			tmpPic = cssUrl($(this).attr('style'));
		//	console.log(tmpPic);
			if(tmpPic){
				if(addInf(tmpPic,cssLinkRe,linkCss) || addInf(tmpPic,fontRe,fonts) || addInf(tmpPic,fontReData,fonts)){return true}
				//console.log(tmpPic+' url(');
				tmpPic = SRC+tmpPic+attrStr2+'Css","r0":"'+R0(this)+'"},';
				
				if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
			}
		});

		for(var i=0; i<document.styleSheets.length; i++){
			var cssi=document.styleSheets[i],cssih=cssi.href,tCss =cssi.cssRules;
			if(tCss){
				for(var j=0;j<tCss.length;j++){
					tmpPic = cssUrl(tCss[j].cssText,cssih);
					if(tmpPic){

						if(addInf(tmpPic,cssLinkRe,linkCss) || addInf(tmpPic,fontRe,fonts) || addInf(tmpPic,fontReData,fonts)){continue}

						tmpPic = SRC+tmpPic+attrStr2+'CssFile","r0":"'+R0(this)+'"},';
//console.log(tmpPic,i,cssih);
						if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
					}

					tmpPic=tCss[j].cssText;
					if(/-webkit-canvas\(/i.test(tmpPic)){

						tmpPic = tmpPic.split(/-webkit-canvas\(/gi)[1].replace(/\).+/,'');
						if(tmpPic && wcanvas.indexOf(tmpPic)<0){
							n=$(tCss[j].selectorText).eq(0);
							wcanvas.push(tmpPic);
							tmpPic=document.getCSSCanvasContext("2d", tmpPic, n.height(), n.width()).canvas.toDataURL();
							tmpPic=SRC+tmpPic+attrStr2+'Canvas","r0":"'+R0(this)+'"},';
							if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
						}
					}
				}
			}
		}


		var TEXT=attrStr2+'Text","r0":"'+R0(this)+'"},';
		n=":contains('://'),:contains(':\/\/')";
		Node(n).each(function(){
			var tmpAry = $(this).text().replace(/\\/g,'').match(textImgRe);
			
			if(tmpAry){
				
				for(var i=0;i<tmpAry.length;i++){
				//console.log(i,tmpAry[i]);
					tmpPic = absUrl(tmpAry[i]);
					if(tmpPic){
						tmpPic = SRC+tmpPic+TEXT;
						if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
					}
				}
			}
		});

		n=":contains('url')";
		Node(n).each(function(){
			var tmpAry = $(this).text().replace(/\\/g,'').match(textCssImgRe);
			if(tmpAry){
				for(var i=0;i<tmpAry.length;i++){
					tmpPic = cssUrl(tmpAry[i]);
//	console.log(tmpPic);
					if(tmpPic){
						if(addInf(tmpPic,cssLinkRe,linkCss) || addInf(tmpPic,fontRe,fonts) || addInf(tmpPic,fontReData,fonts)){continue}
						tmpPic = SRC+tmpPic+attrStr2+'TextCss","r0":"'+R0(this)+'"},';
						if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
					}
				}
			}
		});

		n=":contains('data:image/')";
		Node(n).each(function(){
			var tmpAry = $(this).text().match(imgPreReData);
			if(tmpAry){
				for(var i=0;i<tmpAry.length;i++){
					if( hanzi.test(tmpAry[i]) ){continue}
					tmpPic = absUrl(tmpAry[i]);
					if(tmpPic){
						tmpPic = SRC+tmpPic+TEXT;
						if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
					}
				}
			}
		});

		n="[style*='-webkit-canvas(']";
		Node(n).each(function(){
			tmpPic = $(this).attr('style').split('-webkit-canvas(')[1].replace(/\).+/,'');
			if(tmpPic && wcanvas.indexOf(tmpPic)<0){
				wcanvas.push(tmpPic);
				tmpPic=document.getCSSCanvasContext("2d", tmpPic, $(this).height(), $(this).width()).canvas.toDataURL();
				tmpPic=SRC+tmpPic+attrStr2+'Canvas","r0":"'+R0(this)+'"},';
				if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
			}
		});

		n='canvas';
		Node(n).each(function(){
			try{
				tmpPic = this.toDataURL();
				if(tmpPic){
					tmpPic = SRC+tmpPic+attrStr2+'Canvas","r0":"'+R0(this)+'"},';
					if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
				}
			}catch(e){
				//console.log(e);
			}
		});


		n='[href]';
		Node(n).not(zlr3('[href^="','javascript: #','"]',',')).each(function(){
			var h = $(this).attr('href');
			tmpPic = absUrl(h);
			if(addInf(tmpPic,cssLinkRe,linkCss) || addInf(tmpPic,fontRe,fonts) || addInf(tmpPic,fontReData,fonts)){return true}
			imgPreReData.lastIndex=0;
			if(imgPreReData.test(h) || hrefImgRe.test(h) ){
				tmpPic = absUrl(h);
				if(tmpPic){
					tmpPic = SRC+tmpPic+attrStr2+'Link","r0":"'+R0(this)+'"},';
					if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
				}
			}
		});

	}
// metaList

	if(linkCss.length){
		for(var j=0;j<linkCss.length;j++){
			var u=unescape(linkCss[j]), a=function(u){

				$.ajax({type:'get', url:u, error:function(xhr, status, error){
							}, success:function(d){
					var tmpAry = d.match(textCssImgRe),linkCss=[],fonts=[], pic=['{"picList":['];

					if(tmpAry){
						for(var i=0;i<tmpAry.length;i++){
							tmpPic = cssUrl(tmpAry[i],u);
							if(tmpPic){
								if(addInf(tmpPic,cssLinkRe,linkCss) || addInf(tmpPic,fontRe,fonts) || addInf(tmpPic,fontReData,fonts)){continue}
								
								tmpPic = SRC+tmpPic+attrStr2+'Css","r0":"'+R0(this)+'"},';
//console.log(tmpPic,u);
								if(pic.indexOf(tmpPic)<0){pic.push(tmpPic)}
							}
						}
						if(pic.length){
							var Pic = pic.join('').replace(/,$/, '')+
								'],"metaList":[{"linkCss":"'+linkCss.join(' ')+
							   '","fonts":"'+fonts.join(' ')+
							   '","pages":"0"}]}';
							chrome.extension.sendRequest({pic:Pic, hope:'addpic',
								from:'getImage',order:-1});
						}
					}
				}});
			};
			a(u);
		}

	}


	return pic.join('').replace(/,$/, '')+
			'],"metaList":['+
			getMeta().replace(/\}$/, ',"linkCss":"'+linkCss.join(' ')+
		   '","fonts":"'+fonts.join(' ')+
		   '","pages":"1"}]}');
}

function getMeta(){
	var kwd='',desc='',titl=(document.title||'').replace(/"/g,'');
	$('meta[name*=eywords],meta[name*=EYWORDS],meta[name*=escription],meta[name*=ESCRIPTION]').each(function(){
		var n=this.name.toLowerCase();
   		if(n=='keywords'){
			kwd = $(this).attr('content').replace(/"/g,'');
		}else if(n == "description"){
			desc = $(this).attr('content').replace(/"/g,'');
		}
 	});
	return JSON.stringify({title:altTxt(titl), url:baseUrl, kwd:altTxt(kwd), desc:altTxt(desc), sech:altTxt(getSelection().toString())})
}

var Engin={
	bd:function(html, u){
		var bd=$(XML.wrapE('div', (html||'')));

		bd.find(zlr2('img :image','[src]',',')).each(function(){
			var me=$(this), s0=me.attr('file')||me.attr('data-original')||me.attr('src'), s=H_a(s0, u);
			if(s!=s0){me.attr('src', s)}
		});

		bd.find('form').attr('method','');
		bd.find(':button, :submit').attr('disabled','disabled');
		var ZBD='#ZBD'+Time.now5()+' ';
		bd.attr('id', 'ZBD').find('style').html(function(i, v){
			return ZBD+v.trim().replace(/[\},(\*\/)][\n\s]+/g, '$&'+ZBD).trim();
		});

		bd.find(zlr3('[on','click load',']',',')).removeAttr(zlr('on','click load'));
		bd.find(zlr3('a[href','^="javascript:" =#',']',',')).removeAttr('href');
		bd.find('a[href]').not('[href^=#]').attr('target','_blank').not(zlr3('[href^=','http "mailto:"',']',',')).attr('href', function(i,v){return H_a(v,u)});

		bd.find('object').has('embed[src]').each(function(){
			var em=$(this).find('embed[src]'), src=em.attr('src'), wd=em.attr('width'), ht=em.attr('height');
			$(this).replaceWith('<iframe src="'+src+'" style="width:'+(wd||500)+'px;height:'+(ht||400)+'px" >');
		});

		return bd;
	},

	bdLoadHtml:function(html, aB){
		aB.html(html||'');
		return Engin
	}
};

chrome.extension.onRequest.addListener(function(request, sender, sendResponse){
	switch(request.hope){
		case 'eval':
			sendResponse({eval:eval(request.eval)});
			return;

		case 'scrollDown':
		case 'scrollUp':
		case 'scrollT':
		case 'scrollB':
		case 'scrollY':
			Scroll(request.hope);
			break;

		case 'hideFixed':
		case 'hideBottom':
		case 'hideTop':
		case 'showFixed':
		case 'showBottom':
			var hope=request.hope.toLowerCase();
			Node(':'+hope.substr(4)).css('opacity',hope.substr(0,4)=='hide'?0:1);
			break;

		case 'savImage':
		case 'getImage':
		case 'getImages':
		case 'getImages1':
		case 'getImagesTab':
		case 'getImagesWindow':
			var Pic = getImage(request.img0rule,request.img1rule,request.img2rule,request.url2rule,request.r0,request.fav);
			chrome.extension.sendRequest({pic:Pic, hope:(request.hope=='savImage'?'sav':'add')+'pic',
				from:request.hope, order:request.order});
			break;

		case 'getUrl':
			var Url=getUrl(request.imgrCode);
			chrome.extension.sendRequest({url:Url, hope:'addurl'});
			break;

		case 'capScrW_Q':
		case 'capScrR_Q':
			hopeCapScr(request.hope);
			break;

		case 'getMeta':
			if(window==top){chrome.extension.sendRequest({meta:getMeta(), hope:'addmeta'}) }
			break;

		case 'getCode':
		case 'getCodes':
		case 'getCodes1':
		case 'getCodesTab':
		case 'getCodesWindow':
		case 'getCode2':
			var reg=new RegExp(unescape(request.reg));
			if(!reg.test(location.href)){break}
			var Code = getCode(request.codeCode), rqst={code:Code, hope:'addcode', from:request.hope, order:request.order};
			if(request.hope=='getCode2'){
				rqst.hope += 2;
				rqst.to = request.to;
			}

			chrome.extension.sendRequest(rqst);
			break;

		case 'engi':
			engi();
			break;
	}
	sendResponse({});
});

if(window==top){
	$(document).off(zlr3('mouse','down up move out','.zzllrr'),'.zzllrrEngineMove').on('mousedown.zzllrr', '.zzllrrEngineMove',function(e){mDn(e)}).on('mousemove', '.zzllrrEngineMove', function(e){mMv(e)}).on('mouseup', '.zzllrrEngineMove', function(e){mUp(e)}).on('mouseout', '.zzllrrEngineMove', function(e){mUp(e)});
	$(document).off('keydown.zzllrr','body').on('keydown.zzllrr','body',function(e){
		if(window!=top){return}
		var k=e.keyCode, c=e.ctrlKey, s=e.shiftKey, a=e.altKey, b=document.activeElement.tagName;
		if(b){b=b.toLowerCase()=='body'}
		if(b){
			if(c && !s && !a){
				if(k==38){Scroll('scrollUp')}
				if(k==40){Scroll('scrollDown')}
			}
			if(!c && s && !a){
				if(k==81){hopeOn('capScr')}
				if(k==74){hopeOn('bulkImage')}
			}
		}

		if(c && !s && !a){
			if(k==77){hopeOn('capMHTML')}
			if(k==81){hopeOn('getImage')}
			if(k==106){chrome.extension.sendRequest({hope:'eng_Q'})}

		}

		if(!c && !s && a){
			if(k==81){hopeOn('savImage')}
			if(k>47 && k<58){
				k=(k==48?9:k-49);
				chrome.extension.sendRequest({hope:'func_Alt', n:k});
			}
		}

		if(c && s && !a){
			if(k==77){hopeOn('capMHTMLTab')}
			if(k==83){hopeOn('capScrR')}
			if(k==85){hopeOn('capScrW')}
			if(k==88){hopeOn('capScrS')}
			if(k==106 || k==56){engi()}
		}

		if(c && !s && a){
			if(k==32){hopeOn('showImage')}
			if(k==81){hopeOn('pinImage')}
		}
	});
}

function mMv(e){
	var eos=e.originalEvent.srcElement, act=eos.tagName, shpN=$('#zzllrrEngine');
	var X=e.screenX, Y=e.screenY,lt,tp,offX=X-Number(shpN.attr('data-x')),offY=Y-Number(shpN.attr('data-y'));
	if(shpN.attr('data-move')=='true'){$('body').css('-webkit-user-select','none');}
	shpN.attr({'data-x':X,'data-y':Y});
	shpN.filter('[data-move=true]').css({left:function(i,v){lt=Number(v.replace('px',''))+offX; return lt+'px'},top:function(i,v){tp=Number(v.replace('px',''))+offY; return tp+'px'}});
	e.stopPropagation()
}

function mDn(e){
	var eos=e.originalEvent.srcElement, act=eos.tagName, shpN=$('#zzllrrEngine');
	var X=e.screenX, Y=e.screenY;
	$('body').css('-webkit-user-select','none');
	shpN.attr({'data-move':true,'data-x':X,'data-y':Y});
	e.stopPropagation()
}

function mUp(e){
	var eos=e.originalEvent.srcElement, act=eos.tagName, shpN=$('#zzllrrEngine');
	shpN.attr('data-move',false);
	$('body').css('-webkit-user-select','auto');
	e.stopPropagation()
}

function addInf(src,reg,arr){
	reg.lastIndex = 0;
	var u=unescape(src);
	if(reg.test(u)){
		if(arr.indexOf(src)<0){arr.push(src)}
		return true;
	}else{
		return false;
	}
}

function hopeOn(t){if(window!=top){return}
	chrome.extension.sendRequest({hope:t+'_Q_on'}, function(response){if(response.on){
	if(/capScr[RW]/.test(t)){hopeCapScr(t+'_Q')}else{chrome.extension.sendRequest({hope:t+'_Q'})}}})
}

function hopeCapScr(t){
	if(window==top){
		Y=window.scrollY;
		chrome.extension.sendRequest({hope:t, h:$(window).height(), w:$(window).width(), H:scrollH(), Y:Y});
	}
}

function engi(){
	chrome.extension.sendRequest({hope:'eng'}, function(response){
		var zE='zzllrrEngine', Re=$('#'+zE), eng=response.eng;
		if(Re.length){Re.remove(); return}
		var kwd=altTxt(getSelection().toString());
//if(!kwd){return}
		var se=$(getSelection().focusNode.parentNode), seo=se.offset(), lt=seo.left+se.width()/2, tp=seo.top, bdr='border-radius:5px;';
		$('body').prepend('<div style="'+bdr+'background:white;padding:2px;box-shadow:0 0 15px rgba(0,0,0,.3);max-width:220px;position:absolute;top:'+tp+
				'px;left:'+lt+'px;z-index:2147483647;text-align:left;" id='+zE+'>'+
				DC+zE+'Move style="cursor:move;padding:1px;-webkit-user-select:none">'+
				XML.wrapE('style', '#'+zE+' *{word-wrap:break-word;word-break:break-all;-webkit-transition:all .2s linear} .'+
				zE+'Move:hover{background-image: -webkit-linear-gradient(top, white, rgb(170, 204, 0) 20%, rgb(170, 204, 0) 80%, white);}')+
				'<span style="font-weight:bold">'+gM('eng')+': '+
				XML.wrapE('a', eng.ttl)+
				sC+zE+'Off style="cursor:pointer;float:right;color:red">╳'+sc+
				DC+zE+'0 style="'+bdr+'font-weight:bold;box-shadow:0 1px 3px rgba(0,0,0,.1);-webkit-user-select:none">'+
				'<input type=search results size=24 style="'+bdr+'margin:2px;padding:0" />'+dc+
				dC+zE+'1 style="padding:2px;max-height:300px;overflow:auto;">'+dc+dc);

		Re=$('#'+zE);
		re=Re.find('.'+zE+'1');
		Re.find('.'+zE+'Off').on('click',function(){
			Re.remove();
		});

		Re.find('.'+zE+'0').find('input').eq(0).on('dblclick',function(){
			engin($(this).val());
		}).on('keydown',function(e){
			var k=e.keyCode, shft=e.shiftKey, ctrl=e.ctrlKey, alt=e.altKey;
			if(!(shft || ctrl)){
				if(k==13){
					engin($(this).val());
				}
			}
		});

		var engin=function(kw){
			re.html(prog);
			var k=kw.replace(/['"]/g,'').trim();
			var kwd=altTxt(k)
			var Url=eng.href;
			var fn=eng.fn, Fns=fn=='fns()';
			var lnk=eng.lnk;
			var sz=eng.sz;

			if(Fns && !kwd.match(/^https*:/)){kwd=''}

			fn=fn.replace(/fn[0-9a-z]\(\)/g,function(t){return t.replace(')','%n)')});


			var jArr=[location.href, document.title], eva='';
			if(Fns){
				//fns(webid, url, title, smry, pic)
				Url=fns(lnk, Url||jArr[0], kwd||jArr[1], sel||kwd||'', '');
			}else if(lnk){
				if(fn){
					eva=fn.replace(/%n/g,"'" + kwd + "'").replace(/%u/g,"'" + jArr[0] + "'").replace(/%t/g,"'" + jArr[1] + "'");
				}
			}

			if(!Fns){Url=lnk.replace(/%s/g,eva?eval(eva):kwd)}
			if(!/^https?:/.test(Url)){Url=H+Url}
			if(/^https:/.test(location.href)){Url=Url.replace(/^http:/,'https:')}
			re.prev().find('a').attr({'href':Url, 'title':Url,'target':'_blank'}).next().val(kwd);

			$.ajax({type:'get', url:Url, error:function(xhr, status, error){
						Engin.bdLoadHtml(error, re)}, success:function(d){
				var sep='<body', tArr=d.split(sep), html, node=sz || 'body,BODY';
				if(tArr.length<2){
					sep=sep.toUpperCase(), tArr=d.split(sep);
				}
				if(tArr.length>1){
					html=sep+tArr[1].substr(0, tArr[1].lastIndexOf(sep.replace('<','</')+'>')+7);
					html=Engin.bd(blking(html), Url);
					t=html.find(node).eq(0);
					if(t.length<1){t=html}
					t.find('footer').nextAll().remove();
					Engin.bdLoadHtml(t.html()+'<div style="font-size:14px;color:gainsboro;padding:2px;text-align:center">Powered by '+gM('eng')+dc, re);
				}
			}});
		};
		engin(kwd);

	});
}
function noti(t){chrome.extension.sendRequest({hope:'noti', noti:t})}
function SaveText2(t,path,filename,conflictAction,cb){
	var mime='text/plain';
	SaveAs2('data:'+mime+';charset=utf-8;base64,' + Base64.encode(t), path,filename,conflictAction||'',cb||null)
}
function SaveAs2(Url,path,filename,extOpt,conflictAction,cb){
	//uniquify overwrite prompt
	var pname=filename||'',pext;
	if(!pname){
		imgPreReData.lastIndex=0;
		txtPreReData.lastIndex=0;
		if(imgPreReData.test(Url) || txtPreReData.test(Url)){
			pname = Url.replace(/[;,].*/,'');
			pext = pname.split('/')[1].toLowerCase().replace('x-icon','ico').replace('+xml','');
			imgPreReData.lastIndex=0;
			pname=imgPreReData.test(Url)?'image.'+pext:'text.txt';
			
		}else{
			pname = Url.replace(/\?.+$/g,'').replace(/.+[/]/,'').replace(/\&.+$/,'');
			try{
				pname=decodeURIComponent(pname);
			} catch(e) {
			//	console.log(pname);
				 console.log(e);
			}
		}
	}
	if(!/\./.test(pname) && extOpt){
		pname+='.'+extOpt;
	}
	
	chrome.extension.sendRequest({pic:{Url:Url,path:path||'',
		filename:pname,conflictAction:conflictAction||'',cb:cb||null}, hope:'saveAs2'});
}
function SavImg(src,p){
//savImg(src,wh,path,rename,extOpt,JpgQ,flipRotate,clip,cut,Scale,Transform,Filter,DropShadow)
	chrome.extension.sendRequest({src:src,p:p, hope:'SavImg'});
}