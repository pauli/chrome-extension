﻿/*
 * zzllrr lib for ZIG
 * Copyright by zzllrr. All rights reserved.
 * zzllrr@gmail
 * Released under MIT License
 */

var H='http://',Hs='https://', w3c='www.w3.org/', xmlns=H+w3c+'2000/svg', xhtml=H+w3c+'1999/xhtml', xmlnsxlink=H+w3c+'1999/xlink';
var imgPreReg='((file:[/]|https?:|ftp:)[/]|data:image)[/].+',
	imgPre=new RegExp(imgPreReg,'gi'), imgPreRe=new RegExp('^'+imgPreReg,'gi'),

	hanzi=/[\u4E00-\u9FA5\uFF00-\uFFFF]+/, hanziRe =/[^\x00-\xff]/g,

	fontReData=/^data.+font[/].{40,}/i, imgPreReData=/^data.+image\/.{40,}/gi, txtPreReData=/^data.+text\/plain/gi,

	cssLinkRe=/\.css($|\?.*)/i, fontRe=/\.(eot|[ot]tf|ttc|font?|woff2?)($|#|\?.*)/i,
	cssImgReg='url\\([\'"]?[^\\\'"\\)\\s]+[\'"]?\\)', cssImgRe=new RegExp(cssImgReg,'gi'), textCssImgRe=new RegExp('([\\s:,]|^)'+cssImgReg,'gi'),

	imgReg='(bmp|gif|ico|jpeg|jpg|apng|png|svg|webp)',
	hrefImgRe=new RegExp('/\\S*\\.'+imgReg+'[\\?\\&]*.*','i'), textImgRe=new RegExp('(file:[/]|https?:|ftp:)[/]{2}[^/][^\'"\\s\\(\\)]*\\.'+imgReg,'gi'),
	digiReg=/^\d+(\.\d)?$/
;


$.expr[':'].bottom = function(obj){return $(obj).css('position') == 'fixed' && $(obj).css('bottom') == '0px' };
$.expr[':'].top = function(obj){return $(obj).css('position') == 'fixed' && $(obj).css('top') == '0px' };
$.expr[':'].fixed = function(obj){return $(obj).css('position') == 'fixed'};
$.expr[':'].encoded = function(obj){return /:?encoded/i.test($(obj)[0].localName)};
$.expr[':'].number = function(obj){return $(obj).attr('type') == 'number'};
$.expr[':'].range = function(obj){return $(obj).attr('type') == 'range'};
$.expr[':'].color = function(obj){return $(obj).attr('type') == 'color'};
$.expr[':'].date = function(obj){return $(obj).attr('type') == 'date'};
$.expr[':'].time = function(obj){return $(obj).attr('type') == 'time'};

$.fn.extend({
  twinkle: function() {
    return $(this).fadeTo('slow', 0).fadeTo('slow', 1);
  }
});

function gM(msg,str) {return chrome.i18n.getMessage(msg, str)||msg}

var H5Colors='aliceblue,antiquewhite,aqua,aquamarine,azure,beige,bisque,black,blanchedalmond,blue,blueviolet,brown,burlywood,cadetblue,chartreuse,chocolate,coral,cornflowerblue,cornsilk,crimson,cyan,darkblue,darkcyan,darkgoldenrod,darkgray,darkgreen,darkgrey,darkkhaki,darkmagenta,darkolivegreen,darkorange,darkorchid,darkred,darksalmon,darkseagreen,darkslateblue,darkslategray,darkslategrey,darkturquoise,darkviolet,deeppink,deepskyblue,dimgray,dimgrey,dodgerblue,firebrick,floralwhite,forestgreen,fuchsia,gainsboro,ghostwhite,gold,goldenrod,gray,green,greenyellow,grey,honeydew,hotpink,indianred,indigo,ivory,khaki,lavender,lavenderblush,lawngreen,lemonchiffon,lightblue,lightcoral,lightcyan,lightgoldenrodyellow,lightgray,lightgreen,lightgrey,lightpink,lightsalmon,lightseagreen,lightskyblue,lightslategray,lightslategrey,lightsteelblue,lightyellow,lime,limegreen,linen,magenta,maroon,mediumaquamarine,mediumblue,mediumorchid,mediumpurple,mediumseagreen,mediumslateblue,mediumspringgreen,mediumturquoise,mediumvioletred,midnightblue,mintcream,mistyrose,moccasin,navajowhite,navy,oldlace,olive,olivedrab,orange,orangered,orchid,palegoldenrod,palegreen,paleturquoise,palevioletred,papayawhip,peachpuff,peru,pink,plum,powderblue,purple,red,rosybrown,royalblue,saddlebrown,salmon,sandybrown,seagreen,seashell,sienna,silver,skyblue,slateblue,slategray,slategrey,snow,springgreen,steelblue,tan,teal,thistle,tomato,turquoise,violet,wheat,white,whitesmoke,yellow,yellowgreen';
function H5Color(neg,pos){
	var A=H5Colors.replace(/[^,]*grey[^,]*,/g,'').replace(/,(cyan|magenta),/g,',');
	if(neg){
		var B=neg.split(',');
		for(var i=0;i<B.length;i++){
			A=A.replace(new RegExp('[^,]*'+B[i]+'[^,]*','g'),'');
		}
	}
	A=A.replace(/,{2,}/g,',').replace(/^,|,$/g,',').split(',');
	if(pos){
		var B=pos.split(','),C=[];
		for(var j=0;j<A.length;j++){
			var b=false;
			for(var i=0;i<B.length;i++){
				if(A[j].indexOf(B[i])>-1){
					b=true;
					break
				}
			}
			if(b){
				C.push(A[j])
			}
		}
		A=C.slice();
	}
	return A
}
var ZRL='aphanomkkjgledipighdfjnilhfenpam', ZRC='jobnmmcljcfepgnecadofbjdklkibgei', ZIG='gfjhimhkjmipphnaminnnnjpnlneeplk', ZIL='bedbigoemkinkepgmcmgnapjcahnedmn', webStore=H+'chrome.google.com/webstore/detail/', CN='', isCN=false;

if(gM('@@ui_locale').slice(0,2)=='zh'){isCN=true; CN='?hl=zh_cn'}
var HOM={
	'ZIG':webStore+ZIG+CN,
	'ZIL':webStore+ZIL+CN,
	'ZRL':webStore+ZRL+CN,
	'ZRC':webStore+ZRC+CN,
	'Z':webStore.replace('detail','search')+'zzllrr',
	'ZQR':H+'site.douban.com/127068/',
	'ZIGPic':H+'img3.douban.com/view/photo/photo/public/p1376698902.jpg'
};

var strop='</option><option value=', strchkbx0='<input type=checkbox ', strbtn='<input type=button value="', btnGo=strbtn+'GO" class=vipurl />',
imgSRC='<img src="img/', prog=imgSRC+'loading.gif" width=16 class=prog />', chked=' checked', seled=' selected';
bgfrom='-webkit-gradient(linear, 0% 0%, 0% 100%, from(', bgto='), to(', grad=function(t){
	//return '-webkit-gradient(radial, 20 20, 0, 20 20, 50, from(white), to(white), color-stop(.9,'+t+'))'
	return '-webkit-linear-gradient(top, white, '+t+' 20%, '+t+' 80%, white)'
},
SC='<span class=', sc='</span>', sC=sc+SC, SCtv=function(t,v){return SC+'"'+t+'">'+(v||'')+sc},
DC='<div class=', dc='</div>', dC=dc+DC, DCtv=function(t,v){return DC+'"'+t+'">'+(v||'')+dc},br='<br/>',
detail=function(s,v){return XML.wrapE('details',XML.wrapE('summary',s)+v)},
mark=function(v,t){return '<mark title='+(t||'API')+'>'+v+'</mark>'},

sup=function(v){return XML.wrapE('sup',v)}, sub=function(v){return XML.wrapE('sub',v)},
subReg=function(v,b,u){var t=u?v.replace(u,function(t){return sup(t)}):v; return b?t.replace(b,function(t){return sub(t)}):t},

scRed=function(v){return SCtv('red',v)},scGain=function(v){return SCtv('gainsboro',v)},
sci=function(v){var ar=arguments,s='';for(var i=ar.length-1;i>-1;i--){s=ar[i]+'\n'+s} return SCtv('inblk',s.substr(0,s.length-1)||v)},
scit=function(v){return SCtv('bdt inblk notm',v)},scib=function(v){return SCtv('bdb inblk notm',v)},scil=function(v){return SCtv('bdl inblk notm',v)},scir=function(v){return SCtv('bdr inblk notm',v)},
scbt=function(v,brad){return SCtv('bdb bdt'+(arguments.length>1?' brad':''),v)},sclr=function(v){return SCtv('bdl bdr'+(arguments.length>1?' brad':''),v)},
scbox=function(v,b){return SCtv('bdl bdr bdb bdt scbox '+(b||''),v)},scblr=function(v,b){return SCtv('bdl bdr bdb scbox inblk'+(b||''),v)},sctlr=function(v,b){return SCtv('bdl bdr bdt scbox inblk'+(b||''),v)},
tmb=function(t,m,b,v){return SCtv('inblk alignc',SCtv('small',t)+DCtv('large',m)+SCtv('vat',b))+sci(v)},
eq=function(t,m,b){return SCtv('inblk pd10 alignc',SCtv('small pd10', t||'')+DCtv(m||'eq')+SCtv('small',b||''))},
Table=function(t,bd){	//bd 指定边框风格（或其他class）
		var a='<table class="'+(bd||'bd0')+'"><tbody>',b='</tbody></table>',r=[],isArr=t instanceof Array,A=isArr?t:t.split('\n'),n=A.length,m=(isArr?A[0]:A[0].split('\t')).length;
		for(var i=0;i<n;i++){
			var ri=[],Ai=isArr?A[i]:A[i].split('\t');
			for(var j=0;j<m;j++){
				ri.push('<td class="'+(bd||'bd0')+'">'+Ai[j]+'</td>')
			}
			r.push(XML.wrapE('tr',ri.join('')))
		}
		return a+r.join('')+b
	},
mtrx=function(v,lr,lcr){return SCtv('mtrx'+(lr||'')+' inblk align'+(lcr||lr||'c'), v instanceof Array?Table(v):v)},
det=function(A){return SCtv('bdl bdr inblk alignc',Table(A))},
lp=function(l,v){var t=arguments.length==1; return (!t && !l?'':SCtv('inblk xxlarge', t?'{':l))+SCtv('inblk alignl', t?l:v)},
rp=function(v,r){return SCtv('inblk alignr',v)+(r===''?'':SCtv('inblk xxlarge', r||'}'))},
lrp=function(l,v,r){var t=arguments.length==1; return SCtv('inblk xxlarge', t?'(':l)+sci(t?l:v)+SCtv('inblk xxlarge', t?')':r)},
frac=function(t,b){return SCtv('inblk alignc',SCtv('alignc',t)+DCtv('fracline')+SCtv('alignc',b))},
root=function(t,n){return sci(n?sup(n):'')+SCtv('rootleft','√')+SCtv('bdt',sci(t))},

sceg=function(v){return SCtv('eg',v)},sceg2=function(v){return SCtv('eg eg2',v)};



var FNS={
	'weixin':' weixin.ico',
	'tsina':'sinaweibo Weibo.ico 1133462976',
	'tqq':' tqq.gif 801404261',
	't163':' t163.gif 78JugW1nGMG47X1z',
	'douban':'douban Douban.ico 0dd41f9f32ae2c80297c5edd980a8580',
	'renren':' renren.ico aeaed3d9245b449094eb8d7fe0691713',
	'baidu':'baidu Baidu.ico',
	'fb':'facebook Facebook.png 227606350619834',
	'twitter':'twitter Twitter.gif',
	'googleplus':'google_plusone_share g+.ico',
	'gmail':'gmail Gmail.ico',
	'translate':'googletranslate lang.ico',
	'more':'addthis Share.ico'
};

function zlr(pre,s,sep){var t=(sep===undefined?' ':sep)+pre;return pre+s.split(' ').join(t)}
function zlr2(s,sur,sep){var t=sur+(sep===undefined?' ':sep);return s.split(' ').join(t)+sur}
function zlr3(pre,s,sur,sep){return zlr(pre,zlr2(s,sur),sep)}
function ZLR(s0,s1,s){
	var al=arguments.length;
	if(al==1){return s0.split(' ')}
	if(typeof(s1)=='number'){
		return Array(s1+1).join(s0);
	}else{
		return zlr(s0,s1).split(al<3?' ':s);
	}
}
function copyA(s,n){
	//return ZLR(s+'\n',n).trim().split('\n')
	var a=[];
	for(var i=0;i<n;i++){
		a.push(s);
	}
	return a;
}
function ZLR3(preA,sA,surA,sep){
	var n=sA.length,s='',PA=preA instanceof Array, SA=surA instanceof Array;

	for(var i=0;i<n;i++){
		s+=(sA[i]==='⋯'?'⋯':(PA?preA[i]:preA)+sA[i]+(SA?surA[i]:surA))+sep;
	}
	return s.substr(0,s.length-sep.length)
}

function imgFav(t){return 'chrome://favicon/size/16@1x/'+(t.indexOf(Hs)==0?Hs:H)+H_W1(t)}

function altTxt(t){return (t||'').replace(/[\s\r?\n]+/g,' ').replace(/\\/g,'').trim()}
function pathTxt(t){return t.replace(/\\/g,'/').replace(/(\s*\/+\s*)/g,'/').trim().replace(/^\/+|\/+$/g,'').replace(/["\\:\?\*<>\|]/g,'-')}
function CntN(t,i){
	var arr=(t||'').replace(/[\s\(\)]/g,'').split('-'), tArr=Array(4), t;

	if(arr.length<2){tArr[2]=1; tArr[3]=1}else{
		t=arr[1].split('/');
		tArr[2]=t[0];
		if(t.length<2){tArr[3]=t[0]}
	}
	t=arr[0].split('/');
	tArr[0]=t[0];
	if(t.length<2){tArr[1]=t[0]}

	return Number(tArr[i||0]);
}
function Cnt(n,n2,m,m2){
	var arg=arguments.length, t1=n, t2='';

	if(arg>=2){
		t1=n+(n==n2?'':'/'+n2);
		if(arg>2){
			t2=m+(m==m2?'':'/'+m2);
			t2=t2=='1'?'':'-'+t2;
		}
	}
	return ' ('+t1+t2+')'
}

function q_key(s,a,ins,noAppend,hotk){
	var tR=RegExp(a), tr=RegExp(a,'i'), inS=ins?'<ins>$&</ins>':'&$&', t;
	t=tR.test(s)?s.replace(tR,inS):((tr.test(s) || noAppend)?s:s+'('+a+')').replace(tr,inS);
	return hotk?t.replace(/\(.+\)/, spanHotk(a)):t;
}



function scrollH(){return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)}
function Scroll(t){
	if(/scroll(T|B)/.test(t)){$('body').scrollTop(t=='scrollT'?0:scrollH())}
	if(/scroll(Up|Down)/.test(t)){$('body').scrollTop($('body').scrollTop()+$(window).height()*(t=='scrollDown'?1:-1))}
	if(t=='scrollY'){$('body').scrollTop(Y)}
}

function titleRe(t){document.title=t}

function Node(node){
	var t=$(node);
	$('iframe').each(function(){
		try{
			//if(this.contentDocument){
				t=t.add($(this.contentDocument.body).find(node));
				//console.log(node, t.length, this.src);
			//}
		}catch(e){
			return t
			console.log('iframe err', e);
		}

	});
	return t;
}
function attr2dataset(t){
	return t.replace(/data-(.+)/,'dataset.$1').replace(/-[^-]+/g,function(a){return a.substr(1,1).toUpperCase()+a.substr(2)})
}
function urlArr(jQExp,attr,attr2){
	var jQ=jQExp||'a[href]:has(img)', t=[], a,s;
	Node(jQExp).each(function(){
		if(attr){
			s=eval('this.'+attr2dataset(attr))
		}else{
			s=this.href
		}
		if(attr2){
			a=eval('this.'+attr2dataset(attr2)).trim()+'\t'
		}else{
			a=''
		}
		if(s && s.indexOf('javascript:')< 0 && t.indexOf(a+s)<0){ t.push(a+s)}});
	return t;
}
//function isArr(obj) {return Object.prototype.toString.call(obj) === '[object Array]'} x instanceof Array

function fn0(k){return encodeURIComponent(k)}
function fn1(k){return decodeURIComponent(k)}
function fna(k){return k.replace(/ /g,'+')}
function fna0(k){return fn0(k).replace(/%20/g,'+')}
function fnb(k){return k}
function fnc(k){return escape(k)}
function fnd(k){return k.replace(/ /g,'_')}
function fne(k){return escape(k.replace(/ /g,'-'))}
function fnt(k){return escape(H_d(k))}
function fnx(k){return k.replace(/^<!\[CDATA\[|\]{2}>$/g,'') }
function fns(webid, url, title, smry, pic){
	var arr=(FNS[webid]||'').split(' '), p=pic?'&pic='+pic:'', str, k, web=webid, jia=isCN || !arr[0];
	if(webid=='more'){web=''}else{web='&'+(jia?'webid='+web:'s='+arr[0])}

	if(jia){
		if(arr.length>2){k='&appkey='+arr[2]}else{k=''}
		if(webid=='tsina'){k+='&ralateuid=2356984903'}
		str='jiathis.com/send/?uid=1515817'+ k+p +'&summary';
	}else{
		if(webid=='twitter'){web += '&via=zzllrrImager'}
		str='addthis.com/bookmark.php?pubid=ra-4eeb29d528c674a8&description';
	}
	return H+'www.'+ str +'='+ fn0(smry) + web +'&url='+ url +'&title='+ fn0(title);
}
function fnq(k,w,d){
	var t=[Hs+'chart.googleapis.com/chart?chs='+(w||150)+'x'+(w||150)+'&cht=qr&chl=',
		Hs+'api.qrserver.com/v1/create-qr-code/?size='+(w||150)+'x'+(w||150)+'&data=',
		H+'www.qrstuff.com/generate.generate?preview=1&type=URL&url=',
		H+'www.qr-code-generator.com/phpqrcode/getCode.php?cht=qr&chl=',
		H+'qrfree.kaywa.com/?l=1&s=8&d='];
	return t[d||0]+fn0(k)
}

function H_u(t){return (t||'').replace(/[\?&]utm_source=rss&utm_medium=rss&utm_campaign=.*$/,'')}
function H_w(t){return (t||'').replace(/^https*:[/]{2}/,'')}
function H_W(t){return H_w(t).replace(/[/\?].*$/,'')}
function H_W1(t){return H_W(t).replace(/.+\.(.*\.co.*)/,'$1')}
function H_d(t){return H_W(t).replace(/^www\./,'')}
function H_h(F,H){var f=H_W(F), h=H_w(H); if(h==f || h==f+'/'){h=''} return h}

function H_a(u,base){

	var b=base||'';

	if(b.indexOf('/',8)>0){
		var b0 = b.substr(0,b.indexOf('/',8)) + '/';
		var b1 = b.substr(0,b.lastIndexOf('/')) + '/';
	}else{
		var b0 = b + '/';
		var b1 = b + '/';
	}


	var t=(u||'').replace(/chrome-extension:\/\/[^\/]+/,'').replace(/\n/g,'').trim();
	if(!t){return ""}
	if(imgPreReData.test(t)||/^(file|chrome):[/]{2}/.test(t)){return t.replace(/\s/g,'')}
	if(/^\?/.test(t)){return b.replace(/\?.+/,t)}

	var src = t.replace(/[/\\]{2,}/g,'//');
	imgPreRe.lastIndex=0;
	if( !src.match(imgPreRe) ){
		if( t.substr(0,2)=='//'){
			src = b.split('//')[0] + t;
		}else if( t.charAt(0)=='/'){
			src = b0 + t.substr(1);
		}else if( t.match(/\.\.[/].+/)){
			var i=t.lastIndexOf('../') + 3;
			var j=t.slice(0,i).split('../').length * (-1);
			if(b1==b0){
				var tmpAry = (b1+'/').split('/');
			}else{
				var tmpAry = b1.split('/');
			}
			src = tmpAry.slice(0,j).join('/') + '/' + t.substr(i);

		}else{
			src = b1 + t.replace('./','');
		}
	}

	return src;
}

function html2txt(h){return $('<b>'+h+'</b>').text().trim()}
function html2html(h){return $('<div>'+h+dc).html().trim()}

function blking(t, Neg){
	var s=t, arr;
	//s=s.replace(/<script[^>]*>[\D\d]*<[/]script>/gi,'');
	arr=s.split(/<\/script>/gi);
	for(var i in arr){
		arr[i]=arr[i].split(/<script/gi)[0];
	}
	s=arr.join('');

	arr=s.split(/<\/style>/gi);
	for(var i in arr){
		arr[i]=arr[i].split(/<style/gi)[0];
	}
	s=arr.join('');
	return s;
}

function saveText(t,filename){
	var mime='text/plain';
	saveAs('data:'+mime+';charset=utf-8;base64,' + Base64.encode(t), filename)
}
function saveText2(t,path,filename,conflictAction,cb){
	var mime='text/plain';
	saveAs2('data:'+mime+';charset=utf-8;base64,' + Base64.encode(t), path,filename,conflictAction,cb)
}
function saveAs(Url,filename){
	var blob=new Blob([''], {type:'application/octet-stream'}),u=URL||webkitURL;
    var url = u.createObjectURL(blob);
    var a = document.createElementNS(xhtml,'a');
    a.href = Url;
    a.download = filename;
    var e = document.createEvent('MouseEvents');
    e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    a.dispatchEvent(e);
    u.revokeObjectURL(url);
}
function saveAs2(Url,path,filename,conflictAction,cb){
	//uniquify overwrite prompt
	return cdl.download({url:Url,conflictAction:conflictAction||'uniquify',filename:pathTxt(pathTxt(path)+'/'+pathTxt(filename))},cb);
}
function svgAs(svg,base64){
	var t=$(svg).attr('xmlns',xmlns).attr('xmlns:xlink',xmlnsxlink), xml=(new XMLSerializer).serializeToString(t[0]);
	return (base64?"data:image/svg+xml;base64,"+Base64.encode(xml):xml);
}

var svgf={
		path:function(d){
			return '<path d="'+d+'" stroke="white" fill="none"></path>'
		},text:function(i,t){
			return '<text y="'+(t?t[0]:22)+'" x="'+(t?t[1]:6)+'" font-size="'+(t?t[2]:16)+'" fill="'+(t?'yellow':'white')+'">'+i+'</text>'
		},rect:function(i,j,k){
			return '<rect x="'+i+'" y="'+j+'" width="'+k+'" height="'+k+'" stroke="white" fill="none"></rect>'
		},circle:function(cx,cy,r){
			return '<circle r="'+r+'" cx="'+cx+'" cy="'+cy+'" stroke="white" fill="none"></circle>'
		},line:function(a,b,c,d){
			return '<line x1="'+a+'" y1="'+b+'" x2="'+c+'" y2="'+d+'" stroke="white" fill="none"></line>'
		}
	}, svgs={imgr:svgf.path('M11 5 H19 V15 H25 L15 25 5 15 H11 V5z')
		,home:svgf.path('M7 25 H23 V13 L15 5 7 13 V25 M12 25 V18 H18 V25z')
		,func:svgf.text('Fn')
		,code:svgf.text('JS')
		,sech:svgf.circle(10,10,6)+svgf.line(14,14,24,24)
		,weib:svgf.path('M22 18 L23 25 17 20 A10 8 0 1 1 22 18z')
		,wdgt:svgf.path('M5 15 Q10 5 15 15 T 25 15 M5 15 Q10 25 15 15 T 25 15')
		,dona:svgf.path('M15 8 C15 7 14 5 10 5 C4 5 4 10 4 10 C4 16 8 20 15 24 C22 20 26 16 26 12 C26 12 26 5 20 5 C17 5 15 7 15 8z')
		,qrcd:svgf.rect(8,8,5)+svgf.rect(18,8,5)+svgf.rect(8,18,5)
		,memo:svgf.circle(15,16,8)+svgf.path('M10 14 L14 20 20 10 M10 5 A6 6 0 0 0 5 10Z M20 5 A6 6 0 0 1 25 10Z')

	},
Time={
	now: function(TDA){var d=new Date(), t=d.getFullYear() +'-'+ (d.getMonth() + 1) +'-'+ d.getDate()+'_'+d.toTimeString().substr(0,8).replace(/:/g,'.');
		if(/Time|Date/.test(TDA)){t=t.split('_')[TDA=='Time'?1:0]}
		return t;
	},
	now5: function(t){var d=t || new Date(); return (d.getTime()+'').substr(5)},
	YMD: function(t, TDA){
		var d=t || new Date(), t=[d.getFullYear(),(d.getMonth() + 1),d.getDate()].join('-')+'_'+[('0'+d.getHours()).substr(-2),('0'+d.getMinutes()).substr(-2)].join(':');
		if(/Time|Date/.test(TDA)){t=t.split('_')[TDA=='Time'?1:0]}
		return t;
	},
	lastM: function(t){var d=t || new Date(), m=d.getMonth(), jan=m==0; return d.getFullYear()+(jan?-1:0)+'-'+(jan?12:d.getMonth())},
	lastDate: function(fmt,tim,t){
		var tm=(t || new Date()).getTime(), d0=new Date(), d1=new Date(), tO={"y":0,"M":0,"w":0,"d":0,"H":0,"m":0,"s":0,"S":0} ,y=0, m=0, d=0, ys=0;
		if(/\d/.test(tim)){
			//tim = \d+[yMwdHmsS]
			tO[tim.substr(-1)]=Number(tim.replace(/[A-z]/g,''));
			d1.setTime(tm+(tO.w*7*24+tO.d*24+tO.H)*3600*1000+tO.m*60*1000+tO.s*1000+tO.S);
		}

		if(tO.M){
			tO.y += Math.floor((d0.getMonth()+tO.M)/12);
			d1.setMonth((d0.getMonth()+tO.M)%12);
		}

		if(tO.y){d1.setFullYear(d0.getFullYear()+tO.y)}

		if(fmt){
		//format case sensitive! Dd Hh Mm q Ss y

		//yyyy-MM(OOO)-dd HHhh:mm:ss.SSS A/P 上/下午 DDD q

			var MM=d1.getMonth()+1, HH=d1.getHours(), hh=HH==12?12:HH%12, o={
				'M+':MM,
				'd+':d1.getDate(),
				'H+':HH,
				'h+':hh,
				'm+':d1.getMinutes(),
				's+':d1.getSeconds(),
				'S+':d1.getMilliseconds()
			}, week={"0":"日","1":"一","2":"二","3":"三","4":"四","5":"五","6":"六"};


			for(var k in o){
				var r=new RegExp(k, 'g'), ok=o[k];
				if(r.test(fmt)){
					fmt=fmt.replace(r, function(t){return t.length==1?ok:('00'+ok).substr(-1*t.length)})
				}
			}

			fmt=fmt.replace(/y+/g, function(t){return (d1.getFullYear()+'').substr(4-t.length)});

			return fmt.replace(/A[/]P/g, (HH<12?'A':'P')+'M').replace(/上[/]下午/g, (HH<12?'上':'下')+'午')
				.replace(/D{3}/g, (d1+'').split(' ')[0]).replace(/D/g, week[d1.getDay()+''])
				.replace(/q/g, Math.ceil(MM/3)).replace(/O{3}/g, (d1+'').split(' ')[1]);
		}
		return d1
	},
	reg: function(t){
		var o={
			"M":"[个個]?(Month|月)",
			"S":"(milli|ms|毫秒)",
			"s":"[s|秒]",
			"m":"(m|分[钟鐘]?)",
			"H":"[个個]?[h小]",
			"d":"[d天]",
			"w":"[个個]?[w周星禮礼]",
			"y":"[y年]",
			}, n={"〇":0,"零":0,"日":0,"天":0,"元":1,"一":1,"二":2,"三":3,"四":4,"五":5,"六":6,"七":7,"八":8,"九":9}, M={
			"Jan":1,"Feb":2,"Mar":3,"Apr":4,"May":5,"Jun":6,"Jul":7,"Aug":8,"Sep":9,"Oct":10,"Nov":11,"Dec":12}, D={
			"Sun":0,"Mon":1,"Tue":2,"Wed":3,"Thu":4,"Fri":5,"Sat":6
		};

		var s=t.replace(/公元/g,'').replace(/(周+|期|拜)[天日一二三四五六七八九]/, function(t){return t+' '}).trim()
				.replace(/一十/g,'十').replace(/廿/g,'二十').replace(/卅/g,'三十')
				.replace(/[二三四五]十[一二三四五六七八九]/g, function(t){return ''+n[t.substr(0,1)]+n[t.substr(-1)]})
				.replace(/[二三四五]十/g, function(t){return ''+n[t.substr(0,1)]+0})
				.replace(/十[一二三四五六七八九]/g, function(t){return '1'+n[t.substr(-1)]})
				.replace(/十/g,'10').replace(/[〇零元一二三四五六七八九]/g, function(t){return n[t]})
				.replace(/[13]刻/, function(t){return 15*Number(t.substr(0,1))+'分钟'});

		if(/[点點时時]/.test(s) && !/小/.test(s)){s=s.replace(/[点點时時分]/g,':').replace(/半/g,'30').replace(/整/g,'00')
			.replace(/[13]刻/g, function(t){return 15*n[t.substr(0,1)]}).replace(/毫?秒/g,'');
		}

		s=s.replace(/半/g, 0.5);

		var tim=(s.match(/[01]?\d(:[0-5]?\d)+/) || [''])[0];
		if(!tim & /[01]?\d:/.test(s)){tim=s.match(/[01]?\d:/)[0]+'00'}
		if(tim && /下午|晚|夜|PM/i.test(s)){tim=tim.replace(/^\d+/, function(t){return Number(t)<12?Number(t)+12:t})}

		var yr=(s.match(/\d{4}/)||[''])[0], ys, ms, ds;
		if(yr){s=s.replace(/\d{4}[-年/]?/,'')}
//console.log(yr);
		var Md=(s.match(/[01]?\d[-月/][0-3]?\d/)||s.match(/\d+(\.\d+){2}/)||[''])[0]
				.replace(/\d+\./,'').split(/\D+/),
			MM=Md[0], dd=Md.length>1?Md[1]:0, tdy=/[今本当當这這]1?[天日]|(to|this )day/i.test(s);
//console.log(Md, MM, dd, tdy);
		if(yr && !MM){
			//.replace(/[月\./]/g,'-').split('-')
			if(/\d{8}/.test(t)){
				Md=t.match(/\d{8}/)[0].substr(4);
				MM=Md.substr(0,2); dd=Md.substr(2);
			}else{
				Md=(t.match(/\d{4}[-年/\.][01]?\d[-月/\.][0-3]?\d?/)||[''])[0].replace(/^\d+\D+/,'').split(/\D+/);
				MM=Md[0]; dd=Md.length>1?Md[1]:0;
			}
		}
//console.log(Md, MM, dd);
		if(/[前去明后後]年|(last|next) year|yesteryear/i.test(s)){
			ys=(/大.年/.test(s)?3:(/[前后後]年|year (before|after)/i.test(t)?2:1))*
				(/[前去]年|year before|(last |yester)year/i.test(s)?-1:(/[明后後]年|year after|next year/i.test(s)?1:0));
			if(tdy){
				return Time.lastDate('yyyy-MM-dd ', ys+'y')+tim;
			}
			yr=Time.lastDate('yyyy', ys+'y');
		}
//console.log(ys, yr);
		if(/[上下本当當这這][个個]?月|(last|next|this) month/i.test(s)){
			ms=(/[上下]{2}.?月|month (before|after)/i.test(s)?2:1)*
				(/[上]1?[个個]?月|month before|last month/i.test(s)?-1:(/[本当當这這]1?[个個]?月/.test(s)?0:1));
			if(tdy){
				return Time.lastDate('yyyy-MM-dd ', ms+'M')+tim;
			}
			MM=Time.lastDate('MM', ms+'M');

			dd=(s.match(/[0-3]?\d[日号號]/)||[''])[0];
			if(dd){return (yr||Time.lastDate('yyyy'))+'-'+MM+'-'+dd+' '+tim}
		}

		if(/([上下本这這]1?[个個]?)?(周+|星期|[禮礼]拜)[天日\d]/.test(s)){
			ds=Number(s.split(/周+|星期|[禮礼]拜/)[1].substr(0,1).replace(/天|日/,0))-n[Time.lastDate('D')]+
				7*(/[上下]{2}.?(周+|星期|[禮礼]拜)/.test(s)?2:1)*
				(/上1?[个個]*(周+|星期|[禮礼]拜)/.test(s)?-1:(/下1?[个個]*(周+|星期|[禮礼]拜)/.test(s)?1:0))
			return Time.lastDate('yyyy-MM-dd ', ds+'d')+tim;
		}

		if(/last|next|this/.test(s)){
			for(var k in D){
				var Dk=D[k], r=new RegExp(k, 'i');
				if(r.test(s)){
					ds=Dk-n[Time.lastDate('D')]+
						7*(/last/i.test(s)?-1:(/next/i.test(s)?1:0))
					return Time.lastDate('yyyy-MM-dd ', ds+'d')+tim;
				}
			}

		}

		if(/[前昨明后後]天|yesterday|tomorrow/i.test(s)){
			ds=(/大.天/.test(s)?3:(/[前后後]天|day (before|after)/i.test(t)?2:1))*
				(/[前昨]天|day before|yesterday/i.test(s)?-1:1);
			return (yr||Time.lastDate('yyyy'))+'-'+Time.lastDate('MM-dd ', ds+'d')+tim;
		}
		if(tdy){return (yr||Time.lastDate('yyyy'))+'-'+(MM||Time.lastDate('MM'))+'-'+Time.lastDate('dd')+' '+tim}

		if(!MM){

			for(var k in M){
				var Mk=M[k], r=new RegExp(k, 'i');
				if(r.test(s)){

					MM=Mk;
					var tmp=s.split(r);
					tmp[0]=tmp[0].split(/\D+/);
					if(tmp[0].length>1){tmp[0]=tmp[0][tmp[0].length-2]}else{tmp[0]=tmp[0][0]}
					tmp[1]=tmp[1].split(/\D+/);
					if(tmp[1].length>1){tmp[1]=tmp[1][1]}else{tmp[1]=tmp[1][0]}

					dd=tmp[0]||tmp[1]||0;
					if(dd){return (yr||Time.lastDate('yyyy'))+'-'+MM+'-'+dd+' '+tim}
				}
			}
		}

		if(MM || dd){return (yr||Time.lastDate('yyyy'))+'-'+(MM||Time.lastDate('MM'))+'-'+(dd||'01')+' '+tim}



		for(var k in o){
			var ok=o[k], r=new RegExp('\\d *'+ok, 'i'), nA=s.split(/[^\d\.]+/);

			if(r.test(s)){

				return Time.lastDate('yyyy-MM-dd HH:mm:ss', (/later|后|後/i.test(s)?'':'-')+
					Number(nA[0]||nA[1])+k);

			}
		}

		return (yr||Time.lastDate('yyyy'))+'-'+Time.lastDate('MM-dd ')+tim;
	},
	local: function(d, timOrDat){
		if(d && timOrDat){return timOrDat=='Date'?d.toLocaleDateString():d.toLocaleTimeString()}
		return (d || new Date()).toLocaleString().replace(/:00$/,'').replace(/:00 /,' ');
	},
	lite: function(d){var n=new Date();
		if(d.getFullYear()!=n.getFullYear()){return d.toLocaleDateString()}
		var today=Date.parse((new Date()).toDateString());

		if(d>=today-3600*1000*24 && d<today){return gM('yesterday')}

		if(d>today && d<Date.parse(Time.reg('tomorrow'))){
			return d.toLocaleTimeString().replace(/:\d+$/,'').replace(/:\d+ /,' ');
		}
		return d.toLocaleDateString().replace(/\D*20\d+\D*/,'');

	},
	str2date: function(s, func, timOrDat){
		var tm=new Date();
		tm.setTime(Date.parse(s));
		if(func){
			if(timOrDat){return Time[func](tm, timOrDat)}
			return Time[func](tm);
		}
		return tm;
	}
};

function noti(icon,title,text,type,json,cb){
	var opt={"iconUrl":icon||'img/ZIG.png',"title":title||gM('Zname'),"message":text,"type":type||'basic',"isClickable":true};
	//type:basic image list progress
	if(json){$.extend(opt,json)}
	chrome.notifications.create('123',opt,function(d){});
	chrome.notifications.onClicked.addListener(function (d){if(d=='123'){chrome.notifications.clear('123',cb)}});
}

function jSon(str){
	//return JSON.parse(str);
	//return eval('('+ Str +')');
	//return (new Function("return " + str))();
	var Str=(str||'').trim();
	try{
		return JSON.parse(str);
	}catch(e){
	//	console.log(str);
	//	console.log(e);
		if(/^\{/.test(Str) && /\}$/.test(Str)){
			var str0=Str.replace(/^\{|\}$/g,'').replace(/,([^":]*):/g, ',"$1":').replace(/^([^":]*):/g, '"$1":');

			return JSON.parse('{'+str0+'}');
		}
		return Str;
	}
}

function jSon2str(json){ //json is an array
	var str='[';
	for(var i in json){
		var tmp='{';
		$.each( json[i], function(j, n){
		    tmp += '"' + j + '":"' + n + '",';
		});
		str += tmp.replace(/,$/, '') + '},';
	}
	return str.replace(/,$/, '') + ']';
}

function urlTran(urls){
	var tArr= urls.match(/\S+/gi);
	if(!tArr){return ''}

	var patt0 = /^.+\[\*[^(\[\*)]+\].*$/;
	var patt1 = /\[\*[^(\[\*)]+\]/;	//[* ]
	var patt2 = /\d+-\d+/;
	//var patt3 = new RegExp(/\D+-\D+/);
	var patt4 = /^0\d+/;
	var patt5 = /[1-9]\d*/;
	var patt6 = /\[>\d*\]/;
	for (var i in tArr){
		var u=tArr[i];
		imgPreRe.lastIndex=0;
		if(!imgPreRe.test(u)){u=H+u; tArr[i]=u}
		if(patt0.test(u)){
			var tmpStr0 = u.match(patt1);	//[* ]
			var tmpAry3 = tmpStr0[0].substr(2,tmpStr0[0].length-3).split(',');	//a-z,2,4,6-9
			for (var k in tmpAry3){
				if(tmpAry3[k].indexOf('-')==-1) {continue}
				var s0 =tmpAry3[k].split('-')[0];
				var s1 =tmpAry3[k].split('-')[1];
				var tmpStr = s0;

				if(patt2.test(tmpAry3[k])) {	//02-45
					if(patt4.test(s0)) {		//00-18
						var t=1;
						if(patt5.test(s0)){t=parseInt(s0.match(patt5)[0])+1}
						for (var l = t; l<=parseInt(s1.match(patt5)[0]); l++) {
							tmpStr += ',' + '00000'.substr(0,s0.length - (''+l).length) + l;  //!no more than 5 '0's
						}
					}else{
						for (var l = parseInt(s0)+1; l<=parseInt(s1); l++) {tmpStr += ',' + l}
					}
				}else{
					for (var l = s0.charCodeAt(0)+1; l<=s1.charCodeAt(0); l++) {tmpStr += ',' + String.fromCharCode(l)}
				}
				tmpAry3[k]=tmpStr;
			}

			var tmpAry = tmpAry3.join(',').split(',');
			tArr[i]='';
			for (var k in tmpAry){
				tArr[i] += u.replace(tmpStr0[0],tmpAry[k]).replace(patt6,tmpAry[k]) + ' ';
			}
		}
	}
	var t=tArr.join(' ').match(/\S+/gi).join('\n');
	if(patt1.test(t)){return urlTran(t)}else{return t}
}

function hex2rgba(h,a,arr){
	var Arr=[parseInt(h.substr(1,2),16),parseInt(h.substr(3,2),16),parseInt(h.substr(5,2),16),Number(a)];
	if(arr){return Arr}
	return 'RGBA('+Arr.join(',')+')';
}
function rgb2hex(r,g,b){
	return '#'+(1<<24|r<<16|g<<8|b).toString(16).substring(1);
}
function bodyFocus(){
	$('body')[0].tabIndex=0;
	$('body').focus();
}
function run(s){
	console.log(s);
	chrome.extension.sendRequest({hope:'run',code:s});
}
function xhrcb(src,cb){
	//cb(length,type)
	var xhr=new XMLHttpRequest();
	xhr.open('HEAD', src, true);
	xhr.onerror = function(){
		xhr.abort();
	};
	xhr.onreadystatechange = function(){
	  if(xhr.readyState==4){
		if(xhr.status==200 || xhr.status==206){
			var s=xhr.getResponseHeader('Content-Length')||0,t=(xhr.getResponseHeader('Content-Type')||'').replace(/image[/]/,'').replace('x-icon','ico').replace(/[;\+].*/,'');
			cb(s,t);
			//console.log(xhr.getAllResponseHeaders());
			
		}
	  }
	};
	xhr.send();
}
var Admin={
	testAjax:function(t){$.ajax({type:'get', url:t, success:function(d){saveText(d, '123.txt')}})},
	testAjax2:function(t){$.ajax({type:'get', url:'', success:function(d){console.log($(d).find('').text())}})}
}, fCC=function(A){return String.fromCharCode.apply(null,A)
}, seqA=function(start,n,type,step){//序列
	var t=[],y=type||'arith',p=step==undefined?1:step;
	for(var i=0;i<n;i++){
		t.push(y=='arith'?start+i*p:(y=='geo'?start*Math.pow(p,i):''));
	}
	return t
}, seqsA=function(s){//缩写 ,~
	var t=s.replace(/\d+~\d+/g, function(t){var tA=t.split('~');return seqA(+tA[0],+tA[1]-(+tA[0])+1).join()})
		.replace(/[A-z]~[A-z]/g, function(t){var tA=t.split('~'),t0=tA[0].charCodeAt(0),t1=tA[1].charCodeAt(0);return fCC(seqA(t0,t1-t0+1)).split('').join()})
		.split(',')
	return t
}, Arri=function(A,i){//提取矩阵第i列（从0开始编号）
	var t=[];
	for(var j=0;j<A.length;j++){t.push(A[j][i])}
	return t
}, ArrI=function(A,a,start){//提取数组元素，按照索引集和起始偏移
	var t=[];
	for(var j=0;j<a.length;j++){t.push(A[a[j]-(start||0)])}
	return t
}, subMtrx=function(A,i1,i2,j1,j2){//提取矩阵的子矩阵，编号从1开始
	var t=[];//t.t='Mtrx'
	for(var i=i1-1;i<i2;i++){
		var ti=[];
		for(var j=j1-1;j<j2;j++){ti.push(A[i][j])}
		t.push(ti);
	}
	return t
}, Arrf=function(f,A,rtnTyp){
	var ty=rtnTyp||'arr',g=function(j,k){
		var t,ar=arguments;
		for(var i=1;i<ar.length;i++){
			if(ty=='arr'){//映射数组
				if(i==1){t=[]}
				t.push(f(ar[i]))
			}else if(ty=='str'){//字符串累加
				if(i==1){t=''}
				t+=f(ar[i])
			}else if(ty=='cp1'){//一元迭代
				if(i==1){t=[f(ar[i])]}else{
					t.push(f(t[t.length-1]))
				}
			}else if(ty=='cp2'){//二元迭代
				if(i==1){t=ar[i];continue}else{
					t=f(t,ar[i])
				}
			}
		}
		return t
	};return g.apply(null,[f].concat(A))

},cartestian=function(A){//笛卡尔乘积 序列化，二维
	var t=[A.slice(0)],n=A.length, tmp=new Array(n);
	for(var i=0;i<n;i++){//维数
		var Ai=A[i],Ain=Ai.length, AA=[];
		for(var j=0;j<t.length;j++){
			var tj=t[j], arr=Arrf(function(a){var tt=[].concat(tj);tt[i]=a; return tt},Ai);
			//arr.t='Set_Cartesian'; 注意concat会丢失arr.t信息
			AA=AA.concat(arr);
		}
		t=AA;
	}
	for(var i=0;i<AA.length;i++){
		AA[i].t='Set_Cartesian'
	}
	return t
},concat=function(){//数组中元素分别字符串拼接，得到新数组
	var ar=arguments, arl=ar.length, n=ar[0].length,t=[];
	for(var j=0;j<n;j++){
		var s='';
		for(var i=0;i<arl;i++){
			s+=ar[i][j]
		}
		t.push(s)
	}
	return t
}, Latin=function(t,caps){
	var f=function(i){var s=html2txt('&'+String.fromCharCode(i)+t+';'); if(/;/.test(s)){s=''} return s};
	return Arrf(f,seqA(65+32*(+!caps),26))
}, Options=function(A){
	var f=function(i){return i?'<option value="'+i+'">'+i+'</option>':''};
	return Arrf(f,A)
}, entity=ZLR('scr fr grave acute circ tilde uml ring opf');
