﻿var Auth={
tsina:{
	ico:'Weibo.ico',

	key:'1133462976',
	secr:'e7204b01889915d5cdb2ff91ff7a5beb',

	response:'response_type=code',

	grantUrl:Hs+'api.weibo.com/oauth2/authorize',
	callbUrl:Hs+'api.weibo.com/oauth2/default.html',
	tokenUrl:Hs+'api.weibo.com/oauth2/access_token',

	updateUrl:Hs+'api.weibo.com/2/statuses/update.json',
	uploadUrl:Hs+'upload.api.weibo.com/2/statuses/upload.json',
	sharePicUrl:H+'service.weibo.com/share/share.php', //api.weibo.com/2/statuses/upload_url_text.json

	weibosUrl:Hs+'api.weibo.com/2/statuses/user_timeline.json',
	repostUrl:Hs+'api.weibo.com/2/statuses/repost.json',

	userUrl:'weibo.com/',
	userInfoUrl:Hs+'api.weibo.com/2/users/show.json',
	logoutUrl:'weibo.com/logout.php'
},

tqq:{
	ico:'tqq.gif',

	key:'801404261',
	secr:'214ce202dfcf3c04071e7812e56becf7',

	response:'response_type=token',

	grantUrl:Hs+'open.t.qq.com/cgi-bin/oauth2/authorize', //H+'open.t.qq.com/cgi-bin/authorize',
	callbUrl:HOM.ZIG,
	tokenUrl:Hs+'open.t.qq.com/cgi-bin/oauth2/access_token',

	updateUrl:H+'open.t.qq.com/api/t/add',
	uploadUrl:Hs+'open.t.qq.com/api/t/add_pic',
	sharePicUrl:Hs+'open.t.qq.com/api/t/add_pic_url',

	userUrl:'t.qq.com/',
	logoutUrl:'t.qq.com/logout.php'
},

t163:{
	ico:'t163.gif',

	key:'78JugW1nGMG47X1z',
	secr:'D8kNbHYuzgIINeykysML6UARqkKv94jU',

	response:'response_type=token',

	grantUrl:Hs+'api.t.163.com/oauth2/authorize',
	callbUrl:H+'www.baidu.com',
	tokenUrl:Hs+'api.t.163.com/oauth2/access_token',

	updateUrl:Hs+'api.t.163.com/statuses/update.json',
	uploadUrl:Hs+'api.t.163.com/statuses/upload.json',

	userUrl:'t.163.com/',
	logoutUrl:'reg.163.com/Logout.jsp?username=&url='+H+'t.163.com'
},

douban:{
	ico:'Douban.ico',

	key:'0dd41f9f32ae2c80297c5edd980a8580',
	secr:'30bf37d4298d12fa',

	response:'response_type=code',

	grantUrl:Hs+'www.douban.com/service/auth2/auth',
	callbUrl:H+'www.baidu.com',
	tokenUrl:Hs+'www.douban.com/service/auth2/token',

	updateUrl:Hs+'api.douban.com/shuo/v2/statuses/',
	uploadUrl:Hs+'api.douban.com/shuo/v2/statuses/',
	sharePicUrl:Hs+'api.douban.com/shuo/v2/statuses/',

	userUrl:'www.douban.com/people/',
	logoutUrl:'www.douban.com/accounts/logout'
},

renren:{ //213079
	ico:'renren.ico',

	key:'aeaed3d9245b449094eb8d7fe0691713',
	secr:'07afb062c75e4cd99134da01d11e5031',

	response:'response_type=code',

	grantUrl:Hs+'graph.renren.com/oauth/grant',
	callbUrl:H+'graph.renren.com/oauth/login_success.html',
	tokenUrl:Hs+'graph.renren.com/oauth/token',

	apiUrl:H+'api.renren.com/restserver.do',

	userUrl:'www.renren.com/',
	logoutUrl:'www.renren.com/Logout.do' //?requesttoken=1067576001
},

fb:{
	ico:'Facebook.png',

	key:'227606350619834',
	secr:'752ac3e3349965fff057fe972685456e',

	response:'response_type=token',

	grantUrl:Hs+'www.facebook.com/dialog/oauth',
	callbUrl:Hs+'www.facebook.com/connect/login_success.html', //apps.facebook.com/zzllrrimager/
	tokenUrl:Hs+'graph.facebook.com/me',

	updateUrl:Hs+'graph.facebook.com/me/feed',
	uploadUrl:Hs+'graph.facebook.com/me/photos',
	sharePicUrl:Hs+'graph.facebook.com/me/feed',

	pageUrl:Hs+'graph.facebook.com/me/accounts',

	userUrl:'www.facebook.com/',
	logoutUrl:'www.facebook.com/logout.php'
}

}, Auth2={


vdisk:{
	ico:'vdisk.ico',

	key:'1176139638',
	secr:'8ed194a4c79cac0e10ee25a8af4a4ab2',

	response:'response_type=token',

	grantUrl:Hs+'auth.sina.com.cn/oauth2/authorize',
	callbUrl:H+'www.baidu.com',

	uploadUrl:Hs+'upload.openapi.vdisk.me/2/files/sandbox/<path>', //root: basic | sandbox

	folderUrl:Hs+'openapi.vdisk.me/2/fileops/create_folder',
	chkUrl:Hs+'openapi.vdisk.me/2/metadata/sandbox/<path>',
	thumbUrl:Hs+'openapi.vdisk.me/2/thumbnails/sandbox/<path>',

	userUrl:'vdisk.weibo.com/file/list',
	userInfoUrl:Hs+'openapi.vdisk.me/2/account/info',
	logoutUrl:'weibo.com/logout.php'
},

bd_disk:{
	ico:'Baidu.ico',

	key:'GqE8DmmBu6IlQza7fgszCWmW',
	secr:'Xntcf7xXKAvC5rEbZiAMGT7zhNBwoeTs',

	response:'response_type=token',

	grantUrl:Hs+'openapi.baidu.com/oauth/2.0/authorize',
	callbUrl:H+'www.baidu.com',

	uploadUrl:Hs+'pcs.baidu.com/rest/2.0/pcs/file',

	folderUrl:Hs+'pcs.baidu.com/rest/2.0/pcs/file',
	chkUrl:Hs+'pcs.baidu.com/rest/2.0/pcs/file',
	thumbUrl:Hs+'pcs.baidu.com/rest/2.0/pcs/thumbnail',

	userUrl:'pan.baidu.com/disk/home',
	userInfoUrl:Hs+'pcs.baidu.com/rest/2.0/pcs/quota',
	logoutUrl:'passport.baidu.com/?logout'
},

g_drive:{
	ico:'Gdrive.ico',

	key:'897844280706.apps.googleusercontent.com',
	secr:'-nPizEwe0lQ98H9l_qdzI4_k',

	response:'response_type=token',

	grantUrl:Hs+'accounts.google.com/o/oauth2/auth',
	callbUrl:H+'www.baidu.com',

	uploadUrl:Hs+'www.googleapis.com/upload/drive/v2/files',

	folderUrl:Hs+'www.googleapis.com/drive/v2/files',
	chkUrl:Hs+'www.googleapis.com/drive/v2/files',
	thumbUrl:'',

	userUrl:'drive.google.com/',
	userInfoUrl:Hs+'www.googleapis.com/drive/v2/about',
	logoutUrl:'www.google.com/accounts/Logout'
},

renren:{
	ico:'renren.ico',

	key:'aeaed3d9245b449094eb8d7fe0691713',
	secr:'07afb062c75e4cd99134da01d11e5031',

	response:'response_type=code',

	grantUrl:Hs+'graph.renren.com/oauth/grant',
	callbUrl:H+'graph.renren.com/oauth/login_success.html',
	tokenUrl:Hs+'graph.renren.com/oauth/token',

	apiUrl:H+'api.renren.com/restserver.do',

	userUrl:'www.renren.com/',
	userInfoUrl:'',
	logoutUrl:'www.renren.com/Logout.do' //?requesttoken=1067576001
},

fb:{
	ico:'Facebook.png',

	key:'227606350619834',
	secr:'752ac3e3349965fff057fe972685456e',

	response:'response_type=token',

	grantUrl:Hs+'www.facebook.com/dialog/oauth',
	callbUrl:Hs+'www.facebook.com/connect/login_success.html', //apps.facebook.com/zzllrrimager/
	tokenUrl:Hs+'graph.facebook.com/me',

	uploadUrl:Hs+'graph.facebook.com/me/photos',

	folderUrl:'',

	pageUrl:Hs+'graph.facebook.com/me/accounts',

	userUrl:'www.facebook.com/',
	userInfoUrl:'',
	logoutUrl:'www.facebook.com/logout.php'
}


/*
,

dropbox:{
	ico:'dropbox.png',

	key:'rq9cfrp08ewanle',
	secr:'j61q5miws8yvlei',

	response:'',
	grantUrl0:Hs+'api.dropbox.com/1/oauth/request_token',
	grantUrl:Hs+'www.dropbox.com/1/oauth/authorize',
	callbUrl:H+'www.baidu.com',
	tokenUrl:'',

	updateUrl:'',
	uploadUrl:Hs+'api-content.dropbox.com/1/files/sandbox/<path>', //root = sandbox  | dropbox.
	sharePicUrl:'',

	folderUrl:Hs+'api.dropbox.com/1/fileops/create_folder',
	chkUrl:Hs+'api.dropbox.com/1/search/sandbox/<path>',
	thumbUrl:Hs+'api-content.dropbox.com/1/thumbnails/sandbox/<path>',

	userUrl:'www.dropbox.com/home/',
	userInfoUrl:Hs+'api.dropbox.com/1/account/info',
	logoutUrl:'www.dropbox.com/logout'
},

immio:{
	ico:'imm.io.ico',

	key:'',
	secr:'',

	response:'response_type=code',

	grantUrl:'',
	callbUrl:'',
	tokenUrl:'',

	updateUrl:'',
	uploadUrl:H+'imm.io/store/',
	sharePicUrl:'',

	userUrl:'imm.io/',
	logoutUrl:''
},

imgur:{
	ico:'imgur.ico',

	key:'741d5599487cdbf',
	secr:'efb0100a88260501a43038d06f559814bc7dcfa4',

	response:'response_type=code',

	grantUrl:Hs+'api.imgur.com/oauth2/authorize',
	callbUrl:H+'www.baidu.com',
	tokenUrl:Hs+'api.imgur.com/oauth2/token',

	updateUrl:'',
	uploadUrl:Hs+'api.imgur.com/3/image', //api.imgur.com/3/upload
	sharePicUrl:Hs+'api.imgur.com/3/image',

	userUrl:'',
	logoutUrl:''
}
*/


};

Auth.renrenPg=Auth.renren;
Auth.fbPg=Auth.fb;

var auth={
grant:function(i,Up){
	var tWs=jSon(L[Up?'upld':'weib']), Ath=Up?Auth2:Auth, w=tWs[i],t=w.site, a=Ath[t];
	var url=a.grantUrl+'?'+a.response+'&client_id='+a.key+'&redirect_uri='+a.callbUrl;
	if(t=='tsina'){url += '&forcelogin=true'}
	if(t=='renren'){url += '&scope=status_update+publish_feed+publish_blog+photo_upload+create_album'}
	if(t=='renrenPg'){url += '&scope=admin_page'} //www.renren.com/601132562
	if(/fb/.test(t)){url += '&scope=publish_stream'; if(t=='fbPg'){url += ',manage_pages'}}
	if(t=='bd_disk'){url += '&scope=netdisk'}
	if(t=='vdisk'){}
	if(t=='g_drive'){url += '&scope='+Hs+'www.googleapis.com/auth/drive&approval_prompt=force'} //.file +www.googleapis.com/auth/userinfo.profile &state=&approval_prompt=auto(force)

	if(t=='dropbox'){

	}

	//window.open(url);
	//console.log(url);

	chrome.tabs.getSelected(null,function(Tab){
		L.OutTabId=Tab.id;
		chrome.tabs.create({index: Tab.index+1, url: url, active: true},function(TAb){
			chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, TAB){
				if(tabId==TAb.id && changeInfo.url){
					if(changeInfo.url.indexOf('access_token=')>0){

						var token=changeInfo.url.split('access_token=')[1];
					//	console.log('#access_token=');
					//	console.log(token);

						if(/fb/.test(t) || /t163|tqq|baidu|vdisk|bd_disk|g_drive/.test(t)){

							w.token=token.split('&')[0];
							w.end=(new Date()).getTime()+Number(changeInfo.url.split('expires_in=')[1].split('&')[0])*1000;

							if(t=='tqq'){
								w.info=escape('{"openid":"'+token.split('openid=')[1].split('&')[0]+
									'","openkey":"'+token.split('openkey=')[1].split('&')[0]+'"}');
								w.id=token.split('name=')[1].split('&')[0];
								w.name=escape(token.split('nick=')[1].split('&')[0]);
							}



							L[Up?'upld':'weib']=jSon2str(tWs);

							if(Up){
								loadupld();
							}else{
								loadweib();
							}

							chrome.tabs.remove(TAB.id);

							chrome.tabs.update(Number(L.OutTabId), {active:true}, function(){




								if(Up){
									clk_popout($('#btnUpload')[0]);
								}else{
									if(t=='t163' || t=='tqq'){
										$('#tileWeib').trigger('click');
									}

									if(/fb/.test(t)){
										if(/Pg/.test(t)){auth.page(i)}else{auth.token(i,Up)}
									}
								}
							});
						}
					}

					if(changeInfo.url.indexOf('code=')>0){

						var code=changeInfo.url.split('code=')[1];
					//	console.log('&code=');
					//	console.log(code);

						if(/tsina|renren|t163|douban/.test(t)){w.code=code}

						L[Up?'upld':'weib']=jSon2str(tWs);

						if(Up){
							loadupld();
						}else{
							loadweib();
						}
						chrome.tabs.remove(TAB.id);

						chrome.tabs.update(Number(L.OutTabId), {active:true}, function(){
							auth.token(i,Up);
						});
					}
				}
			});
		});
	});

},

token:function(i,Up){
	var tWs=jSon(L[Up?'upld':'weib']), Ath=Up?Auth2:Auth, w=tWs[i],t=w.site, tdata, f=t=='fb', a=Ath[t], url=a.tokenUrl;
	if(/douban/.test(t)){
		tdata={grant_type:'authorization_code',client_id:a.key,client_secret:a.secr,redirect_uri:a.callbUrl,code:w.code};
	}else if(f){
		tdata={access_token:w.token};
	}else{
		tdata={};
		url += '?grant_type=authorization_code&client_id='+a.key +'&client_secret='+a.secr +'&redirect_uri='+a.callbUrl +'&code='+w.code;
	}
	//console.log(url);

	var ajax={
		type: (f?'GET':'POST'),
		url:url,
		data:tdata,
		error:function (xhr, status, error){
			if(Up){
				upldResult(2, error||status);
			}else{
				tileWeibResult(i,status||error,2);
			}
		},
		success:function (data, status){
		//	console.log('success data:');
		//	console.log(data);

			if(/douban|tsina/.test(t)){
				var Data=JSON.parse(data);
				w.token=Data.access_token;
				w.end=(new Date()).getTime()+Number(Data.expires_in)*1000;
				if(t=='douban'){w.id=Data.douban_user_id}else{w.id=Data.uid}
			}

			if(/renren/.test(t)){
				//L[t+'Scope']=data.scope; //photo_upload publish_blog status_update create_album publish_feed admin_page
				w.token=data.access_token;
				w.end=(new Date()).getTime()+Number(data.expires_in)*1000;

				if(/renren/.test(t)){
					var u=data.user;
					w.id=u.id;
					w.name=escape(u.name);
				}
			}
			if(/fb/.test(t)){
				w.id=data.id;
				w.name=escape(data.name);
			}




			L[Up?'upld':'weib']=jSon2str(tWs);
			if(Up){
				loadupld();
				$('#btnUpload').click();
			}else{
				loadweib();
				if(/Pg/.test(t)){auth.page(i)}else{$('#tileWeib').trigger('click')}
			}

		}
	};
	if(f){ajax.dataType='json'}
	$.ajax(ajax);
},

page:function(i,upl){
	var tWs=jSon(L.weib), w=tWs[i],t=w.site,  a=Auth[t], url=a.pageUrl || a.apiUrl, tdata, r=t=='renrenPg', f=t=='fbPg';

	if(r){tdata={access_token:w.token,method:'pages.getManagedList',v:'1.0',format:'JSON'};
		var kv=[];
		for(var k in tdata){kv.push([k+'='+tdata[k]])}
		kv.sort();
		tdata.sig=hex_md5(kv.join('')+a.secr);
	}

	if(f){tdata={access_token:w.token}}
	//console.log(url);
	//return false;

	var ajax={
		type: (f?'GET':'POST'),
		url:url,
		data:tdata,
		error:function (xhr, status, error){
			tileWeibResult(i,status||error,2);
		},
		success:function (data, status){
			//console.log('Page data:');
			//console.log(data);
			var d=(r?JSON.parse(data):data.data);
			for(var j=0;j<d.length;j++){
				var dj=d[j];
				//console.log(dj);
				// headurl desc classification fans_count is_fan
				// category

				if(f && dj.category!='App page'){continue} // 'Application' cause error when publish feed

				if(j>0){tWs.splice(i,0,JSON.parse(jSon2str([w]))[0])}

				tWs[i].id=dj[(r?'page_':'')+'id'];
				tWs[i].name=escape(dj.name);
				if(f){tWs[i].token = dj.access_token}

			}
			L.weib=jSon2str(tWs);
			loadweib();
			$('#tileWeib').trigger('click');
		}
	};
	if(f){ajax.dataType='json'}
	$.ajax(ajax);
}
};
