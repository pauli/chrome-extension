/****************************************************************************
 * WebXDK - Web Browser Extension SDK
 *
 * Requires jQuery internally but you don't have to use it yourself.
 * TODO: port so that jQuery is no longer required.
 ****************************************************************************/

;(function($) { // Semicolon coz https://github.com/mootools/slick/wiki/IIFE
  
// Legacy
window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder;


/**********************************************************
 * Webx class
 */

var DEBUG = false;

/**
 * Constructor for Webx.
 * 
 * @param config {Object} configuration parameters, some required, most optional.
 * These config parameters are to be exposed for end-user extensions:
 * 
 * extendJQuerySelectors     {Boolean} Change jQuery selectors to accept '%post' selectors [default: false]
 * extendJQueryPseudoClasses {Boolean} Extend jQuery selectors to accept ':Xpost' [default: false]
 * extendQuerySelectors      {Boolean} Extend (Document|Element).querySelector(All|) to accept '%post' selectors
 *                             [default: false]
 * aliasAPI                  {Boolean} or {String} If set, aliases API functions to shorthand, by default
 *                             'X', so that you have access to $X('%post') for jQuery or X('%post') for DOM.
 *                             If set to string, then overrides 'X' with your choice
 *
 * strict                    {Boolean} If true, keys that are not (yet) mapped to selectors will generate
 *                             syntax exceptions from jQuery and querySelector(All|). If false, then the error
 *                             will be silent, and that part of the selectors will not match any elements,
 *                             e.g. '.top > div%post, .comment' will then only return elements that
 *                             match '.comment'
 *                             
 * These config parameters are for web-specific libraries and/or associated developer extensions:
 * 
 * warn                      {Boolean} If true, get additional warnings from extraction rules. [default: false]
 * debug                     {Boolean} If true, get debugging messages. [default: false]
 *
 * mapIdFunc:         {Function} Required function that computes the ID,
 * mappingRulesForId: {Function} Required function that extracts enough selectors to compute the ID,
 * mappingRules:      {Function} Required function that extracts all the rules
 * 
 * bundledMapFilepath   {String}
 * bundledRulesFilepath {String}
 * classNamesFilter  {RegExp} Optional, RegExp of class names added by the extension; similar to hard-coded
 *                     internal CLASSNAMES_FILTER = / gpme-\S+/g;
 * stylesheetsFilter {RegExp} Optional, RegExp of CSS text that will let Webx detect which stylesheets are
 *                     added by extensions and to be ignored; similar ot the hard-coded internal
 *                     WebxMap.STYLESHEETS_FILTER = /\.gpme-/;
 *                      
 * fallbackRemote:    {Boolean} If true, tries to download mappings in case of mapping ID mismatch
 * fallbackAutomap:   {Boolean} If true, tries to figure out mappings based on rules. fallbackRemote
 *                      takes precedeence over fallbackAutomap
 *
 * @param initCallback {Function} For convenience, this triggers the automatic calling of init()
 *   with the initCallback as argument.
 */
function Webx(config, initCallback) {
  // If this object is instantiated without a config, then this
  // is just the assignment to the prototype (e.g. of GPlusX)
  if (! config) {
    // Mode is either "normal" or "automap"
    this.mode = 'normal';
    
    return;
  }
  
  this.config = $.extend({
    
    storagePrefix: arguments.callee.caller.name,
    serializationPrefix: '__' + arguments.callee.caller.name + '__:'
  }, config);
  this.rules = {};
  
  // XXX Temporary until things settle down
  this.config.warn = true;
  this.config.debug = true;
  
  if (this.config.debug)
    DEBUG = true;
  
  this._createNonJQueryFunctions();
  
  // Create a new WxMap, which will most likely be overwritten by _readMapsFromBundledFile.
  this.newMap();
  
  // If the caller provided a callback, that means they want to init as a shortcut
  if (typeof initCallback == 'function')
    this.init(initCallback);
}

// Current version of Webx being used
Webx.version = '0.1.0';


// Class names to ignore because they've been added by extensions
// NOTE: This regex depends on the overall classname to be preceded by a space
var CLASSNAMES_FILTER = / gpme-\S+/g;
// Selector patterns for stylesheets to ignore because they come from extensions
var STYLESHEETS_FILTER = /\.gpme-/;

Webx.debug = function() {
  if (DEBUG) {
    var args = Array.prototype.slice.call(arguments);
    args.unshift('Webx:');
    console.debug.apply(console, args);
  }
};

Webx.info = function() {
  var args = Array.prototype.slice.call(arguments);
  args.unshift('Webx:');
  console.info.apply(console, args);
};

Webx.warn = function() {
  var args = Array.prototype.slice.call(arguments);
  args.unshift('Webx:');
  console.warn.apply(console, args);
};

Webx.error = function() {
  var args = Array.prototype.slice.call(arguments);
  args.unshift('Webx:');
  console.error.apply(console, args);
};

Webx._onFsApiError = function(e) {
  var msg = '';

  switch (e.code) {
    case FileError.QUOTA_EXCEEDED_ERR:
      msg = 'QUOTA_EXCEEDED_ERR';
      break;
    case FileError.NOT_FOUND_ERR:
      msg = 'NOT_FOUND_ERR';
      break;
    case FileError.SECURITY_ERR:
      msg = 'SECURITY_ERR';
      break;
    case FileError.INVALID_MODIFICATION_ERR:
      msg = 'INVALID_MODIFICATION_ERR';
      break;
    case FileError.INVALID_STATE_ERR:
      msg = 'INVALID_STATE_ERR';
      break;
    default:
      msg = 'Unknown Error';
      break;
  }

  Webx.error('file error', msg);
};

// Cache of cssRuleWrappers for the given key.
// This depends on the page's CSS stylesheets.
Webx.cssRuleWrappersForKey = {};

/**
 * Creates non-jQuery equivalents of function.
 * To be used by Webx's subclass after defining its jQuery-based
 * API.
 */
Webx.createNonJQueryFunctions = function() {
  var object = this;
  for (var i in object.prototype) {
    if (object.prototype.hasOwnProperty(i) && typeof object.prototype[i] == 'function') {
      var fn = object.prototype[i];
      var fnName = object.prototype[i].toString();
      if (fnName.charAt(fnName.length - 1) === '$') {
        (function(fn) {
          object.prototype[fnName.substring(0, fnName.length - 1)] = function() {
            return fn.apply(this, arguments).get();
          };
        })(fn);
      }
    }
  }
};


/**
 * Prepare caches
 */
(Webx._newCaches = function() {
  Webx.caches = {
    // We leave classNameToCssRuleWrappers undefined until we create the cache
    // because it's done all in one go.'
  };
}).call(Webx);


Webx.prototype = {

  /**************************************************************************
   * Init
   **************************************************************************/

  /**
   * Initializes Webx by loading local JSON mappings and storing in localStorage.
   * @param callback {Function} called when initialization is finished (successfully or not)
   * @return {Promise}
   */
  init: function(callback) {
    Webx.debug('Initializing...');
    var _this = this;
    var result = false;

    // Read in the rules
//    _this._readRulesFromBundledFile(function() {
      // Read in mappings from local JSON and stores in local storage
      return _this._readMapsFromBundledFile().promise().done(function(wxMaps) {
        // Write maps to localStorage
        _this._writeMapsToLocalStorage(wxMaps);
        
        var id = _this._getIdFromPage();
        // Pick out the wxMap with a matching ID
        if (id) {
          // Take the mapping from the bundled file that fits the page
          if (wxMaps && wxMaps[id])
            result = _this.newMap(wxMaps[id]);
          else
            // Or, read in the mapping from localStorage that fits the page
            result = _this._newMapFromLocalStorage();

          if (result) {
            _this._exposeAPI();
            Webx.debug('Done initializing.');
          } else {
            Webx.error('Initialization failed because mapping not found in bundled file and' +
              ' cannot be read from localStorage.');
          }
        } else {
          Webx.error('Initialization failed because mapping ID cannot be extracted from page.');
        }

        _this.dumpToConsole('After reading from bundled & localStorage');
        
        // Testing capabilities of Slick.
        // Slick seems to be able to parse almost all Sizzle selectors
        // as specified in https://github.com/jquery/sizzle/wiki/Sizzle-Home
        // - Doesn't support namespaces.
        // - Doesn't support Sizzle's complex :not()
//        Webx.debug(Webx.Slick.parse('::root ns|p > tag.class::after')); // Doesn't work: no ns support'
//        Webx.debug(Webx.Slick.parse('::root div:after, #id.class tag.class1[attr=".value"].class2 , *+:not(.notclass2)>.class3:not( .notclass1 ):not(#notid1)'));      
//        Webx.debug(Webx.Slick.parse('.foo[id="adsf\\"adfdsf"][attr=value][not!=equal]:parent:input:eq(0):not(.bar.foo):contains(text)'));
//        Webx.debug(Webx.Slick.parse('.foo.foo2%mything[id="adsf\\"adfdsf"][attr=value]%mything2[not!=equal]:parent:input:eq(0):not(.bar.foo):contains(text)'));     
//        ['', '*', 'tag', '#id', '.class', '[attr]', ':eq(0)', '::after', ':not(a)', ':not(a:not(b > d) > c, d)',
//         ',*', 'tag#id1#id2', '.class1.class2.class\\+split', '[attr!=value][attr="value"][attr="quoted\\"value"]',
//         ':input::contains("quo\\"ted").class', ':contains(quo\\(\\)ted)', ':contains("quo)ted")', ':contains(quo\\)ted)', ':contains("quo\\)ted")',
//         '*>*,tag.class::after%gbar[attr=value]%post:not(* > div%postIsSelected, %gplusBar)',
//        ].
//         forEach(function(query) {
//          Webx.debug('Original:', query);
//          Webx.debug('Processed:', _this.wxMap.getNormalizedQuery(query));
//          Webx.debug(Webx.Slick.parse(query));
//         });
      }).fail(function() {
        
        // We can't read from the bundled file but we can try reading from local storage
        if (_this._newMapFromLocalStorage()) {
          _this._exposeAPI();
          Webx.debug('Done initializing after falling back to localStorage.');
        } else {
          Webx.error('Initialization failed.');
        }
      }).always(function() {
        if (typeof callback == 'function')
          callback();
      });
//    });
  },

  /**
   * Creates new mapping.
   * @param map {WebxMap} Optional WebxMap to use
   */
  newMap: function(wxMap) {
    Webx.debug("Creating new map" + (wxMap ? " from the one passed-in" : "") + ".");
    this.wxMap = wxMap ? wxMap : new WebxMap(this, this.config);
    this.map = this.wxMap.map; // Alias. Remember that map belongs to WebxMap

    // Reset the selector lookup table
    delete Webx.simpleSelectorLookup;
    
    return this.wxMap;
  },

  /**
   * Creates new mapping from local storage matching the page
   */
  _newMapFromLocalStorage: function() {
    var wxMap = new WebxMap(this, this.config);
    if (wxMap.readFromLocalStorage(this._getIdFromPage())) {
      return this.newMap(wxMap);
    } else {
      return null;
    }
  },
  
  /**************************************************************************
   * API for developer extension, e.g. Gplusx
   **************************************************************************/

  /**
   * Surveys all existing rules and generates dependencies.
   * As a side effect, creates a new map and auto-maps as much as possible.
   * This API function is for developer extensions, not user-facing extensions.
   */
  surveyRules: function() {
    Webx.debug("Surveying rules...");
    this.rules = {};
    try {
      this.newMap().surveyRules(this.rules);
    } finally {
      // Clear the caches to free up memory
      Webx._newCaches();
    }
    this.wxMap.writeToLocalStorage();
    Webx.debug("Done surveying rules.");
    return this.rules;
  },

  /**
   * Writes rules to FileSystem API file.
   * This will only work if your extension has the 'unlimitedStorage' permission.
   */
  writeRulesToFile: function(filename, callback) {
    this._writeToFile(this._rulesToString(), filename, callback);
  },

  /**
   * Writes map to FileSystem API file.
   */
  writeMapToFile: function(filename, callback) {
    Webx.debug("Writing map to file: ", this.wxMap.toString());
    this._writeToFile(this.wxMap.toString(), filename, callback);
  },
  
  dumpToConsole: function(msg) {
    msg = msg ? msg + ': ' : '';
    Webx.debug(msg + "rules=", this.rules);
    Webx.debug(msg + "s=", this.map.s);
    Webx.debug(msg + "sss=", this.map.sss);
    Webx.debug(msg + "sAlt=", this.map.sAlt);
    Webx.debug(msg + "c=", this.map.c);
    Webx.debug(msg + "cAlt=", this.map.cAlt);
    Webx.debug(msg + "cStyled=", this.map.cStyled);
  },

  /**************************************************************************
   * I/O
   **************************************************************************/

  /**
   * Reads maps from file.
   * @param filepath {String} Optional to override system default
   *   which is set for a user-facing extension.
   * @param {Function} callback. Optional function to call which will
   *   be passed an array of WebxMaps from the URL.  You could just
   *   read from localStorage, but this bypasses the localStorage
   *   in case there are errors, e.g. out of quota.
   *   I recommend you use the return value instead of this parameter
   *   and specify callbacks using done() or pipe()
   * @return {jQXHR} which implements the Promise interface.
   */
  _readMapsFromBundledFile: function(filepath, callback) {
    Webx.debug("Reading maps from bundled file.");
    if (typeof filepath != 'string')
      filepath = this.config.bundledMapFilepath;

    return this._readMapsFromURL(chrome.extension.getURL(filepath)).pipe(function(wxMaps) {
      if (typeof callback == 'function')
        callback(wxMaps);
      return wxMaps;
    });
  },

  /**
   * Reads multiple maps from URL
   * @param {Function} callback. Optional function to call which will
   *   be passed an array of WebxMaps from the URL.  You could just
   *   read from localStorage, but this bypasses the localStorage
   *   in case there are errors, e.g. out of quota.
   *   I recommend you use the return value instead of this parameter
   *   and specify callbacks using done() or pipe()
   * @return {jqXHR} which implements the Promise interface.
   */
  _readMapsFromURL: function(url, callback) {
    var _this = this;
    
    var deferred;
    
    var chromeVersion = navigator.appVersion.match(/Chrome\/([^\.\s]+)/)[1];
    
    // For older versions of Chrome, we have to get the data from the background
    if (chromeVersion && chromeVersion < 13) {
      deferred = $.Deferred(function(df) {
        chrome.extension.sendRequest({
            action: 'webxAjax',
            url: url,
            dataType: 'json'
          }, function(data) {
            if (data)
              df.resolve(data);
            else
              df.reject();
          });
      });
    } else { // For later Chrome, we read it ourselves
      deferred = $.getJSON(url);
    }
    
    return deferred.pipe(function(data) {
      var wxMaps = {};
      for (var id in data) {
        if (data.hasOwnProperty(id)) {
          var wxMap = new WebxMap(_this, _this.config);
          wxMap.fromCompactJSON(data[id]);
          Webx.debug("Done reading map with id '" + id + "' (version " + data[id].version + ") from url " + url);
          wxMaps[id] = wxMap;
        }
      }
      if (typeof callback == 'function')
        callback(wxMaps);
      return wxMaps;
      
    }, function() {
      Webx.error("Can't read JSON mappings from url '" + url + "'");
    });
  },

  /**
   * Reads rules from file
   * @param {String} filepath: Optional to override system default
   *   which is set for a user-facing extension
   */
  /*
  _readRulesFromBundledFile: function(callback, filepath) {
    Webx.debug("Reading rules from bundled file.");
    var _this = this;

    if (typeof filepath != 'string')
      filepath = this.config.bundledRulesFilepath;

    $.get(chrome.extension.getURL(filepath), function(data) {
      _this.rules = _this._rulesFromString(data);
      if (typeof callback == 'function')
        callback(data);
    }).error(function(e) {
      Webx.error("Can't read JS rules from filepath '" + filepath + "'", e);
    });
  },
*/

  /**
   * Writes string to FileSystem API file.
   */
  _writeToFile: function(string, filename, callback) {
    Webx.debug("Writing to Filesystem API file \"" + filename + "\"");
    // window.PERSISTENT seems to be broken in Chrome 13 and my Chrome 15
    // http://code.google.com/p/chromium/issues/detail?id=85000
    window.requestFileSystem(window.TEMPORARY, 1024*1024 /*1MB*/, function(fs) {
      fs.root.getFile(filename, {create: true}, function(fileEntry) {
        fileEntry.createWriter(function(fileWriter) {
          fileWriter.onerror = function(e) {
            Webx.error('Write to ' + filename + 'failed', e);
          };

          fileWriter.onwriteend = function(trunc) {
            fileWriter.onwriteend = null; // Prevent infinite recursion

            var bb = new BlobBuilder(); // Note: window.WebKitBlobBuilder in Chrome 12.
            bb.append(string);
            fileWriter.write(bb.getBlob('text/plain'));

            // Open the file to see
            Webx.debug('To get written file "' + filename + '", open your browser to: ' + fs.root.toURL());
            if (typeof callback == 'function')
              callback(fs.root.toURL());
          };

          // Nuke the current contents of the file
          fileWriter.seek(fileWriter.length); // Start write position at EOF.
          fileWriter.truncate(0);
        }, Webx._onFsApiError);
      }, Webx._onFsApiError);
    }, Webx._onFsApiError);
  },

  /**
   * Converts rules to string.
   * Must handle serialization of functions
   */
  _rulesToString: function() {
    var _this = this;
    return JSON.stringify(this.rules, function(key, value) {
      //return typeof value === 'function' ? ('' + value).replace(/\n\s*/g, ' ') : value;
      return typeof value === 'function' ? _this.config.serializationPrefix + value : value;
    });
  },

  /**
   * Converts string to rules, which includes functions
   */
  _rulesFromString: function(string) {
    var _this = this;

    return JSON.parse(string, function(key, value) {
      if (value && typeof value == 'string' &&
          value.substr(0, _this.config.serializationPrefix.length) == _this.config.serializationPrefix) {
        try {
          var substr = value.substr(_this.config.serializationPrefix.length);
          substr = substr.replace(/\\n/g, '');
          return eval('(' + substr + ')');
        } catch(e) {
          Webx.error('_rulesFromString', 'Error while deserializing functions in rules from string.', e);
          return value;
        }
      }
      return value;
    });
  },
  
  /**
   * Writes all given maps to file
   */
  _writeMapsToLocalStorage: function(wxMaps) {
    var count = 0;
    for (var w in wxMaps) {
      if (wxMaps[w].writeToLocalStorage())
        count++;
    }
    if (count > 0)
      Webx.debug("Wrote " + count + " map(s) to localStorage.");
  },
  
  /**************************************************************************
   * Utilities for WebxMap
   **************************************************************************/
  
  /**
   * Returns all the CSS rules that are relevant to the specified class name,
   * i.e. the class name is mentioned in the last 'simple_selector_sequence'
   * in any of the 'selectors' within a 'selectors_group' but not in the 'negation'.
   * See http://www.w3.org/TR/css3-selectors/#w3cselgrammar for definitions
   */
  _getCssRuleWrappersForClassName: function(className) {
    // Check if cache exists and the wrappers, minus the ones that are negated.
    if (Webx.caches.classNameToCssRuleWrappers) {
      var cssRuleWrappers = Webx.caches.classNameToCssRuleWrappers[className];
      if (! cssRuleWrappers) {
        return null;
      } else {
        cssRuleWrappers = cssRuleWrappers.filter(function(cssRuleWrapper) {
          return ! cssRuleWrapper.negated;
        });
        return cssRuleWrappers.length ? cssRuleWrappers : null;
      }
    }
    
    // No cache hit, so construct the cache
    this._mapClassNamesToRuleWrappers();

    // Recurse
    return this._getCssRuleWrappersForClassName(className);
  },

  /**
   * Goes through all stylesheets and rules and maps className to an array of cssRuleWrappers,
   * each of which contains a reference to the CSSRule from the DOM and possibly a 'negated'
   * boolean which indicates that the class name is also mentioned in the :not() negation
   * for some reason. Note that we only take class names from the last simple_selector_sequence;
   * we ignore all class names before ' ' '>' '+' '~'
   * cssRuleWrapper: {cssRule: cssRule, negated: true // optional }
   */
  _mapClassNamesToRuleWrappers: function() {
   // Init
    Webx.caches.classNameToCssRuleWrappers = {};

    this._forEachCssRule(function(cssRule) {
      var selectorObj = Webx.Slick.parse(cssRule.selectorText, {nocache: true});
      if (! selectorObj) {
        Webx.error("Can't parse CSS rule from stylesheets with selector='" + cssRule.selectorText + "'");
      } else {
        // See Slick documentation https://github.com/mootools/slick
        // - For each selector in the selectors_group, we just need to find one match; hence 'some'
        // - We take the *last* simple_selector_sequence in that selector, i.e. ignore everything before
        //   ' ' '>' '+' '~'
        // - We check the class list for a match
        selectorObj.expressions.forEach(function(expression) {
          var simpleSelectorSequence = expression[expression.length - 1];
          if (simpleSelectorSequence.classList) {
            simpleSelectorSequence.classList.forEach(function(c) {
              if (! Webx.caches.classNameToCssRuleWrappers[c])
                Webx.caches.classNameToCssRuleWrappers[c] = [];
              
              // If not already stored
              if (Webx.caches.classNameToCssRuleWrappers[c].every(function(rule) {
                  return rule != cssRule;
                }))
              {
                // Check if for some silly reason the class name is also in the :not().  You never know.
                // - There must either (a) be no pseudos, or (b) no pseudos are negations (':not'),
                //   or (c) no negation pseudo has '.classname'
                var negated = false;
                if (simpleSelectorSequence.pseudos) {
                  var classRegExp = new RegExp('\\.' + c + '\\s*$');
                  negated = simpleSelectorSequence.pseudos.some(function(pseudo) {
                    return pseudo.key == 'not' && pseudo.value.match(classRegExp) >= 0;
                  });
                }
              
                var cssRuleWrapper = {cssRule: cssRule};
                if (negated)
                  cssRuleWrapper.negated = true;
                Webx.caches.classNameToCssRuleWrappers[c].push(cssRuleWrapper);
              }
            });
          }
        });
      }
    });
    
//    Webx.debug(Webx.caches.classNameToCssRuleWrappers.ki);
//    Webx.debug(Webx.caches.classNameToCssRuleWrappers.uj);
  },
    

  /**
   * Traverses the stylesheets and calls the callback with each rule
   * as long as the stylesheet is not from an extension.
   */
  _forEachCssRule: function(callback) {
    var _this = this;
    
    var processCssRules = function(cssRules) {      
      // For each rule in the stylesheet
      for (var c = 0; c < cssRules.length; c++) {
        var cssRule = cssRules[c];
        // If this is a media rule, recurse through its rules
        if (cssRule.media && cssRule.media.length) {
          processCssRules(cssRule.cssRules);
          
        } else { // Direct rule
          var style = cssRule.style; 
          // Skip if no style for some reason
          if (! style)
            continue;

          // If this comes from an extension, skip the stylesheet
          // For efficiency only check for the first few rules
          if (c < 4) {
            var cssRulesText = cssRule.cssText;
            // XXX Do we need this check anymore now that we've changed the for-loop?
            if (cssRulesText) {
              if (cssRulesText.match(STYLESHEETS_FILTER))
                break;
              if (_this.config.stylesheetsFilter && cssRulesText.match(_this.config.stylesheetsFilter))
                break;
            }
          }

          callback(cssRule);
        }
      }
    };
    
    var sheets = document.styleSheets;
    // For each stylesheet
    for (var s = 0; s < sheets.length; s++) {
      var cssRules = sheets[s].cssRules || sheets[s].rules;
      // If too early, a stylesheet is empty if the stylesheet comes from an extension.
      if (! cssRules)
        continue;
      processCssRules(cssRules);
    }
  },

  
  /**************************************************************************
   * API functions for the rules to use
   **************************************************************************/
  
  isClassNameStyled: function (className) {
    if (! CLASSNAME_REGEXP_TEST.test(className)) {
      Webx.error("In rules, classname '" + className + "' is not valid.");
      return false;
    }
    return !! this._getCssRuleWrappersForClassName(className);
  },

  /**************************************************************************
   * API for end-user extension, e.g. G+me
   * Automapping
   **************************************************************************/

  /**
   * Checks that the current mapping is valid for the current page,
   * and automaps otherwise.
   */
  checkAndAutomap: function() {
    if (! this._checkId())
      this.automapPage();
  },

  /**
   * Checks that the current mapping is valid for the current page.
   */
  _checkId: function() {
    return this.wxMap.getId() == this._getIdFromPage();
  },

  /**
   * Returns id that identifies mapping on the current page.
   */
  _getIdFromPage: function() {
    var wxMap = new WebxMap(this, this.config);
    wxMap.invokeMappingRulesForId();
    return wxMap.getId();
  },

  /**
   * Invokes mapping rules for the current page
   */
  automapPage: function() {
    Webx.debug("Automapping page...");
    this.wxMap.automap(this.rules);
    this.wxMap.writeToLocalStorage();
    Webx.debug("Done automapping page.");
    this._exposeAPI();
  },

  /**
   * Invokes mapping rules for the specified key, with the given optional context
   */
  _automapMasterBranch: function(key, $context) {
    var rules = this.rules[key];
    if (typeof rules == 'undefined') {
      Webx.warn('_automapMasterBranch', 'Rules not found for key "' + key + '"');
      return;
    }

    var masterKey = this.rules.masterKey;

    // If we've reached the top level, then invoke the search function and extract
    if (typeof rules.masterKey == 'undefined') {
      if (typeof rules.search == 'undefined') {
        Webx.error('_automapMasterBranch', 'Can\'t find search function for key "' + key + '"');
        return;
      } else {
        this.wxMap.extractWithRules(key, rules);
      }

    } else if (this.map.s[masterKey]) { // If the master has a mapping
      do {
        // Find a match for the master key, and possibly recurse
        // XXX This may not work.  The rules don't necessarily go down the tree; e.g.
        // they may go sideways or even upwards, in which case the context may make sense for key
        // while the context may not make sense for masterKey.
        // This all depends on how far up the specified context is.
        // But I need to imagine more scenarios to see what kind of trouble we'll get into.
        var $master = this.find$(masterKey, $context);

        // If we've found the master element.
        // NOTE: this could return multiple hits, in which the case the slaveRules will
        // have a `this` with multiple elements, unlike when we execute mappingRules
        if ($master.length) {
          // Invoke the slave rules to automap the slave keys
          rules.slaveRules.call(this.ruleContext($master),
            key); // Passing the key, but no use so far; no use for return value either
          break;
        }

        // Try with context higher up the tree.
        $context = $context.parent();
      } while ($context.length);

    } else { // If the master has no mapping
      // Recurse
      _automapMasterBranch(masterKey, $context);
    }
  },

  /**************************************************************************
   * API for end-user extension, e.g. G+me
   * Usage of mappings
   **************************************************************************/

  /**
   * Extends selectors of jQuery and DOM, as per config.
   * Needs to be called every time the mapping has changed.
   */
  _exposeAPI: function() {
    if (this.config.extendJQuerySelectors)
      this.extendJQuerySelectors();
    if (this.config.extendJQueryPseudoClasses)
      this.extendJQueryPseudoClasses();
    if (this.config.extendQuerySelectors)
      this.extendQuerySelectors();
    if (this.config.aliasAPI)
      this.aliasAPI(this.config.aliasAPI);    
  },

  /**
   * Extends jQuery's selectors to handle Webx mappings.
   * This is faster than using custom pseudo-selectors/filters because
   * jQuery can still use its optimizations on class names, ids, etc.
   */
  extendJQuerySelectors: function() {
    if (! jQuery)
      return;
    
    var extendSelector = this._genExtendSelector();

    ['find',
     'not',
     'filter',
     'closest'].forEach(function(fnName) {
     [jQuery, jQuery.fn].forEach(function(ns) { // XXX Don't know yet which
        var oldFn = ns[fnName];
        if (! oldFn)
          return;

        var newFn = function() {
          var args = Array.prototype.slice.call(arguments);
          var query = args[0];
          if (typeof query == 'string')
            args.splice(0, 1, extendSelector(query) || query);
          else if ($.isArray(query))
            args.splice(0, 1, query.map(function(i) { return extendSelector(i) || i; }));
          return oldFn.apply(this, args);
        };
        $.extend(newFn, oldFn);
        ns[fnName] = newFn;
     });     
   });
  },
  
  /**
   * Extends querySelector(All) selectors to handle Webx mappings.
   */
  extendQuerySelectors: function() {
    var extendSelector = this._genExtendSelector();

    [Document, Element].forEach(function(c) {
      ['querySelector', 'querySelectorAll'].forEach(function(f) {
        var proxied = c.prototype[f];
        c.prototype[f] = function() {
          var args = Array.prototype.slice.call(arguments);
          args[0] = extendSelector(args[0]) || args[0];
          return proxied.apply(this, args);
        };
        $.extend(c.prototype[f], proxied);
      });
    });
  },
  
  /**
   * @return a function that takes a selector query supporting our mappings
   * and translates it to a jQuery/querySelector-compatible selector
   */
  _genExtendSelector: function() {
    var _this = this;
    // Construct quick lookup table.
    // We now store it as a class property because it can be used by both jQuery and querySelector.
    if (! Webx.simpleSelectorLookup)
      Webx.simpleSelectorLookup = {};
    for (var key in this.map.s) {
      Webx.simpleSelectorLookup['%' + key] = this.map.s[key];
    }
    
    return function(query) {
      if (query) {
        query = query.replace(/^\s*|\s*$/g, '');
        // Fast substitution for simple selectors
        var simpleQuerySub = Webx.simpleSelectorLookup && Webx.simpleSelectorLookup[query];
        if (simpleQuerySub) {
          query = simpleQuerySub;
        } else if (query.indexOf('%') >= 0) {
          var normalizedQuery = _this.wxMap.getNormalizedQuery(query);
          query = normalizedQuery || query;
        }
        return query;
      } else {
        return null;
      }
    };
  },
  
  /**
   * Extends jQuery with custom pseudo-selectors/filters so that one can do
   * $(':Xpost') for example.
   * The problem with this technique is that we lose the performance optimizations
   * of Sizzle, e.g. when it calls querySelectorAll or elementById
   */
  extendJQueryPseudoClasses: function() {
    Element.prototype.matchesSelector =
      Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector ||
      Element.prototype.webkitMatchesSelector || Element.prototype.msMatchesSelector;

    jQuery.each(this.map.s, function(key, selector) {
      jQuery.expr[':']['X' + key] = function(elem) {
        return selector && elem.matchesSelector(selector);
      };
    });
  },
  
  /**
   * Creates non-jQuery equivalents of function.
   * To be used by Webx's subclass after defining its jQuery-based
   * API.
   * FIXME: doesnt' work.  I don't know how to access the functions. __proto__ ?
   */
  _createNonJQueryFunctions: function() {
    var prototype = Object.getPrototypeOf(this);
    for (var i in prototype) {
      if (typeof prototype[i] == 'function') {
        var fn = prototype[i];
        if (i.charAt(i.length - 1) === '$') {
          (function(fn) {
            // e.g. find$() -> find()
            prototype[i.substring(0, i.length - 1)] = function() {
              return fn.apply(this, arguments).get(0);
            };
            // e.g. find$() -> findAll()
            prototype[i.substring(0, i.length - 1) + 'All'] = function() {
              return fn.apply(this, arguments).get();
            };
          })(fn);
        }
      }
    }
  },

  /**
   * Exposes our API functions as aliases
   * @param alias {String} Optional, by default 'X'
   */
  aliasAPI: function (alias) {
    if ((typeof alias != 'boolean' || ! alias) && (typeof alias != 'string' || ! alias || ! alias.length))
      throw "Webx: Alias for exposing API cannot be empty.";    
    
    if (typeof alias == 'boolean')
      alias = 'X';
    
    var baseFunctions = {
      '': 'find' // $X() => Webx.find$
    };
    var elementFunctions = {
    };
    var generalFunctions = {
      's': 'selector', // $X.s() = X.s() => Webx.selector()
      'cl': 'classList', // $X.cl() = X.cl() => Webx.classList()
      'cn': 'className', // $X.cn() = X.cn() => Webx.className()
      'scl': 'styledClassList',
      'scn': 'styledClassName'
    };
    
    var i;
    var prototype = Object.getPrototypeOf(this);
    for (i in baseFunctions) {
      if (typeof this[baseFunctions[i]] != 'undefined') {
        window[alias] = prototype[baseFunctions[i]].bind(this);
      } else {
        Webx.error("Can't find API function Webx.prototype." + baseFunctions[i]);
        return;
      }
      if (typeof this[baseFunctions[i] + '$'] != 'undefined') {
        window['$' + alias] = prototype[baseFunctions[i] + '$'].bind(this);
      } else {
        Webx.error("Can't find API function Webx.prototype." + baseFunctions[i] + '$');
        return;
      }
    }
    for (i in elementFunctions) {
      if (typeof prototype[elementFunctions[i]] != 'undefined')
        window[alias][i] = prototype[elementFunctions[i]].bind(this);
      if (typeof prototype[elementFunctions[i] + '$'] != 'undefined')
        window['$' + alias][i] = this[elementFunctions[i] + '$'].bind(this);

    }
    for (i in generalFunctions) {
      if (typeof prototype[generalFunctions[i]] != 'undefined')
        window[alias][i] = window['$' + alias][i] = prototype[generalFunctions[i]].bind(this);
    }
  },

  /**
   * @return a jQuery object containing all the matches for
   * the key within that context
   * Usage:
   *   Webx.find$('gbar'); // Full name
   *   $X('gbar'); // Shortcut name
   *   $X(':Xgbar'); // With pseudo, just as our extended jQuery
   * @param context {DOMELement or jQuery} Optional context
   * @param settings {
   */
  find$: function(selector, context, callback) {
    // Accept DOM element or jQuery object
    var $context = context ? (context.jquery ? context : $(context)) : null;

    // If no mapping and we're in automap mode
    if (! this.map.s[key] && this.mode == 'automap')
      this.wxMap._automapMasterBranch(key, $context);

    if (this.map.s[key])
      // Accept context or none
      return $(this.map.s[key], $context);
    else
      return $();
  },
  
  /**
   * @return {Array} of class names associated with the specified mapping key,
   * or null if not found
   */
  classList: function(key) {
    if (typeof key == 'undefined' || key === null || key === '') {
      Webx.error("classList: empty key parameter.");
      return null;
    }
    if (key.charAt(0) == '%')
      key = key.substr(1);
    return this.map.c[key] || null;
  },
  
  /**
   * @return {String} of class names associated with the specified mapping key,
   * separated by spaces, or emptry string if not found
   */
  className: function(key) {
    if (typeof key == 'undefined' || key === null || key === '') {
      Webx.error("className: empty key parameter.");
      return null;
    }
    var result = this.classList(key);
    return result && result.join(' ') || '';
  },
  
  /**
   * @return {Array} of class names associated with the specified mapping key,
   * or null if not found
   */
  styledClassList: function(key) {
    if (typeof key == 'undefined' || key === null || key === '') {
      Webx.error("styledClassList: empty key parameter.");
      return null;
    }
    if (key.charAt(0) == '%')
      key = key.substr(1);
    return this.map.cStyled[key] || null;
  },
  
  /**
   * @return {String} of class names associated with the specified mapping key,
   * separated by spaces, or emptry string if not found
   */
  styledClassName: function(key) {
    if (typeof key == 'undefined' || key === null || key === '') {
      Webx.error("styledClassName: empty key parameter.");
      return null;
    }
    var result = this.styledClassList(key);
    return result && result.join(' ') || '';
  }
};

/**************************************************************************
 * WebxMap class
 **************************************************************************/

/**
 * Constructor for Webxmap
 */
function WebxMap(webx, config) {

  this.webx = webx;
  this.config = config; // We share config with Webx

  // Normally, 'library' mode; but in its dev extension, Webx can run 'survey' mode
  // to output rules
  this.mode = 'library';

  /**
   * Creates new mapping.
   * Defined here so that we can call it upon `new`
   */
  (this.newMap = function() {
    // NOTE: keep this reference permanent so that Webx can alias it
    this.map = {
      s: {},
      sss: {},
      sAlt: {},
      c: {},
      cAlt: {},
      cStyled: {}
    };
    // Aliases for convenience in mapping rules
    $.extend(this, this.map);
    
    // This version number will be less than official ones with format "2011-08-31.001"
    this.version = "0.000";
    
    this.caches = {
      keyToCssRuleWrappersForFilters: {},
      normalizedQuery: {'true': {}, 'false': {}} // One for each config.strict
    };
  }).call(this);
  

  /**
   * Special function to send as `this` to the mapping rules.
   * Defined here so that elements can be bound to the current object `this`.
   * It serves 2 purposes:
   * - the 'this' in the outest scope of the CoffeeScript rules, so that we can have
   *   consistent syntax  between the outermost scope (-> $('#id') ) and
   *   the inner scopes, even though the outermost scope has no 'this' (different 'this'
   *   than the one previously mentioned) to give to the anonymous function within
   *   which $('#id') is called.
   * - container for rule functions
   * @returns {Function} A rule context, i.e. a function which takes a 'search' function parameter
   *   and returns an array: [$ruleContext, search]
   */
  var _this = this;
  this.genTopRuleContext = function(wxMap) {
    // Default to wxMap
    if (typeof wxMap == 'undefined')
      wxMap = _this;

    var result = wxMap.ruleContext($(document)); // In case a top-level rule accidentally references `this`
    $.extend(result, {
      debug: WebxMap.debug,
      error: WebxMap.error,
      e:    wxMap.extract.bind(wxMap),
      ss:   wxMap.extractFromStylesheets.bind(wxMap),
      X: {
        isStyled: _this.webx.isClassNameStyled.bind(_this.webx) /*,
        cssRules: _this.webx.cssRulesForElement.bind(_this.webx) */
      }
    }, wxMap.map);
    return result;
  };

  this.topRuleContext = this.genTopRuleContext();
}

// RegExp from Slick.parser.
var CLASSNAME_REGEXP_TEST = /^(?:[\w\u00a1-\uFFFF-]|\\[^\s0-9a-f])+$/;
// Is it me or JS RegExps suck?  I need 2 regexps?
var CLASSNAMES_REGEXP_TEST = /^\s*(?:\.((?:[\w\u00a1-\uFFFF-]|\\[^\s0-9a-f])+))+\s*$/g;
var CLASSNAMES_REGEXP = /\s*\.((?:[\w\u00a1-\uFFFF-]|\\[^\s0-9a-f])+)\s*/g;


WebxMap.debug = function() {
  var args = Array.prototype.slice.call(arguments);
  args.unshift("While digging for '" + args.shift() + "':");
  Webx.debug.apply(undefined, args);
};
WebxMap.info = function(key, msg) {
  var args = Array.prototype.slice.call(arguments);
  args.unshift("While digging for '" + args.shift() + "':");
  Webx.info.apply(undefined, args);
};
WebxMap.warn = function(key, msg) {
  var args = Array.prototype.slice.call(arguments);
  args.unshift("While digging for '" + args.shift() + "':");
  Webx.warn.apply(undefined, args);
//console.trace();
};
WebxMap.error = function(key) {
  var args = Array.prototype.slice.call(arguments);
  args.unshift("While digging for '" + args.shift() + "':");
  Webx.error.apply(undefined, args);
//console.trace();
};

/**
 * Gets rid of pseudoclasses that jQuery doesn't support, e.g. hover.
 */
WebxMap.sanitizeSelectorText = function(selectorText) {
 return selectorText.replace(WebxMap.NON_DOM_SELECTOR_FILTER, '');
};


/**
 * Returns true if the given CSS style matches any of the sets of specified patterns
 * which are either strings or RegExps.
 * The structure of ssPatterns is:
 * [
 *   {borderLeftColor: 'rgb(0,0,0)', borderRightColor: /255/},
 *   ...
 * ]
 */
WebxMap.styleMatch = function(style, ssPatternsGroup) {
  return ssPatternsGroup.some(function(ssSimplePattern) {
    for (var p in ssSimplePattern) {
      var styleValue = style[p];
      if (! styleValue)
        return false;

      var pattern = ssSimplePattern[p];
      if (typeof pattern == 'string' && styleValue.indexOf(pattern) < 0 ||
          typeof pattern != 'string' && ! styleValue.match(pattern))
        return false;
    }
    return true;
  });
};

/**
 * Parses ssPatternsSet options and adds them to the specified collection in canonical format:
// [ { // ssFilter 1
//     key: 'postIsSelected', // Optional if one wants to map the filtered map class name
//     ssPatterns: [{borderLeftColor: 'rgb(77', borderRightColor: /rgb\(77/}, ...]
//   },
//    ...
// ]
 * @return true if a set of filters is in the proper format as either
 *   a single filter or an array fo filters
 */
WebxMap.collectSsPatterns = function(collection, ssPatternsSet, key) {
  if (typeof ssPatternsSet != 'object') {
     Webx.error('Error in rule for key "' + key + '": filters should be an object or array.');
     return false;
  } else if (ssPatternsSet instanceof Array) {
    // If array of ssPatterns, check each
    for (var s in ssPatternsSet) {
      var ssSimplePattern = ssPatternsSet[s];
      if (WebxMap.checkSsSimplePattern(ssSimplePattern)) {
        Webx.error('Error in rule for key "' + key + '": an individual filter should be a map with values of type string or RegExp.');
        return false;
      }
    }
    collection.push(typeof key != 'undefined' ? {key: key, ssPatterns: ssPatternsSet} :
      {ssPatterns: ssPatternsSet});
  } else { // Non-Array object
    // If single ssPatterns
    if (WebxMap.checkSsSimplePattern(ssPatternsSet)) {
      collection.push(typeof key != 'undefined' ? {key: key, ssPatterns: [ssPatternsSet]} :
        {ssPatterns: [ssPatternsSet]});

    } else if (typeof key == 'undefined') {
      if (! WebxMap.collectKeyedSsPatterns(collection, ssPatternsSet))
        return false;
    } else {
      // Can't recurse more
       Webx.error('Error in rule for key "' + key + '": ssPatterns structure is too deep.');
       return false;            
    }
  }
  return true;
};

/**
 * Parsers ssPatternsSet options that come with a key to map to when class names
 * are filtered out.
 */
WebxMap.collectKeyedSsPatterns = function(collection, ssPatternsSet) {
  // If filters with mapping of filtered class names to key,
  // recurse one time
  for (var key in ssPatternsSet) {
    if (! WebxMap.collectSsPatterns(collection, ssPatternsSet[key], key))
      return false;
  }
  return true;
};

/**
 * @return true if a single patternGroup is in the proper format as a map
 *   of CSS style to string or RegExp
 */
WebxMap.checkSsSimplePattern = function(ssPatterns) {
  for (var i in ssPatterns) {
    if (typeof ssPatterns[i] != 'string' && ! (ssPatterns[i] instanceof RegExp)) {
      return false;
    }
  }
  return true;
};

/**
 * @return a string concatenation of all the keys in the patterns object
 */
WebxMap.keysInPatterns = function(patterns) {
  var result = [];
  for (var i in patterns) {
    result.push(i);
  }
  return result;
};

/**
 * Adds the specified selectors and class names to the given mapEntry.
 * This merges values, allowing for alternate selectors and class names.
 */
WebxMap.addToMapItem = function(mapItem, selectorText, origSelectorText, styledClassNameList) {
  // If not yet set, 
  if (typeof mapItem.s == 'undefined') {
    mapItem.s = selectorText;
    if (typeof origSelectorText != 'undefined' && origSelectorText !== null && origSelectorText !== selectorText)
      mapItem.sss = origSelectorText;
  } else {
    if (! mapItem.sAlt)
      mapItem.sAlt = [];
    mapItem.sAlt.push(selectorText);
  }

  if (styledClassNameList) {
    // Set c, if not yet set
    if (typeof mapItem.c == 'undefined') {
      mapItem.c = styledClassNameList;
    } else { // Or add to cAlt
      if (! mapItem.cAlt)
        mapItem.cAlt = [];
      Util.addUniqueStringArray(mapItem.cAlt, styledClassNameList);
      if (mapItem.cAlt.length === 0)
        delete mapItem.cAlt;
    }

    // Uniquely add to cStyled
    styledClassNameList.forEach(function(c) {
      if (typeof mapItem.cStyled == 'undefined')
        mapItem.cStyled = [];
      if (mapItem.cStyled.every(function(cStyledClassName) {return cStyledClassName !== c;}))
        mapItem.cStyled.push(c);
    });
  }

  return mapItem;
};


// Simple of selectors to remove CSS selectors that jQuery doesn't support because
// they don't actually select DOM elements (you get syntax error)
WebxMap.NON_DOM_SELECTOR_FILTER = /:(?:link|visited|active|hover|focus|target|:?selection|:?before|:?after|:(?:scrollbar|-webkit)[-\w:]*)\b/g;

WebxMap.prototype = {

  /**
   * Returns id that identifies mapping as set in the properties of this WxMap.
   */
  getId: function() {
    if (! this.config.mapIdFunc || typeof this.config.mapIdFunc != 'function')
      throw 'Webx: improper function for "mapIdFunc" configuration';
      
    var id = this.config.mapIdFunc.call(this.map);
    if (id === null)
      Webx.error('getId:', "Not enough to create a mapping id");
    else
      // Translate all spaces to underscore
      id = id.replace(/\s/g, '_');

    return id;
  },

  /**
   * Invoke enough rules to be able to generate an id.
   * Useful for getting the ID from the current page.
   */
  invokeMappingRulesForId: function() {
    if (! this.config.mappingRulesForId || typeof this.config.mappingRulesForId != 'function')
      throw 'Webx: improper function for "mappingRulesForId" configuration';

    this.config.mappingRulesForId.call(this.topRuleContext);
  },
  
  /**
   * Invokes the rules for the current page
   */
  automap: function(rules) {
    if (! this.config.mappingRules || typeof this.config.mappingRules != 'function')
      throw 'Webx: improper function for "mappingRules" configuration';

    this.rules = rules;
    this.config.mappingRules.call(this.topRuleContext);
    
    this.version = Util.incrementVersion(this.version);
  },

  /**
   * Returns string representation of map, in compact JSON format,
   * with ID and version
   */
  toString: function() {
    var data = {};
    data[this.getId()] = {
      version: this.version,
      map: this.toCompactJSON()
    };
    return JSON.stringify(data);
  },

  /**
   * Returns the map in compact JSON format
   */
  toCompactJSON: function() {
    var map = {};
    for (var type in this.map) {
      for (var i in this.map[type]) {
        if (!(i in map))
          map[i] = {};
        map[i][type] = this.map[type][i];
      }
    }
    return map;
  },

  /**
   * Sets the map from the given compact JSON format
   */
  fromCompactJSON: function(data) {
    this.version = data.version;
    this.newMap();
    for (var type in this.map) {
      for (var i in data.map) {
        // NOTE: In JSON format, to save space, we keep the data as
        //   key: { s: 'selector', c: 'className' }
        // but internally we want to be able to do map.s.key so that we don't have
        // to have guards for 'undefined' everywhere when doing map.key.s
        if (typeof data.map[i][type] != 'undefined')
          this.map[type][i] = data.map[i][type];
      }
    }
  },

  /**
   * Takes a single object with s, sss, sAlt, c, cStyled, cAlt and copies into this.map
   */
  setMapEntry: function(key, entry) {
    for (var type in this.map) {
      if (typeof entry[type] != 'undefined')
        this.map[type][key] = entry[type];
    }
  },

  /**
   * Returns a single object with s, sss, sAlt, c, cStyled, cAlt from this.map
   */
  getMapEntry: function(key) {
    var entry = {};
    for (var type in this.map) {
      if (typeof this.map[type][key] != 'undefined')
        entry[type] = this.map[type][key];
    }
    return entry;
  },

  
  /**
   * Adds the selector and class names to the mapped key
   */
  addToMapEntry: function(key, selectorText, styledClassNameList) {
    this.setMapEntry(key,
      WebxMap.addToMapItem(this.getMapEntry(key), selectorText, undefined, styledClassNameList));
  },

       
  /**
   * Persists mappings to localStorage.
   * @return true on success
   */
  writeToLocalStorage: function() {
    // 2011-08-31 XXX Temporary for G+me 6.0.0
    function wipeOldHistory() {
      var OLD_KEYS = {
        LS_FOLDED                      : 'gpme_post_folded_',
        LS_COMMENTS_FOLDED             : 'gpme_comments_folded_',
        LS_COMMENTS_UNFOLDED           : 'gpme_comments_unfolded_',
        LS_COMMENTS_READ_COUNT         : 'gpme_post_seen_comment_count_',
        LS_COMMENTS_READ_COUNT_CHANGED : 'gpme_post_seen_comment_count_changed_',
        LS_URL_LIST_LAST_UNFOLDED      : 'gpme_post_last_open_'
      };
      for (var i = 0; i < localStorage.length; i++) {
        var storedKey = localStorage.key(i);
        for (var k = 0; k < OLD_KEYS.length; k++) {
          if (storedKey.indexOf(OLD_KEYS[k]) === 0) {
            localStorage.removeItem(storedKey);
          }
        }
      }
    }

    var _this = this;
    var id = this.getId();
    if (id) {
      // Store with mapping id
      var store = function() {
        // Check for existing version
        var previousMap = localStorage.getItem(_this.config.storagePrefix + '_map_' + id);
        if (previousMap) {
          var data = JSON.parse(previousMap);
          var version = data.version || '';
          Webx.debug("Found previous map with id '" + id + "' (version " + version + ") in localStorage.");
          if (! Util.isVersionNewer(_this.version, version)) {
            Webx.debug("Skipping writing map because the stored one is the same or more recent.");
            return false;
          }
        }

        Webx.debug("Writing map with id '" + id + "' (version " + _this.version + ") to localStorage.");
        localStorage.setItem(_this.config.storagePrefix + '_map_' + id, JSON.stringify({
          version: _this.version,
          map: _this.map
        }));
      };
      try {
        store();
      } catch (e) {
        Webx.error("Cannot save map to localStorage.", e);
        alert("It looks like G+me is running out of localStorage space.  Let's do some cleaning, which may take a few moments.  Please wait a minute while the page may be unresponsive.");
        wipeOldHistory();
        try {
          store();
        } catch (e) {
          Webx.error("Still cannot save map to localStorage.", e);
          alert("Still out space.  Please delete the cookies for \"plus.google.com\" and reload Google+");
          return false;
        }
      }
      return true;
    } else {
      Webx.error("The bundled maps don't have an ID.  Skipping writing to local storage.");
      return false;
    }
  },

  /**
   * Restores specified mappings from localStorage
   * @return true on success
   */
  readFromLocalStorage: function(id) {
    if (typeof id != 'string' || ! id) {
      Webx.error("Cannot read map from localStorage: invalid mapping id '" + id + "'");
      return false;
    }
    
    // Restore with mapping id
    var val = null;
    try {
      val = localStorage.getItem(this.config.storagePrefix + '_map_' + id);
    } catch (e) {
      Webx.error("Can't read map from localStorage: " + this.config.storagePrefix + '_map_' + id);
      return false;
    }
    if (val === null) {
      Webx.error("No selectors or classNames found in localStorage for in map with id '" + id + "'");
      return false;
    } else {
      try {
        // Internal format is different from JSON format in that the id
        // is stored in the key and we preserve s, sAlt, and c as only 3 separate objects.
        var data = JSON.parse(val);
        this.version = data.version;
        this.map = data.map;
      } catch (e) {
        Webx.error("Can't parse map with id '" + id + "' from localStorage: ", e);
        return false;
      }
    }
   Webx.debug("Done reading map with id '" + id + "' (version " + (this.version || '') + ") from localStorage.");
   return true;
  },

  /**
   * Takes jQuery/querySelector string and returns it with the Webx mapping
   * keys replaced with the equivalent selectors.
   * TODO: we don't support substitutions when the selector is complex, i.e. has combinators.
   */
  getNormalizedQuery: function(query) {
    /**
     * Escapes a CSS3 selector identifier
     */
    function escapeSelectorIdent(ident) {
      return ident.replace(/([^\w\u00a1-\uFFFF-])/g, '\\$1');
    }

    /**
     * Extends the simple_selector_sequence (part of Slick's parsed selectorObj)
     * with another one.
     * NOTE: The source's combinator is ignored
     */
    function extendSimpleSelectorSequence(target, source, key) {
      // Due to Slick bug https://github.com/mootools/slick/issues/51
      // we can only support one #id
      ['tag', 'id'].forEach(function(k) {
        if (k == 'tag' && source[k] == '*')
          delete source[k];
        if (typeof source[k] != 'undefined' && source[k] !== null && target[k] !== source[k]) {
          if (typeof target[k] != 'undefined' && target[k] !== null) {
            Webx.warn(k + " '" + source[k] + "' from mapping key '" + key + "' conflicts with " + k + " '" +
              target[k] + "' in selector '" + query + "'");
          } else {
            target[k] = source[k];
          }
        }
      });
      ['attributes', 'classList', 'classes', 'pseudos'].forEach(function(a) {
        if (source[a])
          target[a] = target[a] ? target[a].concat(source[a]) : source[a];
      });

      // Remove the universal if no longer needed
      if (target.tag == '*' && ! universalNeeded(target))
        delete target.tag;
    }

    /**
     * Returns true if the simple_selector_sequence requires a universal '*' tag
     */
    function universalNeeded(simpleSelectorSequence) {
      return ['id', 'classList', 'attributes', 'pseudos'].every(function(k) {
          return typeof simpleSelectorSequence[k] == 'undefined' || simpleSelectorSequence[k] === null;
        });
    }

    var _this = this;
    var result = this.caches.normalizedQuery['' + !!this.config.strict][query];
    if (! result) {
      // We're already caching, so no need for Slick to cache.'
      var selectorObj = Webx.Slick.parse(query, {nocache:true});
      
      // Find any webx keys and merge them in
      selectorObj.expressions.forEach(function(expression) {
        if (! expression)
          return;
        expression.forEach(function(sequence, sequenceIndex) {
          if (sequence.pseudos)
            sequence.pseudos.forEach(function(pseudo) {
              if (pseudo.key == 'not' && pseudo.value.indexOf('%') >= 0)
                // Recurse because we don't only support CSS3 -- we support jQuery's Sizzle's superset
                // which allows for complex selectors inside negation and even :not(:not(.a%gbar))
                // See below about "rawValue".
                pseudo.value = pseudo.rawValue = _this.getNormalizedQuery(pseudo.rawValue);
            });
          
          // Look for mentions of Webx selector keys inside negation :not()
          // NOTE: we do this after sequence.pseudos because we may add to them
          if (sequence.webxKeyList)
            sequence.webxKeyList.forEach(function(webxKey) {
              var selector = _this.map.s[webxKey];
              if (typeof selector != 'undefined') {
                // FIXME: we only support simple selectors for now.
                var webxSelector = Webx.Slick.parse(selector);
                if (webxSelector && webxSelector.expressions && webxSelector.expressions.length)
                  webxSelector = webxSelector.expressions[0];
                if (webxSelector && webxSelector.length)
                  extendSimpleSelectorSequence(sequence, webxSelector[webxSelector.length - 1], webxKey);
              } else {
                Webx.error("No selector mapped for key '" + webxKey + "' in query '" + query + "'");
                if (_this.config.strict) {
                  // Add an invalid pseudo class so that we can get a syntax err
                  if (typeof sequence.pseudos == 'undefined')
                    sequence.pseudos = [];
                  sequence.pseudos.push({
                    key: 'WEBX_UNMAPPED_ERROR_' + webxKey,
                    type: 'class'
                  });
                } else { // Add an invalid class so that that part of the selector doesn't match'
                  if (typeof sequence.classList == 'undefined')
                    sequence.classList = [];
                  // XXX we don't add to the corresponding sequence.classes, but we don't need it anyway
                  sequence.classList.push('WEBX_UNMAPPED_ERROR_' + webxKey);
                }
              }
            });          
        });
      });
      
      // Now, reconstruct the resulting string.
      // XXX Do we have to worry about other types of escaping, as discussed here
      // http://mathiasbynens.be/notes/html5-id-class#css ?
      // It may not matter because Slick doesn't seem to worry about these: it just blindly
      // removes backslashes anyway.
      result = '';
      selectorObj.expressions.forEach(function(expression, expressionIndex) {
        if (! expression)
          return;
        if (expressionIndex > 0)
          result += ',';
        expression.forEach(function(sequence, i) {
          if (i > 0 || sequence.combinator != ' ')
            result += sequence.combinator;
          if (typeof sequence.tag != 'undefined' && sequence.tag !== null) {
            // Only print out '*' if we have to
            if (sequence.tag == '*') {
              if (universalNeeded(sequence))
                result += '*';
            } else {
              result += escapeSelectorIdent(sequence.tag);
            }
          }
          if (typeof sequence.id != 'undefined' && sequence.id !== null)
            result += '#' + escapeSelectorIdent(sequence.id);
          if (sequence.classList)
            sequence.classList.forEach(function(className) {
              result += '.' + escapeSelectorIdent(className);
            });
          if (sequence.attributes)
            sequence.attributes.forEach(function(attribute) {
              result += '[' + escapeSelectorIdent(attribute.key) + 
                (typeof attribute.operator != 'undefined' && attribute.operator !== null ?
                  attribute.operator + '"' + attribute.value.replace(/"/g, '\\"') + '"' : '') +
                ']';
            });
          if (sequence.pseudos)
            sequence.pseudos.forEach(function(pseudo) {
              result += ':' + (pseudo.type == 'element' ? ':' : '') +
                escapeSelectorIdent(pseudo.key);
              
              // rawValue: My own modification to Slick to get around bug
              // https://github.com/mootools/slick/issues/52
              if (typeof pseudo.rawValue != 'undefined' && pseudo.rawValue !== null)
                result += '(' + pseudo.rawValue + ')';
            });
       });
      });
    
      this.caches.normalizedQuery['' + !!this.config.strict][query] = result;
    }
    
    return result;
  },

  /**
   * Helper function for creating context for the slave rules.
   * Works for both DOM search rules and CSS pattern rules
   */
  ruleContext: function($domContext) {
// No longer needed, since we're simplifying the syntax with (-> @children('.boo')) rather than
// @(-> @children('.boo'))
/*
    return function(searchFunctionOrCSSPatterns) {
      return [$domContext, searchFunctionOrCSSPatterns];
    };
*/
    return $domContext;
  },

  /**
   * Surveys all existing rules and generates dependencies.
   * As a side effect, creates a new map and automaps as much as possible.
   */
  surveyRules: function(rules) {
    this.rules = rules;
    try {
      this.mode = 'survey';
      this.config.mappingRules.apply(this.topRuleContext);
      this.version = Util.incrementVersion(this.version);
    } finally {
      this.mode = 'library';
    }
  },

  /**
   * Extracts, given pre-recorded rules.
   * This is used by automap to invoke 
   */
  extractWithRules: function(key, rules) {
    return this._extract(undefined, key, {dontStoreRules: true}, undefined, rules);
  },

  /**
   * Wrapper for _extract function which handles unspecified parameters.
   * @param {String} key: the key we're trying to map
   * @param {Object} options: Optional map of options to modify behavior.
   *   @option {String} pri: primary selector that takes precedence over what we can compute,
   *     which we'll instead add to alternate selectors
   *   @option {String}/{Array} alt: lists alternate selectors to add to mapping.
   *   @option {Object of Objects}/{Array of Objects}/{Object} ssFilter:
   *     patterns (strings or RegExps) which will filter out class names from the list of
   *     styled class names (map.c).
   *     A match of any of the patterns will cause the class name to be filtered out.
   *     The structure can be one of the following:
   *     1) ssFilter: {borderLeftColor: 'rgb(77, 144, 240)'}
   *        ssFilter: {borderLeftColor: /rgb\(77/}
   *        which filters out any class names that make CSS rules, with matching border-left-color,
   *        select the element
   *     2) ssFilter: [{borderLeftColor: rgb(77, 144, 240)'},
   *                   {borderLeftColor: rgb(77, 144, 240)', borderRightColor: 'rgb(167, 199, 247)'}]
   *        which filters out any class names that make CSS rules, that match of the pattern groups
   *        (border-left-color OR both matching border-left-color and border-right-color), select the element
   *     3) ssFilter: postIsSelected: {borderLeftColor: 'rgb(77, 144, 240)'}
   *        As (1), but also assigns the filtered class name(s) as a mapping for key 'postIsSelected'
   *     4) ssFilter: postIsSelected: [{borderLeftColor: rgb(77, 144, 240)'},
   *                   {borderLeftColor: rgb(77, 144, 240)', borderRightColor: 'rgb(167, 199, 247)'}]
   *        As (3), but also assigns the filtered class name(s) as a mapping for key 'postIsSelected'
   * @param {String} selector / {Function}search:
   *   Either a selector is provided, in which case jQuery is called on it to find the element
   *   and it will be recorded as the primary selector.  The detected selector becomes an alternate.
   *   Or a search function will be invoked to find the element.
   * @param {Function} slaveRules: Optional callback to call with 'this' set to $element
   *   for convenience.  If the selector/search function returns multiple elements, this slaveRules
   *   function will be called for every match.  So you may want to specify ':first' or .first()
   *   on your selector/search if you only want to search for slave keys one time.  If you do want
   *   to invoke a slave rules for the entire set of matched elements, use aggSlaveRules instead.
   * @param {Function} aggSlaveRules: Optional callback to call with 'this' set to $element
   *   for convenience, but only one time regardless of the number of matched elements.  This is
   *   useful if you want to generate slave rules after an entire loop of elements is processed.
   */
  extract: function(key, tentOptions, tentSelectorOrSearch, tentSlaveRules, tentLoopSlaveRules) {
    var ssFilters;
    var options = typeof tentOptions == 'object' ? tentOptions : {};
    if (options) {
      if (typeof options.pri != 'undefined' && typeof options.pri != 'string') {
        Webx.error('Error in rule for key "' + key + '": for pri option, expected a string');
        return null;
      }
      // Accept a string or an array for alternates
      if (typeof options.alt == 'string') {
        options.alt = [ options.alt ];
      } else if (typeof options.alt != 'undefined' && ! (options.alt instanceof Array)) {
        Webx.error('Error in rule for key "' + key + '": for alt option, expected a string or array; instead got ' + typeof options.alt);
        return null;
      }

      // Canonicalize format
      if (typeof options.ssFilter != 'undefined') {
        ssFilters = [];
        WebxMap.collectSsPatterns(ssFilters, options.ssFilter);
//        Webx.debug(key, ssFilters);
      }
    }

    var selectorOrSearch = typeof tentOptions == 'object' ? tentSelectorOrSearch : tentOptions;
    var search, selector;
    if (typeof selectorOrSearch == 'function') {
      search = selectorOrSearch;
    } else if (typeof selectorOrSearch == 'string') {
      selector = selectorOrSearch;
    } else {
      Webx.error('Error in rule for key "' + key + '": selectorOrSearch must be specified.');
      return null;
   }

    var slaveRules =
      typeof tentOptions == 'object' ? tentSlaveRules : tentSelectorOrSearch;
    var aggSlaveRules =
      typeof tentOptions == 'object' ? tentLoopSlaveRules : tentSlaveRules;
    if (typeof slaveRules != 'undefined' && typeof slaveRules != 'function')
      Webx.error("Error in rule for key '" + key + "': expected a function for slaveRules; instead got '" + typeof slaveRules + "'");
    if (typeof aggSlaveRules != 'undefined' && typeof aggSlaveRules != 'function')
      Webx.error("Error in rule for key '" + key + "': expected a function for aggSlaveRules; instead got '" + typeof aggSlaveRules + "'");

    // Copy, leaving out undefined properties
    var rules = $.extend({}, {
      pri: options.pri,
      alt: options.alt,
      ssFilters: ssFilters, // NOTE: we pluralize because there may be more than one patternset
      selector: selector,
      search: search,
      slaveRules: slaveRules,
      aggSlaveRules: aggSlaveRules
    });


    return this._extract(arguments.callee.caller, key, {}, rules);
  },

  /**
   * Internal function that does all the work of extracting selectors and class names.
   * @return null if no result, or an Object {s:, c:, rules:}
   */
  _extract: function(masterSlaveRules, key, options, rules) {
    
    /**
     * Invokes slaveRules or aggSlaveRules function with the right parameters
     * Has side effect of recording slaveRules.masterKey
     */
    function callSlaveRules(key, $elements, slaveRules, result) {
      if (typeof slaveRules == 'function') {
        slaveRules.masterKey = key;
        // Temporarily put the result in slaveRules, to give _extractFromStylesheets
        // access to cStyled for this element
        slaveRules.$master = $elements;
        slaveRules.masterResult = result;
        // Call the slaveRules, setting 'this' to a function that can be called
        // conveniently in CoffeeScript syntax @(search), where search is a function that
        // will be called now (and later, since the rule will recorded for future use) with
        // its own 'this' set to the current element $elements
        slaveRules.call(this.ruleContext($elements),
          key); // Passing the key, but no use so far; no use for return value either
        // Clean up
        delete slaveRules.$master;
        delete slaveRules.masterResult;
      }
    }

    var result;
    
    // Handle the case when there is one level of indirection in call stack
    if (typeof masterSlaveRules != 'undefined') {
      if (typeof masterSlaveRules.masterKey == 'undefined')
        masterSlaveRules = masterSlaveRules.caller;
    }

    // Call the search function, or jquery the selector
    var $elements = rules.search ? rules.search.call(masterSlaveRules.$master) : $(rules.selector);

    // If multiple elements, then loop through
    if ($elements.length > 1) {

      // XXX Right now we take the smallest selector of them all.
      // Maybe we need to take the most common or look deeper into the class names in the future
      var bestEntry;
      var _this = this;
      var cAlts = [];

      // Set up rules and options:
      // - don't let _extract store into this.map; we'll do that at the end of the loop
      //   XXX This may call for a refactor for consistency.
      // - overwrite means that we want _extract to recomputer the map, even if we already
      //   have one.  This way we get the best result from all the elements.
      $.extend(options, {dontStore: true, overwrite: true});
      var elRules = $.extend({}, rules);

      // Loop through the elements
      $elements.each(function(i, el) {
        var mapEntry = _this._extract(masterSlaveRules, key, options, $.extend(elRules, {
          search: function() {return $(el);}
        }));
        
        if (mapEntry) {
          if (typeof bestEntry == 'undefined' || (bestEntry.s && bestEntry.s.length > mapEntry.s.length))
            bestEntry = mapEntry;
          if (typeof mapEntry.c != 'undefined')
            cAlts.push(mapEntry.c);
        }
      });

      // If we don't already have mapping
      if (typeof this.map.s[key] == 'undefined') {
        // Copy the data from the entry with the shortest selector.
        // XXX Time will tell if this is a good heuristic.
        this.setMapEntry(key, bestEntry);
        
        // XXX I wonder if I should be checking if bestEntry has an undefined `c`, in which
        // case I should copy cAlts[0] if applicable.
        
        // Copy in the alternate `c`s
        cAlts.forEach(function(cAlt) {
          if (typeof _this.map.cAlt[key] == 'undefined')
            _this.map.cAlt[key] = [];
          Util.addUniqueStringArray(_this.map.cAlt[key], cAlt);
        });
        if (bestEntry.c && typeof _this.map.cAlt[key] != 'undefined') {
          Util.removeUniqueStringArray(_this.map.cAlt[key], bestEntry.c);
          if (_this.map.cAlt[key].length === 0)
            delete _this.map.cAlt[key];
        }

        if (this.mode == 'survey') {
          this.rules[key] = bestEntry.rules;
          this.rules[key].search = rules.search;
        }
      }

      result = bestEntry;

      // If the element was found, call the aggSlaveRules if any, to get at more rules.
      // If we're doing a survey of rules, we'll go deeper into the rules anyway
      if (result || this.mode == 'survey')
        callSlaveRules.call(this, key, $elements, rules.aggSlaveRules, result);

    } else { // For a single or no match

      // If we already have a mapping set, use that, unless there's an option to overwrite (from loop)
      if (typeof this.map.s[key] != 'undefined' && ! options.overwrite) {
        result = this.getMapEntry(key);
        result.rules = this.rules[key];

      } else if (! $elements.length) { // If no match, give warning
        WebxMap.warn(key, "No element to extract from.");
        result = null;

      } else { // If just one element
        result = {};

        var classes = this.getClasses(key, $elements, rules.ssFilters);

        // Set the class names
        if (classes.styled.length)
          result.cStyled = classes.styled;
        var concatClasses = classes.styled.concat(classes.nonStyled);
        if (concatClasses.length)
          result.c = concatClasses;

        // Compute a selector
        var computedSelector;
        if (classes.nonStyled.length) { // If we have non-styled classes, then use that as a selector
          computedSelector = '.' + classes.nonStyled.join('.');
        }

        // Take into account rulewriter's choice about primary or alternate selectors
        // Precedence is: options.pri > selector (from selectorOrSearch) > computed selector
        if (typeof rules.pri != 'undefined') {
          result.s = rules.pri;
        }
        if (typeof rules.selector != 'undefined') {
          if (typeof result.s == 'undefined')
            result.s = rules.selector;
          else
            result.sAlt = [ rules.selector ];
        }
        if (typeof computedSelector != 'undefined') {
          if (typeof result.s == 'undefined') {
            result.s = computedSelector;
          } else {
            if (! result.sAlt)
              result.sAlt = [];
            result.sAlt.push(computedSelector);
          }
        }

        // Keep other alternate(s)
        if (rules.alt) {
          if (! result.sAlt)
            result.sAlt = [];
          rules.alt.forEach(function(alt) {
            result.sAlt.push(alt);
          });
        }

        // Check if we have a selector at all
        if (typeof result.s == 'undefined') {
          // Take the styled classes as alternate
          if (result.cStyled) {
            // XXX Should we just make a selector out of styled classes intead of returning null?
            // I think we should so that our composed selectors such as gbarToolsProfileEmail work.
//            result.s = null; // Record that we have an entry
            result.s = '.' + classes.styled.join('.');
            if (! result.sAlt)
              result.sAlt = [];
            result.sAlt.push('.' + result.cStyled.join('.'));
            if (this.config.warn)
              WebxMap.warn(key, "Can't compute a definite selector; taking styled classes as alternate selector.");
          } else {
            WebxMap.warn(key, "Can't compute a selector.");
            result = null;
          }
        }

        if (result) {
          if (this.mode == 'survey') {
            result.rules = rules;

            // Record rule dependency
            if (typeof masterSlaveRules.masterKey != 'undefined')
              result.rules.masterKey = masterSlaveRules.masterKey;
          }

          // Record result into map, unless option is not to store, e.g. in a loop
          if (! options.dontStore) {
            this.setMapEntry(key, result);
            if (this.mode == 'survey' && ! options.dontStoreRules)
              this.rules[key] = result.rules;
          }
        }
      }

      // If the element was found, call the slaveRules if any, to get at more rules.
      // If we're doing a survey of rules, we'll go deeper into the rules anyway
      if (result || this.mode == 'survey')
        callSlaveRules.call(this, key, $elements, rules.slaveRules, result);
    }

    return result;
  },

  /**
   * Given an element, returns which classes are for styling and which seem primarily
   * usable as selectors
   * 
   * @option {object} ssFilters: Optional map to filter out styled class names
   */
  getClasses: function(key, $element, ssFilters) {
    var result = {
      styled: [],
      nonStyled: []
    };
    
    var element = $element.get(0);
    var _this = this;

    var savedClassNames = element.className;
    if (! savedClassNames)
      return result;

    // First, get rid of all the class names from other extensions
    var spacedClassNames = (' ' + savedClassNames + ' ').replace(CLASSNAMES_FILTER, '');
    if (this.config.classNamesFilter)
      spacedClassNames = spacedClassNames.replace(this.config.classNamesFilter, '');
    var classNames = spacedClassNames.replace(/^\s*|\s*$/g, '');
    
    // If there are no filters, we just look up which classes are styled
    if (! ssFilters) {
      classNames.split(/\s+/).forEach(function(c) {
        if (_this.webx._getCssRuleWrappersForClassName(c))
          result.styled.push(c);
        else
          result.nonStyled.push(c);
      });
      
    } else { // If there are filters
      
      if (classNames != savedClassNames)
        element.className = classNames;

      var cssRuleWrappersForFilters;
    
      // Find all the CSS rules that select the element and classify them
      // as matching or not matching each of the filters.
      // The ending structure will be like this:
      // [
      //   [ // List of cssRuleWrappers
      //     { // cssRuleWrapper 1
      //       sanitizedSelectorText: "string"
      //       cssRule: CSSStyleRule,
      //       // Optionally, if we later find out the rule doesn't apply to all elements
      //       // for this key:
      //       disabled: true,
      //       // Optionally, keeps track of class names already mapped to that filter
      //       filterIsMappedToClassNames: {class1, ...}
      //     },
      //     ...
      //   ],
      //   { // ssFilter 1
      //     key: 'postIsSelected', // Optional
      //     matching: [ cssRuleWrapper (as above), ... ]
      //     nonmatching: [ cssRuleWrapper (as above, ... ]
      //   },
      //   { // ssFilter 2
      //     ...
      //   }
      // ]

      // First, check the cache for our filter set structure.
      cssRuleWrappersForFilters = this.caches.keyToCssRuleWrappersForFilters[key];
      if (cssRuleWrappersForFilters) {
        // Make sure that the cssRules in the cache still select this specific element.
        // We only need to iterate through the first filter because all the filters
        // have references to the same objects: disabling one disables for all.
        // Also, reset the isNoLongerSelected for the algorithm below that classifies
        // the class names.
        cssRuleWrappersForFilters[0].forEach(function(cssRuleWrapper) {
          if (! $element.is(cssRuleWrapper.sanitizedSelectorText))
            cssRuleWrapper.disabled = true;
        });
        
      } else { // No cache hit, we need to iterate through the stylesheets
        
        // Get all the rules that select the element.
        // NOTE: this is convoluted for historical reasons.  It should be cleaned up.
        var cssRuleWrappers = [];
        this.forEachCssRuleSelectingElement($element, function(callback) {
//          // Pass in the generic iterator
//          return _this.webx._forEachCssRule(callback);

          // To speed things up, we'll just re-use our one-time stylesheet traversal
          var uniqueCssRuleWrappers = [];
          classNames.split(/\s+/).forEach(function(c) {
            var cssRuleWrappers = _this.webx._getCssRuleWrappersForClassName(c);
            if (cssRuleWrappers)
              cssRuleWrappers.forEach(function(cssRuleWrapper) {
                // Make sure to remove duplicates because multiple class names could be
                // pointing to the same rules.
                if (! cssRuleWrapper.negated && uniqueCssRuleWrappers.every(function(wrapper) {
                    return wrapper != cssRuleWrapper;
                    }))
                  uniqueCssRuleWrappers.push(cssRuleWrapper);
              });
          });
          uniqueCssRuleWrappers.forEach(function(cssRuleWrapper) {
            callback(cssRuleWrapper.cssRule);
          });
        }, function(cssRule, sanitizedSelectorText) {
          cssRuleWrappers.push({cssRule: cssRule, sanitizedSelectorText: sanitizedSelectorText});
        });
        
        cssRuleWrappersForFilters = [cssRuleWrappers];
        
        // For each filter, go through each rule and classify based on whether it matches
        ssFilters.forEach(function(ssFilter, i) {
          var obj = {matching: [], nonmatching: []};
          cssRuleWrappers.forEach(function(cssRuleWrapper) {
            if (WebxMap.styleMatch(cssRuleWrapper.cssRule.style, ssFilter.ssPatterns)) {
              obj.matching.push(cssRuleWrapper);
            } else {
              obj.nonmatching.push(cssRuleWrapper);
            }
          });
          if (obj.matching.length) {
//            obj.ssFilter = ssFilter;  // NOt used
            if (ssFilter.key)
              obj.key = ssFilter.key;
            cssRuleWrappersForFilters.push(obj);
          }
        });
     
        // Cache.
        this.caches.keyToCssRuleWrappersForFilters[key] = cssRuleWrappersForFilters;
      }
    
      // For each className in the specified element, consecutively
      // removes it from element and calls the callback.
      // NOTE: For efficiency, the class name needs to be restored back to its original state.
      classNames.split(/\s+/).forEach(function(c) {
        element.className = spacedClassNames.replace(' ' + c + ' ', ' ');
          
        // XXX Very hairy algorithm: we need unit tests for this.
        // Classify the class name under these 3 categories:
        // - nonStyled: removing the class name doesn't affect the selection
        //   of the element by any CSS Rule.  We should know this after one
        //   round of CSS rules.
        // - styled but filtered: removing the class name affects one or
        //   more CSS rules but in a specific way:
        //   For any of the filters,
        //   - *any* of the rules that matched the filter's patterns no longer select
        //     the element,
        //   - AND *all* the rules that didn't match the filter's patterns continue
        //     to select the element.
        //   So a shortcut out of a single filter (when we would know that the filter doesn't
        //   apply) would be the boolean opposite:
        //   - if *all* of the rules that matched the filter's patterns still
        //     select the element
        //   - OR if *any* of the rules that didn't match the filter's patterns stops
        //     selecting the element
        //   And a shortcut out of all the filters is when any of the filters applies.
        //   Actually, we do want to go through all the filters that have a key assigned.
        // - styled and unfiltered: whatever's leftover.
        //   We would normally be able to shortcut out as soon as any rule stops
        //   selecting the element, but for the 'unfiltered' part, we still have to go
        //   on to the next filter.  So the only shorcuts for this is, for each single filter,
        //   we can break out early -- but this is already taken care of in the "styled but
        //   filtered" category.

        // First go through every CSS rule to see which no longer select 
        var isStyled = false;
        var cssRuleWrappers = cssRuleWrappersForFilters[0];
        cssRuleWrappers.forEach(function(cssRuleWrapper) {
          if (! $element.is(cssRuleWrapper.sanitizedSelectorText)) {
            isStyled = true;

            if (! cssRuleWrapper.disabled)
              cssRuleWrapper.isNoLongerSelected = true;
          }
        });

        // If the class name is not styled, we're done: add it to the result
        if (! isStyled) {
          result.nonStyled.push(c);

        } else { // If the class name is styled, we have to check against the filters
          var isFiltered = false;
          cssRuleWrappersForFilters.slice(1).every(function(cssRuleWrappers) {
            if (cssRuleWrappers.filterIsMappedToClassNames &&
                c in cssRuleWrappers.filterIsMappedToClassNames) {
              isFiltered = true;
            
              return true; // Continue on to next filter
            }
            
            var thisFilterApplies = true;
            var isSelectedForAllMatching = true;
            ['nonmatching', 'matching'].every(function(type) {
              cssRuleWrappers[type].every(function(cssRuleWrapper) {
                // Skip any disabled CSS rule
                if (cssRuleWrapper.disabled)
                  return true;

                if (cssRuleWrapper.isNoLongerSelected) {
                  // Shortcut: we don't need to check the rest of the rules for this filter
                  if (type == 'nonmatching') {
                    // This filter doesn't apply because removing the class name makes
                    // a non-matching rule deselect the element
                    thisFilterApplies = false;
                    return false;
                  } else {
                    // This filter can still apply because removing the class names
                    // makes a matching rule deselect the element
                    isSelectedForAllMatching = false;
                  }
                }                
                return true; // next CSS rule for this type (matching/nonmatching)
              });

              // Shortcut
              if (! thisFilterApplies)
                return false;             
              return true; // next type, i.e. 'matching' if we've done 'nonmatching'
            });

            // All the matching rules for this filter still select the element;
            // so this filter doesn't apply
            if (isSelectedForAllMatching)
              thisFilterApplies = false;

            // If the filter applies to this class name
            if (thisFilterApplies) {
              isFiltered = true;

              // If there is an associated key, record it
              var filterKey = cssRuleWrappers.key;
              if (filterKey)
                _this.addToMapEntry(filterKey, '.' + c, [c]);

              // Record that this class name is to be filtered out
              if (! cssRuleWrappers.filterIsMappedToClassNames)
                cssRuleWrappers.filterIsMappedToClassNames = {};
              cssRuleWrappers.filterIsMappedToClassNames[c] = true;
/* Not useful and probably breaks other filters.
              // For efficiency, we can disable any of the css rules that are relevant to that
              // filter.
              // XXX Because I'm too lazy to put this into its own loop, this means
              // that the filter patterns must not overlap.  For example, if both
              // postIsSelected and postIsNew match the same rule because then
              // disabling early means that postIsSelected may be mapped while postIsNew isn't.
              // Easy fix, but not worth it right now.
              // XXX Also, if a particular key has multiple filters associated, then
              // it's not clear which class name it will end up with.  Not worth thinking about
              // right now.
              cssRuleWrappers['matching'].forEach(function(cssRuleWrapper) {
                if (cssRuleWrapper.isNoLongerSelected)
                  cssRuleWrapper.disabled = true;

                // Add the rule's selectors as alternate selector for that key
                var selectorText = cssRuleWrapper.sanitizedSelectorText;
                if (cssRuleWrappers.key && selectorText != selector) {
                  if (! _this.map.sAlt[filterKey]) {
                    _this.map.sAlt[filterKey] = [selectorText];
                  } else {
                    if (_this.map.sAlt[filterKey].indexOf(selectorText) < 0)
                      _this.map.sAlt[filterKey].push(selectorText);
                  }
                }
              });
*/

              // We don't shortcut out because we want to check the other filters
              // in case they have an associated key to map.
            }

            return true; // next filter
          });


          // If the class name is styled but unfiltered, add it to the result
          if (! isFiltered)
            result.styled.push(c);
        }

        // Reset the isNoLongerSelected for the next classname
        cssRuleWrappers.forEach(function(cssRuleWrapper) {
          delete cssRuleWrapper.isNoLongerSelected;
        });
      });

      // Restore the class name
      if (element.className != savedClassNames)
        element.className = savedClassNames;
    }

    // We try non-styled classes first because that's probably why G+ has them
    // there: to selecte elements.
    return result;
  },


  /**
   * Wrapper for _extractFromStylesheets function which handles unspecified parameters.
   * @param tentPatterns {Object} or {Array}: Optional 2-element array which
   *   contains the $element of the master rule and the patterns.
   * @param tentFilter {Function}: Optional function which takes the selectorText
   *   and returns a processed version
   */
  extractFromStylesheets: function(tentOptions, tentPatterns, tentFilter) {
    /* No options right now
    var options =
      typeof tentPatterns == 'object' ? tentOptions : {};
    */
   
    var patterns =
      typeof tentPatterns == 'object' ? tentPatterns : tentOptions;
    
    if (typeof patterns != 'object') {
      Webx.error('Error in ss rule: missing argument with patterns.');
      return null;
    }
      
    var ssPatterns = [];
    var keys = WebxMap.keysInPatterns(patterns);
    // We only support one key per line, right now
    if (keys.length != 1) {
      Webx.error('Error in rule for keys "' + keys.join(' ') + '": we only support one key and one set of patterns righ tnow.');
      return null;
    }
    var key = keys[0];
    
    // Re-use collectKeyedFilters even though the name 'Filter' doesn't make sense here
    if (! WebxMap.collectKeyedSsPatterns(ssPatterns, patterns)) {
      Webx.error('Error in rule for key "' + key + '": no proper `patterns` specified.');
      return null;
    }
//      Webx.debug(key, ssPatterns);

    var filter =
      typeof tentPatterns == 'object' ? tentFilter : tentPatterns;
    if (typeof filter != 'undefined' && typeof filter != 'function') {
      Webx.error('Error in rule for key "' + key + '": expected function filter; instead got ' + typeof filter);
      return null;
    }

    return this._extractFromStylesheets(arguments.callee.caller, keys[0], {},
      {ssPatterns: ssPatterns}, filter);
  },

  /**
   * Extract selectors from stylesheets based on matches of CSS styles
   */
  _extractFromStylesheets: function(masterSlaveRules, key, options, rules, filter) {
    /**
     * Given that the className's cssRules does match the patterns,
     * this adds the relevant data to the result and matches
     */
    var classNameMatchesPatterns = function(result, matches, cssRule) {
      var origSelectorText = cssRule.selectorText;
      var sanitizedSelectorText = WebxMap.sanitizeSelectorText(origSelectorText);
      
      // Let the filter process the filter
      if (typeof filter == 'function') {
        var proposedSelectorText = filter.call(sanitizedSelectorText);
        if (typeof proposedSelectorText != 'string') {
          WebxMap.error(key, "For value returned from filter, expected string, but got " + typeof proposedSelectorText);
          return;
        }
        sanitizedSelectorText = proposedSelectorText;
      }

      // If the selector text simply consists of class names, then record them
      var styledClassNameList;
      if (CLASSNAMES_REGEXP_TEST.test(origSelectorText)) {
        styledClassNameList = [];
        var m;
        while ((m = CLASSNAMES_REGEXP.exec(origSelectorText)) !== null)
          styledClassNameList.push(m[1]);
      }

      // Add it all to the result
      WebxMap.addToMapItem(result, sanitizedSelectorText, origSelectorText, styledClassNameList);        

      matches.push({cssRule: cssRule});
    };
    
    var ssKeyedPatterns = rules.ssPatterns[0]; // We only support one key per line, right now

    var result = {};
    var matches = [];
    var _this = this;

    // Handle the case when there is one level of indirection in call stack
    if (typeof masterSlaveRules != 'undefined') {
      if (typeof masterSlaveRules.masterKey == 'undefined')
        masterSlaveRules = masterSlaveRules.caller;
    }

    // If we already have a mapping set, use that.
    // This is great if we already have a mapping from an ssFilter already, eg.
    // postIsNew or postIsSelected can come out of extracting from 'post'.
    if (typeof this.map.s[key] != 'undefined') {
      result = this.getMapEntry(key);
      result.rules = this.rules[key];
      
    } else {
      
      // We ignore contexts that are document, which means that the rule was written as a top-level rule
      // outside of any rule context.
      var $context = masterSlaveRules && masterSlaveRules.$master;
      if ($context && $context.is(document))
        $context = null;
      
      // If we have context, then we look up the CSS rules and go through those.
      if ($context) {
        if (masterSlaveRules.masterResult.c) {
          masterSlaveRules.masterResult.c.forEach(function(className) {
            var cssRuleWrappers = _this.webx._getCssRuleWrappersForClassName(className);
            if (cssRuleWrappers) {
              cssRuleWrappers.forEach(function(cssRuleWrapper) {
                if (! cssRuleWrapper.negated &&
                    WebxMap.styleMatch(cssRuleWrapper.cssRule.style, ssKeyedPatterns.ssPatterns))
                  classNameMatchesPatterns(result, matches, cssRuleWrapper.cssRule);
              });
            }
          });
        }
        
      } else { // Without context, we have to go through all the CSS rules from the stylesheets

        // Iterate over all CSS rules that match patterns
        this.forEachCssRuleMatchingPatterns(ssKeyedPatterns.ssPatterns, function(cssRule) {
          classNameMatchesPatterns(result, matches, cssRule);
        });
      }
      
      if (matches.length === 0) {
        WebxMap.error(key, "Can't find a match in stylesheets");
        result = null;
      }
    }

    if (result) {
      if (matches.length > 1) {
        WebxMap.warn(key, "Found " + matches.length + " matches.");
        matches.forEach(function(match, i) {
          WebxMap.warn(key, "Match " + i + ": " + match.cssRule.cssText);
        });
      }

      if (this.mode == 'survey') {
        // Return the rule
        result.rules = rules;

        if (typeof masterSlaveRules != 'undefined') {
          // Record rule dependency
          if (typeof masterSlaveRules.masterKey != 'undefined')
            result.rules.masterKey = masterSlaveRules.masterKey;
        }
      }

      // Record result into map, unless option is not to store, e.g. in a loop
      if (! options.dontStoreRules)
        this.rules[key] = result.rules;

      // Record result into map, unless option is not to store, e.g. in a loop
      if (! options.dontStore) {
        this.setMapEntry(key, result);
        if (this.mode == 'survey' && ! options.dontStoreRules)
          this.rules[key] = result.rules;
      }
    }

    return result;
  },
  

  /** 
   * Traverses the stylesheets and calls the callback for each rule,
   * checking the selector against $elements if any, and calls the callback
   * with the arguments (cssRule, selectorText), where selectorText is the
   * cssRule's selectorText possibly modified to remove non-DOM selectors.
   * Note that $elements could be a list of elements (e.g. coming from the loop
   * portion of _extract), in which case we have a match if any of the
   * elements matches the selector.
   */
  forEachCssRuleSelectingElement: function($elements, iterator, callback) {
    iterator(function(cssRule) {
       var selectorText = cssRule.selectorText;
       if ($elements) {
         try {
           if (! $elements.is(selectorText))
             return;
         } catch (e) {
           // Get rid of pseudoclasses that jQuery doesn't support, e.g. hover.
           // Since this is relatively rare, we do it in an exception handler for
           // efficiency
           var sanitizedSelectorText = WebxMap.sanitizeSelectorText(selectorText);
           if (sanitizedSelectorText != selectorText) {
             selectorText = sanitizedSelectorText;
             if (! $elements.is(selectorText))
               return;
           }
         }
       }
       callback(cssRule, selectorText);
    });
  },

  /**
   * Traverses the stylesheets and calls the callback for each
   * rule that matches the ssPatterns (map of CSS style to
   * strings or RegExps to be matched against CSS value)
   */
  forEachCssRuleMatchingPatterns: function(ssPatternSet, callback) {
    this.webx._forEachCssRule(function(cssRule) {
      var style = cssRule.style; 
      if (WebxMap.styleMatch(style, ssPatternSet))
        callback(cssRule);
    });
  }
};


/**
 * Namespace for general utility functions
 */
function Util() {
}

/**
 * Returns true if the 2 arrays of strings are equal
 */
Util.stringArraysAreEqual = function(a,b) {
  if (a === null && b === null)
    return true;

  if (a === null || b === null)
    return false;

  if (a.length != b.length)
    return false;

  for (var i=0;i<a.length;i++) {
    if (a[i] !== b[i]) 
      return false;
  }

  return true;
};

/**
 * Adds string array to an array of unique string arrays unless already there
 */
Util.addUniqueStringArray = function(collection, array) {
  array.sort();
  if (collection.every(function(c) {
    return ! Util.stringArraysAreEqual(c, array);
  }))
    collection.push(array);
};

/**
 * Removes string array from array of unique string arrays
 */
Util.removeUniqueStringArray = function(collection, array) {
  array.sort();
  var offset = 0;
  collection.forEach(function(c, i) {
    if (Util.stringArraysAreEqual(c, array)) {
      collection.splice(i - offset, 1);
      offset++;
    }
  });
};

/**
 * @return true if a is a version that's newer than b.
 * Handles both 0.001 format and 2011-08-30.001 format, where
 * a date is considered more recent because it's official.
 * Adapted from:
 * http://stackoverflow.com/questions/2809909/get-version-number-from-string-in-javascript/2809967#2809967
 */
Util.isVersionNewer = function(a, b) {
  if (a == b)
    return false;
  else if (!a && b)
    return false;
  else if (a && !b)
    return true;
  
  // Compare versions in date format
  var aDate = a.indexOf('-') >= 0;
  var bDate = b.indexOf('-') >= 0;
  if (aDate && bDate)
    return a > b;
  else if (aDate)
    return true;
  else if (bDate)
    return false;  
  
  // Compare versions in normal x.x.x format
  var partsA = a.split('.');
  var partsB = b.split('.');
  var numParts = partsA.length > partsB.length ? partsA.length : partsB.length;

  for (var i = 0; i < numParts; i++) {
    var aInt = parseInt(partsA[i], 10) || 0;
    var bInt = parseInt(partsB[i], 10) || 0;
    if (aInt !== bInt)
      return aInt > bInt;
  }

  return false;
};

/**
 * Increments the last part of a version string where the string can be of format
 * - 2011-08-30.1  [A date must have a "decimal"]
 * - 1.1
 * - 1
 */
Util.incrementVersion = function(v) {
  if (!v)
    return 1;
  
  var parts = v.split('.');
  var result = parts.slice(0, -1).join('.');
  if (result.length)
    result += '.'
  return  result + (parseInt(parts[parts.length - 1], 10) + 1);
};

// Webx namespace
if (!this.Webx)
  this.Webx = Webx;

}).call(/*<CommonJS>*/(typeof exports != 'undefined') ? exports : /*</CommonJS>*/this, jQuery);

/* Last integrated into Webx: 2011-08-27
---
name: Slick.Parser
description: Standalone CSS3 Selector parser
provides: Slick.Parser
...
*/

;(function(){

var parsed,
	separatorIndex,
	combinatorIndex,
	reversed,
	cache = {},
	reverseCache = {},
	reUnescape = /\\/g;

// huyz 2011-08-28
// We need to use this for a lot of selectors just one time
//var parse = function(expression, isReversed){
var parse = function(expression, isReversed, options){
	if (expression == null) return null;
	if (expression.Slick === true) return expression;
	expression = ('' + expression).replace(/^\s+|\s+$/g, '');
	reversed = !!isReversed;
	var currentCache = (reversed) ? reverseCache : cache;
	if (currentCache[expression]) return currentCache[expression];
	parsed = {
		Slick: true,
		expressions: [],
		raw: expression,
		reverse: function(){
			return parse(this.raw, true);
		}
	};
	separatorIndex = -1;
	while (expression != (expression = expression.replace(regexp, parser)));
	parsed.length = parsed.expressions.length;
        // huyz 2011-08-28
        if (options && options.nocache)
          return (reversed) ? reverse(parsed) : parsed;
        else
          return currentCache[parsed.raw] = (reversed) ? reverse(parsed) : parsed;
};

var reverseCombinator = function(combinator){
	if (combinator === '!') return ' ';
	else if (combinator === ' ') return '!';
	else if ((/^!/).test(combinator)) return combinator.replace(/^!/, '');
	else return '!' + combinator;
};

var reverse = function(expression){
	var expressions = expression.expressions;
	for (var i = 0; i < expressions.length; i++){
		var exp = expressions[i];
		var last = {parts: [], tag: '*', combinator: reverseCombinator(exp[0].combinator)};

		for (var j = 0; j < exp.length; j++){
			var cexp = exp[j];
			if (!cexp.reverseCombinator) cexp.reverseCombinator = ' ';
			cexp.combinator = cexp.reverseCombinator;
			delete cexp.reverseCombinator;
		}

		exp.reverse().push(last);
	}
	return expression;
};

var escapeRegExp = function(string){// Credit: XRegExp 0.6.1 (c) 2007-2008 Steven Levithan <http://stevenlevithan.com/regex/xregexp/> MIT License
	return string.replace(/[-[\]{}()*+?.\\^$|,#\s]/g, function(match){
		return '\\' + match;
	});
};

var regexp = new RegExp(
/*
#!/usr/bin/env ruby
puts "\t\t" + DATA.read.gsub(/\(\?x\)|\s+#.*$|\s+|\\$|\\n/,'')
__END__
	"(?x)^(?:\
	  \\s* ( , ) \\s*               # Separator          \n\
	| \\s* ( <combinator>+ ) \\s*   # Combinator         \n\
	|      ( \\s+ )                 # CombinatorChildren \n\
	|      ( <unicode>+ | \\* )     # Tag                \n\
	| %    ( <unicode>+ )           # Webx mapping key   \n\
	| \\#  ( <unicode>+       )     # ID                 \n\
	| \\.  ( <unicode>+       )     # ClassName          \n\
	|                               # Attribute          \n\
	\\[  \
		\\s* (<unicode1>+)  (?:  \
			\\s* ([*^$!~|]?=)  (?:  \
				\\s* (?:\
					([\"']?)(.*?)\\10 \
				)\
			)  \
		)?  \\s*  \
	\\](?!\\]) \n\
	|   (:+) ( <unicode>+ )(?:\
	\\( (?:\
		(?:([\"'])([^\\13]*)\\13)|((?:\\([^)]+\\)|[^()]*)+)\
	) \\)\
	)?\
	)"
*/
// huyz 2011-08-28 We reserve the '%' character
//	"^(?:\\s*(,)\\s*|\\s*(<combinator>+)\\s*|(\\s+)|(<unicode>+|\\*)|\\#(<unicode>+)|\\.(<unicode>+)|\\[\\s*(<unicode1>+)(?:\\s*([*^$!~|]?=)(?:\\s*(?:([\"']?)(.*?)\\9)))?\\s*\\](?!\\])|(:+)(<unicode>+)(?:\\((?:(?:([\"'])([^\\13]*)\\13)|((?:\\([^)]+\\)|[^()]*)+))\\))?)"
//	.replace(/<combinator>/, '[' + escapeRegExp(">+~`!@$%^&={}\\;</") + ']')
	"^(?:\\s*(,)\\s*|\\s*(<combinator>+)\\s*|(\\s+)|(<unicode>+|\\*)|%(<unicode>+)|\\#(<unicode>+)|\\.(<unicode>+)|\\[\\\s*(<unicode1>+)(?:\\s*([*^$!~|]?=)(?:\\s*(?:([\"']?)(.*?)\\10)))?\\s*\\](?!\\])|(:+)(<unicode>+)(?:\\((?:(?:([\"'])([^\\13]*)\\13)|((?:\\([^)]+\\)|[^()]*)+))\\)\)?)"
	.replace(/<combinator>/, '[' + escapeRegExp(">+~`!@$^&={}\\;</") + ']')
	.replace(/<unicode>/g, '(?:[\\w\\u00a1-\\uFFFF-]|\\\\[^\\s0-9a-f])')
	.replace(/<unicode1>/g, '(?:[:\\w\\u00a1-\\uFFFF-]|\\\\[^\\s0-9a-f])')
);

function parser(
	rawMatch,

	separator,
	combinator,
	combinatorChildren,

	tagName,
	webxKey,
	id,
	className,

	attributeKey,
	attributeOperator,
	attributeQuote,
	attributeValue,

	pseudoMarker,
	pseudoClass,
	pseudoQuote,
	pseudoClassQuotedValue,
	pseudoClassValue
){
	if (separator || separatorIndex === -1){
		parsed.expressions[++separatorIndex] = [];
		combinatorIndex = -1;
		if (separator) return '';
	}

	if (combinator || combinatorChildren || combinatorIndex === -1){
		combinator = combinator || ' ';
		var currentSeparator = parsed.expressions[separatorIndex];
		if (reversed && currentSeparator[combinatorIndex])
			currentSeparator[combinatorIndex].reverseCombinator = reverseCombinator(combinator);
		currentSeparator[++combinatorIndex] = {combinator: combinator, tag: '*'};
	}

	var currentParsed = parsed.expressions[separatorIndex][combinatorIndex];

	if (tagName){
		currentParsed.tag = tagName.replace(reUnescape, '');

	} else if (webxKey){ // huyz 2011-08-28 Copied from className
		webxKey = webxKey.replace(reUnescape, '');

		if (!currentParsed.webxKeyList) currentParsed.webxKeyList = [];
		if (!currentParsed.webxKeys) currentParsed.webxKeys = [];
		currentParsed.webxKeyList.push(webxKey);
		currentParsed.webxKeys.push({
			value: webxKey,
                        // This regexp is not need, but who knows down the road
			regexp: new RegExp('(^|\\s)' + escapeRegExp(webxKey) + '(\\s|$)')
		});

	} else if (id){
		currentParsed.id = id.replace(reUnescape, '');

	} else if (className){
		className = className.replace(reUnescape, '');

		if (!currentParsed.classList) currentParsed.classList = [];
		if (!currentParsed.classes) currentParsed.classes = [];
		currentParsed.classList.push(className);
		currentParsed.classes.push({
			value: className,
			regexp: new RegExp('(^|\\s)' + escapeRegExp(className) + '(\\s|$)')
		});

	} else if (pseudoClass){
		pseudoClassValue = pseudoClassValue || pseudoClassQuotedValue;
                // huyz 2011-08-28 To workaround bug https://github.com/mootools/slick/issues/52
                var rawPseudoClassValue = pseudoClassValue;
		pseudoClassValue = pseudoClassValue ? pseudoClassValue.replace(reUnescape, '') : null;

		if (!currentParsed.pseudos) currentParsed.pseudos = [];
		currentParsed.pseudos.push({
			key: pseudoClass.replace(reUnescape, ''),
			value: pseudoClassValue,
                        rawValue: rawPseudoClassValue,
			type: pseudoMarker.length == 1 ? 'class' : 'element'
		});

	} else if (attributeKey){
		attributeKey = attributeKey.replace(reUnescape, '');
		attributeValue = (attributeValue || '').replace(reUnescape, '');

		var test, regexp;

		switch (attributeOperator){
			case '^=' : regexp = new RegExp(       '^'+ escapeRegExp(attributeValue)            ); break;
			case '$=' : regexp = new RegExp(            escapeRegExp(attributeValue) +'$'       ); break;
			case '~=' : regexp = new RegExp( '(^|\\s)'+ escapeRegExp(attributeValue) +'(\\s|$)' ); break;
			case '|=' : regexp = new RegExp(       '^'+ escapeRegExp(attributeValue) +'(-|$)'   ); break;
			case  '=' : test = function(value){
				return attributeValue == value;
			}; break;
			case '*=' : test = function(value){
				return value && value.indexOf(attributeValue) > -1;
			}; break;
			case '!=' : test = function(value){
				return attributeValue != value;
			}; break;
			default   : test = function(value){
				return !!value;
			};
		}

		if (attributeValue == '' && (/^[*$^]=$/).test(attributeOperator)) test = function(){
			return false;
		};

		if (!test) test = function(value){
			return value && regexp.test(value);
		};

		if (!currentParsed.attributes) currentParsed.attributes = [];
		currentParsed.attributes.push({
			key: attributeKey,
			operator: attributeOperator,
			value: attributeValue,
			test: test
		});

	}

	return '';
};

// Slick NS

var Slick = (this.Slick || {});

// huyz 2011-08-28
//Slick.parse = function(expression){
//        return parse(expression);
//};
Slick.parse = function(expression, options){
        return parse(expression, undefined, options);
};

Slick.escapeRegExp = escapeRegExp;

if (!this.Slick) this.Slick = Slick;

// huyz 2011-08-27 Move into our namespace in case the user adds Slick or MooTools.
// Is this the idiomatic way to do it?
//}).apply(/*<CommonJS>*/(typeof exports != 'undefined') ? exports : /*</CommonJS>*/this);
}).apply(/*<CommonJS>*/(typeof exports != 'undefined') ? exports.Webx : /*</CommonJS>*/Webx);
