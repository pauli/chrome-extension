/*
 * Filename:         webx-background.js
 * More info, see:   webx.js
 *
 * Web:              http://huyz.us/webx/
 * Source:           https://github.com/huyz/webx
 * Author:           Huy Z  http://huyz.us/
 *
 * Usage:
 * This file is only needed if you want to support Chrome 12 or earlier.
 * - Load this script in your background.html
 * - Make sure that you have some version of jQuery loaded in your background.html
 */

/****************************************************************************
 * Event handlers
 ***************************************************************************/

;(function($) {
  // Listen to incoming messages from Webx content scripts
  chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
    switch (request.action) {
      case 'webxAjax':
        // Proxy the Ajax request for the content script
        $.ajax({
          url: request.url,
          dataType: request.dataType,
          data: request.data
        }).done(function(data) {
          if (typeof sendResponse == 'function')
            sendResponse(data);
        }).fail(function() {
          console.error("Webx: Ajax request to url \"" + request.url + "\" failed.");
          if (typeof sendResponse == 'function')
            sendResponse(null);
        });
        break;
      default: break;
    }
  });
})(jQuery);
