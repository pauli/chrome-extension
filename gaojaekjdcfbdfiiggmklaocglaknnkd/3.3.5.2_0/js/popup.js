$(document).ready(function() {
    $(".parent").click(function() {
        var key = $( this ).attr('aria-key');
        $('.'+ key).slideDown().addClass('active');
        
        $( "#mainNav .child1" ).each(function() {
             if (($(this).hasClass('active') == true ) && ( $(this).hasClass(key) == false)) {
                $(this).slideUp().removeClass('active');
             }
        });
        $(".parent").each(function() { $( this ).removeClass('active'); });
        $( this ).addClass('active');
    });
});

$(document).ready(function() {
  listFilter($(".list"));
});

// Called when the user clicks on the browser action.
//  chrome.browserAction.onClicked.addListener(function(tab) {
//    chrome.tabs.create({ url: tab.href });
//  });
	
  // custom css expression for a case-insensitive contains()
  jQuery.expr[':'].Contains = function(a,i,m){
      return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
  };


  function listFilter(list) { // header is any element, list is an unordered list
    $(list).find("li").hide();
    $('.divider').hide();
    $('.filterinput')
      .change( function () {
        var filter = $(this).val();
        if(filter) {
          // this finds all links in a list that contain the input,
          // and hide the ones not containing the input while showing the ones that do
          $(list).find("a:not(:Contains(" + filter + "))").parent().slideUp();
          $(list).find("a:Contains(" + filter + ")").parent().slideDown();
          $('.divider').show();
        } else {
          $('.divider').hide();
          $(list).find("li").hide();
        }
        return false;
      })
    .keyup( function () {
        // fire the above change event after every letter
        $(this).change();
        $( "#mainNav .child1" ).each(function() {
             if (($(this).hasClass('active') == true )) {
                $(this).slideUp().removeClass('active');
             }
        });
        $('.parent.active').removeClass('active');
    });
  }
