var Config = {
	extensionName: 'Multiple-Account Weibo',
	extensionKey: '',
	settingKey: '-SETTING-',
	dataKey: '-DATA-',
	defaultSettings: {
		"is_new_user": true
	}
};

var EXTENSION_NAME = Config.extensionName;
var DEFAULT_SETTINGS = Config.defaultSettings;
var SETTING_KEY = Config.settingKey;
var DATA_KEY = Config.dataKey;