(function(){

	Object.isEmpty = function(obj){
		var _is = true;
		for (var i in obj) {
			if (obj.hasOwnProperty(i)) {
				if (typeof(obj[i]) == 'object') {
					if (!arguments.callee.call(null, obj[i])) _is = false;
				} else {
					_is = false;
				}
			}
		}
		return _is;
	}

	Object.equal = function(a,b) {
		var bKeys = [];
		for (var i in b) {
			if (b.hasOwnProperty(i)) bKeys.push(i);
		} 
		for (var i in a) {
			if (a.hasOwnProperty(i)) {
				if (a[i] !== b[i]) return false;
				else bKeys.splice(bKeys.indexOf(i),1);
			}
		}
		return bKeys.length == 0;
	}

	location.getParams = function(){
		var params = {};
		var found = location.search.match(/([^&?=]+)=([^&#$]+)/ig);
		
		if (!!found) {
			for (var i=0, len=found.length; i<len; i++) {
				var _tmp = found[i].split('=');
				params[_tmp[0]] = decodeURIComponent(_tmp[1]);
			}
		}
		return params;
	}

})();


var LsKeyPool = function(){
	var KEYPOOL_STORAGE_KEY = 'CY_KEYPOOL';
	var default_pool = [];
	return {
		add: function(key){
			var pool = LS(KEYPOOL_STORAGE_KEY) || default_pool;
			if(pool.indexOf(key) == -1) {pool.push(key);LS(KEYPOOL_STORAGE_KEY,pool,false);return true;}
			else {return false;}
		},
		del: function(key) {
			var pool = LS(KEYPOOL_STORAGE_KEY) || default_pool;
			pool.splice(pool.indexOf(key),1);
			return LS(KEYPOOL_STORAGE_KEY,pool,false);
		},
		list: function() {
			return LS(KEYPOOL_STORAGE_KEY) || default_pool;
		}
	}
}();

var LS = function(){
	
	function set(key, value, addToPool) {
		if (addToPool !== false) addToPool = true;
		if (value != null) {
			if (typeof(value) == 'object') value = JSON.stringify(value);
			!!addToPool && LsKeyPool.add(key);
			return localStorage.setItem(key, value);
		} else {
			LsKeyPool.del(key);
			return localStorage.removeItem(key);
		}
	}
	function get (key) {
		var value = localStorage.getItem(key);
		try {
			return JSON.parse(value);
		} catch(ex) {
			return value;
		}
	}
	return function (key, value, addToPool) {
		var len = arguments.length;
		if (len == 0) {
			var pool = LsKeyPool.list();
			var storage = {};
			for (var i=0,len=pool.length; i<len; i++) {
				storage[pool[i]] = get(pool[i]);
			}
			return storage;
		} else if (len == 1) {
			return get(key);
		} else {
			return set(key, value, addToPool);
		}
	};
}();


var URL = function(){
	return {
		isAbsolute: function(url){
			return /^https?:\/\//.test(url);
		},
		addHost: function(url, host, notPortocol) {
			// notPortocol, still not implemented
			if (!URL.isAbsolute(url)) {
				host = host || location.origin;
				url = host + url;
			}
			else if (!!host){
				url = url.replace(/^https?:\/\/[^\/$]+/, host);
			}
			return url;
		},
		getOrigin: function(url) {
			return url.replace(/^([^:]+:\/\/[^\/]+).*$/i, "$1");
		},
		getDomain: function(url) {
			return url.replace(/^[^:]+:\/\/([^\/]+).*$/i, "$1");
		},
		getProtocol:function(url) {
			return url.replace(/^([^:]+):\/\/[^\/]+.*$/i, "$1");
		}
	}
}();


var Notice = function(){
	var text_defaults = {
		'timeout': 1500,
		'clear': false,
		'icon' : 'icon.png',
		'ondisplay': null,
		'onerror': null,
		'onclick': null,
		'onclose': null
	};
	var html_defaults = {
		'timeout': 1500,
		'clear': false,
		'ondisplay': null,
		'onerror': null,
		'onclick': null,
		'onclose': null
	};
	var pool = [];
	function clear() {
		//chrome.extension.getViews({type:"notification"}).forEach(function(win) {
			//console.log(win);
			////win.cancel();
		//});
	}
	function bindEvents(notification, settings) {
		//!!settings['onclose'] && notification.onclose = settings['onclose'];
		//!!settings['onerror'] && notification.onerror = settings['onerror'];
		//!!settings['onclick'] && notification.onclick = settings['onclick'];
		//!!settings['ondisplay'] && notification.ondisplay = settings['ondisplay'];
	}
	return {
		'text': function(title, body, timeout, options){
			body = body || '';
			options['timeout'] = (timeout==null) ? 1500 : timeout;
			var settings = $.extend({}, text_defaults, options);
			if (!!settings['clear']) {
				clear();
			}
			var notification = window.webkitNotifications.createNotification(settings['icon'], title, body);
			if (!!notification) {
				pool.push(notification);
				bindEvents(notification, settings);
				notification.show();
				if (!!settings['timeout']) {
					setTimeout(function(){
						notification.cancel();
					},timeout);
				}
				return notification;
			} else {
				return null;
			}
		},
		'html': function(url, timeout, options){
			options['timeout'] = (timeout==null) ? 1500 : timeout;
			var settings = $.extend({}, html_defaults, options);
			if (!!settings['clear']) {
				clear();
			}
			var notification = window.webkitNotifications.createHTMLNotification(url);
			if (!!notification) {
				pool.push(notification);
				bindEvents(notification, settings);
				notification.show();
				if (!!settings['timeout']) {
					setTimeout(function(){
						notification.cancel();
					},timeout);
				}
				return notification;
			} else {
				return null;
			}
		},
		'clear': function(){
			clear();
		}
	}
}();

function getDateTimeStr(date) {
	date = date || new Date();
	return date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+":"+date.getSeconds();
}

function setBrowserIcon(url) {
	iconImg = new Image();
	iconImg.src = url;
	iconImg.onload = function(){
		var canvas = $('<canvas></canvas>')[0];
		var ctx = canvas.getContext('2d');
		ctx.drawImage(iconImg,0,0,19,19);
		chrome.browserAction.setIcon({
			imageData: ctx.getImageData(0,0,19,19)
		});	
	};
}

function cinfo(msg, showTime) {
	console.info(""+EXTENSION_NAME+" [INFO] "+(!!showTime?getDateTimeStr()+' ':'')+msg);
}

function ctrace(msg, showTime) {
	console.log(""+EXTENSION_NAME+" [TRACE] "+(!!showTime?getDateTimeStr()+' ':'')+msg);
}

function cerror(msg, showTime) {
	console.error(""+EXTENSION_NAME+" [ERROR] "+(!!showTime?getDateTimeStr()+' ':'')+msg);
}

function cresult(msg, showTime) {
	console.info(""+EXTENSION_NAME+" [-RESULT-] "+(!!showTime?getDateTimeStr()+' ':'')+msg);
}

var Validator = function(){
	return {
		url: function(val) {
			return /https?:\/\/([a-zA-Z0-9-]\.)+[a-zA-Z0-9]+.*/i.test(str);
		},
		mobile_number: function(val) {
			return /^\d{11}$/.test(val);
		},
		enum: function(val, arr) {
			return arr.indexOf(val) != -1;
		},
		regexp: function(val, exp) {
			return new RegExp(exp).test(val);
		}
	}
}();

var FormUtil = function() {
	var errorList;
	function fillForm(form, data) {
		for(var i in data) {
			!!form[i] && (form[i].value = data[i]);
		}
	}
	function formData(form) {
		var fields = form.querySelectorAll('[name]');
		var data = {};
		for (var i=0; i<fields.length; i++) {
			data[fields[i].name] = fields[i].value;
		}
		return data;
	}
	return {
		data: function(form, data) {
			var argslen = arguments.length;
			if (argslen == 1) {
				return formData(form);
			} else if (argslen == 2) {
				fillForm(form, data);
			}
		}
	}
}();
