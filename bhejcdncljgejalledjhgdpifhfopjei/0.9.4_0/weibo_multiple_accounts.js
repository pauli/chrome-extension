
var Page = function(){
	return {
		'getUid': function() {
			return $('li[node-type=account] .person_infos dt img').attr('src')
				.replace(/^http:\/\/[^\.]+\.sinaimg\.cn\/(\d+)\/.+$/, "$1");
		},
		'getNick': function() {
			return $('li[node-type=account] .person_infos dt img').attr('alt');
		},
		'getPhoto': function() {
			return $('li[node-type=account] .person_infos dt img').attr('src');
		},
		'getUserInfo': function() {
			return {
				uid: Page.getUid(),
				nick: Page.getNick(),
				photo: Page.getPhoto()
			}
		},
		'isLogin': function() {
			return !!$('li[node-type=account] .person_infos dt img');
		}
	}
}();


$(function(){

	if (Page.isLogin()) {

		//console.log(Page.getUserInfo());

		var exitBtn = $('li[node-type=account] .layer_topmenu_list ul li').last();
		var changeAccountBtn = $('<li><a href="#">切换到其他帐号</a></li>');
		changeAccountBtn.insertBefore(exitBtn);
		changeAccountBtn.on('click', function(e){
			e.preventDefault();
			chrome.extension.sendRequest(
				{
					cmd: "store_and_clear", 
					data: Page.getUserInfo()
				}, 
				function(response) {
					location.href = "http://weibo.com/login.php";
				}
			);
		});

		exitBtn.on('click', function(e){
			e.preventDefault();
			chrome.extension.sendRequest(
				{
					cmd: "clear_all"
				}, 
				function(){
					location.href = "http://weibo.com/logout.php?backurl=/";
				}
			);
		});


		chrome.extension.sendRequest(
			{
				cmd: "get_store_data"
			}, 
			function(response) {
				var data = response.data;
				//console.log(data);
				var html = [];
				for (var i=0,len=data.length; i<len; i++) {
					var info = data[i].userInfo;
					if (Page.getUid() != info.uid) {
						html += '<li><a href="#" data-uid="'+info.uid+'">切换到@'+info.nick+'</a></li>';
					}
				}
				$(html).insertBefore(changeAccountBtn).delegate('a[data-uid]', 'click', function(e){
					e.preventDefault();
					chrome.extension.sendRequest(
						{
							cmd: "store_and_login", 
							data: {
								oUser: Page.getUserInfo(),
								loginUid: e.target.getAttribute('data-uid')
							}
						}, 
						function(response) {
							location.reload();
						}
					);
				});
				//console.log(response.data);
			}
		);

	}

	


});