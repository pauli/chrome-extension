(function($) {
$(function() {

	// default options
	var option_theme = 'default';
	var option_font = 'font: 16px/20px monospace';
	var option_width = '400';
	var option_height = '300';
	var option_dock = 'right';
	var option_opacity = '100';
	var option_tabsize = '2';

	// load options
	chrome.extension.sendMessage({getOptions: 'options'}, function(response) {

		if (response.status !== undefined) {
			var options = JSON.parse(response.status);
			if (options.theme) option_theme = options.theme;
			if (options.font) option_font = decodeURIComponent(options.font);
			if (options.width) option_width = options.width;
			if (options.height) option_height = options.height;
			if (options.dock) option_dock = options.dock;
			if (options.opacity) option_opacity = options.opacity;
			if (options.tabsize) option_tabsize = options.tabsize;
		}

	}); // end of chrome.extension.sendMessage

	// load current site styles
	var site = window.location.hostname;
	var stylesObject = {};
	var stylesArray = [];
	var currentSiteStyles = '';
	chrome.extension.sendMessage({getStyles: 'styles'}, function(response) {
		if (response.status !== undefined) {
			stylesArray = JSON.parse(response.status);
			for (var i in stylesArray) {
				if (site == stylesArray[i].site) {
					currentSiteStyles = stylesArray[i].styles;
				}
			}
		}
	}); // end of chrome.extension.sendMessage

	setTimeout(function() {

		var wrapper   = $('#ucss-code-wrapper');
		var textarea  = $('<textarea id="ucss-code" placeholder="User CSS goes here..."></textarea>');
		var codeTheme = $('<div id="ucss-code-theme"></div>');
		var fontStyle = $('<div id="ucss-font-style"></div>');
		var resize = $('<div id="ucss-editor-resize"></div>');
		var panel =
			$('<div class="ucss-panel">' +
					'<div class="ucss-options" title="Options"></div>' +
					'<div class="ucss-position">' +
						'<div class="ucss-position__item  ucss-position__item--bottom" title="Dock to bottom"></div>' +
						'<div class="ucss-position__item  ucss-position__item--right" title="Dock to right"></div>' +
					'</div>' +
					'<div class="ucss-toggle  ucss-toggle--active" title="Toggle styles"></div>' +
					'<div class="ucss-opacity">' +
						'<input type="range" min="30" max="100" value="' + option_opacity + '" title="Editor opacity" />' +
						'<div class="ucss-opacity__number">' + option_opacity + '%</div>' +
					'</div>' +
				'</div>');

		// if editor is visible
		if (wrapper.length) {

			if ( wrapper.is('.ucss-dock-to-right') ) {
				wrapper.css({right: '-' + ( option_width * 1 + 30 ) + 'px'});
			} else {
				wrapper.css({bottom: '-' + ( option_height * 1 + 30 ) + 'px'});
			}
			setTimeout(function() {
				wrapper.remove();
			}, 500);

		// if editor is hidden
		} else {

			$('body').append('<div id="ucss-code-wrapper"></div>');
			wrapper = $('#ucss-code-wrapper');
			wrapper
				.append(textarea)
				.wrapInner('<div id="ucss-code-inner"></div>')
				.append(panel)
				.append(resize)
				.append(codeTheme)
				.append(fontStyle)
				.data('width', option_width)
				.data('height', option_height)
				.css({'opacity': (option_opacity / 100)})
			;
			if (currentSiteStyles) textarea.val(currentSiteStyles);

			if (option_dock == 'right') {
				wrapper.addClass('ucss-dock-to-right');
				$('div.ucss-position__item--right').addClass('ucss-position__item--active');
			} else if (option_dock == 'bottom') {
				wrapper.addClass('ucss-dock-to-bottom');
				$('div.ucss-position__item--bottom').addClass('ucss-position__item--active');
			}

			if ( $('#user-css').length && $('#user-css').html().match(/<!-- <style>/) ) {
				$('div.ucss-toggle').removeClass('ucss-toggle--active');
			}

			if ( option_opacity < 66 ) {
				$('div.ucss-opacity__number').addClass('ucss-opacity__number--right');
			}

			var saveOptions = function() {
				var storage = '{' +
					'"theme": "' + option_theme + '",' +
					'"font": "' + encodeURIComponent(option_font) + '",' +
					'"width": "' + wrapper.data('width') + '",' +
					'"height": "' + wrapper.data('height') + '",' +
					'"dock": "' + option_dock + '",' +
					'"opacity": "' + option_opacity + '"' +
				'}';
				chrome.extension.sendMessage({updateOptions: storage}, function(response) {});
			};

			$('div.ucss-options').click(function() {
				chrome.extension.sendMessage({openOptionsPage: true}, function(response) {});
			});

			wrapper.on('click', 'div.ucss-position__item:not(.ucss-position__item--active)', function() {
				if ( $(this).is('.ucss-position__item--bottom') ) {
					wrapper
						.removeClass('ucss-dock-to-right')
						.addClass('ucss-dock-to-bottom')
						.css({
							'right': '',
							'bottom': 0,
							'width': '',
							'height': wrapper.data('height'),
						})
					;
					option_dock = 'bottom';
				} else {
					wrapper
						.removeClass('ucss-dock-to-bottom')
						.addClass('ucss-dock-to-right')
						.css({
							'right': 0,
							'bottom': '',
							'width': wrapper.data('width'),
							'height': '',
						})
					;
					option_dock = 'right';
				}
				$(this).addClass('ucss-position__item--active').siblings().removeClass('ucss-position__item--active');
				saveOptions();
				editor.refresh();
			});

			wrapper.on('click', 'div.ucss-toggle', function() {
				var toggle = $(this);
				var styles = textarea.val();
				if ( toggle.is('.ucss-toggle--active') && styles !== '' ) {
					toggle.removeClass('ucss-toggle--active');
					if (styles !== '') {
						$('#user-css style').remove();
						$('#user-css').append('<!-- <style>' + styles + '</style> -->');
					}
				} else {
					toggle.addClass('ucss-toggle--active');
					if (styles !== '') {
						$('#user-css').empty().append('<style>' + styles + '</style>');
					}
				}
			});

			wrapper.on('input', 'div.ucss-opacity input', function() {
				var number = $('div.ucss-opacity__number');
				var value = $(this).val();
				number.text( $(this).val() + '%' );
				if ( value < 66 ) {
					number.addClass('ucss-opacity__number--right');
				} else {
					number.removeClass('ucss-opacity__number--right');
				}
				wrapper.css({'opacity': (value / 100)});
			}).on('change', 'div.ucss-opacity input', function() {
				option_opacity = $(this).val();
				saveOptions();
			});

			var editor = CodeMirror.fromTextArea(document.getElementById('ucss-code'), {
				tabSize: option_tabsize,
				lineWrapping: true,
				matchBrackets: true,
				styleActiveLine: true,
				autoCloseBrackets: true,
				showTrailingSpace: false,
			});

			setTimeout(function() {
				editor.refresh();
			}, 100);

			editor.on('change', function() {
				editor.save();
				var styles = textarea.val();
				if (styles === '') {
					$('#user-css').remove();
					$('div.ucss-toggle').addClass('ucss-toggle--active');
				} else {
					if ( $('#user-css').length ) {
						$('#user-css style').text(styles);
					} else {
						$('body').append('<div id="user-css"><style>' + styles + '</style></div>');
					}
				}

				chrome.extension.sendMessage({updateBadge: styles}, function(response) {});

				var tempStylesArray = [];
				for (var i in stylesArray) {
					if (site != stylesArray[i].site) {
						tempStylesArray.push(stylesArray[i]);
					}
				}
				stylesArray = tempStylesArray;
				stylesObject.site = site;
				stylesObject.styles = styles;
				if (styles !== '') {
					stylesArray.push(stylesObject);
				}
				stylesArray.sort(function(a, b) {
					var key = 'site';
					var x = a[key];
					var y = b[key];
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				var storageStyles = JSON.stringify(stylesArray);
				chrome.extension.sendMessage({updateStyles: storageStyles}, function(response) {});
			});

			if (option_theme != 'default') {
				codeTheme.html('<link href="' + chrome.extension.getURL('css/themes/' + option_theme + '.css') + '" rel="stylesheet">');
				editor.setOption('theme', option_theme);
			}

			var important = ' !important';
			if (/!important/.test(option_font)) important = '';
			fontStyle.html('<style>#ucss-code-wrapper .CodeMirror * {' + option_font + important + '}</style>');

			if ( wrapper.is('.ucss-dock-to-right') ) {
				wrapper.css({right: '-' + ( option_width * 1 + 30 ) + 'px'});
				wrapper.width(option_width);
				wrapper.css({'right': 0});
			} else {
				wrapper.css({bottom: '-' + ( option_height * 1 + 30 ) + 'px'});
				wrapper.height(option_height);
				wrapper.css({'bottom': 0});
			}

			// editor resizing
			var isResizing = false;
			$('#ucss-editor-resize').on('mousedown', function(e) {
				isResizing = true;
				$('body').css({
					'-webkit-user-select': 'none',
					'user-select': 'none',
				});
			});
			$(document).on('mousemove', function(e) {
				if (!isResizing) return;

				if ( wrapper.is('.ucss-dock-to-right') ) {
					var newWidth = $(window).width() - e.clientX;
					if ( newWidth > 250 && (newWidth + 20) < $(window).width() ) {
						wrapper.css('width', newWidth).data('width', newWidth);
					}
				} else {
					var newHeight = $(window).height() - e.clientY;
					if ( newHeight > 150 && (newHeight + 20) < $(window).height() ) {
						wrapper.css('height', newHeight).data('height', newHeight);
					}

				}

			}).on('mouseup', function() {
				if (isResizing) {
					$('body').css({
						'-webkit-user-select': '',
						'user-select': '',
					});
					saveOptions();
					editor.refresh();
				}
				isResizing = false;
			});
			// end editor resizing

		}

	}, 100);

});
})(jQuery);