(function($) {
$(function() {

	var manifest = chrome.runtime.getManifest();
	$('#version').text(manifest.version);

	$('ul.tabs__caption').on('click', 'li:not(.active)', function() {
		$(this)
			.addClass('active').siblings().removeClass('active')
			.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
			$('div.wrapper, div.preview').removeClass('active');
	});

	// default options
	var def_options = {
		theme: 'default',
		font: 'font: 16px/20px monospace',
		width: '400',
		height: '300',
		dock: 'right',
		opacity: '100',
		tabsize: '2',
	};

	var options = '';
	if (localStorage.userCSSoptions) options = JSON.parse(localStorage.userCSSoptions);

	var option_theme   = (options.theme) ? options.theme : def_options.theme;
	var option_font    = (options.font) ? decodeURIComponent(options.font) : def_options.font;
	var option_width   = (options.width) ? options.width : def_options.width;
	var option_height  = (options.height) ? options.height : def_options.height;
	var option_dock    = (options.dock) ? options.dock : def_options.dock;
	var option_opacity = (options.opacity) ? options.opacity : def_options.opacity;
	var option_tabsize = (options.tabsize) ? options.tabsize : def_options.tabsize;

	$('#option-theme').val(option_theme);
	$('#option-font').val(option_font).attr('placeholder', option_font);
	$('#option-width').val(option_width);
	$('#option-height').val(option_height);
	$('#option-tabsize').val(option_tabsize);

	var styles = '';
	if (localStorage.userCSSstyles) {
		styles = localStorage.userCSSstyles;
		styles = JSON.parse(styles);
		for (var i = 0; i < styles.length; i++) {
			var siteName = styles[i].site;
			var siteStyles = styles[i].styles;
			$('div.sites').append(
				'<div class="site">' +
					'<div class="site__name" data-site="' + siteName + '"><img src="http://' + siteName + '/favicon.ico" width="16" height="16" alt="" />' + siteName + '</div>' +
					'<div class="site__styles">' +
						'<textarea id="textarea-' + i +'" data-index="' + i +'">' + siteStyles + '</textarea>' +
					'</div>' +
					'<div class="site__save">Save</div>' +
				'</div>'
			);
		}
		$('div.site img').error(function() {
			$(this).replaceWith('<div class="site__no-image">×</div>');
		});

		$('div.site__name').click(function() {
			var parent = $(this).parent();
			var textarea = parent.find('textarea');
			parent
				.toggleClass('active')
				.find('.site__styles').slideToggle(150)
				.end()
				.siblings()
					.removeClass('active')
					.find('.site__styles').slideUp(150)
					.end()
					.find('div.site__save').hide()
			;
			if ( parent.data('refreshed') != 1 ) {
				parent.data('refreshed', '1');
				var index = textarea.data('index');
				setTimeout(function() {
					var code = CodeMirror.fromTextArea(document.getElementById('textarea-' + index), {
						theme: def_options.theme,
						tabSize: option_tabsize,
						lineWrapping: true,
						matchBrackets: true,
						styleActiveLine: true,
						autoCloseBrackets: true,
						showTrailingSpace: false,
					});
					code.on("change", function() {
						code.save();
						parent.find('div.site__save').show();
					});
				}, 150);
			}
		});

	} else {
		$('div.sites').text('No styles.');
	}

	var editor = CodeMirror.fromTextArea(document.getElementById('preview-code'), {
		theme: option_theme,
		tabSize: option_tabsize,
		lineWrapping: true,
		matchBrackets: true,
		styleActiveLine: true,
		showTrailingSpace: false,
		readOnly: true,
	});

	$('div.preview__toggle').click(function() {
		$('div.wrapper, div.preview').toggleClass('active');
		editor.refresh();
	});

	$('#option-theme').change(function() {
		editor.setOption('theme', $(this).val());
	});
	$('#preview-font').html('<style>#ucss-code-wrapper .CodeMirror * {' + option_font + '} .site__styles .CodeMirror * {' + option_font + '}</style>');
	$('#option-font').on('change keyup', function() {
		$('#preview-font').html('<style>#ucss-code-wrapper .CodeMirror * {' + $(this).val() + '} .site__styles .CodeMirror * {' + $(this).val() + '}</style>');
	});
	$('#option-tabsize').on('change keyup', function() {
		var tabsize = $(this).val();
		if (tabsize.length >= 1) {
			$(this).val(tabsize.substr(0, 1));
		}
		if (tabsize > 8) {
			$(this).val(8);
		}
		editor.setOption('tabSize', tabsize);
	});

	$('#save').click(function() {

		$(this).addClass('active');
		setTimeout( function() { $('#save').removeClass('active'); }, 800 );

		localStorage.userCSSoptions = '{' +
			'"theme":"' + $('#option-theme').val() + '",' +
			'"font":"' + encodeURIComponent( $('#option-font').val() ) + '",' +
			'"width":"' + option_width + '",' +
			'"height":"' + option_height + '",' +
			'"dock":"' + option_dock + '",' +
			'"opacity":"' + option_opacity + '",' +
			'"tabsize":"' + $('#option-tabsize').val() + '"' +
		'}';

	});

	$('#reset').click(function() {
		$(this).addClass('active');
		setTimeout( function() {
			$('#reset').removeClass('active');

			localStorage.userCSSoptions = '{' +
				'"theme":"' + def_options.theme + '",' +
				'"font":"' + def_options.font + '",' +
				'"width":"' + def_options.width + '",' +
				'"height":"' + def_options.height + '",' +
				'"dock":"' + def_options.dock + '",' +
				'"opacity":"' + def_options.opacity + '",' +
				'"tabsize":"' + def_options.tabsize + '"' +
			'}';

			$('#option-theme').val(def_options.theme);
			$('#option-font').val(def_options.font).attr('placeholder', def_options.font);
			$('#option-width').val(def_options.width);
			$('#option-height').val(def_options.height);
			$('#option-tabsize').val(def_options.tabsize);

			editor.setOption('theme', def_options.theme);
			$('#preview-font').html('<style>#ucss-code-wrapper .CodeMirror * {' + def_options.font + '}</style>');
			editor.setOption('tabSize', def_options.tabsize);

		}, 800 );
	});

	$('div.site__save').click(function() {
		$(this).addClass('active');
		var parent = $(this).closest('div.site');
		var currentSiteStyles = parent.find('textarea').val();

		var site = parent.find('div.site__name').data('site');
		var stylesObject = {};
		var stylesArray = styles;
		var tempStylesArray = [];

		for (var i in stylesArray) {
			if (site != stylesArray[i].site) {
				tempStylesArray.push(stylesArray[i]);
			}
		}
		stylesArray = tempStylesArray;
		stylesObject.site = site;
		stylesObject.styles = currentSiteStyles;
		if (currentSiteStyles !== '') {
			stylesArray.push(stylesObject);
		}
		stylesArray.sort(function(a, b) {
			var key = 'site';
			var x = a[key];
			var y = b[key];
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		});
		var storageStyles = JSON.stringify(stylesArray);
		chrome.extension.sendMessage({updateStyles: storageStyles}, function(response) {});

		setTimeout( function() { $('div.site__save').removeClass('active').hide(); }, 800 );
	});

	var top = ($(window).height() - $('div.modal').outerHeight()) / 2;
	var left = ($(window).width() - $('div.modal').outerWidth()) / 2;
	$('div.modal').css({top: (top > 0 ? top : 0)+'px', left: (left > 0 ? left : 0)+'px'});

	$('div.file input').change(function() {
		var value = $(this).val();
		var fileName = $(this).parent().find('div.file__name');
		if (value === '') {
			$('div.file__name').text( fileName.data('text') );
			$('#import-button').addClass('button--inactive');
		} else {
			$('div.file__name').text(value.replace(/.+[\\\/]/, ''));
			$('#import-button').removeClass('button--inactive');
		}
	});

	$('body').on('click', '#import-button:not(.button--inactive)', function() {
		$('#overlay, #confirm-import').addClass('visible');
	});

	// import data
	$('#import-yes').on('click', function() {
		$('#confirm-import').removeClass('visible');
		$('#done div.modal__headline').hide();
		$('#done').addClass('visible');

		var file = $('#import')[0].files[0];
		var fileReader = new FileReader();
		fileReader.readAsText(file);
		fileReader.onload = function(e) {
			var data = e.target.result;

			// check for correct JSON
			var IS_JSON = true;
			try {
				data = $.parseJSON(data);
			} catch(err) {
				IS_JSON = false;
			}
			if (IS_JSON) {
				localStorage.clear();
				for (var key in data) {
					localStorage[key] = JSON.stringify(data[key]);
				}
				$('#data-imported-successfully, div.modal__text').show();
			} else {
				$('#could-not-be-imported').show();
			}

			$('#import').val('').change();
		};
	});

	$('#overlay, #import-no, #ok').on('click', function() {
		$('#overlay, div.modal').removeClass('visible');
	});

	// export data
	if (localStorage.userCSSoptions && localStorage.userCSSstyles) {
		var exportJSON = {
			"userCSSoptions": JSON.parse(localStorage.userCSSoptions),
			"userCSSstyles": JSON.parse(localStorage.userCSSstyles)
		};
		var file = new Blob([ JSON.stringify(exportJSON) ]);
		$('#export-button').removeClass('button--inactive').attr({'href': URL.createObjectURL(file), 'download': 'user-css.json'});
	}

	$('span.current-year').text( new Date().getFullYear() );

});
})(jQuery);