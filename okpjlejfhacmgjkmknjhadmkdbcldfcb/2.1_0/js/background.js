// extention options and styles
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {

	if (request.updateOptions) {
		localStorage.userCSSoptions = request.updateOptions;
		sendResponse({status: localStorage.userCSSoptions});
	}
	else if (request.getOptions) {
		sendResponse({status: localStorage.userCSSoptions});
	}
	else if (request.updateStyles) {
		localStorage.userCSSstyles = request.updateStyles;
		sendResponse({status: localStorage.userCSSstyles});
	}
	else if (request.getStyles) {
		sendResponse({status: localStorage.userCSSstyles});
	}
	else {
		sendResponse({});
	}

});

// loading editor on badge click
chrome.browserAction.onClicked.addListener(function(tab) {
	chrome.tabs.executeScript(tab.id, {
		file: "js/scripts.js",
		allFrames: true,
		runAt: "document_idle"
	});
});

// set badge text on tab activation
chrome.tabs.onActivated.addListener(function(tabId) {
	var tab_id = tabId.tabId;
	chrome.browserAction.setBadgeText({text: ''});
	chrome.tabs.get(tab_id, function(tab) {
		var site = tab.url;
		var a = document.createElement('a');
		a.href = site;
		site = a.hostname;
		stylesArray = JSON.parse(localStorage.userCSSstyles);
		for (var i in stylesArray) {
			if (site == stylesArray[i].site) {
				styles = stylesArray[i].styles;
				if (styles) {
					chrome.browserAction.setBadgeText({text: '+'});
					chrome.browserAction.setBadgeBackgroundColor({color: '#70DF00'});
				}
			}
		}
	});
});

// change badge on editor change
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.updateBadge !== '' && request.updateBadge !== undefined) {
		chrome.browserAction.setBadgeText({text: '+'});
		chrome.browserAction.setBadgeBackgroundColor({color: '#70DF00'});
	} else if (request.updateBadge === '') {
		chrome.browserAction.setBadgeText({text: ''});
	}
});

// open options page
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.openOptionsPage) {
		chrome.tabs.create({'url': 'options.html'});
	}
	else {
		sendResponse({});
	}
});