chrome.extension.sendMessage({getStyles: 'styles'}, function(response) {
	var site = window.location.hostname;
	if (response.status !== undefined) {
		stylesArray = JSON.parse(response.status);
		for (var i in stylesArray) {
			if (site == stylesArray[i].site) {
				styles = stylesArray[i].styles;
				if (styles) {
					$('body').append('<div id="user-css"><style>' + styles + '</style></div>');
					chrome.extension.sendMessage({updateBadge: styles}, function(response) {});
				}
			}
		}
	}
});