var api = chrome.extension.getBackgroundPage().api;

$(document).ready(function(){
	
	prepareApiSuggest(api);
	
	$("#api_suggest").bind("focus", function() {
		$(this).autocomplete("search", $(this).val());
	});
	
});


function prepareApiSuggest(api) {
	
	var descMaxLength = 150;
	
	$("#api_suggest").autocomplete({
		minLength: 0,
		source: api.entries,
		focus: function(event, ui) {
			return false;
		},
		select: function(event, ui) {
			$("#api_suggest").blur();
			displayEntry(ui.item);
			return false;
		}
	})
	.data("autocomplete")._renderItem = function( ul, item ) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a>" + item.title + "<br><em>" + (item.desc.length > descMaxLength ? item.desc.substring(0, descMaxLength)+"..." : item.desc) + "</em></a>" )
			.appendTo( ul );
	};
	$("#api_suggest").focus();
	
}

function displayEntry(entry) {
	
	var entries = new Array();
	entries[0] = entry;
	entries = entries.concat(findSimilarEntries(entry));
	
	var sigs = $('<div id="signatures"></div>');
	
	for(var e=0;e<entries.length;e++) {
	
		var header = $('<h3></h3>');
		var title = $('<span class="title"></span>');
		var version = $('<span class="version"></span>');
		var details = $('<div class="details"></div>');
		var desc = $('<div class="desc"></div>');
		var args = $('<div class="arguments"></div>');
		var link = $('<div class="api-link" onclick="openApiLink(\''+entries[e].pagename + '\')">More</div>');
		var examples = $('<div class="examples"></div>');

		if(entries[e].arg != null) {
			args.append($('<p class="argument"><strong>&lt;' + entries[e].arg.type + '> ' + entries[e].arg.name + '</strong> - ' + entries[e].arg.desc + '</p>'));
		}
		
		if(entries[e].sig != null) {
			for(var i=0;i<entries[e].sig.args.length;i++) {
				args.append($('<p class="argument"><strong>&lt;' + entries[e].sig.args[i].type + '> ' + entries[e].sig.args[i].name + '</strong> - ' + entries[e].sig.args[i].desc + '</p>'));
			}
		}

		if(entries[e].examples.length) {
			examples.append($('<h4>Example' + (entries[e].examples.length != 1 ? 's' : '') + ':</h4>'));
			for(var i=0;i<entries[e].examples.length;i++) {
				examples.append($('<p>' + entries[e].examples[i].desc + '</p>'));
				examples.append($('<pre>' + entries[e].examples[i].code + '</pre>'));
			}
		}

		title.html(chrome.extension.getBackgroundPage().buildTitle(entries[e], true));
		version.html("v "+entries[e].sig.added);
		desc.html(entries[e].desc);

		header.append(title);
		header.append(version);
		
		details.append(desc);
		details.append(args);
		details.append(examples);
		details.append(link);
		
		sigs.append(header);
		sigs.append(details);
	
	}
	
	$("#api").html("");
	$("#api").html(sigs);
	sigs.accordion({
		autoHeight:false
	});

}

function findSimilarEntries(entry) {
	var similar = new Array();
	for(var i=0;i<api.entries.length;i++) {
		if(api.entries[i].name == entry.name && api.entries[i].title != entry.title) {
			similar.push(api.entries[i]);
		} 
	}
	
	return similar;
	
}

function openApiLink(pagename) {
	chrome.tabs.create({url: "http://api.jquery.com/" + pagename + "/"});
}

