// Event listeners.
chrome.extension.onMessage.addListener(
	function(request, sender, sendResponse) {
		if ( request.css ) {
			if ( request.css == "" ) {
				sendResponse({message: 'CSS is empty'});
			}
			sendResponse({message: request.tabId});
			chrome.tabs.get(request.tabId, function(t){ 
				sendResponse({message: request.css});
				var details = {allFrames: false, code: request.css};
				// Inject parsed css to selectd tab.
				chrome.tabs.insertCSS(t.id, details, function(d){ 
					sendResponse({message: 'Done'}); 
				}); 
			});
			
		} 
		if ( request.insertJquery ) {
			insertJquery( request.tabId );
		}
});

// Execute js.
function insertJquery( tabId ) {
	chrome.tabs.executeScript(tabId, {file: "js/jquery172.js"}, function(){
		sendResponse({message: 'jQuery inserted...'}); 
	}
}