// Event listeners.
var currentResponse = null;
var cssPath = '';
var myid = '';
chrome.extension.onMessage.addListener(
	function(request, sender, sendResponse) {
		if ( request.extId ) { myid = request.extId; console.log( myid ) }
		/*if ( request.css ) {
			if ( request.css == "" ) {
				sendResponse({message: 'CSS is empty'});
			}
			sendResponse({message: request.tabId});
			chrome.tabs.get(request.tabId, function(t){ 
				sendResponse({message: request.css});
				var details = {allFrames: false, code: request.css};
				// Inject parsed css to selectd tab.
				chrome.tabs.insertCSS(t.id, details, function(d){ 
					sendResponse({message: 'Done'}); 
				}); 
			});
			
		} */
		if ( request.task ) {
			currentResponse = sendResponse;
			chrome.tabs.get(request.tabId, function(tab) {
				var details = {task: request.task};
				if ( request.task == "update" ) details.css = request.css;
				if ( request.task == "end_inspect" ) cssPath = '';
				chrome.tabs.sendMessage(tab.id, details);
			});
		}
		if ( request.inspectorMessage ) {
			cssPath = request.inspectorMessage;
			jumpCss( cssPath );
		}
		if ( request.getCssPath ) {
			sendResponse({path: cssPath});
		}
});

// Port connection.
var ports = {};
chrome.extension.onConnect.addListener(function(port) {
    if (port.name !== "devtools") return;
    ports[port.portId_] = port;
    // Remove port when destroyed (eg when devtools instance is closed)
    port.onDisconnect.addListener(function(port) {
        delete ports[port.portId_];
    });
    port.onMessage.addListener(function(msg) {
        // Whatever you wish
        console.log(msg);
    });
});
// Function to send a message to all devtool.html views:
function notifyDevtools(msg) {
    Object.keys(ports).forEach(function(portId_) {
        ports[portId_].postMessage(msg);
    });
}

// Jump next element css id.
function jumpCss( cssId ) {
	notifyDevtools( {'cid': cssId} );
}