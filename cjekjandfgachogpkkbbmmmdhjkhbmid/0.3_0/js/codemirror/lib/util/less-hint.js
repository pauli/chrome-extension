(function () {
  function forEach(arr, f) {
    for (var i = 0, e = arr.length; i < e; ++i) f(arr[i]);
  }
  
  function arrayContains(arr, item) {
    if (!Array.prototype.indexOf) {
      var i = arr.length;
      while (i--) {
        if (arr[i] === item) {
          return true;
        }
      }
      return false;
    }
    return arr.indexOf(item) != -1;
  }

  function scriptHint(editor, keywords, getToken) {
    // Find the token at the cursor
    var cur = editor.getCursor(), token = getToken(editor, cur), tprop = token;
    // If it's not a 'word-style' token, ignore the token.
		if (!/^[\w$_]*$/.test(token.string)) {
      token = tprop = {start: cur.ch, end: cur.ch, string: "", state: token.state,
                       className: token.string == "." ? "property" : null};
    }
    // If it is a property, find out what it is a property of.
    while (tprop.className == "property") {
      tprop = getToken(editor, {line: cur.line, ch: tprop.start});
      if (tprop.string != ".") return;
      tprop = getToken(editor, {line: cur.line, ch: tprop.start});
      if (tprop.string == ')') {
        var level = 1;
        do {
          tprop = getToken(editor, {line: cur.line, ch: tprop.start});
          switch (tprop.string) {
          case ')': level++; break;
          case '(': level--; break;
          default: break;
          }
        } while (level > 0);
        tprop = getToken(editor, {line: cur.line, ch: tprop.start});
				if (tprop.className == 'variable')
					tprop.className = 'function';
				else return; // no clue
      }
      if (!context) var context = [];
      context.push(tprop);
    }
    return {list: getCompletions(token, context, keywords),
            from: {line: cur.line, ch: token.start},
            to: {line: cur.line, ch: token.end}};
  }

  CodeMirror.lessHint = function(editor) {
    return scriptHint(editor, htmlKeywords,
                      function (e, cur) {return e.getTokenAt(cur);});
  };

  CodeMirror.pushVar = function( newVar ) {
    htmlKeywords.push( newVar );
    console.log( htmlKeywords );
  }

  function getCoffeeScriptToken(editor, cur) {
  // This getToken, it is for coffeescript, imitates the behavior of
  // getTokenAt method in javascript.js, that is, returning "property"
  // type and treat "." as indepenent token.
    var token = editor.getTokenAt(cur);
    if (cur.ch == token.start + 1 && token.string.charAt(0) == '.') {
      token.end = token.start;
      token.string = '.';
      token.className = "property";
    }
    else if (/^\.[\w$_]*$/.test(token.string)) {
      token.className = "property";
      token.start++;
      token.string = token.string.replace(/\./, '');
    }
    return token;
  }

  CodeMirror.coffeescriptHint = function(editor) {
    return scriptHint(editor, coffeescriptKeywords, getCoffeeScriptToken);
  };

  var stringProps = ("charAt charCodeAt indexOf lastIndexOf substring substr slice trim trimLeft trimRight " +
                     "toUpperCase toLowerCase split concat match replace search").split(" ");
  var arrayProps = ("length concat join splice push pop shift unshift slice reverse sort indexOf " +
                    "lastIndexOf every some filter forEach map reduce reduceRight ").split(" ");
  var funcProps = "prototype apply call bind".split(" ");
  var javascriptKeywords = ("break case catch continue debugger default delete do else false finally for function " +
                  "if in instanceof new null return switch throw true try typeof var void while with").split(" ");
  var htmlKeywords = ("html#head#title#base#link#meta#style#script#noscript#body#section#nav#article#aside#hgroup#header#footer#address#p#hr#pre#blockquote#ol#ul#li#dl#dt#dd#figure#figcaption#div#a#em#strong#small#s#cite#q#dfn#abbr#data#time#code#var#samp#kbd#sub#sup#i#b#u#mark#ruby#rt#rp#bdi#bdo#span#br#wbr#ins#del#img#iframe#embed#object#param#video#audio#source#track#canvas#map#area#svg#math#table#caption#colgroup#col#tbody#thead#tfoot#tr#td#th#form#fieldset#legend#label#input#button#select#datalist#optgroup#option#textarea#keygen#output#progress#meter#details#summary#command#menu#align-content: #align-items: #align-self: #animation: #animation-delay: #animation-direction: #animation-duration: #animation-fill-mode: #animation-iteration-count: #animation-name: #animation-play-state: #animation-timing-function: #azimuth: #backface-visibility: #background: #background-attachment: #background-clip: #background-color: #background-image: #background-origin: #background-position: #background-position-x: #background-position-y: #background-repeat: #background-size: #behavior: #border: #border-bottom: #border-bottom-color: #border-bottom-left-radius: #border-bottom-right-radius: #border-bottom-style: #border-bottom-width: #border-collapse: #border-color: #border-image: #border-image-outset: #border-image-repeat: #border-image-slice: #border-image-source: #border-image-width: #border-left: #border-left-color: #border-left-style: #border-left-width: #border-radius: #border-right: #border-right-color: #border-right-style: #border-right-width: #border-spacing: #border-style: #border-top: #border-top-color: #border-top-left-radius: #border-top-right-radius: #border-top-style: #border-top-width: #border-width: #bottom: #box-decoration-break: #box-shadow: #box-sizing: #break-after: #break-before: #break-inside: #caption-side: #clear: #clip: #clip-path: #clip-rule: #color: #color-interpolation: #color-interpolation-filters: #color-rendering: #column-count: #column-fill: #column-gap: #column-rule: #column-rule-color: #column-rule-style: #column-rule-width: #columns: #column-span: #column-width: #content: #counter-increment: #counter-reset: #crop: #cue: #cue-after: #cue-before: #cursor: #direction: #display: #elevation: #empty-cells: #enable-background: #fill: #fill-opacity: #fill-rule: #filter: #fit: #fit-position: #flex: #flex-basis: #flex-direction: #flex-flow: #flex-grow: #flex-shrink: #flex-wrap: #float: #flood-color: #flood-opacity: #font: #font-family: #font-feature-settings: #font-kerning: #font-language-override: #font-size: #font-size-adjust: #font-stretch: #font-style: #font-synthesis: #font-variant: #font-variant-alternates: #font-variant-caps: #font-variant-east-asian: #font-variant-ligatures: #font-variant-numeric: #font-variant-position: #font-weight: #glyph-orientation-horizontal: #glyph-orientation-vertical: #grid-cell: #grid-column: #grid-column-align: #grid-columns: #grid-column-sizing: #grid-column-span: #grid-flow: #grid-row: #grid-row-align: #grid-rows: #grid-row-sizing: #grid-row-span: #grid-template: #hanging-punctuation: #height: #hyphens: #icon: #image-orientation: #image-rendering: #ime-mode: #justify-content: #kerning: #left: #letter-spacing: #lighting-color: #line-break: #line-height: #list-style: #list-style-image: #list-style-position: #list-style-type: #margin: #margin-bottom: #margin-left: #margin-right: #margin-top: #marker: #marker-end: #marker-mid: #marker-start: #marquee-direction: #marquee-loop: #marquee-speed: #marquee-style: #mask: #max-height: #max-width: #min-height: #min-width: #move-to: #nav-down: #nav-index: #nav-left: #nav-right: #nav-up: #opacity: #order: #orphans: #outline: #outline-color: #outline-offset: #outline-style: #outline-width: #overflow: #overflow-style: #overflow-wrap: #overflow-x: #overflow-y: #padding: #padding-bottom: #padding-left: #padding-right: #padding-top: #page: #page-break-after: #page-break-before: #page-break-inside: #page-policy: #pause: #pause-after: #pause-before: #perspective: #perspective-origin: #pitch: #pitch-range: #pointer-events: #position: #quotes: #resize: #rest: #rest-after: #rest-before: #richness: #right: #rotation: #rotation-point: #ruby-align: #ruby-overhang: #ruby-position: #ruby-span: #scrollbar-3dlight-color: #scrollbar-arrow-color: #scrollbar-base-color: #scrollbar-darkshadow-color: #scrollbar-face-color: #scrollbar-highlight-color: #scrollbar-shadow-color: #scrollbar-track-color: #shape-rendering: #size: #speak: #speak-as: #speech-rate: #src: #stop-color: #stop-opacity: #stress: #stroke: #stroke-dasharray: #stroke-dashoffset: #stroke-linecap: #stroke-linejoin: #stroke-miterlimit: #stroke-opacity: #stroke-width: #table-layout: #tab-size: #text-align: #text-align-last: #text-anchor: #text-decoration: #text-decoration-color: #text-decoration-line: #text-decoration-skip: #text-decoration-style: #text-emphasis: #text-emphasis-color: #text-emphasis-position: #text-emphasis-style: #text-indent: #text-justify: #text-overflow: #text-rendering: #text-shadow: #text-space-collapse: #text-transform: #text-underline-position: #text-wrap: #top: #transform: #transform-origin: #transform-style: #transition: #transition-delay: #transition-duration: #transition-property: #transition-timing-function: #unicode-bidi: #unicode-range: #user-select: #vertical-align: #visibility: #voice-balance: #voice-duration: #voice-family: #voice-pitch: #voice-range: #voice-rate: #voice-stress: #voice-volume: #volume: #white-space: #widows: #width: #word-break: #word-spacing: #word-wrap: #z-index: #zoom: #-moz-animation: #-moz-animation-delay: #-moz-animation-direction: #-moz-animation-duration: #-moz-animation-iteration-count: #-moz-animation-name: #-moz-animation-play-state: #-moz-animation-timing-function: #-moz-appearance: #-moz-backface-visibility: #-moz-background-clip: #-moz-background-inline-policy: #-moz-background-origin: #-moz-background-size: #-moz-border-bottom-colors: #-moz-border-image: #-moz-border-left-colors: #-moz-border-radius: #-moz-border-radius-bottomleft: #-moz-border-radius-bottomright: #-moz-border-radius-topleft: #-moz-border-radius-topright: #-moz-border-right-colors: #-moz-border-top-colors: #-moz-box-align: #-moz-box-direction: #-moz-box-flex: #-moz-box-flexgroup: #-moz-box-ordinal-group: #-moz-box-orient: #-moz-box-pack: #-moz-box-shadow: #-moz-box-sizing: #-moz-column-count: #-moz-column-gap: #-moz-column-rule: #-moz-column-rule-color: #-moz-column-rule-style: #-moz-column-rule-width: #-moz-column-width: #-moz-font-feature-settings: #-moz-hyphens: #-moz-opacity: #-moz-perspective: #-moz-perspective-origin: #-moz-text-align-last: #-moz-text-decoration-color: #-moz-text-decoration-line: #-moz-text-decoration-style: #-moz-transform: #-moz-transform-origin: #-moz-transition: #-moz-transition-delay: #-moz-transition-duration: #-moz-transition-property: #-moz-transition-timing-function: #-moz-user-select: #-ms-accelerator: #-ms-behavior: #-ms-block-progression: #-ms-content-zoom-chaining: #-ms-content-zooming: #-ms-content-zoom-limit: #-ms-content-zoom-limit-max: #-ms-content-zoom-limit-min: #-ms-content-zoom-snap: #-ms-content-zoom-snap-points: #-ms-content-zoom-snap-type: #-ms-filter: #-ms-flex: #-ms-flex-align: #-ms-flex-direction: #-ms-flex-flow: #-ms-flex-item-align: #-ms-flex-line-pack: #-ms-flex-order: #-ms-flex-pack: #-ms-flex-wrap: #-ms-flow-from: #-ms-flow-into: #-ms-grid-column: #-ms-grid-column-align: #-ms-grid-columns: #-ms-grid-column-span: #-ms-grid-layer: #-ms-grid-row: #-ms-grid-row-align: #-ms-grid-rows: #-ms-grid-row-span: #-ms-high-contrast-adjust: #-ms-hyphenate-limit-chars: #-ms-hyphenate-limit-lines: #-ms-hyphenate-limit-zone: #-ms-hyphens: #-ms-ime-mode: #-ms-interpolation-mode: #-ms-layout-grid: #-ms-layout-grid-char: #-ms-layout-grid-line: #-ms-layout-grid-mode: #-ms-layout-grid-type: #-ms-line-break: #-ms-overflow-style: #-ms-perspective: #-ms-perspective-origin: #-ms-perspective-origin-x: #-ms-perspective-origin-y: #-ms-progress-appearance: #-ms-scrollbar-3dlight-color: #-ms-scrollbar-arrow-color: #-ms-scrollbar-base-color: #-ms-scrollbar-darkshadow-color: #-ms-scrollbar-face-color: #-ms-scrollbar-highlight-color: #-ms-scrollbar-shadow-color: #-ms-scrollbar-track-color: #-ms-scroll-chaining: #-ms-scroll-limit: #-ms-scroll-limit-x-max: #-ms-scroll-limit-x-min: #-ms-scroll-limit-y-max: #-ms-scroll-limit-y-min: #-ms-scroll-rails: #-ms-scroll-snap-points-x: #-ms-scroll-snap-points-y: #-ms-scroll-snap-type: #-ms-scroll-snap-x: #-ms-scroll-snap-y: #-ms-scroll-translation: #-ms-text-align-last: #-ms-text-autospace: #-ms-text-justify: #-ms-text-kashida-space: #-ms-text-overflow: #-ms-text-size-adjust: #-ms-text-underline-position: #-ms-touch-action: #-ms-touch-select: #-ms-transform: #-ms-transform-origin: #-ms-transform-origin-x: #-ms-transform-origin-y: #-ms-transform-origin-z: #-ms-user-select: #-ms-word-break: #-ms-word-wrap: #-ms-wrap-flow: #-ms-wrap-margin: #-ms-wrap-through: #-ms-writing-mode: #-ms-zoom: #-ms-zoom-animation: #-o-animation: #-o-animation-delay: #-o-animation-direction: #-o-animation-duration: #-o-animation-fill-mode: #-o-animation-iteration-count: #-o-animation-name: #-o-animation-play-state: #-o-animation-timing-function: #-o-background-size: #-o-border-image: #-o-object-fit: #-o-object-position: #-o-table-baseline: #-o-tab-size: #-o-text-overflow: #-o-transform: #-o-transform-origin: #-o-transition: #-o-transition-delay: #-o-transition-duration: #-o-transition-property: #-o-transition-timing-function: #-webkit-animation: #-webkit-animation-delay: #-webkit-animation-direction: #-webkit-animation-duration: #-webkit-animation-fill-mode: #-webkit-animation-iteration-count: #-webkit-animation-name: #-webkit-animation-play-state: #-webkit-animation-timing-function: #-webkit-appearance: #-webkit-backface-visibility: #-webkit-background-clip: #-webkit-background-composite: #-webkit-background-origin: #-webkit-background-size: #-webkit-border-bottom-left-radius: #-webkit-border-bottom-right-radius: #-webkit-border-image: #-webkit-border-radius: #-webkit-border-top-left-radius: #-webkit-border-top-right-radius: #-webkit-box-align: #-webkit-box-direction: #-webkit-box-flex: #-webkit-box-flex-group: #-webkit-box-ordinal-group: #-webkit-box-orient: #-webkit-box-pack: #-webkit-box-reflect: #-webkit-box-shadow: #-webkit-box-sizing: #-webkit-column-count: #-webkit-column-gap: #-webkit-column-rule: #-webkit-column-rule-color: #-webkit-column-rule-style: #-webkit-column-rule-width: #-webkit-columns: #-webkit-column-width: #-webkit-filter: #-webkit-flow-from: #-webkit-flow-into: #-webkit-hyphens: #-webkit-line-break: #-webkit-margin-bottom-collapse: #-webkit-margin-collapse: #-webkit-margin-start: #-webkit-margin-top-collapse: #-webkit-marquee-direction: #-webkit-marquee-increment: #-webkit-marquee-repitition: #-webkit-marquee-speed: #-webkit-marquee-style: #-webkit-nbsp-mode: #-webkit-overflow-scrolling: #-webkit-padding-start: #-webkit-perspective: #-webkit-perspective-origin: #-webkit-tap-highlight-color: #-webkit-text-fill-color: #-webkit-text-shadow: #-webkit-text-size-adjust: #-webkit-text-stroke: #-webkit-text-stroke-color: #-webkit-text-stroke-width: #-webkit-touch-callout: #-webkit-transform: #-webkit-transform-origin: #-webkit-transform-origin-x: #-webkit-transform-origin-y: #-webkit-transform-origin-z: #-webkit-transform-style: #-webkit-transition: #-webkit-transition-delay: #-webkit-transition-duration: #-webkit-transition-property: #-webkit-transition-timing-function: #-webkit-user-drag: #-webkit-user-select: ").split("#");
  var cssPropertyKeywords = ("align-content: #align-items: #align-self: #animation: #animation-delay: #animation-direction: #animation-duration: #animation-fill-mode: #animation-iteration-count: #animation-name: #animation-play-state: #animation-timing-function: #azimuth: #backface-visibility: #background: #background-attachment: #background-clip: #background-color: #background-image: #background-origin: #background-position: #background-position-x: #background-position-y: #background-repeat: #background-size: #behavior: #border: #border-bottom: #border-bottom-color: #border-bottom-left-radius: #border-bottom-right-radius: #border-bottom-style: #border-bottom-width: #border-collapse: #border-color: #border-image: #border-image-outset: #border-image-repeat: #border-image-slice: #border-image-source: #border-image-width: #border-left: #border-left-color: #border-left-style: #border-left-width: #border-radius: #border-right: #border-right-color: #border-right-style: #border-right-width: #border-spacing: #border-style: #border-top: #border-top-color: #border-top-left-radius: #border-top-right-radius: #border-top-style: #border-top-width: #border-width: #bottom: #box-decoration-break: #box-shadow: #box-sizing: #break-after: #break-before: #break-inside: #caption-side: #clear: #clip: #clip-path: #clip-rule: #color: #color-interpolation: #color-interpolation-filters: #color-rendering: #column-count: #column-fill: #column-gap: #column-rule: #column-rule-color: #column-rule-style: #column-rule-width: #columns: #column-span: #column-width: #content: #counter-increment: #counter-reset: #crop: #cue: #cue-after: #cue-before: #cursor: #direction: #display: #elevation: #empty-cells: #enable-background: #fill: #fill-opacity: #fill-rule: #filter: #fit: #fit-position: #flex: #flex-basis: #flex-direction: #flex-flow: #flex-grow: #flex-shrink: #flex-wrap: #float: #flood-color: #flood-opacity: #font: #font-family: #font-feature-settings: #font-kerning: #font-language-override: #font-size: #font-size-adjust: #font-stretch: #font-style: #font-synthesis: #font-variant: #font-variant-alternates: #font-variant-caps: #font-variant-east-asian: #font-variant-ligatures: #font-variant-numeric: #font-variant-position: #font-weight: #glyph-orientation-horizontal: #glyph-orientation-vertical: #grid-cell: #grid-column: #grid-column-align: #grid-columns: #grid-column-sizing: #grid-column-span: #grid-flow: #grid-row: #grid-row-align: #grid-rows: #grid-row-sizing: #grid-row-span: #grid-template: #hanging-punctuation: #height: #hyphens: #icon: #image-orientation: #image-rendering: #ime-mode: #justify-content: #kerning: #left: #letter-spacing: #lighting-color: #line-break: #line-height: #list-style: #list-style-image: #list-style-position: #list-style-type: #margin: #margin-bottom: #margin-left: #margin-right: #margin-top: #marker: #marker-end: #marker-mid: #marker-start: #marquee-direction: #marquee-loop: #marquee-speed: #marquee-style: #mask: #max-height: #max-width: #min-height: #min-width: #move-to: #nav-down: #nav-index: #nav-left: #nav-right: #nav-up: #opacity: #order: #orphans: #outline: #outline-color: #outline-offset: #outline-style: #outline-width: #overflow: #overflow-style: #overflow-wrap: #overflow-x: #overflow-y: #padding: #padding-bottom: #padding-left: #padding-right: #padding-top: #page: #page-break-after: #page-break-before: #page-break-inside: #page-policy: #pause: #pause-after: #pause-before: #perspective: #perspective-origin: #pitch: #pitch-range: #pointer-events: #position: #quotes: #resize: #rest: #rest-after: #rest-before: #richness: #right: #rotation: #rotation-point: #ruby-align: #ruby-overhang: #ruby-position: #ruby-span: #scrollbar-3dlight-color: #scrollbar-arrow-color: #scrollbar-base-color: #scrollbar-darkshadow-color: #scrollbar-face-color: #scrollbar-highlight-color: #scrollbar-shadow-color: #scrollbar-track-color: #shape-rendering: #size: #speak: #speak-as: #speech-rate: #src: #stop-color: #stop-opacity: #stress: #stroke: #stroke-dasharray: #stroke-dashoffset: #stroke-linecap: #stroke-linejoin: #stroke-miterlimit: #stroke-opacity: #stroke-width: #table-layout: #tab-size: #text-align: #text-align-last: #text-anchor: #text-decoration: #text-decoration-color: #text-decoration-line: #text-decoration-skip: #text-decoration-style: #text-emphasis: #text-emphasis-color: #text-emphasis-position: #text-emphasis-style: #text-indent: #text-justify: #text-overflow: #text-rendering: #text-shadow: #text-space-collapse: #text-transform: #text-underline-position: #text-wrap: #top: #transform: #transform-origin: #transform-style: #transition: #transition-delay: #transition-duration: #transition-property: #transition-timing-function: #unicode-bidi: #unicode-range: #user-select: #vertical-align: #visibility: #voice-balance: #voice-duration: #voice-family: #voice-pitch: #voice-range: #voice-rate: #voice-stress: #voice-volume: #volume: #white-space: #widows: #width: #word-break: #word-spacing: #word-wrap: #z-index: #zoom: #-moz-animation: #-moz-animation-delay: #-moz-animation-direction: #-moz-animation-duration: #-moz-animation-iteration-count: #-moz-animation-name: #-moz-animation-play-state: #-moz-animation-timing-function: #-moz-appearance: #-moz-backface-visibility: #-moz-background-clip: #-moz-background-inline-policy: #-moz-background-origin: #-moz-background-size: #-moz-border-bottom-colors: #-moz-border-image: #-moz-border-left-colors: #-moz-border-radius: #-moz-border-radius-bottomleft: #-moz-border-radius-bottomright: #-moz-border-radius-topleft: #-moz-border-radius-topright: #-moz-border-right-colors: #-moz-border-top-colors: #-moz-box-align: #-moz-box-direction: #-moz-box-flex: #-moz-box-flexgroup: #-moz-box-ordinal-group: #-moz-box-orient: #-moz-box-pack: #-moz-box-shadow: #-moz-box-sizing: #-moz-column-count: #-moz-column-gap: #-moz-column-rule: #-moz-column-rule-color: #-moz-column-rule-style: #-moz-column-rule-width: #-moz-column-width: #-moz-font-feature-settings: #-moz-hyphens: #-moz-opacity: #-moz-perspective: #-moz-perspective-origin: #-moz-text-align-last: #-moz-text-decoration-color: #-moz-text-decoration-line: #-moz-text-decoration-style: #-moz-transform: #-moz-transform-origin: #-moz-transition: #-moz-transition-delay: #-moz-transition-duration: #-moz-transition-property: #-moz-transition-timing-function: #-moz-user-select: #-ms-accelerator: #-ms-behavior: #-ms-block-progression: #-ms-content-zoom-chaining: #-ms-content-zooming: #-ms-content-zoom-limit: #-ms-content-zoom-limit-max: #-ms-content-zoom-limit-min: #-ms-content-zoom-snap: #-ms-content-zoom-snap-points: #-ms-content-zoom-snap-type: #-ms-filter: #-ms-flex: #-ms-flex-align: #-ms-flex-direction: #-ms-flex-flow: #-ms-flex-item-align: #-ms-flex-line-pack: #-ms-flex-order: #-ms-flex-pack: #-ms-flex-wrap: #-ms-flow-from: #-ms-flow-into: #-ms-grid-column: #-ms-grid-column-align: #-ms-grid-columns: #-ms-grid-column-span: #-ms-grid-layer: #-ms-grid-row: #-ms-grid-row-align: #-ms-grid-rows: #-ms-grid-row-span: #-ms-high-contrast-adjust: #-ms-hyphenate-limit-chars: #-ms-hyphenate-limit-lines: #-ms-hyphenate-limit-zone: #-ms-hyphens: #-ms-ime-mode: #-ms-interpolation-mode: #-ms-layout-grid: #-ms-layout-grid-char: #-ms-layout-grid-line: #-ms-layout-grid-mode: #-ms-layout-grid-type: #-ms-line-break: #-ms-overflow-style: #-ms-perspective: #-ms-perspective-origin: #-ms-perspective-origin-x: #-ms-perspective-origin-y: #-ms-progress-appearance: #-ms-scrollbar-3dlight-color: #-ms-scrollbar-arrow-color: #-ms-scrollbar-base-color: #-ms-scrollbar-darkshadow-color: #-ms-scrollbar-face-color: #-ms-scrollbar-highlight-color: #-ms-scrollbar-shadow-color: #-ms-scrollbar-track-color: #-ms-scroll-chaining: #-ms-scroll-limit: #-ms-scroll-limit-x-max: #-ms-scroll-limit-x-min: #-ms-scroll-limit-y-max: #-ms-scroll-limit-y-min: #-ms-scroll-rails: #-ms-scroll-snap-points-x: #-ms-scroll-snap-points-y: #-ms-scroll-snap-type: #-ms-scroll-snap-x: #-ms-scroll-snap-y: #-ms-scroll-translation: #-ms-text-align-last: #-ms-text-autospace: #-ms-text-justify: #-ms-text-kashida-space: #-ms-text-overflow: #-ms-text-size-adjust: #-ms-text-underline-position: #-ms-touch-action: #-ms-touch-select: #-ms-transform: #-ms-transform-origin: #-ms-transform-origin-x: #-ms-transform-origin-y: #-ms-transform-origin-z: #-ms-user-select: #-ms-word-break: #-ms-word-wrap: #-ms-wrap-flow: #-ms-wrap-margin: #-ms-wrap-through: #-ms-writing-mode: #-ms-zoom: #-ms-zoom-animation: #-o-animation: #-o-animation-delay: #-o-animation-direction: #-o-animation-duration: #-o-animation-fill-mode: #-o-animation-iteration-count: #-o-animation-name: #-o-animation-play-state: #-o-animation-timing-function: #-o-background-size: #-o-border-image: #-o-object-fit: #-o-object-position: #-o-table-baseline: #-o-tab-size: #-o-text-overflow: #-o-transform: #-o-transform-origin: #-o-transition: #-o-transition-delay: #-o-transition-duration: #-o-transition-property: #-o-transition-timing-function: #-webkit-animation: #-webkit-animation-delay: #-webkit-animation-direction: #-webkit-animation-duration: #-webkit-animation-fill-mode: #-webkit-animation-iteration-count: #-webkit-animation-name: #-webkit-animation-play-state: #-webkit-animation-timing-function: #-webkit-appearance: #-webkit-backface-visibility: #-webkit-background-clip: #-webkit-background-composite: #-webkit-background-origin: #-webkit-background-size: #-webkit-border-bottom-left-radius: #-webkit-border-bottom-right-radius: #-webkit-border-image: #-webkit-border-radius: #-webkit-border-top-left-radius: #-webkit-border-top-right-radius: #-webkit-box-align: #-webkit-box-direction: #-webkit-box-flex: #-webkit-box-flex-group: #-webkit-box-ordinal-group: #-webkit-box-orient: #-webkit-box-pack: #-webkit-box-reflect: #-webkit-box-shadow: #-webkit-box-sizing: #-webkit-column-count: #-webkit-column-gap: #-webkit-column-rule: #-webkit-column-rule-color: #-webkit-column-rule-style: #-webkit-column-rule-width: #-webkit-columns: #-webkit-column-width: #-webkit-filter: #-webkit-flow-from: #-webkit-flow-into: #-webkit-hyphens: #-webkit-line-break: #-webkit-margin-bottom-collapse: #-webkit-margin-collapse: #-webkit-margin-start: #-webkit-margin-top-collapse: #-webkit-marquee-direction: #-webkit-marquee-increment: #-webkit-marquee-repitition: #-webkit-marquee-speed: #-webkit-marquee-style: #-webkit-nbsp-mode: #-webkit-overflow-scrolling: #-webkit-padding-start: #-webkit-perspective: #-webkit-perspective-origin: #-webkit-tap-highlight-color: #-webkit-text-fill-color: #-webkit-text-shadow: #-webkit-text-size-adjust: #-webkit-text-stroke: #-webkit-text-stroke-color: #-webkit-text-stroke-width: #-webkit-touch-callout: #-webkit-transform: #-webkit-transform-origin: #-webkit-transform-origin-x: #-webkit-transform-origin-y: #-webkit-transform-origin-z: #-webkit-transform-style: #-webkit-transition: #-webkit-transition-delay: #-webkit-transition-duration: #-webkit-transition-property: #-webkit-transition-timing-function: #-webkit-user-drag: #-webkit-user-select: ").split("#");
  var varKeywords = Array();

  // Get instance type.
  function getType( str ) {
    var firstChar = str.slice(0, 1);
    var lastTwoChar = str.slice(-2);
    var type = '';
    if ( firstChar == '@' ) type = 'var';
    else if ( lastTwoChar == ': ' ) type = 'prop';
    else type = 'html';

    return type;
  }

  function getCompletions(token, context, keywords) {
    var found = [], start = token.string;
    function maybeAdd(str) {
      if (str.indexOf(start) == 0 && !arrayContains(found, str)) found.push(str);
    }
    function gatherCompletions(obj) {
      console.log( token );
      console.log( context );
      console.log( keywords );
      forEach(htmlKeywords, maybeAdd);
      /*if (getType(obj) == 'html') forEach(htmlKeywords, maybeAdd);
      else if (getType(obj) == 'prop') forEach(cssPropertyKeywords, maybeAdd);
      else if (getType(obj) == 'var') forEach(varKeywords, maybeAdd);*/
      /* if (typeof obj == "string") forEach(stringProps, maybeAdd);
      else if (obj instanceof Array) forEach(arrayProps, maybeAdd);
      else if (obj instanceof Function) forEach(funcProps, maybeAdd); */
      for (var name in obj) maybeAdd(name);
    }

    if (context) {
      // If this is a property, see if it belongs to some object we can
      // find in the current environment.
      var obj = context.pop(), base;
      if (obj.className == "variable")
        base = window[obj.string];
      else if (obj.className == "string")
        base = "";
      else if (obj.className == "atom")
        base = 1;
      else if (obj.className == "function") {
        if (window.jQuery != null && (obj.string == '$' || obj.string == 'jQuery') &&
            (typeof window.jQuery == 'function'))
          base = window.jQuery();
        else if (window._ != null && (obj.string == '_') && (typeof window._ == 'function'))
          base = window._();
      }
      while (base != null && context.length)
        base = base[context.pop().string];
      if (base != null) gatherCompletions(base);
    }
    else {
      // If not, just look in the window object and any local scope
      // (reading into JS mode internals to get at the local variables)
      for (var v = token.state.localVars; v; v = v.next) maybeAdd(v.name);
      gatherCompletions(window);
      forEach(keywords, maybeAdd);
    }
    return found;
  }
})();
