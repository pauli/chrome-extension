$(function(){
      
      var last_element = null
      
      $("body *").bind("click", function(){
        var is_different_elem = $(this)[0] != $(last_element)[0]
        if (last_element == null || is_different_elem || $(".dialogs").length == 0) {
          $(".dialogs").remove()
          $(this).append("<div id='dialog' class='dialogs'></div>")
          var element = $(this).get(0).tagName.toLowerCase()
        
          var id = $(this).attr("id")
          if (id)
            id = "#"+id
          else
            id = ""
          
          var klass = $(this).attr("class") // TODO: multiple classes support
          if (klass) 
            klass = "."+klass.replace(/\s*dh_hover/, '')
          else
            klass = ""
        
          var infos = "element: "+element+id+klass
          $("#dialog").html(infos).show()
        } else {
          $(".dialogs").remove()
        }
        
        last_element = this
      })
  
      $("body *").hover(function(){
        $(this).addClass("dh_hover")
        $(this).width($(this).width()-2).height($(this).height()-2)
      }, function(){
        $(".dialogs").remove()
        $(this).removeClass("dh_hover")
        $(this).width($(this).width()+2).height($(this).height()+2)
      })
    })