/*
 * Javascript file for less css live editor.
 * c Joe Cserko 2012-07-24.
 * All right reserved.
 */

// Global vars.
var _g = new Object();
_g.file = null;
var less_mirror_editor;
var css_mirror_editor;
var inspected = false;
var wto = 0;
var is_watching = false;
var lessWorker = new Worker('js/lessWorker.js');
var myVars = Array();
// Get current tab.
function thisTab(){
	_g.win = chrome.devtools.inspectedWindow;
}
// Get settings from localStorage.
function getSettings() {
	_g.myid = chrome.i18n.getMessage("@@extension_id");
	_g.s = {};
	_g.s.less_font_size = localStorage['less_font_size'] ? localStorage['less_font_size'] : '14';
	_g.s.less_color_theme = localStorage['less_color_theme'] ? localStorage['less_color_theme']: 'ambiance';

	// Set.
	$('#less_editor_div').css('font-size', _g.s.less_font_size+'px');
	$('#css_editor_div').css('font-size', _g.s.less_font_size+'px');
	if ( localStorage['last_less'] ) $('#less_editor_area').val( localStorage['last_less'] );
}
// Preview css in developer window.
function previewCSS( apply ) {
	thisTab();
	// Get less code into codemirror.
	var current_val = less_mirror_editor.getValue();
	// Set local storage buffer.
	localStorage['last_less'] = current_val;
	// Post less code to worker.
	lessWorker.onmessage = function(event){
		console.log( event.data );
		css_mirror_editor.setValue(event.data.parsed);
		if ( event.data.debug_str ) console.log ( event.data.debug_str ); 
		// lessWorker.terminate();
		if ( apply ) {
	    	var parsed = event.data.parsedCompress.replace(/\n/g,"");
			chrome.extension.sendMessage({task: "update", tabId: _g.win.tabId, css: parsed});	    
	    }
	}
	// Message object.
	var mo = { 'lessCode' : current_val, 'compress' : apply }
	lessWorker.postMessage( mo );
	return;
	
	thisTab();
	// Get less code into textarea.
	// less_mirror_editor.save();
	var current_val = less_mirror_editor.getValue();
	localStorage['last_less'] = current_val;
	// Create new instance from less parser.
	parser = new less.Parser({});
	// Parse less code to css.
	parser.parse(current_val, function (error, root) {
		// If some error, show error message.
		if ( error ) alert( error.message );
		else {
			// Fill css area.
		    var parsed = root.toCSS();
		    $('#css_output_area').val( parsed );
		    css_mirror_editor.setValue(parsed);
		    // If apply.
		    if ( apply ) {
		    	// Create inject details.
		    	//chrome.extension.sendMessage({css: parsed, tabId: _g.win.tabId}, function(response) {
				  // alert(response.message);
				//});
				var parsed = root.toCSS({ compress: true });
				parsed = parsed.replace(/\n/g,"");
				chrome.extension.sendMessage({task: "update", tabId: _g.win.tabId, css: parsed});	    
		    }
		} 
	});
}
// Insert char pairs.
function insertAny( cm, ins, shiftLine, shiftChar ) {
	var pos = cm.getCursor();
	cm.replaceSelection(ins, {line: pos.line, ch: pos.ch}, {line: pos.line, ch: pos.ch + 1});
	cm.setCursor({line: pos.line + shiftLine, ch: pos.ch + shiftChar})
}
// Create and save files.
function onSave() {
	less_mirror_editor.save();
	var css = $('#less_editor_area').val();
	window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
	var fs = null;
	window.requestFileSystem(window.TEMPORARY, 1024*1024, function(filesystem) {
		fs = filesystem;
		fs.root.getFile('teszt.less', {create: true}, function(fileEntry){
			fileEntry.createWriter(function(fileWriter) {
				var blob = new Blob([css], { type: "text/css" });
				fileWriter.onwriteend = function( end ) {
					console.log( fileEntry.toURL() );
					if ( $('#temp_link').length < 1 ) 
						$('body').append('<a id="temp_link"></a>')
					$('#temp_link').attr('href', fileEntry.toURL()).trigger('click');					
	            };
	            fileWriter.write( blob );
			});
		}, errorHandler);

	}, errorHandler);
}
// Prepare save.
function preSave() {
	var css = less_mirror_editor.getValue();
	var blob = new Blob([css], { type: "text/css" });
	$('#less_save_link').attr('download', 'mywork.less');
	$('#less_save_link').attr('href', window.webkitURL.createObjectURL(blob) );
	$('#less_save_link').trigger('select');
}
// File error handling.
function errorHandler(e) {
  var msg = '';
  switch (e.code) {
    case FileError.QUOTA_EXCEEDED_ERR:
      msg = 'QUOTA_EXCEEDED_ERR';
      break;
    case FileError.NOT_FOUND_ERR:
      msg = 'NOT_FOUND_ERR';
      break;
    case FileError.SECURITY_ERR:
      msg = 'SECURITY_ERR';
      break;
    case FileError.INVALID_MODIFICATION_ERR:
      msg = 'INVALID_MODIFICATION_ERR';
      break;
    case FileError.INVALID_STATE_ERR:
      msg = 'INVALID_STATE_ERR';
      break;
    default:
      msg = 'Unknown Error';
      break;
  };
  console.log( msg );
}
// Disable all existing style.
function disableAllStyle(){
		for(i=0;i<document.styleSheets.length;i++)
			(document.styleSheets.item(i).disabled=true);
	el=document.getElementsByTagName('*');
		for(i=0;i<el.length;i++)
			(el[i].style.cssText='');
}
// Inject js and css this page.
function switchInspect() {
	thisTab();
	var key = inspected ? 'end_inspect' : 'start_inspect';
	inspected = inspected ? false : true;
	chrome.extension.sendMessage({task: key, tabId: _g.win.tabId, extId: _g.myid}, function(response) {
	  alert( response.message );
	});
}
// Receive any messages.
function receiveMessages( msg ) {
	if( msg.alert ) alert(msg.alert);
	if( msg.cid ) less_mirror_editor.replaceSelection( pInspector( msg.cid ) );
}
// respond('Another stupid example!');
// Create css file.
function createCSS(){
	thisTab();
	chrome.extension.sendMessage({task: "create", tabId: _g.win.tabId});
}
// Update css file.
function updateStyle(){
	thisTab();
	chrome.extension.sendMessage({task: "create", tabId: _g.win.tabId, css: parsed});
}
// Push variables runtime in runtime.
function pushVariables( cm, char ) {
	var cr = cm.getCursor();
	var line = cm.getLine( cr.line );
	if ( line.indexOf('@') != -1 ) {
		myVars.push( line.slice( line.indexOf('@') ) );
		/* CodeMirror.commands.autocomplete = function(cm) {
			CodeMirror.simpleHint(cm, CodeMirror.lessHint);
		} */
	}
	cm.replaceSelection(char, cr, cr);
	cm.setCursor( {line: cr.line, ch: cr.ch + 1} )
}
// Process dom inspector messages.
function pInspector( im ) {
	var ret = '';
	var arr = im.split(' ');
	var l = arr.length;
	for( var i = 0; i < l; i++ ) 
		if ( (l - i) < 4 ) 
			if ( arr[i] ) ret += ' '+arr[i];

	return ret.slice(1);
}
// Initialize app. //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
document.addEventListener('DOMContentLoaded', function () {
	// Get user settings.
	getSettings();
	// Set codemirror.
	CodeMirror.commands.autocomplete = function(cm) {
		CodeMirror.simpleHint(cm, CodeMirror.lessHint);
	}
	var options = {
		theme			: _g.s.less_color_theme,
        lineNumbers 	: true,
        matchBrackets 	: true,
        extraKeys		: {
        	"Ctrl-Space": "autocomplete",
        	"'('"		: function(cm) { insertAny(cm, "(  )", 0, 2); },
        	"'{'"		: function(cm) { insertAny(cm, "{\n  \n}", 1, 4); },
        	"';'"		: function(cm) { insertAny(cm, ";", 0, 1); previewCSS(); },
        	"':'"		: function(cm) { pushVariables(cm, ':'); }
        },
        indentWithTabs 	: true,
        autofocus 		: true,
        lineWrapping 	: true
	}; 
	less_mirror_editor = CodeMirror.fromTextArea( document.getElementById('less_editor_area'), options );
	options.readOnly = true;
	css_mirror_editor = CodeMirror.fromTextArea( document.getElementById('css_output_area'), options );
	// Set actions.
	$('#refresh_btn').click(function(){ previewCSS( true ); });
	// Increase font.
	$('#font_increase_btn').click(function (e) {
		$('#less_editor_div').css('font-size', '+=1');
		$('#css_editor_div').css('font-size', '+=1');
		localStorage['less_font_size'] = parseInt( $('#less_editor_div').css('font-size'), 10 );
	});
	// Decrease editor font.
	$('#font_decrease_btn').click(function (e) {
		$('#less_editor_div').css('font-size', '-=1');
		$('#css_editor_div').css('font-size', '-=1');
		localStorage['less_font_size'] = parseInt( $('#less_editor_div').css('font-size'), 10 );
	});
	// Toggle dom inspector.
	$('#inspect_btn').click(function (e) { switchInspect(); $(this).toggleClass('toggled'); });
	// Down enter key.
	$('#less_editor_div').on('keydown', function (e){
		if ( e.keyCode == 13 && !e.shiftKey ) previewCSS();
	});
	// Save less file.
	$('#less_save_btn').click(function(e){ preSave(); });
	// Invoke file saving.
	$('#less_save_link').select(function(e){ e.currentTarget.click(); });
	// Unload window.
	$(window).unload(function() {
		less_mirror_editor.save();
		localStorage['last_less'] = $('#less_editor_area').val();
	});
	// Create CSS file.
	thisTab();
	chrome.extension.sendMessage({task: "create", tabId: _g.win.tabId});
	previewCSS();

});