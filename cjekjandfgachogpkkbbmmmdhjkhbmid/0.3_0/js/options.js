// Saves options to localStorage.
function save_options() {
  localStorage["less_font_size"] = $('#less_font_size').val();
  localStorage["less_color_theme"] = $('#less_color_theme').val();

  alert('Save done');
}

// Restores select box state to saved value from localStorage.
function restore_options() {
  $('#less_font_size').val( localStorage["less_font_size"] );
  $('#less_color_theme').val( localStorage["less_color_theme"] );
}
document.addEventListener('DOMContentLoaded', function() {
  restore_options();
  $('#save_btn').click( function(){ 
    save_options(); 
  });
});
