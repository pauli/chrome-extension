        $(document).ready(function () {
            $("#donateTitle").html(chrome.i18n.getMessage("donateTitle") + "<br/>");
            BG.loadUserAgents();
        });
            $("#options").click(function () {
        window.close();
        chrome.tabs.create({url:chrome.extension.getURL("options.html")});
    });
    $("#donateTitle").click(function () {
        window.close();
        chrome.tabs.create({url:chrome.extension.getURL("donate.html")});
    });

function clickHandler(e) {
  var BG = chrome.extension.getBackgroundPage();
  BG.log("clicked:" + e);
  var id = e.currentTarget.val;
  selectUa(id);
  //setTimeout(awesomeTask, 1000);
}

        var BG = chrome.extension.getBackgroundPage();
        chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
            renderUaList(request);
        });

        function selectUa(id) {
            BG.changeUserAgent(id);
            window.close();
        }

        var currentUa = BG.getUa();

        function renderUaList(arrayOfUa) {
            var ul = document.getElementById("ualist");
            var ii = 1;
            for (var i = 0; i < arrayOfUa.length; i++) {
                var name = arrayOfUa[i].name;
                var uastring = arrayOfUa[i].string;
                var liclass = "";
                if (currentUa == uastring) {
                    liclass = "selected";
                }
                var li = document.createElement("li");
                li.className = liclass;
                var a = document.createElement("a");
                //a.setAttribute("onclick", "selectUa('" + uastring + "');");
                a.val = uastring;
                a.href = "#";
                a.textContent = name;
                a.addEventListener('click', clickHandler);
                if(ii <= 9) {
                    var shortcut = document.createElement("div");
                    shortcut.textContent = ii;
                    shortcut.className = "shortcut";
                    a.id = "ua" + ii;
                    li.appendChild(shortcut);
                }
                ii += 1;
                li.appendChild(a);
                ul.appendChild(li);
            }
        }

        $(document).keydown(function (event) {
            var keymap = new Array();
            var base = 49;
            for (var i = 1; i <= 9; i++) {
                keymap[base] = i;
                base += 1;
            }
            if (event.keyCode >= 49 && event.keyCode <= 57) {
                $("#ua" + keymap[event.keyCode]).click();
            }
        });

         

