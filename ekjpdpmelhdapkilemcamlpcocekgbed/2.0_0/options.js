var displayStack;
var displayVariables;
var displaySuperglobals;

function save_options() {
	var select = document.getElementById("displayStack");
	displayStack = select.children[select.selectedIndex].value;
	localStorage["displayStack"] = displayStack;

	var select = document.getElementById("displaySuperglobals");
	displaySuperglobals = select.children[select.selectedIndex].value;
	localStorage["displaySuperglobals"] = displaySuperglobals;

	var select = document.getElementById("displayVariables");
	displayVariables = select.children[select.selectedIndex].value;
	localStorage["displayVariables"] = displayVariables;

	var status = document.getElementById("status");
	status.innerHTML = "Options Saved.";
	setTimeout(function() {
		status.innerHTML = "&nbsp;";
	}, 750);
}

function restore_options() {
	displayStack = localStorage["displayStack"];
	if (!displayStack) {
		displayStack = true;
	}
	var select = document.getElementById("displayStack");
	for (var i = 0; i < select.children.length; i++) {
		var child = select.children[i];
		if (child.value == displayStack) {
			child.selected = "true";
			break;
		}
	}

	displaySuperglobals = localStorage["displaySuperglobals"];
	if (!displaySuperglobals) {
		displaySuperglobals = true;
	}
	var select = document.getElementById("displaySuperglobals");
	for (var i = 0; i < select.children.length; i++) {
		var child = select.children[i];
		if (child.value == displaySuperglobals) {
			child.selected = "true";
			break;
		}
	}

	displayVariables = localStorage["displayVariables"];
	if (!displayVariables) {
		displayVariables = true;
	}
	var select = document.getElementById("displayVariables");
	for (var i = 0; i < select.children.length; i++) {
		var child = select.children[i];
		if (child.value == displayVariables) {
			child.selected = "true";
			break;
		}
	}
}

document.addEventListener('DOMContentLoaded', restore_options);
document.querySelector('#save').addEventListener('click', save_options);

window.onfocus = function() {
	window.location.reload(true);
};
