function tableToggleOff(className) {
	var myElements = document.getElementsByClassName(className);
	for (var i = 0, ii = myElements.length; i < ii; i++) {
		var el = (myElements[i]);
		el.style.display = 'none';
		if (el.parentElement.tagName == 'FONT') {
			el = el.parentElement.previousSibling.previousSibling;
			if (el.tagName == 'BR') {
				el.style.display = 'none';
			}
		}
	}
};
tableToggleOff('xdebug-error');
tableToggleOff('xdebug-superglobals');
tableToggleOff('xdebug-trace');
