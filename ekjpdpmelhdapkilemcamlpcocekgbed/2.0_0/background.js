var icon;

var displayStack = localStorage["displayStack"];
var displaySuperglobals = localStorage["displaySuperglobals"];
var displayVariables = localStorage["displayVariables"];
updateMenu();

function updateMenu() {
	displayStack = localStorage["displayStack"];
	displayStack = (displayStack == "true" || displayStack == true) ? true : false;
	chrome.contextMenus.update("checkboxStack", {
		"title" : "Stack",
		"parentId" : "parentDisplay",
		"type" : "checkbox",
		"checked" : displayStack
	});

	displaySuperglobals = localStorage["displaySuperglobals"];
	displaySuperglobals = (displaySuperglobals == "true" || displaySuperglobals == true) ? true : false;
	chrome.contextMenus.update("checkboxSuperglobals", {
		"title" : "Superglobals",
		"parentId" : "parentDisplay",
		"type" : "checkbox",
		"checked" : displaySuperglobals
	});

	displayVariables = localStorage["displayVariables"];
	displayVariables = (displayVariables == "true" || displayVariables == true) ? true : false;
	chrome.contextMenus.update("checkboxVariables", {
		"title" : "Variables",
		"parentId" : "parentDisplay",
		"type" : "checkbox",
		"checked" : displayVariables
	});

};

function updateStorage() {
	localStorage["displayStack"] = displayStack;
	localStorage["displaySuperglobals"] = displaySuperglobals;
	localStorage["displayVariables"] = displayVariables;
};

function toggleBug() {
	if (icon != 'icons/bug_on.png') {
		icon = "icons/bug_on.png";
		chrome.browserAction.setIcon({
			path : icon
		});
	} else {
		icon = "icons/bug_off.png";
		chrome.browserAction.setIcon({
			path : icon
		});
	}
};

function refresh() {
	updateMenu();
	if (icon != 'icons/bug_on.png') {
		chrome.windows.getAll(undefined, function(windows) {
			for (var w = 0, window; window = windows[w]; w++) {
				chrome.tabs.getAllInWindow(window.id, function(tabs) {
					for (var i = 0; i < tabs.length; i++) {
						chrome.tabs.executeScript(tabs[i].id, {
							file : "jquery.min.js",
							allFrames : true
						});
						chrome.tabs.executeScript(tabs[i].id, {
							file : "toggleOn.js",
							allFrames : true
						});
						if (displayStack == true) {
							chrome.tabs.executeScript(tabs[i].id, {
								file : "toggleStackOn.js",
								allFrames : true
							});
						} else {
							chrome.tabs.executeScript(tabs[i].id, {
								file : "toggleStackOff.js",
								allFrames : true
							});
						}
						if (displaySuperglobals == true) {
							chrome.tabs.executeScript(tabs[i].id, {
								file : "toggleSuperglobalsOn.js",
								allFrames : true
							});
						} else {
							chrome.tabs.executeScript(tabs[i].id, {
								file : "toggleSuperglobalsOff.js",
								allFrames : true
							});
						}
						if (displayVariables == true) {
							chrome.tabs.executeScript(tabs[i].id, {
								file : "toggleVariablesOn.js",
								allFrames : true
							});
						} else {
							chrome.tabs.executeScript(tabs[i].id, {
								file : "toggleVariablesOff.js",
								allFrames : true
							});
						}
					}
				});
			}
		});
	} else {
		chrome.windows.getAll(undefined, function(windows) {
			for (var w = 0, window; window = windows[w]; w++) {
				chrome.tabs.getAllInWindow(window.id, function(tabs) {
					for (var i = 0; i < tabs.length; i++) {
						chrome.tabs.executeScript(tabs[i].id, {
							file : "toggleOff.js",
							allFrames : true
						});
					}
				});
			}
		});
	}
};

function toggleErrors() {
	toggleBug();
	refresh();
};

function onClickHandler(info, tab) {
	switch(info.menuItemId) {
		case "checkboxStack":
			displayStack = info.checked;
			localStorage["displayStack"] = displayStack;
			refresh();
			break;
		case "checkboxSuperglobals":
			displaySuperglobals = info.checked;
			localStorage["displaySuperglobals"] = displaySuperglobals;
			refresh();
			break;
		case "checkboxVariables":
			displayVariables = info.checked;
			localStorage["displayVariables"] = displayVariables;
			refresh();
			break;
		case "menuToggle":
			toggleBug();
			refresh();
			break;
		case "menuRefresh":
			refresh();
			break;
		default:
			toggleBug();
			refresh();
			break;
	}
};

chrome.contextMenus.onClicked.addListener(onClickHandler);

chrome.runtime.onInstalled.addListener(function() {
	var menuToggle = chrome.contextMenus.create({
		"title" : "Toggle",
		"id" : "menuToggle"
	});
	var menuRefresh = chrome.contextMenus.create({
		"title" : "Refresh",
		"id" : "menuRefresh"
	});
	var menuParentDisplay = chrome.contextMenus.create({
		"title" : "Display",
		"id" : "parentDisplay"
	});
	var checkboxStack = chrome.contextMenus.create({
		"title" : "Stack",
		"parentId" : "parentDisplay",
		"type" : "checkbox",
		"id" : "checkboxStack",
		"checked" : true
	});
	var checkboxSuperglobals = chrome.contextMenus.create({
		"title" : "Superglobals",
		"parentId" : "parentDisplay",
		"type" : "checkbox",
		"id" : "checkboxSuperglobals",
		"checked" : true
	});
	var checkboxVariables = chrome.contextMenus.create({
		"title" : "Variables",
		"parentId" : "parentDisplay",
		"type" : "checkbox",
		"id" : "checkboxVariables",
		"checked" : true
	});
	displayStack = true;
    displaySuperglobals = true;
	displayVariables = true;
	updateStorage();
	updateMenu();
});

chrome.tabs.onActivated.addListener(function() {
	refresh();
});

chrome.browserAction.onClicked.addListener(function(tab) {
	toggleErrors();
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.command == "refresh") {
		refresh();
	}
});

