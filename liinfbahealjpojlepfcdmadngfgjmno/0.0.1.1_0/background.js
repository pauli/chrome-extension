var m_oLastTarget = null;

// Track the last element during the mouse down
document.addEventListener('mousedown', function(event)
{
	m_oLastTarget = event.target;
}, true);

chrome.extension.onRequest.addListener(function(event)
{
	m_oLastTarget.parentNode.removeChild(m_oLastTarget);
});