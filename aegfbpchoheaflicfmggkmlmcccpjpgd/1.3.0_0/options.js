var defaultConfig = {
  noreferrer: true,
  link_style: 'cursor:help;display:inline !important;',
  open_in_newtab: true
};

function L10N() {
  chrome.i18n.getAcceptLanguages(function (langs) {
    if (langs.indexOf('ja') < 0) {
      document.querySelector('#menu_tabs > li.news').style.display = 'none';
    }
  });
  var elems = document.querySelectorAll('*[class^="MSG_"]');
  Array.prototype.forEach.call(elems, function (node) {
    var key = node.className.match(/MSG_(\w+)/)[1];
    var message = chrome.i18n.getMessage(key);
    if (message) node.textContent = message;
  });
}
L10N();

var init = function(Config) {
  $X('//input[@type="checkbox"]').forEach(function (input) {
    var id = input.id;
    input.checked = !!Config[id];
    input.addEventListener('click', function () {
      Config[id] = input.checked;
      chrome.storage.sync.set({'TextURLLinker': Config});
    }, false);
  });

  $X('//input[@type="text"]').forEach(function (input) {
    var id = input.id;
    input.value = Config[id] || 'cursor:help;display:inline !important;';
    input.addEventListener('change', function () {
      Config[id] = input.value;
      chrome.storage.sync.set({'TextURLLinker': Config});
    }, false);
  });
};

chrome.storage.sync.get('TextURLLinker', function(data) {
  if (data.TextURLLinker) {
    init(data.TextURLLinker);
  } else {
    chrome.storage.sync.set({'TextURLLinker': defaultConfig},function(){
      init(defaultConfig);
    });
  }
});

document.getElementById('Version').textContent = chrome.runtime.getManifest().version;

var sections = $X('id("container")/section[contains(@class, "content")]');
var btns = $X('id("menu_tabs")/li/a');
var default_title = document.title;
window.addEventListener('hashchange', function (evt) {
  var hash = location.hash;
  btns.forEach(function (btn, i) {
    btn.className = (!hash && !i) || (btn.hash === hash) ? 'active' : '';
  });
  sections.forEach(function (sc, i) {
    sc.style.display = (!hash && !i) || ('#' + sc.id === hash) ? 'block' : 'none';
  });
  document.title = default_title + hash;
  window.scrollBy(0, -1000);
}, false);
if (location.hash && sections.some(function (section, i) {
  if ('#' + section.id === location.hash) {
    btns.forEach(function (btn) {
      btn.className = '';
    })
    btns[i].className = 'active';
    section.style.display = 'block';
    document.title = default_title + location.hash;
    return true;
  }
})) {
} else {
  sections[0].style.display = 'block';
  document.title = default_title + '#' + sections[0].id;
}
