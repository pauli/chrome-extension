// Copyright (c) 2013 Gerard D. Sexton. All rights reserved.

chrome.devtools.panels.elements.createSidebarPane(
  "CSS Compare",
  function(sidebar) {
    sidebar.setPage("pane.html");
    sidebar.setHeight("400px");    
});