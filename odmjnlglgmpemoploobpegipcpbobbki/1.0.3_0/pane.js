// Copyright (c) 2013 Gerard D. Sexton. All rights reserved.

var compareResults = function() {
  if (!$1 || !$0) return {__proto__:null};
  var first = $1,
      second = $0,
      firstStyle = {
        __proto__:null
      }, 
      secondStyle = {
        __proto__:null
      },
      firstName = first.id && "#"+first.id || first.tagName,
      secondName = second.id && "#"+second.id || second.tagName,
      ret = {
        __proto__: null
      };
      
  if (firstName == secondName) {
    firstName = "1st."+firstName;
    secondName = "2nd."+secondName;
  }

  try {
    if (document.defaultView && document.defaultView.getComputedStyle) {
      var first1 = document.defaultView.getComputedStyle(first,null);
      var second1 = document.defaultView.getComputedStyle(second,null);
      var shortLen = first1.length <= second1.length && first1.length || second1.length;
      for (var i = 0; i < shortLen; i++) {
        
        if (first1[first1[i]] != second1[second1[i]]) {
          firstStyle[first1[i]] = first1[first1[i]];
          secondStyle[second1[i]] = second1[second1[i]];
        }  
      }
    }
  }
  catch (e) {
    ret.Error = e;
  } 
  
  ret.firstStyleName = firstName;
  ret.firstStyle = firstStyle;
  ret.secondStyleName = secondName;
  ret.secondStyle = secondStyle;
  return ret;
};

var initDisplay = function(results) {
  var mainTable = document.getElementById("main");
  mainTable.innerHTML = "";

  if (!results.firstStyle) {
    return;
  }
  if (Object.keys(results.firstStyle).length === 0) {

    var noticeRow = document.createElement("tr");
    var noticeCell = document.createElement("td");
    var notice = document.createElement("span");
    notice.innerText = "Elements are identical";
    noticeCell.className = "notice";
    noticeCell.appendChild(notice);
    noticeRow.appendChild(noticeCell);
    mainTable.appendChild(noticeRow);
    return;
  }
  
  if (localStorage.getItem("layout") === "portrait") {
    drawVertical(mainTable, results);
  }
  else {
    drawHorizontal(mainTable, results);
  }
};

var drawHorizontal = function(mainTable, results) {

  var header = document.createElement("tr");
  var title1 = document.createElement("th");
  title1.innerText = "Property";
  var title2 = document.createElement("th");
  title2.innerText =results.firstStyleName;
  var title3 = document.createElement("th");
  title3.innerText = results.secondStyleName;

  header.appendChild(title1);
  header.appendChild(title2);
  header.appendChild(title3);
  mainTable.appendChild(header);

  for (var key in results.firstStyle) {
    if (!results.firstStyle.hasOwnProperty(key)) continue;

    var row = document.createElement("tr");
    var prop = document.createElement("td");
    prop.innerText = key;
    prop.className = "css-prop";

    var left = document.createElement("td");
    left.innerText = results.firstStyle[key];
    left.className = "css-left";

    var right = document.createElement("td");
    right.innerText = results.secondStyle[key];
    right.className = "css-left";
    
    row.appendChild(prop);
    row.appendChild(left);
    row.appendChild(right);
    mainTable.appendChild(row);
  }
};

var drawVertical = function(mainTable, results) {
  var header = document.createElement("tr");
  var title1 = document.createElement("th");
  var title2 = document.createElement("th");
  var topTitle = document.createElement("div");
  var bottomTitle = document.createElement("div");
  title1.innerText = "Property";
  topTitle.innerText = results.firstStyleName;
  bottomTitle.innerText = results.secondStyleName;
  title2.appendChild(topTitle);
  title2.appendChild(bottomTitle);
  
  header.appendChild(title1);
  header.appendChild(title2);
  mainTable.appendChild(header);

  for (var key in results.firstStyle) {
    if (!results.firstStyle.hasOwnProperty(key)) continue;

    var row = document.createElement("tr");
    var prop = document.createElement("td");
    prop.innerText = key;
    prop.className = "css-prop";

    var right = document.createElement("td");

    var topText = document.createElement("div");
    topText.innerText = results.firstStyle[key];
    topText.className = "css-top";
    right.className = "css-top";
    right.appendChild(topText);

    var bottomText = document.createElement("div");
    bottomText.innerText = results.secondStyle[key];
    bottomText.className = "css-bottom";
    right.appendChild(bottomText);

    row.appendChild(prop);
    row.appendChild(right);
    mainTable.appendChild(row);
  }
};

var update = function(){
  chrome.devtools.inspectedWindow.eval("("+compareResults.toString()+")()", function(res){
      initDisplay(res);
      new ColumnResize(document.getElementById("main"));
  });   
};

chrome.devtools.panels.elements.onSelectionChanged.addListener(update); 

var rotateButton = document.getElementById("rotate");
rotateButton.addEventListener("click",function() {
  if (localStorage.getItem("layout") === "portrait") {
    localStorage.setItem("layout", "landscape");
  }
  else {
    localStorage.setItem("layout", "portrait");
  }
  update();
},false);

