var UA = null;
var description = null;

handler = function (details) {
    
    if (UA == null) {
        return;
    }

    for (var i = 0, l = details.requestHeaders.length; i < l; ++i) {
        if (details.requestHeaders[i].name === 'User-Agent') {
            details.requestHeaders[i].value = UA;
            break;
        }
    }

    return { requestHeaders: details.requestHeaders };
};

chrome.webRequest.onBeforeSendHeaders.addListener(handler, {urls: ["<all_urls>"]},  ["blocking", "requestHeaders"]);

// called from the popup
function setUA(ua, desc) {    
    UA = ua;
    description = desc;
}

function clearUA() {
    UA = null;
    description = null;
}

function getUA() {
    return UA;
}

function getDescription() {
    return description;
}