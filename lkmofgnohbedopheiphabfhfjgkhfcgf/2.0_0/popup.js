// Add event listeners once the DOM has fully loaded by listening for the
// `DOMContentLoaded` event on the document, and adding your listeners to
// specific elements when it triggers.
document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('default').addEventListener('click', function () { clearUA(); return false; });
    document.getElementById('IE10').addEventListener('click', function () { setUA('Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 'Internet Explorer 10'); return false; });
    document.getElementById('IE9').addEventListener('click', function () { setUA('Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)', 'Internet Explorer 9'); return false; });
    document.getElementById('IE8').addEventListener('click', function () { setUA('Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727)', 'Internet Explorer 8'); return false; });
    document.getElementById('IE7').addEventListener('click', function () { setUA('Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)', 'Internet Explorer 7'); return false; });
    document.getElementById('IE6').addEventListener('click', function () { setUA('Mozilla/5.0 (Windows; U; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)', 'Internet Explorer 6'); return false; });
    document.getElementById('ChromeWin').addEventListener('click', function () { setUA('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17', 'Chrome on Windows'); return false; });
    document.getElementById('ChromeMac').addEventListener('click', function () { setUA('Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5', 'Chrome on Mac'); return false; });

    document.getElementById('FFWin').addEventListener('click', function () { setUA('Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20110814 Firefox/10.0.1', 'Firefox on Windows'); return false; });
    document.getElementById('FFMac').addEventListener('click', function () { setUA('Mozilla/6.0 (Macintosh; I; Intel Mac OS X 11_7_9; de-LI; rv:1.9b4) Gecko/2012010317 Firefox/10.0.1', 'Firefox on Mac'); return false; });
    document.getElementById('SafariMac').addEventListener('click', function () { setUA('Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; de-at) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1', 'Safari on Mac'); return false; });
    document.getElementById('SafariWin').addEventListener('click', function () { setUA('Mozilla/5.0 (Windows; U; Windows NT 6.1; tr-TR) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27', 'Safari on Windows'); return false; });
    document.getElementById('OperaMac').addEventListener('click', function () { setUA('Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52', 'Opera on Mac'); return false; });
    document.getElementById('OperaWin').addEventListener('click', function () { setUA('Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00', 'Opera on Windows'); return false; });
    document.getElementById('iPhone').addEventListener('click', function () { setUA('Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293 Safari/6531.22.7', 'iPhone'); return false; });
    document.getElementById('iPad').addEventListener('click', function () { setUA('Mozilla/5.0(iPad; U; CPU OS 4_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F191 Safari/6533.18.5', 'iPad'); return false; });
    document.getElementById('AndroidHandset').addEventListener('click', function () { setUA('Mozilla/5.0 (Linux; U; Android 2.2; en-us; DROID2 GLOBAL Build/S273) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1', 'Android Handset'); return false; });
    document.getElementById('AndroidTablet').addEventListener('click', function () { setUA('Mozilla/5.0 (Linux; U; Android 2.2; en-gb; GT-P1000 Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1', 'Android Tablet (Galaxy Tab)'); return false; });
    document.getElementById('Kindle').addEventListener('click', function () { setUA('Mozilla/5.0 (Linux; U; en-US) AppleWebKit/528.5+ (KHTML, like Gecko, Safari/528.5+) Version/4.0 Kindle/3.0 (screen 600�800; rotate)', 'Kindle'); return false; });
    document.getElementById('Googlebot').addEventListener('click', function () { setUA('Googlebot/2.1 (+http://www.googlebot.com/bot.html)', 'Googlebot (Google\'s spider)'); return false; });
    document.getElementById('Slurp').addEventListener('click', function () { setUA('Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)', 'Slurp! (Yahoo\'s spider)'); return false; });
    document.getElementById('BingBot').addEventListener('click', function () { setUA('Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 'BingBot (Bing\'s spider)'); return false; });

    document.getElementById('customButton').addEventListener('click', function () { setUA(document.getElementById('ua').value, 'Custom'); });

    if (chrome.extension.getBackgroundPage().getDescription() != null) {
        document.getElementById('title').innerHTML = '<h1>Current: <span style="font-weight:bold;color:green">' + chrome.extension.getBackgroundPage().getDescription() + '</span></h1>';
    }
});

function setUA(ua, desc) {
    chrome.extension.getBackgroundPage().setUA(ua, desc);
    chrome.browserAction.setIcon({ path: (ua == null ? 'icon.png' : 'icon-active.png') }, function () { window.close(); });    
}

if (chrome.extension.getBackgroundPage().getUA() != null) {
    document.getElementById('ua').value = chrome.extension.getBackgroundPage().getUA();
}

function clearUA() {
    chrome.extension.getBackgroundPage().clearUA();
    chrome.browserAction.setIcon({ path: 'icon.png' }, function () { setTimeout('window.close()', 100); });    
}

