
;(function(){
 
  var execRwd = function(i, tab){
    chrome.tabs.executeScript(tab.id, {file: "rwd.js"});
  };

  var context = chrome.contextMenus.create({
      "title": "Responsive design test"
    , "contexts": ["all", "page", "frame", "selection", "link", "editable", "image", "video", "audio"]
    , "onclick": execRwd
  });

})();
