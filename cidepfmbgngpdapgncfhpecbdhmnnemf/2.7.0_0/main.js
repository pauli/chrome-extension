var panels = chrome.devtools.panels;

function getPanelContents() {

	var returnObj;

	if ($0 && window.angular && window.angular.injector) {
		var hasJQuery = window.jQuery,
			$ 		  = window.angular.element,
			$el 	  = $($0),
			_scope 	  = $el.scope && $el.scope(),
			_iScope   = $el.isolateScope && $el.isolateScope(),
			events    = hasJQuery ? window.jQuery._data($0) : {},
			returnObj,
			scope,
			iScope;

		if (_scope) {
			scope = {};
			Object.getOwnPropertyNames(_scope).forEach(function (key) {
				scope[key] = _scope[key];
			});
			Object.setPrototypeOf(scope, Object.getPrototypeOf(_scope));
		}

		if (_iScope) {
			iScope = {};
			Object.getOwnPropertyNames(_iScope).forEach(function (key) {
				iScope[key] = _iScope[key];
			});
			Object.setPrototypeOf(iScope, Object.getPrototypeOf(_iScope));
		}

		window.$el 		= $el;
		window.$s 		= _scope;
		window.$is 		= _iScope;
		window.$events 	= events.events;
		window.$rs 		= _scope && _scope.$root;

		if (!window.$get) {
			var moduleEl,
				moduleName,
				injector,
				allServices;

			function getAllServices(inj, mName, services) {
				angular.forEach(angular.module(mName).requires, function (m) {
					getAllServices(inj, m, services)
				});
				angular.forEach(angular.module(mName)._invokeQueue, function (component) {
					try {
						services[component[2][0]] = inj(component[2][0]);
					} catch (e) {
						//
					}
				});
				return services;
			}
			
			function getService(mName, sName) {
				if (!allServices) {
					allServices = getAllServices(injector, mName, {});
				}
				var service = sName ? allServices[sName] : allServices;
				if (!service) {
					service = injector(sName);
				}
				return service;
			}

			moduleEl = document.querySelector('[data-ng-app]');
			if (!moduleEl) {
				moduleEl   = document.querySelector('[ng-app]');
				if (moduleEl) {
					moduleName = moduleEl.getAttribute('ng-app');
				}
			} else {
				moduleName = moduleEl.getAttribute('data-ng-app');
			}

			if (!moduleEl) {
				// manual bootstrap
				if ($el.scope && $el.scope()) {
					moduleEl = $el.injector().get('$rootElement');
				}
			}

			if (moduleEl) {
				injector = $(moduleEl).injector().get;

				if (moduleName) {
					window.$get = getService.bind(undefined, moduleName);
				} else {
					window.$get = function (sName, mName) {
						if (!mName) {
							console.log('Provide moduleName with which the app is bootstrapped as the second argument');
							return;
						}
						return getService(mName, sName);
					}
				}
			}
		}

		if (!window.$count) {
			window.$count = function () {
				var _s  = {}, //scopes
					_is = {}, //isolateScopes
					lc  = 0, //listenerCount
					wc  = 0; //watcherCount

				Array.prototype.slice.call($$('*')).forEach(
					function (el) {
						el = $(el);
						var s = el.isolateScope();
						if (s) {
							_is[s.$id] = s;
						}
						s = el.scope();
						if (s && !_is[s.$id]) {
							_s[s.$id] = s;
						}
					}
				);

				Object.keys(_s).forEach(function ($id) {
					var s = _s[$id];
					Object.keys(s.$$listenerCount).forEach(function (key) {
						lc += s.$$listenerCount[key];
					});
					wc += s.$$watchers ? s.$$watchers.length : 0;
				});

				Object.keys(_is).forEach(function ($id) {
					var s = _is[$id];
					Object.keys(s.$$listenerCount).forEach(function (key) {
						lc += s.$$listenerCount[key];
					});
					wc += s.$$watchers ? s.$$watchers.length : 0;
				});
				console.log('%cscopes: %c%d, %cisolateScopes: %c%d, %clisteners: %c%d, %cwatchers: %c%d',
					'color: teal', 'color: tomato', Object.keys(_s).length,
					'color: teal', 'color: tomato', Object.keys(_is).length,
					'color: teal', 'color: tomato', lc,
					'color: teal', 'color: tomato', wc
				);
			}
		}

		returnObj = {
			$scope: scope,
			$isolateScope: iScope,
			$events: events.events,
			$rootScope: scope && scope.$root
		};

	} else {
		returnObj = {
			"message": "Time to Angularify.. :)"
		};
	}
	returnObj.__proto__ = null;
	return returnObj;
};

panels.elements.createSidebarPane(
	"ng-inspect",
	function (sidebar) {
		function updatePanelProperties() {
			sidebar.setExpression("(" + getPanelContents.toString() + ")()");
		}
		updatePanelProperties();
		panels.elements.onSelectionChanged.addListener(updatePanelProperties);
	}
);