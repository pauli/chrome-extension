﻿var countNewEventFlag = null;
var redownloadFlag = null;
var newitems = 0;
var ifalert = 0;
var lastTanChu = new Date();
function countNewEvent(){
	newitems = 0;
	ifalert = 0;
	
	for(var i = 0;i < mytask.getLength();i++)
	{
		nowDate = new Date();
		var s = mytask.getTask(i).deathDate - new Date().getTime();
		if(s <= 0)
		{
			newitems++;
			if(ifalert == 0 && mytask.getTask(i).tixing_tanchu == 1 && mytask.getTask(i).isRead == 0 && (nowDate.getTime() - lastTanChu.getTime() >= localStorage["AlertUnreadMsgSecond"]*1000))
				ifalert = 1;
		}
	}
	//设置按钮数字
	if(newitems > 0)
	{
		newitems += "";
		chrome.browserAction.setBadgeBackgroundColor({"color":[11,173,46,190]});
		chrome.browserAction.setBadgeText({"text":newitems});
	}
	else
		chrome.browserAction.setBadgeText({"text":""});
	if(ifalert == 1)
	{
		alert("[时间助手]您有未读事件.");
		ifalert = 0;
		lastTanChu = new Date();
	}
}
function playalert(){
	document.getElementById("sounds").innerHTML = '<audio src="./img/sounds1.ogg" id="bkmp3" controls="true" autoplay="true"></audio>';
}
function init(){
	//
	try{
		var manifest = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "../manifest.json", true);
		xhr.send();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
					manifest = eval('v'+ xhr.responseText +')');
					if(!localStorage["version"])
						localStorage["version"] = manifest.version;
					if(manifest.version > localStorage["version"])
					{
						chrome.browserAction.setBadgeBackgroundColor({"color":[11,173,46,190]});
						chrome.browserAction.setBadgeText({"text":"新!"});
						localStorage["version"] = manifest.version;
					}
			}
		}
	}catch(e){
		localStorage["version"] = "1.0.0";
	}
	//下载存储器数据
	mytask.download("eventlist");
	//下载后立即检测新数据
	countNewEvent();
	countNewEventFlag = setInterval("countNewEvent()",30000);//30秒
}

//添加或删除事件时重新载入变量
chrome.extension.onRequest.addListener(
function(request, sender, sendResponse) {
	switch(request.hope)
	{	
		case "redownload":	mytask.download("eventlist");
							//下载后立即检测新数据
							countNewEvent();
							break;
		case "playalert":   playalert();
							break;
  }
});
