var stopwatch = {};
var runing = 0;
stopwatch.time = [0,0,0,0];//时分秒，百毫秒
stopwatch.clock = {};
stopwatch.start = function(){
	runing = 1;
	document.getElementById("button").getElementsByTagName("button").item(1).innerHTML = "暂停";
	stopwatch.clock.flag = setInterval(function(){		
		var time_str = '';
		stopwatch.time[3] += 10;	//毫秒
		if (stopwatch.time[3] == 1000)
		{
			stopwatch.time[2]++;
			stopwatch.time[3] = 0;
			if (stopwatch.time[2] == 60)
			{
				stopwatch.time[1]++;
				stopwatch.time[2] = 0;
				if (stopwatch.time[1] == 60)
				{
					stopwatch.time[0]++;
					stopwatch.time[1] = 0;
				}
			}
		}
		
		//显
		if (stopwatch.time[0] < 10)
			document.getElementById("hour").innerHTML = "0" + stopwatch.time[0];
		else
			document.getElementById("hour").innerHTML = stopwatch.time[0];

		if (stopwatch.time[1] < 10)
			document.getElementById("minute").innerHTML = "0" + stopwatch.time[1];
		else
			document.getElementById("minute").innerHTML = stopwatch.time[1];
			
			
		if (stopwatch.time[2] < 10)
			document.getElementById("second").innerHTML = "0" + stopwatch.time[2];
		else
			document.getElementById("second").innerHTML = stopwatch.time[2];
			
		if (stopwatch.time[3] < 100)
			document.getElementById("msecond").innerHTML = "0" + stopwatch.time[3]/10;
		else	
			document.getElementById("msecond").innerHTML = stopwatch.time[3]/10;
		
	},10);
	
};
stopwatch.pause = function(){
	try{
		clearInterval(stopwatch.clock.flag);
	}catch(e){}
	document.getElementById("button").getElementsByTagName("button").item(1).innerHTML = "开始计时";
	runing = 0;
	
};
stopwatch.stop = function(){
	try{
		clearInterval(stopwatch.clock.flag);
	}catch(e){}
	stopwatch.time = [0,0,0,0];
	document.getElementById("hour").innerHTML = "00";			
	document.getElementById("minute").innerHTML ="00";		
	document.getElementById("second").innerHTML = "00";	
	document.getElementById("msecond").innerHTML = "00";	
	document.getElementById("button").getElementsByTagName("button").item(1).innerHTML = "开始计时";
	runing = 0;

}
stopwatch.ouptut = function(){
	var redios = document.getElementById("model").getElementsByTagName("input");
	document.getElementById("list").getElementsByTagName("ol").item(0).innerHTML +=	'<li>'+stopwatch.time[0]+'时'+stopwatch.time[1]+'分'+stopwatch.time[2]+'秒'+stopwatch.time[3]+'毫秒'+'</li>';
	if (redios.item(0).checked)
	{
		stopwatch.stop();
		stopwatch.start();
	}
	else
		if (redios.item(2).checked)
		{
			stopwatch.pause();
		}
	document.getElementById("list").getElementsByTagName("ol").item(0).scrollTop = document.getElementById("list").getElementsByTagName("ol").item(0).scrollHeight;
};

function changemodel(){
	var redios = document.getElementById("model").getElementsByTagName("input");
	var imgs = document.getElementById("model").getElementsByTagName("img");
	if (redios.item(0).checked)
	{
		imgs.item(0).style.display = "inline";
		imgs.item(1).style.display = "none";
		imgs.item(2).style.display = "none";
	}
	else
		if (redios.item(1).checked)
		{
			imgs.item(0).style.display = "none";
			imgs.item(1).style.display = "inline";
			imgs.item(2).style.display = "none";
		}
		else
		{
			imgs.item(0).style.display = "none";
			imgs.item(1).style.display = "none";
			imgs.item(2).style.display = "inline";
		}
}
function init(){
	document.getElementById("button").getElementsByTagName("button").item(1).onclick = function(){
		if (runing)
			stopwatch.pause();
		else
			stopwatch.start();
	}
}