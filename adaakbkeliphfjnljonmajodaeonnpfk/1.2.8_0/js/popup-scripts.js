﻿//全局变量
var COLOR = new Array();
COLOR["yellor_1"]  ="#FFFFCC";
COLOR["yellor_2"]  ="#FFF1A8";

MSG = 1;
/*
通过DOM对象与源数据对象映射
*/

//事件列类
function tasklist_li(index){
	this.date = mytask.getTask(index);//引用
	this.id = "task_" + this.date.birthDate;
	this.selected = false;
	this.ended = false;
}


tasklist_li.prototype.show = function(jdom){
	var liColor = "#000";//任务列表的颜色
		var end = new Date();//结束时间，时间对象
		end.setTime(this.date.deathDate);
		var ms = this.date.deathDate - new Date().getTime();
		var str = "";
		var str_tan = "";
		liColor = "#000";
		if(ms >= 0)
		{
			
			//事件还没有结束
			this.ended = false;
			//距离事件结束还差多少秒
			var s = parseInt(ms/1000);
			//还有多少小时
			var h = parseInt(s / 3600); 
			//还有多少分
			var m = parseInt( (s - h*3600)/60 );
			str = '<li class="mytask" ><label for="'+ this.id +'"><input type="checkbox" class="taskcheckbox" id="'+ this.id +'" onchange=""><span class="tasktitle">' + this.date.title + '</span><span class="tasktime" title="事件结束时间:'+ end.getFullYear() + '年' + (end.getMonth()+1) + '月' + end.getDate() + '日'+ end.getHours() + '时' + end.getMinutes() + '分' +'">' + h + '时'+ m + '分' ;
			if(this.date.tixing_tanchu)
				str　+= "[弹]";
			str +='</span></label></li>';
		}
		else
		{
			this.ended = true;
			if(this.date.isRead == false)
			{
				//未读，则设置颜色，如果设置弹，则标记弹
				liColor = "#66CC33";
				if(this.date.tixing_tanchu)
					str_tan = "[弹]";
			}
			str = '<li class="mytask" style="color:'+ liColor +'"><label for="'+ this.id +'"><input type="checkbox" class="taskcheckbox" id="'+ this.id +'" onchange=""><span class="tasktitle">'+ this.date.title +'</span><span class="tasktime" title="事件结束时间:'+ end.getFullYear() + '年' + (end.getMonth()+1) + '月' + end.getDate() + '日'+ end.getHours() + '时' + end.getMinutes() + '分' +'">已结束' + str_tan + '</span></label></li>';
			//设置此事件为已读
		}
	jdom.append(str);
	//添加监听
	$("#"+this.id).change(function(){
		var obj = popup.tasklist.getTaskLiObj(this.id);
		if (obj != null)
			obj.selected = $("#"+this.id).attr("checked");
	});
	//console.log("AddLis",this.id);
}


//对象选择事件
tasklist_li.prototype.select = function(s){
	//s-true/false
	//设置此选择框选中状态
	$("#"+this.id).attr('checked',s);
	//设置对象属性selected
	this.selected = s;
}

//对象删除事件
var popup = {};
popup.tasklist = {};
popup.tasklist.date = [];

popup.event = {};

popup.tasklist.init = function(){
	var i = 0;
	popup.tasklist.date.length = 0;
	for (i = 0;i < mytask.getLength(); i++)
	{
		popup.tasklist.date[i] = new tasklist_li(i);
	}

};
popup.tasklist.del = function(){
	if($("#TaskList").is(":visible"))
	{
		for(var i = 0;i < popup.tasklist.date.length;i++)
		{
			if (popup.tasklist.date[i].selected)
			{
				//删除监听
				$("#"+popup.tasklist.date[i].id).unbind();
				//删除数据源
				mytask.taskarray.splice(i,1);
				//从对象数组中删除
				popup.tasklist.date.splice(i,1);
				//数组长度减小了
				i--;
			}
		}
		
		//
		mytask.upload ("eventlist");
		popup.tasklist.toDom();
		//备份数据
		popup.showMessage("成功删除事件");	
	}	
	else
	{
		popup.showMessage("请切换到列表视图");
	}
};
popup.tasklist.add = function(elem,type){
	//这里用于添加事件，内部将处理数据
	
	//添加一个事件。
	type = parseInt(type);
	var end = null;//结束时间
	var newtask = new eventObj();
	if(type >= 0 && type <= 1)
	{
		elem.disabled = true;
		switch(type)
		{
			case 0:
					var start = new Date();//以当前时间，加上秒数
					var d = $("#AddTask_0_DayList").val();
					var h = $("#AddTask_0_HourList").val();
					var m = $("#AddTask_0_MinuteList").val();
					var s = parseInt(d)*24*3600 + parseInt(h)*3600 + parseInt(m)*60;//秒
					end = new Date(start.getTime()+s*1000);
					break;
			case 1:
					var d = null;
					if(d = $("#AddTask_1_Datepicker").datepicker("getDate")) 
					{
						var h = $("#AddTask_1_HourList").val();
						var m = $("#AddTask_1_MinuteList").val();
						var s = parseInt(h)*3600 + parseInt(m)*60;//秒
						end = new Date(d.getTime()+s*1000);
					}
					else 	
					{
						//没有定义日期
						elem.disabled = false;
						console.log("错误：没有定义日期");
						return false;
					}
					break;
		}
		//添加一个事件到数组
		newtask.title = $("#AddTask_Title").val();
		newtask.birthDate = new Date().getTime();//存储的是毫秒数，根据世界时间
		newtask.deathDate = end.getTime();
		if($("#Alert_Unread_Msg").attr("checked") == true)
			newtask.tixing_tanchu = 1;
		else
			newtask.tixing_tanchu = 0;
		mytask.add(newtask);
		//
		popup.tasklist.init();
		console.log("添加事件：",newtask.title,newtask.birthDate,newtask.deathDate);
		elem.disabled = false;
		return true;
	}
	elem.disabled = false;
	return false;
	
	
}
popup.tasklist.toDom = function(){
	//输出到Dom
	//把变量数据填充到界面
	var jdom = $("#TaskList ul");
	jdom.html("");
	popup.tasklist.init();
	for(var i = 0;i < popup.tasklist.date.length;i++)
	{
		popup.tasklist.date[i].show(jdom);
	}
	$(".content").not($("#TaskList")).hide();
	$("#TaskList").fadeIn("fast");
	//完成后上传数据，保存已读记录
	mytask.upload ("eventlist");

}

popup.tasklist.maskasread = function(){
	//标记选中项为已读
	/*
	查询所有标记项
	逐个检测是否为已读
	如果不是已读则标记为已读
	*/
	if($("#TaskList").is(":visible"))
	{
		for(var i = 0;i < popup.tasklist.date.length;i++)
		{
			if (popup.tasklist.date[i].selected)
			{
				mytask.getTask(i).isRead = 1;
			}		
		}
		mytask.upload("eventlist");
		popup.tasklist.toDom();
		//备份数据
		
		popup.showMessage("操作完成");
	}
	else
	{
		popup.showMessage("请切换到列表视图");
	}
}

popup.tasklist.setAlert = function(m){
	var v = 0;
	if (m == "add")
		v = 1;
	if($("#TaskList").is(":visible"))
	{
		for(var i = 0;i < popup.tasklist.date.length;i++)
		{
			if (popup.tasklist.date[i].selected)
			{
					mytask.getTask(i).tixing_tanchu = v;
			}		
		}
		mytask.upload("eventlist");
		popup.tasklist.toDom();
		//备份数据
		
		popup.showMessage("操作完成");
	}
	else
	{
		popup.showMessage("请切换到列表视图");
	}
}

popup.tasklist.getTaskLiObj = function(id){
	//返回指定ID的任务对象
	var i = 0;
	for (i = 0;i < popup.tasklist.date.length;i++)
	{
		if (popup.tasklist.date[i].id == id)
			return popup.tasklist.date[i];
	}	
	return null;
};
/*
所有由菜单、提交按钮所产生的事件
*/
popup.event.submit_task_by_dao = function(){
	//点击倒计时添加的添加按钮
   if(popup.tasklist.add(this,'0'))
   {
	   mytask.upload("eventlist");
	   //备份数据
		//提示正确信息
		popup.showMessage("成功添加事件");
		if(localStorage["AddTask_0_autoList"] == "true")
			popup.tasklist.toDom();
   }
   else
   {
	   popup.showMessage("出错！请检查输入。");;//提示错误信息
   }

}

popup.event.submit_task_by_mubiao = function(){
	//点击目标时间添加的添加按钮
	if(popup.tasklist.add(this,'1'))
	{
	   mytask.upload("eventlist");
	   //备份数据
		//提示正确信息
		popup.showMessage("成功添加事件");
		if(localStorage["AddTask_1_autoList"] == "true")
			popup.tasklist.toDom();
	}
	else
	{
	   popup.showMessage("出错！请检查输入。");;//提示错误信息
	}

}

popup.event.click_addtask_task_by_dao = function(){
	popup.showDiv('AddTask');
	$('#AddTask_type_radio_0').click();
}

popup.event.click_addtask_by_mubiao = function(){
	popup.showDiv('AddTask');
	$('#AddTask_type_radio_1').click();
}

popup.event.click_select_all = function(){
	if($("#TaskList").is(":visible"))
	{
		for(var i = 0;i < popup.tasklist.date.length;i++)
		{
			popup.tasklist.date[i].select(true);
		}
	}
	else
	{
		popup.showMessage("请切换到列表视图");
	}
}

popup.event.click_select_none = function(){
	if($("#TaskList").is(":visible"))
	{
		for(var i = 0;i < popup.tasklist.date.length;i++)
		{
			popup.tasklist.date[i].select(false);
		}
	}
	else
	{
		popup.showMessage("请切换到列表视图");
	}
}

popup.event.click_select_toggle = function(){
	if($("#TaskList").is(":visible"))
	{
		for(var i = 0;i < popup.tasklist.date.length;i++)
		{
			popup.tasklist.date[i].select(!popup.tasklist.date[i].selected);
		}
	}
	else
	{
		popup.showMessage("请切换到列表视图");
	}
}

popup.event.click_select_finished = function(){
	if($("#TaskList").is(":visible"))
	{
		for(var i = 0;i < popup.tasklist.date.length;i++)
		{
			popup.tasklist.date[i].select(popup.tasklist.date[i].ended);
		}
	}
	else
	{
		popup.showMessage("请切换到列表视图");
	}
}
popup.bookmark = {/*占座*/};
popup.bookmark.open = function(url){
			chrome.tabs.getSelected(null,function(thisTab){
								chrome.tabs.create({
										index:thisTab.index+1,
										url: url
								});	
							});
	
};
popup.model = function(id){
	switch(id)
	{
		case "option":
			chrome.tabs.getSelected(null,function(thisTab){
								chrome.tabs.create({
										index:thisTab.index+1,
										url: "../options.html"
								});	
							});
			break;
			break;
		case "task":
			popup.tasklist.toDom();
			popup.showDiv('TaskList');
			popup.showMessage("已载入列表");
			//回复选择菜单的可用性
			break;			
		case "stopwatch":
			chrome.tabs.getSelected(null,function(thisTab){
								chrome.tabs.create({
										index:thisTab.index+1,
										url: "../stopwatch.html"
								});	
							});
			break;
		case "buzzpage":
			chrome.tabs.getSelected(null,function(thisTab){
								if (thisTab.url.substring(0,4) == "http")
									chrome.tabs.create({
											index:thisTab.index+1,
											url: "http://www.google.com/buzz/post?url="+thisTab.url
									});	
								else
									popup.showMessage("暂不支持分享此页面");
							});
			break;
	}
};
popup.showMessage = function(str){
		var g = document.getElementById("Message");
		var status = document.getElementById("status");
		try{
		  clearTimeout(clockFlag);}catch(e){}
		g.style.display = "block";
		status.innerHTML = str;
		clockFlag = setTimeout(function() {
		status.innerHTML = "";
		g.style.display = "none";
		}, 1000);	
	}

popup.showDiv = function(id){
	$(".content").not($("#"+id)).hide();
	$("#"+id).fadeIn("fast");
	//改变功能是否可执行
}


/*Task==*/
function creatOption(elem,starti,endi){
	for(var i = starti;i <= endi;i++)
	{
		  var oOption = document.createElement("option");
		  oOption.value=i;
		  oOption.text=i;
		  if(i == starti)
		  	oOption.selected = "selected";
		  elem.appendChild(oOption);
	}
}
template.titles.add = function(){
	var str = $("#AddTask_Title").val();
	if(str != "")
	{
		try{
			template.titles.date[template.titles.date.length] = str;
			template.titles.load();
			mytask.upload("defaultEventTitle");
			popup.showMessage("已保存常用标题");
		}catch(e){
			console.log("保存常用语错误。",e.message);
		}
	}
	else
	{
		popup.showMessage("事件标题不能为空");
	}
}
template.titles.load = function(){
	var elem = document.getElementById("AddTask_Title_Default");
	elem.innerHTML = '<option value="">常用语（可在选项页设置）</option>';
	for(var i = 0;i < template.titles.date.length;i++)
	{
		  var oOption = document.createElement("option");
		  oOption.value=template.titles.date[i];
		  oOption.text=template.titles.date[i];
		  elem.appendChild(oOption);
	}
	
}

$(function(){
	
	
	//没有就绪时禁用一些按钮 disabled="disabled"
	$("#btn_addTask_0").attr("disabled","disabled");
	
	//添加事件动作的监视
	$("#AddTask_type_radio_0").click(function(){
		$("#AddTask_Tab_1").hide();									  
		$("#AddTask_Tab_0").show();
	})
	$("#AddTask_type_radio_1").click(function(){
		$("#AddTask_Tab_0").hide();									  
		$("#AddTask_Tab_1").show();
	})
	
	//载入数据
	mytask.download("eventlist");
	mytask.download("defaultEventTitle");
	
	//初始化数据对象
	popup.tasklist.init();
	
	
	/******处理列表DOM******/
	//日期控件
	//$("#AddTask_1_Datepicker").datepicker({ minDate: 0,dateFormat: 'yy-mm-dd' });
	$("#AddTask_1_Datepicker").datepicker({ minDate: 0,dateFormat: "yy' 年 'MM' 'd' 日 '" });
	//常用标题
	template.titles.load();
	popup.tasklist.toDom();
	//生成按倒计时添加的列表
	creatOption(document.getElementById("AddTask_0_DayList"),0,14);
	creatOption(document.getElementById("AddTask_0_HourList"),0,24);
	creatOption(document.getElementById("AddTask_0_MinuteList"),0,59);
	$("#btn_addTask_0").removeAttr("disabled");
	
	//生成按目标时间添加的列表
	creatOption(document.getElementById("AddTask_1_HourList"),0,23);
	creatOption(document.getElementById("AddTask_1_MinuteList"),0,59);
	$("#btn_addTask_1").removeAttr("disabled");
	/*恢复用户设置*/
	if(localStorage["AddTask_0_autoList"] == "true")
		$("#AddTask_0_autoList").attr("checked","checked");
		
	if(localStorage["AddTask_1_autoList"] == "true")
		$("#AddTask_1_autoList").attr("checked","checked");
	if(localStorage["AlertUnreadMsg"] == "true")
		$("#Alert_Unread_Msg").attr("checked","true");
	/******添加事件******/
	//事件标题
	$("#AddTask_Title").one("focus",function(){$(this).val("");});
	
	
	//常用标题事件
	$("#AddTask_Title_Default").change(function(){
		$("#AddTask_Title").val($("#AddTask_Title_Default").val());
		$("#AddTask_Title").unbind("focus");
	})
	//自动返回列表
	$("#AddTask_0_autoList").change(function(){
		if($("#AddTask_0_autoList").attr("checked") == true) 
			localStorage["AddTask_0_autoList"] = "true";
		else
			localStorage["AddTask_0_autoList"] = "false";
		popup.showMessage("设置已保存");	
	});
	$("#AddTask_1_autoList").change(function(){
		if($("#AddTask_1_autoList").attr("checked") == true) 
			localStorage["AddTask_1_autoList"] = "true";
		else
			localStorage["AddTask_1_autoList"] = "false";
		popup.showMessage("设置已保存");
	});

	//匹配样式
	$(".menu_item ul").each(function(i){
		$(this).css("margin-top", -1*$(this).height()-26+"px");
	});
});