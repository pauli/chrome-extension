﻿//全局变量
var MSG = 0;//是否在用户界面显示信息
//事件对象
function eventObj(){
			this.isRead = 0;
			this.title = "";
		  	this.birthDate = "";
		  	this.deathDate = "";
			this.tixing_tanchu = 1;
    }
	
	
var mytask = {};
mytask.taskarray = [];
var template = {};
template.titles = {};
template.titles.date = [];
mytask.add = function(task){
	if (typeof task == "object")
		mytask.taskarray[mytask.taskarray.length] = task;
	mytask.upload ("eventlist");
}
mytask.getTask = function(index){
	return mytask.taskarray[index];
}
mytask.getLength = function(){
	return mytask.taskarray.length;
}
mytask.resort = function(method){
	switch(method)
	{
		//按结束时间
		case "finish" :
			mytask.taskarray.sort(function(x,y){return x.deathDate - y.deathDate});//先结束的任务放在前面
		//按添加时间
		case "start" :
			mytask.tasharray.sort(function(x,y){return x.birthDate - y.birthDate});//先添加到放在前面
	};
}
//转换json的存储对象
var eventArray = [];
var defaultEventTitleArray = [];

//
mytask.upload = function(type){
	//把数据保存到localStorage
	var event_json = '';
	var eventTitle_json = '';
	var eArray;
	var len;
	var i = 0;
	switch(type)
	{
		case "base":
						if(!localStorage["EventList"])
							localStorage["EventList"]= '{"EventList":[]}';
						try{
							eArray = eval('('+ localStorage["EventList"] +')');
							len = eArray.EventList.length;
							}catch(e){
								localStorage["EventList"]= '{"EventList":[]}';
								console.log("数据格式错误，进行初始化");		
							}	
							
						
						if(!localStorage["DefaultEventTitle"])
							localStorage["DefaultEventTitle"]= '{"DefaultEventTitle":[]}';
							
						break;
		case "eventlist":
						event_json = '{"EventList":[';
						for(i = 0;i < mytask.getLength();i++)
							event_json +='{"isRead":'+ mytask.getTask(i).isRead +',"title":"'+ mytask.getTask(i).title +'","birthDate":'+ mytask.getTask(i).birthDate +',"deathDate":'+ mytask.getTask(i).deathDate +',"tixing_tanchu":'+ mytask.getTask(i).tixing_tanchu +'},';
						if(mytask.getLength() != 0)
							event_json = event_json.substring(0,event_json.lastIndexOf(',')); //去掉最后一个数据添加到逗号
						event_json += ']}';
						//得到了json字符串event_json
						//console.log(eval('('+ event_json +')'));
						
						//把json字符串保存到localStorage
						localStorage["EventList"] = event_json;
						//通知后台页面重新下载数据
						chrome.extension.sendRequest({"hope": "redownload"});
						break;
		case "defaultEventTitle":
						eventTitle_json = '{"DefaultEventTitle":[';
						for(i = 0;i < template.titles.date.length;i++)
							eventTitle_json +='"'+ template.titles.date[i] +'",';
						if(template.titles.date.length != 0)
							eventTitle_json = eventTitle_json.substring(0,eventTitle_json.lastIndexOf(',')); //去掉最后一个数据添加到逗号
						eventTitle_json += ']}';
						//得到了json字符串eventTitle_json
						//console.log(eval('('+ eventTitle_json +')'));
						
						//把json字符串保存到localStorage
						localStorage["DefaultEventTitle"] = eventTitle_json;
						break;
		case "usersetting":
						localStorage["AlertUnreadMsg"] =$("#alert_unread_msg").attr("checked");
						localStorage["AlertUnreadMsgSecond"] = $("#alert_unread_msg_minute").val();
						break;
	}
}
mytask.download = function(type){
	//把localStorage载入到内存
	switch(type)
	{
		case "eventlist":
						try{
							mytask.taskarray = eval('('+ localStorage["EventList"] +')').EventList;
							//数据校验
							//下载数据成功信息
							//console.log("下载localStorage成功：eventlist");
							if(MSG == 1)
								popup.showMessage("已下载数据");
						}catch(e){
							mytask.taskarray = [];
							//下载输出失败信息
							console.log("下载localStorage错误：eventlist",e.message);
							if(MSG == 1)
								popup.showMessage("载入列表错误,请联系开发者");
						}
						break;
		case "defaultEventTitle":
						try{
							//template.titles.date = eval('('+ localStorage["DefaultEventTitle"] +')');
							template.titles.date = eval('('+ localStorage["DefaultEventTitle"] +')').DefaultEventTitle;
							//下载数据成功信息
							//console.log("下载localStorage成功：defaultEventTitle");
							if(MSG == 1)
								popup.showMessage("已下载数据");
						}catch(e){
							template.titles.date = [];
							//下载输出失败信息
							console.log("下载localStorage错误：defaultEventTitle",e.message);
							if(MSG == 1)
								popup.showMessage("载入列表错误,请联系开发者");
						}
						break;
		case "usersetting":							
						if(localStorage["AlertUnreadMsg"] == "true")
							$("#alert_unread_msg").attr("checked","true");
						var len = $("#alert_unread_msg_minute")[0].length;
						for(var i = 0;i < len;i++)
							if($("#alert_unread_msg_minute")[0].item(i).value == localStorage["AlertUnreadMsgSecond"])
							{
								$("#alert_unread_msg_minute")[0].item(i).selected = true;
								break;
							}
						break;						
	}
}

function repairLocalStorage(){
	mytask.download("eventlist");
	var eArray = [];
	console.log("修复数据开始");
	for(var i = 0;i < mytask.getLength(); i++)
	{
		eArray[i] = new eventObj();
		//isRead元素
		try{
			if(mytask.getTask(i).isRead != 0 && mytask.getTask(i).isRead != 1)
				mytask.getTask(i).isRead = 0;
			eArray[i].isRead = mytask.getTask(i).isRead;
		}catch(e){
			eArray[i].isRead = 0;
		}
		
		//title
		try{
			eArray[i].title = mytask.getTask(i).title;
		}catch(e){
			eArray[i].title = "";
		}
		
		//birthDate,之前保存到是字符串，现在保存数字
		try{
			var sec = parseInt(mytask.getTask(i).birthDate);
			var newB = new Date(sec);
			if(newB.getTime() >= 0)
				eArray[i].birthDate = sec;
			else
				eArray[i].birthDate = 0;
		}catch(e){
			eArray[i].birthDate = 0;
		}
		
		//deathDate,之前保存到是字符串，现在保存数字
		try{
			var sec = parseInt(mytask.getTask(i).deathDate);
			var newB = new Date(sec);
			if(newB.getTime() >= 0)
				eArray[i].deathDate = sec;
			else
				eArray[i].deathDate = 0;
		}catch(e){
			eArray[i].deathDate = 0;
		}
		//弹窗提醒元素
		try{
			if(mytask.getTask(i).tixing_tanchu != 0 && mytask.getTask(i).tixing_tanchu != 1)
				mytask.getTask(i).tixing_tanchu = 1;
			eArray[i].tixing_tanchu = mytask.getTask(i).tixing_tanchu;
		}catch(e){
			eArray[i].tixing_tanchu = 1;
		}
	}
	console.log("修复数据结束");
	//修复完成后上传
	mytask.taskarray = eArray;
	mytask.upload("eventlist");
	//修复其他数据
	if(!localStorage["AlertUnreadMsg"])
		localStorage["AlertUnreadMsg"] = "true";
	if(!localStorage["AlertUnreadMsgSecond"])
		localStorage["AlertUnreadMsgSecond"] = "300";
		
	if(!localStorage["AddTask_0_autoList"])
		localStorage["AddTask_0_autoList"]= 'false';
	if(!localStorage["AddTask_1_autoList"])
		localStorage["AddTask_1_autoList"]= 'false';

}
//确保数据是正确的数组
mytask.upload("base");
//确保数组格式正确
repairLocalStorage();
//download("eventlist");
