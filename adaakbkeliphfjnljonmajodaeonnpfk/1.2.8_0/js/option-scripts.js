﻿MSG = 0;
var clockFlag=0;
function showStatus(a){
	$("#status").html(a);
	$("#status").css("display","block");
	$("#status").fadeOut(1500);
}

function uploadSet(){
	//保存预定义事件列表
	template.titles.date = $("#defaultEventTitle").val().split("\n");
	for(var i = 0; i < template.titles.date.length; i++)
	{
		//删除空行
		//console.log(i,template.titles.date[i]);
		if(template.titles.date[i] == "")
		{
			template.titles.date.splice(i,1);
			i--;
		}
	}
	//保存到localStorage
	mytask.upload ("defaultEventTitle");
	mytask.upload ("usersetting");
	showStatus("全部设置已保存");
}
function downloadSet(){
	
	//载入个性格设置
	mytask.download("usersetting");
	//载入预定义事件列表
	mytask.download("defaultEventTitle");
	if(template.titles.date.length > 0)
	{
		for(var i = 0;i < template.titles.date.length;i++)
			$("#defaultEventTitle").val($("#defaultEventTitle").val()+template.titles.date[i]+"\n");
	}
	//载入备份数据字符串
	$("#backup_taskList").val(localStorage["EventList"]);
	$("#backup_defaultEventTitle").val(localStorage["DefaultEventTitle"]);
	$("#backup_taskList,#backup_defaultEventTitle").click(function(){this.select();});
	
	//导入数据事件
	$("#btn_restoreTaskList").click(function(){localStorage["EventList"] = $("#restore_taskList").val()});
	$("#btn_restoreDefaultEventTitle").click(function(){localStorage["DefaultEventTitle"] = $("#restore_defaultEventTitle").val()});
}

$(function(){
	
	$(".setTitle").click(function(){
		var elem =$(this).children("span")[0];
		$(this).next("div .setarea").slideToggle('fast',function(){
			if(elem.innerHTML == "+")
				elem.innerHTML = "-";
			else
				elem.innerHTML = "+";
		});
	});
	$("#main-tabs").tabs();
	downloadSet();
	//
});