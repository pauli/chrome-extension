/*
	CTRE v1.0.5
	by @blade_sk
*/

var ctre = {
	hoveredElement: false,
	markedElement: false,
	hideHistory: [],
	targetingMode: false,
	transpose: 0, // how far to travel up the line of ancestors
	maxZIndex: 2147483647,
	
	clickCatcherTimeout: false,
	helpWindow: false,
	
	triggerResize: function()
	{
		var evt = document.createEvent('UIEvents');
		evt.initUIEvent('resize', true, false,window,0);
		window.dispatchEvent(evt);

		setTimeout(function() { // also update overlays
			ctre.removeOverlays();
			ctre.addOverlays();
		}, 10);
	},
	
	highlightElement: function()
	{
		if (!ctre.hoveredElement) return;
		
		if (ctre.markedElement)
		{
			ctre.markedElement.style.outline = "";
			ctre.markedElement.style.outlineOffset = "";
		}
		
		ctre.markedElement = ctre.hoveredElement;
		
		var i = 0;
		for (i = 0; i < ctre.transpose; i++)
		{
			if (ctre.markedElement.parentNode != window.document)
				ctre.markedElement = ctre.markedElement.parentNode;
			else
				break;
		}
		
		ctre.transpose = i;
		document.getElementById("transpose").innerHTML = ctre.transpose;
		document.getElementById("transpose_plural").style.display = (i == 1 ? 'none' : 'inline');
			
		ctre.markedElement.style.outline = "solid 6px rgba(255,0,0,0.5)";
		ctre.markedElement.style.outlineOffset = "-4px";
	},
	
	mouseover: function(e)
	{
		if (e.target == ctre.helpWindow || e.target.parentNode == ctre.helpWindow) return false;
		
		if (ctre.hoveredElement != e.target)
		{
			ctre.transpose = 0;
			ctre.hoveredElement = e.target;
			ctre.highlightElement();
		}
	},
	
	keyDown: function(e)
	{
		if ((e.ctrlKey || e.metaKey) && e.shiftKey && e.keyCode == 88)
		{
			if (ctre.targetingMode)
				ctre.deactivate();
			else
				ctre.activate();
				
			e.stopPropagation(); e.preventDefault();
			return false;
		}

		if (!ctre.targetingMode) return;
		
		if (e.keyCode == 27)
		{
			ctre.deactivate();
		}
		
		if (e.keyCode == 8 || e.keyCode == 90) // backspace or Z
		{
			var elm = ctre.hideHistory.pop();
			
			if (elm)
			{
				elm.style.display = elm.origDisplay;
				if (elm.relatedElement)
				{
					elm.relatedElement.style.display = elm.relatedElement.origDisplay;
				}

				ctre.triggerResize();
			}
		}
		
		if (e.keyCode == 81) // q
		{
			if (ctre.transpose > 0) ctre.transpose--;
			ctre.highlightElement();
		}
		else if (e.keyCode == 87) // w
		{
			ctre.transpose++;
			ctre.highlightElement();
		}

		e.stopPropagation(); e.preventDefault();
		return false;
	},
	
	keyUp: function(e)
	{
		if (!ctre.targetingMode) return;

		e.stopPropagation(); e.preventDefault();
		return false;
	},
	
	hideTarget: function(e)
	{
		if (e.target == ctre.helpWindow || e.target.parentNode == ctre.helpWindow) return;

		if (ctre.markedElement.tagName.toLowerCase() == 'body')
		{
			ctre.markedElement.style.background = 'transparent';
		}
		else if (ctre.markedElement.tagName.toLowerCase() == 'html')
		{
			ctre.markedElement.style.background = 'transparent';
		}
		else
		{
			ctre.markedElement.origDisplay = ctre.markedElement.style.display;
			ctre.markedElement.style.display = "none";
			ctre.hideHistory.push(ctre.markedElement);
			
			if (ctre.markedElement.className == "ctre_overlay") {
				ctre.markedElement.relatedElement.origDisplay = ctre.markedElement.relatedElement.style.display;
				ctre.markedElement.relatedElement.style.display = "none";
			}
			
			ctre.triggerResize();
		}

		e.preventDefault();
		e.stopPropagation();
		return false;
	},

	preventEvent: function(e)
	{
		if (e.target == ctre.helpWindow || e.target.parentNode == ctre.helpWindow) return;

		e.preventDefault();
		e.stopPropagation();
		return false;
	},
	
	
	onFirstActivation: function()
	{
		var div = document.createElement('div');
		div.setAttribute("id", "ctre_help_window");
		document.body.appendChild(div);
		div.style.position = "fixed";
		div.style.left = "0px";
		div.style.top = "0px";
		div.style.width = "260px";
		div.style.padding = "12px";
		div.style.backgroundColor = "rgba(240, 238, 237, 0.9)";
		div.style.fontFamily = "Arial, sans-serif";
		div.style.fontSize = "10px";
		div.style.textAlign = "left";
		div.style.lineHeight = "1.2";
		div.style.borderRadius = "0 0 8px 0";
		div.style.borderBottom = "solid 1px rgba(200, 198, 197, 0.9)";
		div.style.borderRight = "solid 1px rgba(200, 198, 197, 0.9)";
		div.style.color = "#666666";
		div.style.zIndex = ctre.maxZIndex;
		div.style.display = "none";
		div.innerHTML = '<b style="font-size: 12px;">Click to remove element</b> v1.0.5 &nbsp;&nbsp;&nbsp; <span style="color: #888; font-family:  monospace;">Ctrl+Shift+X</span>'
			+'<div style="margin-top: 6px;"></div><span style="font-family: monospace;">Ctrl+Z</span> undo, <span style="font-family: monospace;">ESC</span> exit<br>'
			+'<span style="font-family: monospace;">Q/W</span> go down/up one level. Currently up <span id="transpose" style="font-weight: bold;">0</span> level<span id="transpose_plural">s</span>.'
			+'<div style="margin-top: 6px;"></div>Like this extension? <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=NUVLGGXDAXDTJ&lc=SK&item_name=CTRE%20Chrome%20extension&item_number=CTRE&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted" target="_blank" style="color: #888; text-decoration: underline; font-weight: normal; border: 0;">Please consider donating</a>.';
		
		ctre.helpWindow = div;
	},
	
	activate: function()
	{
		if (!ctre.helpWindow) ctre.onFirstActivation();
		
		ctre.targetingMode = true;
		document.addEventListener('mouseover', ctre.mouseover, true);
		document.addEventListener('mousemove', ctre.mousemove);
		document.addEventListener('mousedown', ctre.hideTarget, true);
		document.addEventListener('mouseup', ctre.preventEvent, true);
		document.addEventListener('click', ctre.preventEvent, true);
		
		ctre.helpWindow.style.display = "block";
		
		ctre.addOverlays();
		
		chrome.extension.sendMessage({action: 'status', active: true});
	},
	
	deactivate: function()
	{
		ctre.targetingMode = false;
		
		if (ctre.markedElement)
		{
			ctre.markedElement.style.outline = "";
			ctre.markedElement.style.outlineStyle = "";
		}
		ctre.markedElement = false;
		
		ctre.helpWindow.style.display = "none";
		
		document.removeEventListener('mouseover', ctre.mouseover, true);
		document.removeEventListener('mousemove', ctre.mousemove);
		document.removeEventListener('mousedown', ctre.hideTarget, true);
		document.removeEventListener('mouseup', ctre.preventEvent, true);
		document.removeEventListener('click', ctre.preventEvent, true);
		
		ctre.removeOverlays();
		
		chrome.extension.sendMessage({action: 'status', active: false});
	},
	
	toggle: function()
	{
		if (ctre.targetingMode) ctre.deactivate();
		else ctre.activate();
	},
	
	addOverlays: function()
	{
		// first we need to add position: relative to all the TDs
		// table based layouts return buggy offsetLeft/Top otherwise
		var elms = document.querySelectorAll("td");

		for (i = 0; i < elms.length; i++)
		{
			var e = elms[i];

			e.origPos = e.style.position;
			e.style.position = e.origPos || "relative";
		};
		
		// add overlay over each iframe / embed
		// this is needed for capturing mouseMove over the whole document
		var elms = document.querySelectorAll("iframe, embed");

		for (i = 0; i < elms.length; i++)
		{
			var e = elms[i];

			var new_node = document.createElement("div");
			new_node.className="ctre_overlay";
			//new_node.innerHTML = html;
			new_node.style.position = "absolute";
			new_node.style.left = e.offsetLeft - e.scrollLeft + "px";
			new_node.style.top = e.offsetTop - e.scrollTop + "px";
			new_node.style.width = e.offsetWidth + "px";
			new_node.style.height = e.offsetHeight + "px";
			new_node.style.background = "rgba(255,128,128,0.2)";
			new_node.style.zIndex = ctre.maxZIndex - 2;
			new_node.relatedElement = e;
			
			e.parentNode.insertBefore(new_node, e.nextSibling);
		};
	},
	
	removeOverlays: function()
	{
		var elms = document.querySelectorAll("td");
		for (i = 0; i < elms.length; i++)
		{
			var e = elms[i];
			e.style.position = e.origPos;
		};
		
		var elms = document.querySelectorAll(".ctre_overlay");
		for (i = 0; i < elms.length; i++)
		{
			var e = elms[i];
			e.parentNode.removeChild(e);
		};
	},
	
	init: function()
	{
		document.addEventListener('keydown', ctre.keyDown);
		document.addEventListener('keyup', ctre.keyUp);
		
		chrome.extension.onMessage.addListener(function(msg, sender, responseFun) {
			if (msg.action == "toggle") {
				ctre.toggle();
				responseFun(1.05);
			}
			else if (msg.action == "getStatus") {
				responseFun(ctre.targetingMode);
			}
		});
	}
}

ctre.init();