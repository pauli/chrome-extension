var manuals = {
	'quickref'		: 'Function list',
	'all'			: 'All php.net sites',
	'local'			: 'This mirror only',
	'manual'		: 'Online documentation',
	'bugdb'			: 'Bug database',
	'news_archive'	: 'Site News Archive',
	'changelogs'	: 'All Changelogs',
	'pear'			: 'Just pear.php.net',
	'pecl'			: 'Just pecl.php.net',
	'talks'			: 'Just talks.php.net',
	'maillist'		: 'General mailing list',
	'devlist'		: 'Developer mailing list',
	'phpdoc'		: 'Documentation mailing list'
};

function getManual() {
	var manual = localStorage["manual"];
	if (!manual) {
		return 'quickref';
	}
	return manual
}

function getAutocompletion() {
	var autocompletion = localStorage["autocompletion"];
	if (!autocompletion) {
		return 1;
	}
	return autocompletion
}

function openLink(term, manual){
	window.open("http://php.net/search.php?pattern=" + term + "&show=" + manual);
}

function recommended(){
	var recommendation = {
		id: 1,
		icon: 'http://goo.gl/06k5z',
		title: 'Recommended extension',
		text: '<a href="https://chrome.google.com/webstore/detail/nfhmhhlpfleoednkpnnnkolmclajemef" target="_blank">PHP Console</a> - display PHP errors and debug in browser console or by notification popups'
	};
	if (localStorage.getItem('lastRecommendation') != recommendation.id) {
		webkitNotifications.createHTMLNotification('recommend.html?' + encodeURIComponent(JSON.stringify(recommendation))).show();
		
		localStorage.setItem('lastRecommendation', recommendation.id);
	}
}

// Show installation PopUp
recommended();