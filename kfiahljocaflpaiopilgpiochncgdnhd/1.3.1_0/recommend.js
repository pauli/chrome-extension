function recommended(){
	var recommendation = {
		id: 1,
		icon: 'http://goo.gl/06k5z',
		text: '<a href="https://chrome.google.com/extensions/detail/nfhmhhlpfleoednkpnnnkolmclajemef" target="_blank">PHP Console</a> - display PHP errors and debug in browser console or by notification popups'
	};
	if (localStorage.getItem('lastRecommendation') != recommendation.id) {
		webkitNotifications.createHTMLNotification('recommend.html?' + encodeURIComponent(JSON.stringify(recommendation))).show();
		
		localStorage.setItem('lastRecommendation', recommendation.id);
	}
}

// Show installation PopUp
recommended();